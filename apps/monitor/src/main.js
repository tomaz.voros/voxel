window.addEventListener('DOMContentLoaded', event => {
    document.querySelector('#connect_button').addEventListener('click', event => {
        document.querySelector('#connect_button').remove()
        const url = document.querySelector('#url').value
        const ws = new WebSocket(url)
        ws.binaryType = 'arraybuffer'
        ws.onopen = () => {
            window['main'] = new Main(ws)
        }
    })
})

class Main {
    constructor(webSocket) {
        this.ws = webSocket
        this.ws.onmessage = message => this.onMessage(message)
        this.lastTime = null
    }

    onMessage(message) {
        const messageData = new DataView(message.data)
        let timeStampValue = ''
        for (let i = 0; i < 8; ++i) {
            const asciiValue = messageData.getUint8(i)
            if (asciiValue < 33 || asciiValue > 126) {
                if (i == 0)
                    timeStampValue += '?'
                break
            }
            timeStampValue += String.fromCharCode(asciiValue)
        }
        const timeStampTime = messageData.getFloat64(8, true)
        // const messageStringFromASCII = String.fromCharCode.apply(null, new Uint8Array(message.data))
        // const messageDiv = document.querySelector('#messages')

        const timeStampTable = document.querySelector('#timer_table')
        const timeStampDt = this.lastTime != null ? (timeStampTime - this.lastTime) : 0.0
        this.lastTime = timeStampTime

        const timeStampTableRow = document.createElement('tr')
        const td1 = document.createElement('td')
        td1.appendChild(document.createTextNode(timeStampValue))
        const td2 = document.createElement('td')
        td2.appendChild(document.createTextNode(timeStampTime))
        const td3 = document.createElement('td')
        td3.appendChild(document.createTextNode(timeStampDt))
        timeStampTableRow.appendChild(td1)
        timeStampTableRow.appendChild(td2)
        timeStampTableRow.appendChild(td3)

        timeStampTable.appendChild(timeStampTableRow)
    }
}