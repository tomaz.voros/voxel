#include <random>
#include <iostream>
#include <cstdint>

int main() {
    using rng_type = std::uint32_t;
    std::size_t n = 11;
    std::mt19937 gen(4128);
    std::uniform_int_distribution<rng_type> dis(
        std::numeric_limits<rng_type>::min(),
        std::numeric_limits<rng_type>::max()
    );

    for (std::size_t i = 0; i < n; i++) {
        const auto v = dis(gen);
        std::cout << "0x" << std::hex << v << ",\n";
    }

    std::cout << std::endl;
}
