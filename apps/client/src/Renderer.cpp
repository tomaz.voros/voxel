#include "Renderer.hpp"
#include <chrono>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Input.hpp"
#include "LineCube.hpp"
#include "Camera.hpp"
#include "CameraControl.hpp"
#include "Monostable.hpp"
#include <glm/gtc/type_ptr.hpp>
#include "cfg.hpp"
#include "Log.hpp"
#include "TextRender.hpp"
#include "Gui.hpp"
#include "LineAxis.hpp"


// #define SHOW_GROUND_THING

#if defined(SHOW_GROUND_THING)
#include "Ground.hpp"
#endif

Renderer::Entities::Entities(const std::filesystem::path & assetPath) :
    mPositionLocation{ -1 },
    mVPLocation{ -1 },
    mVAO{ 0 },
    mVBO{ 0 }
{
    const std::vector<Shader::Source> entityShaderSource{
        { assetPath / "shaders/entity.vert", GL_VERTEX_SHADER },
        { assetPath / "shaders/entity.frag", GL_FRAGMENT_SHADER }
    };

    mEntityShader.reload(entityShaderSource);
    mPositionLocation = glGetUniformLocation(mEntityShader.id(), "uPosition");
    mVPLocation = glGetUniformLocation(mEntityShader.id(), "uVP");

    struct EntityVertex {
        float x, y, z;
    };

    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    glBindVertexArray(mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(EntityVertex), (void *)offsetof(EntityVertex, x));
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    std::vector<EntityVertex> mesh{ {
        {-1.0f,-1.0f,-1.0f},
        {-1.0f,-1.0f, 1.0f},
        {-1.0f, 1.0f, 1.0f},
        {1.0f, 1.0f,-1.0f},
        {-1.0f,-1.0f,-1.0f},
        {-1.0f, 1.0f,-1.0f},
        {1.0f,-1.0f, 1.0f},
        {-1.0f,-1.0f,-1.0f},
        {1.0f,-1.0f,-1.0f},
        {1.0f, 1.0f,-1.0f},
        {1.0f,-1.0f,-1.0f},
        {-1.0f,-1.0f,-1.0f},
        {-1.0f,-1.0f,-1.0f},
        {-1.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,-1.0f},
        {1.0f,-1.0f, 1.0f},
        {-1.0f,-1.0f, 1.0f},
        {-1.0f,-1.0f,-1.0f},
        {-1.0f, 1.0f, 1.0f},
        {-1.0f,-1.0f, 1.0f},
        {1.0f,-1.0f, 1.0f},
        {1.0f, 1.0f, 1.0f},
        {1.0f,-1.0f,-1.0f},
        {1.0f, 1.0f,-1.0f},
        {1.0f,-1.0f,-1.0f},
        {1.0f, 1.0f, 1.0f},
        {1.0f,-1.0f, 1.0f},
        {1.0f, 1.0f, 1.0f},
        {1.0f, 1.0f,-1.0f},
        {-1.0f, 1.0f,-1.0f},
        {1.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f,-1.0f},
        {-1.0f, 1.0f, 1.0f},
        {1.0f, 1.0f, 1.0f},
        {-1.0f, 1.0f, 1.0f},
        {1.0f,-1.0f, 1.0f},
    } };

    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, mesh.size() * sizeof(mesh[0]), mesh.data(), GL_STATIC_DRAW);
}

void Renderer::Entities::draw(const glm::mat4 & viewProjection, const glm::ivec3 & offset, EngineRendererComm & engineRendererComm) {
    if (engineRendererComm.entitiesNew())
        engineRendererComm.moveStateOut(mEntities, mSnapshotTime);

    mEntityShader.use();
    glUniformMatrix4fv(mVPLocation, 1, GL_FALSE, glm::value_ptr(viewProjection));
    glEnable(GL_DEPTH_TEST);
    // TODO: enable
    glDisable(GL_CULL_FACE);

    const float t = std::chrono::duration_cast<std::chrono::duration<float>>(
        std::chrono::steady_clock::now() - mSnapshotTime
    ).count();
    float tt = t / std::chrono::duration<float>{ cfg::TICK_DT }.count();
    static constexpr auto ALLOW_RENDERER_EXTRAPOLATION = false;
    if (!ALLOW_RENDERER_EXTRAPOLATION)
        tt = std::min(tt, 1.0f);

    for (const auto & entity : mEntities) {
        const glm::vec3 pos = glm::mix(entity.positionOld, entity.position, tt);
        glUniform3f(
            mPositionLocation,
            // TODO: deal with precision issues
            pos.x + offset.x,
            pos.y + offset.y,
            pos.z + offset.z
        );
        glBindVertexArray(mVAO);
        glDrawArrays(GL_TRIANGLES, 0, 6 * 6);
    }
}

Renderer::ChunksMeshes::ChunksMeshes(
    const VoxelTextureAtlas & voxelTileAtlas,
    const std::filesystem::path & assetPath
) :
    mVoxelTexture{ voxelTileAtlas }
{
    reloadShader(assetPath);
}

Renderer::MeshInfo Renderer::ChunksMeshes::update(EngineRendererComm & engineRendererComm, const glm::vec3 & cameraPosition, const glm::vec3 & cameraDirection) {
    EngineRendererComm::Ray ray;
    ray.origin = cameraPosition;
    ray.direction = cameraDirection;
    engineRendererComm.updateInformativeRay(ray);

    MeshInfo info;
    info.meshesInMemory = static_cast<int>(mMeshes.size());
    info.meshesUploaded = 0;
    info.meshesRendered = -1;
    info.meshesDeleted = 0;

    struct MeshHeader {
        float offX;
        float offY;
        float offZ;
        uint32_t quadCount;
    };

    static_assert(sizeof(MeshHeader) == 16);

    const auto newMeshes = engineRendererComm.getMeshes(MAX_MESH_UPDATES_PER_UPDATE_CALL);

    for (const auto & newMesh : newMeshes) {
        const auto off = newMesh.position * cfg::CHUNK_SIZE + cfg::CHUNK_SIZE / 2;
        if (newMesh.mesh.size() == 0) {
            const auto mesh = mMeshes.find(newMesh.position);
            assert(mesh != mMeshes.end() && "Removing non-existing mesh.");
            // remove
            assert(mesh->second.elementCount != 0);
            assert(mesh->second.VAO != 0 && mesh->second.VBO != 0);
            assert(glm::all(glm::equal(mesh->second.offset, off)));
            glDeleteBuffers(1, &mesh->second.VBO);
            glDeleteVertexArrays(1, &mesh->second.VAO);
            mQuadCount -= mesh->second.elementCount / 6;

            mVoxelAlloc.deallocate(mesh->second.VBONew);

            mMeshes.erase(mesh);
            ++info.meshesDeleted;
        } else {
            auto meshIt = mMeshes.find(newMesh.position);
            if (meshIt != mMeshes.end()) {
                // replace
                assert(meshIt->second.elementCount != 0);
                assert(meshIt->second.VAO != 0 && meshIt->second.VBO != 0);
                assert(glm::all(glm::equal(meshIt->second.offset, off)));
                glDeleteBuffers(1, &meshIt->second.VBO);
                glDeleteVertexArrays(1, &meshIt->second.VAO);
                meshIt->second.VAO  = 0;
                meshIt->second.VBO  = 0;
                mQuadCount -= meshIt->second.elementCount / 6;
                meshIt->second.elementCount = 0;

                mVoxelAlloc.deallocate(meshIt->second.VBONew);

            } else {
                // add
                auto meshInsertionPair = mMeshes.insert({ newMesh.position, {} });
                assert(meshInsertionPair.second == true);
                meshIt = meshInsertionPair.first;
                meshIt->second.offset = off;
                meshIt->second.VAO = 0;
                meshIt->second.VBO = 0;
                meshIt->second.elementCount = 0;
            }

            auto & mesh = meshIt->second;
            // const GLsizeiptr elementCount = newMesh.mesh.size() + (newMesh.mesh.size() / 2);
            const GLsizeiptr elementCount = newMesh.mesh.size() * 6;
            mQuadCount += elementCount / 6;
            glGenVertexArrays(1, &mesh.VAO);
            glCreateBuffers(1, &mesh.VBO);
            glBindVertexArray(mesh.VAO);
            mQuadEBO.bind();
            mQuadEBO.resize(elementCount);
            glBindVertexArray(0);
            {
                const MeshHeader meshHeader{
                    .offX = static_cast<float>(mesh.offset.x),
                    .offY = static_cast<float>(mesh.offset.y),
                    .offZ = static_cast<float>(mesh.offset.z),
                    .quadCount = static_cast<uint32_t>(elementCount) / 6
                };

                GLsizeiptr quadSize = newMesh.mesh.size() * sizeof(newMesh.mesh[0]);
                GLsizeiptr dataSize = sizeof(MeshHeader) + quadSize;
                GLsizeiptr bufferSize = (dataSize + 3) / 4 * 4;
                glNamedBufferData(mesh.VBO, bufferSize, nullptr, GL_STATIC_DRAW);
                glNamedBufferSubData(mesh.VBO, 0, sizeof(MeshHeader), &meshHeader);
                glNamedBufferSubData(mesh.VBO, sizeof(MeshHeader), quadSize, newMesh.mesh.data());

                const uint32_t zero32 = 0u;
                if (dataSize < bufferSize)
                    glNamedBufferSubData(mesh.VBO, dataSize, bufferSize - dataSize, &zero32);
            }
            mesh.elementCount = elementCount;
            ++info.meshesUploaded;

            {
                size_t meshByteSize = newMesh.mesh.size() * sizeof(newMesh.mesh[0]);
                auto allocation = mVoxelAlloc.allocate(sizeof(VoxelMeshHeader), meshByteSize);
                VoxelMeshHeader header;
                static_assert(VoxelAlloc::BUFFER_SIZE % 4 == 0, "Buffer alignment requirements not met.");
                header.bufferOffset = allocation.offTop / 4;
                header.count = newMesh.mesh.size();
                header.offsetX = static_cast<float>(mesh.offset.x);
                header.offsetY = static_cast<float>(mesh.offset.y);
                header.offsetZ = static_cast<float>(mesh.offset.z);
                glNamedBufferSubData(allocation.vbo, allocation.offBot, sizeof(header), &header);
                glNamedBufferSubData(allocation.vbo, allocation.offTop, meshByteSize, newMesh.mesh.data());

                mesh.header = header;
                mesh.VBONew = allocation.vbo;
            }
        }
    }

    return info;
}

Renderer::MeshInfo Renderer::ChunksMeshes::drawPVF(
    const glm::mat4 & view,
    const glm::mat4 & viewProjection,
    const glm::ivec3 & offset,
    const glm::vec3 & cameraPosition,
    const glm::vec3 & cameraPositionFraction,
    const std::array<float, 4> & fogColor,
    float fogStart,
    float fogTransitionDistance,
    float aoShadeStrength,
    Texturing & texturingParams
) {
    /* TODO:
    Culling:
    1. View-Frustum culling (CPU side)
    2. Occlusion culling (virtual occluders, potentially visible set, cells and portals, occluder fusion ...) (see RTG1)
    3. Backface-Culling (~90% can be eliminated by CPU)
    */

    // TODO: explore vertex position transfromation reuse (some vertex positions can be reused up to 8 times)
   
    if (texturingParams.changed()) {
        auto mipRange = texturingParams.mipmapRange();
        mVoxelTexture.updateSampler(
            texturingParams.magnifyingFilter(),
            texturingParams.minifyingFilter(),
            std::max(0, mipRange.first),
            std::min(mVoxelTexture.maxMipLevel(), mipRange.second),
            texturingParams.anisotropyLevel()
        );
        texturingParams.clearDirty();
    }

    MeshInfo info;
    info.meshesInMemory = static_cast<int>(mMeshes.size());
    info.meshesUploaded = -1;
    info.meshesRendered = 0;
    info.meshesDeleted = -1;

    auto normalizePlane = [](const glm::vec4 & plane) -> glm::vec4 { return plane / glm::length(glm::vec3{plane}); };

    auto matrixToNormalizedFrustumPlanes = [normalizePlane](const glm::mat4 & matrix) -> std::array<glm::vec4, 6> {
        const glm::mat4 tMatrix = glm::transpose(matrix);
        std::array<glm::vec4, 6> planes;
        planes[0] = normalizePlane(tMatrix[3] + tMatrix[0]);
        planes[1] = normalizePlane(tMatrix[3] - tMatrix[0]);
        planes[2] = normalizePlane(tMatrix[3] - tMatrix[1]);
        planes[3] = normalizePlane(tMatrix[3] + tMatrix[1]);
        planes[4] = normalizePlane(tMatrix[3] + tMatrix[2]);
        planes[5] = normalizePlane(tMatrix[3] - tMatrix[2]);
        return planes;
    };

    auto planePointSquareDistance = [](const glm::vec4 & plane, const glm::vec3 & point) -> float {
        return glm::dot(plane, glm::vec4{point, 1.0f});
    };

    auto sphereInFrustum =
        [planePointSquareDistance](
            const std::array<glm::vec4, 6> & planes, const glm::vec3 & center, const float & radius) -> bool {
        for (const auto & plane : planes)
            if (planePointSquareDistance(plane, center) < -radius)
                return false;
        return true;
    };

    const auto planes = matrixToNormalizedFrustumPlanes(viewProjection);

    static constexpr float SQUARE_MESH_RADIUS{cfg::CHUNK_SIZE_X * cfg::CHUNK_SIZE_X / 2.0f +
                                              cfg::CHUNK_SIZE_Y * cfg::CHUNK_SIZE_Y / 2.0f +
                                              cfg::CHUNK_SIZE_Z * cfg::CHUNK_SIZE_Z / 2.0f};

    glUseProgram(mBlockShaderPVF.name());
    glProgramUniform3f(mBlockShaderPVF.name(), mBlockShaderPVF.uFogColor, fogColor[0], fogColor[1], fogColor[2]);
    glProgramUniform1f(mBlockShaderPVF.name(), mBlockShaderPVF.uFogStart, fogStart);
    glProgramUniform1f(mBlockShaderPVF.name(), mBlockShaderPVF.uFogTransitionDistance, fogTransitionDistance);
    glUniformMatrix4fv(mBlockShaderPVF.uVP, 1, GL_FALSE, glm::value_ptr(viewProjection));
    glUniformMatrix4fv(mBlockShaderPVF.uV, 1, GL_FALSE, glm::value_ptr(view));

    const auto cameraViewSpacePosition = glm::vec3{glm::vec4{cameraPositionFraction, 1.0f} * view};
    glUniform3f(
        mBlockShaderPVF.uCameraViewSpacePosition,
        cameraViewSpacePosition.x,
        cameraViewSpacePosition.y,
        cameraViewSpacePosition.z);
    glUniform3f(mBlockShaderPVF.uCameraWorldSpacePosition, cameraPosition.x, cameraPosition.y, cameraPosition.z);

    glUniform1f(mBlockShaderPVF.uAoOffset, 0.0f);
    glUniform1f(mBlockShaderPVF.uAoScale, 1.0f);

    glProgramUniform1f(mBlockShaderPVF.name(), mBlockShaderPVF.uAoShadeStrength, aoShadeStrength);

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);

    glUniform1i(mBlockShaderPVF.uVoxelTexture, 0);
    mVoxelTexture.bind(0);

    const glm::vec3 centerOffset = glm::vec3{offset} + glm::vec3{cfg::CHUNK_SIZE} / 2.0f;
    const float radius = std::sqrt(SQUARE_MESH_RADIUS);

    glUniform3f(
        mBlockShaderPVF.uOffset,
        static_cast<float>(offset.x),
        static_cast<float>(offset.y),
        static_cast<float>(offset.z)
    );

    for (const auto & mesh : mMeshes) {
        assert(mesh.second.elementCount != 0);
        assert(mesh.second.VAO != 0 && mesh.second.VBO != 0);
        const glm::vec3 center = glm::vec3{mesh.second.offset} + centerOffset;
        // frustum culling
        if (!sphereInFrustum(planes, center, radius))
            continue;

        // TODO: fix the false occlusion culling
        // TODO: try AABB instead of sphere and benchmark (less work for GPU, more work for CPU)

        // glUniform3f(mBlockShaderPVF.uWorldSpeceOffset, mesh.second.offset.x, mesh.second.offset.y, mesh.second.offset.z);
        glBindVertexArray(mesh.second.VAO);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mBlockShaderPVF.bufVertexData, mesh.second.VBO);
        glDrawElements(GL_TRIANGLES, mesh.second.elementCount, mQuadEBO.type(), nullptr);
        ++info.meshesRendered;
        info.quadsRendered += mesh.second.elementCount;
    }
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mBlockShaderPVF.bufVertexData, 0);
    mVoxelTexture.unbind(0);

    return info;
}

Renderer::MeshInfo Renderer::ChunksMeshes::drawMeshShader(
    const glm::mat4 & view,
    const glm::mat4 & viewProjection,
    const glm::ivec3 & offset,
    const glm::vec3 & cameraPosition,
    const glm::vec3 & cameraPositionFraction,
    const std::array<float, 4> & fogColor,
    float fogStart,
    float fogTransitionDistance,
    float aoShadeStrength,
    Texturing & texturingParams
) {
    /* TODO:
    Culling:
    1. View-Frustum culling (CPU side)
    2. Occlusion culling (virtual occluders, potentially visible set, cells and portals, occluder fusion ...) (see RTG1)
    3. Backface-Culling (~90% can be eliminated by CPU)
    */

    // TODO: explore vertex position transfromation reuse (some vertex positions can be reused up to 8 times)
   
    if (texturingParams.changed()) {
        auto mipRange = texturingParams.mipmapRange();
        mVoxelTexture.updateSampler(
            texturingParams.magnifyingFilter(),
            texturingParams.minifyingFilter(),
            std::max(0, mipRange.first),
            std::min(mVoxelTexture.maxMipLevel(), mipRange.second),
            texturingParams.anisotropyLevel()
        );
        texturingParams.clearDirty();
    }

    MeshInfo info;
    info.meshesInMemory = static_cast<int>(mMeshes.size());
    info.meshesUploaded = -1;
    info.meshesRendered = 0;
    info.meshesDeleted = -1;

    auto normalizePlane = [](const glm::vec4 & plane) -> glm::vec4 { return plane / glm::length(glm::vec3{plane}); };

    auto matrixToNormalizedFrustumPlanes = [normalizePlane](const glm::mat4 & matrix) -> std::array<glm::vec4, 6> {
        const glm::mat4 tMatrix = glm::transpose(matrix);
        std::array<glm::vec4, 6> planes;
        planes[0] = normalizePlane(tMatrix[3] + tMatrix[0]);
        planes[1] = normalizePlane(tMatrix[3] - tMatrix[0]);
        planes[2] = normalizePlane(tMatrix[3] - tMatrix[1]);
        planes[3] = normalizePlane(tMatrix[3] + tMatrix[1]);
        planes[4] = normalizePlane(tMatrix[3] + tMatrix[2]);
        planes[5] = normalizePlane(tMatrix[3] - tMatrix[2]);
        return planes;
    };

    auto planePointSquareDistance = [](const glm::vec4 & plane, const glm::vec3 & point) -> float {
        return glm::dot(plane, glm::vec4{point, 1.0f});
    };

    auto sphereInFrustum =
        [planePointSquareDistance](
            const std::array<glm::vec4, 6> & planes, const glm::vec3 & center, const float & radius) -> bool {
        for (const auto & plane : planes)
            if (planePointSquareDistance(plane, center) < -radius)
                return false;
        return true;
    };

    const auto planes = matrixToNormalizedFrustumPlanes(viewProjection);

    static constexpr float SQUARE_MESH_RADIUS{cfg::CHUNK_SIZE_X * cfg::CHUNK_SIZE_X / 2.0f +
                                              cfg::CHUNK_SIZE_Y * cfg::CHUNK_SIZE_Y / 2.0f +
                                              cfg::CHUNK_SIZE_Z * cfg::CHUNK_SIZE_Z / 2.0f};

    glUseProgram(mBlockShaderMeshShader.name());
    glProgramUniform3f(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uFogColor, fogColor[0], fogColor[1], fogColor[2]);
    glProgramUniform1f(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uFogStart, fogStart);
    glProgramUniform1f(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uFogTransitionDistance, fogTransitionDistance);
    glUniformMatrix4fv(mBlockShaderMeshShader.uVP, 1, GL_FALSE, glm::value_ptr(viewProjection));
    glUniformMatrix4fv(mBlockShaderMeshShader.uV, 1, GL_FALSE, glm::value_ptr(view));

    const auto cameraViewSpacePosition = glm::vec3{glm::vec4{cameraPositionFraction, 1.0f} * view};
    glUniform3f(
        mBlockShaderMeshShader.uCameraViewSpacePosition,
        cameraViewSpacePosition.x,
        cameraViewSpacePosition.y,
        cameraViewSpacePosition.z);
    // glUniform3f(mBlockShaderMeshShader.uCameraWorldSpacePosition, cameraPosition.x, cameraPosition.y, cameraPosition.z);

    // glUniform1f(mBlockShaderMeshShader.uAoOffset, 0.0f);
    // glUniform1f(mBlockShaderMeshShader.uAoScale, 1.0f);

    glProgramUniform1f(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uAoShadeStrength, aoShadeStrength);

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);

    glUniform1i(mBlockShaderMeshShader.uVoxelTexture, 0);
    mVoxelTexture.bind(0);

    const glm::vec3 centerOffset = glm::vec3{offset} + glm::vec3{cfg::CHUNK_SIZE} / 2.0f;
    const float radius = std::sqrt(SQUARE_MESH_RADIUS);

    glUniform3f(
        mBlockShaderMeshShader.uOffset,
        static_cast<float>(offset.x),
        static_cast<float>(offset.y),
        static_cast<float>(offset.z)
    );

#if 1
    for (const auto & mesh : mVoxelAlloc.buffers()) {
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mBlockShaderMeshShader.bufVertexData, mesh.vbo);

        // TODO: driver implementation constant (needs querying but is 64 * 1024 - 1 on all nvidia GPUs until and including 2020)
        // const uint32_t quadsPerTaskInvo = 64 * 1024 - 1;
        // derived
        // const uint32_t quadCount = mesh.second.elementCount / 6;
        // const uint32_t taskInvocations = quadCount / quadsPerTaskInvo + (quadCount % quadsPerTaskInvo != 0);

        // // glProgramUniform1ui(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uQuadCount, quadCount);
        // what if the allocation is too large :(
        glDrawMeshTasksNV(0, mesh.allocations);

        info.meshesRendered += mesh.allocations;
    }
#else
    for (const auto & mesh : mMeshes) { // iterate by VBO buffers
#if 0
        assert(mesh.second.elementCount != 0);
        assert(mesh.second.VAO != 0 && mesh.second.VBO != 0);
        const glm::vec3 center = glm::vec3{mesh.second.offset} + centerOffset;
        // frustum culling
        if (!sphereInFrustum(planes, center, radius))
            continue;

        // TODO: fix the false occlusion culling
        // TODO: try AABB instead of sphere and benchmark (less work for GPU, more work for CPU)

        // glUniform3f(mBlockShaderMeshShader.uWorldSpeceOffset, mesh.second.offset.x, mesh.second.offset.y, mesh.second.offset.z);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mBlockShaderMeshShader.bufVertexData, mesh.second.VBO);

        // TODO: driver implementation constant (needs querying but is 64 * 1024 - 1 on all nvidia GPUs until and including 2020)
        const uint32_t quadsPerTaskInvo = 64 * 1024 - 1;
        // derived
        const uint32_t quadCount = mesh.second.elementCount / 6;
        const uint32_t taskInvocations = quadCount / quadsPerTaskInvo + (quadCount % quadsPerTaskInvo != 0);

        // glProgramUniform1ui(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uQuadCount, quadCount);

        glDrawMeshTasksNV(0, taskInvocations);

        ++info.meshesRendered;
        info.quadsRendered += mesh.second.elementCount;
#else
        assert(mesh.second.header.count != 0);
        assert(mesh.second.VBONew != 0);
        const glm::vec3 center = glm::vec3{mesh.second.offset} + centerOffset;
        // frustum culling
        if (!sphereInFrustum(planes, center, radius))
            continue;
        // TODO: fix the false occlusion culling
        // TODO: try AABB instead of sphere and benchmark (less work for GPU, more work for CPU)

        // glUniform3f(mBlockShaderMeshShader.uWorldSpeceOffset, mesh.second.offset.x, mesh.second.offset.y, mesh.second.offset.z);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mBlockShaderMeshShader.bufVertexData, mesh.second.VBONew);

        // TODO: driver implementation constant (needs querying but is 64 * 1024 - 1 on all nvidia GPUs until and including 2020)
        const uint32_t quadsPerTaskInvo = 64 * 1024 - 1;
        // derived
        const uint32_t quadCount = mesh.second.elementCount / 6;
        const uint32_t taskInvocations = quadCount / quadsPerTaskInvo + (quadCount % quadsPerTaskInvo != 0);

        // glProgramUniform1ui(mBlockShaderMeshShader.name(), mBlockShaderMeshShader.uQuadCount, quadCount);

        glDrawMeshTasksNV(0, taskInvocations);

        ++info.meshesRendered;
        info.quadsRendered += mesh.second.elementCount;
#endif
    }
#endif

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mBlockShaderMeshShader.bufVertexData, 0);
    mVoxelTexture.unbind(0);

    return info;
}

Renderer::Core::Core(
    EngineRendererComm & engineRendererComm,
    GLFWwindow * window,
    const glm::ivec3 & initialCenterBlock,
    const VoxelTextureAtlas & voxelTileAtlas,
    const std::filesystem::path & assetPath
) :
    mWindow{ window },
    mEntities{ assetPath },
    mChunksMeshes{ voxelTileAtlas, assetPath },
    mEngineRendererComm{ engineRendererComm },
    mInitCenterBlock{ initialCenterBlock },
    mSkyBox{
        {
            assetPath / "textures/clouds.jpg",
            assetPath / "textures/clouds.jpg",
            assetPath / "textures/clouds.jpg",
            assetPath / "textures/clouds.jpg",
            assetPath / "textures/clouds.jpg",
            assetPath / "textures/clouds.jpg"
        },
        1024, 1024,
        {
            { assetPath / "shaders/skybox.vert", GL_VERTEX_SHADER },
            { assetPath / "shaders/skybox.frag", GL_FRAGMENT_SHADER }
        }
    },
    mAssetPath{ assetPath },
    mVoxelTileAtlas{voxelTileAtlas}
{
    {
        GLint queryTimerBitCount = 0;
        glGetQueryiv(GL_TIME_ELAPSED, GL_QUERY_COUNTER_BITS, &queryTimerBitCount);
        log::info("Query timer bit count: {}.", queryTimerBitCount);
    }

    glCreateQueries(GL_TIME_ELAPSED, mGpuTimeQueries.size(), mGpuTimeQueries.data());
    std::fill_n(mGpuTimeQueriesPending.begin(), mGpuTimeQueriesPending.size(), false);
}

Renderer::Core::~Core() {
    glDeleteQueries(mGpuTimeQueries.size(), mGpuTimeQueries.data());
}

void Renderer::Core::run(const VoxelMetaData & voxelTileInfo, const std::filesystem::path & userPath) {
    // TODO: software framerate limiter in case vsync fails

    Input input{mWindow};
    Gui gui{mWindow, userPath}; // initialize after input

    gui.mBlockNames.reserve(voxelTileInfo.names.size());
    gui.mBlockNamesInternal.reserve(voxelTileInfo.names.size());
    for (const auto & name : voxelTileInfo.names) {
        gui.mBlockNames.push_back(name.data());
        gui.mBlockNamesInternal.push_back(gui.mBlockNames.back().c_str());
    }

    LineCube lineCube{ mAssetPath };

    LineAxis lineAxis{ mAssetPath };

    Camera camera{};
    camera.setUp({ 0.0f, 1.0f, 0.0f });
    camera.setAspect(1.0f);
    camera.setNear(0.1f);
    camera.setFar(2000.0f);
    camera.setFov(3.14f / 2.0f);
    camera.setPosition(glm::vec3{ mInitCenterBlock } / 0.5f);
    camera.setFacing(0.0f, 0.0f);

    CameraControl cameraControl{};

    glm::vec3 cameraPositionIntegerFloat;
    const glm::vec3 cameraPositionFraction = glm::modf(cameraControl.getPosition(), cameraPositionIntegerFloat);
    const glm::ivec3 cameraPositionInteger = cameraPositionIntegerFloat;
    glm::ivec3 centerChunk = cfg::blockPositionToChunkPosition(cameraPositionInteger);

    Monostable mouseLockMonostable{};
    glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    bool mouseLocked = false;
    
    float infoUpdatePeriod = 0.5f;
    float lastInfoUpdate = 0.0f;
    int frameCounter = 0;
    int meshesUpdated = 0;
    int meshesDeleted = 0;

    size_t frameIndex = 0;

    std::size_t txSize = 0;
    std::size_t rxSize = 0;
    std::size_t txCapacity = 0;
    std::size_t rxCapacity = 0;

    Monostable lMouseButton;
    Monostable rMouseButton;

    Monostable AGamepadButton;
    Monostable BGamepadButton;

    gui.mTexturing.setDirty();

#if defined(SHOW_GROUND_THING)
    Ground ground{mAssetPath, mAssetPath / "ground.json"};
#endif

    auto currentTime = std::chrono::high_resolution_clock::now();
    while (glfwWindowShouldClose(mWindow) == 0) {
        const auto newCurrentTime = std::chrono::high_resolution_clock::now();
        const double dt = std::chrono::duration_cast<std::chrono::duration<double>>(newCurrentTime - currentTime).count();
        currentTime = newCurrentTime;

        GLuint64 gpuTimeNs = std::numeric_limits<GLuint64>::max();
        if (mGpuTimeQueriesPending[mNextQueryIndex]) {
            glGetQueryObjectui64v(mGpuTimeQueries[mNextQueryIndex], GL_QUERY_RESULT_NO_WAIT, &gpuTimeNs);
            mGpuTimeQueriesPending[mNextQueryIndex] = false;
            if (gpuTimeNs == std::numeric_limits<GLuint64>::max()) {
                log::warning("Query not complete after {} frames.", mGpuTimeQueries.size());
                gpuTimeNs = 0;
            }
        }
        if (gpuTimeNs == std::numeric_limits<GLuint64>::max())
            gui.addGpuTime(0.0);
        else
            gui.addGpuTime(static_cast<double>(gpuTimeNs) / 1'000'000'000.0);

        glBeginQuery(GL_TIME_ELAPSED, mGpuTimeQueries[mNextQueryIndex]);
        mGpuTimeQueriesPending[mNextQueryIndex] = true;
        mNextQueryIndex = (mNextQueryIndex + 1) % mGpuTimeQueries.size();

        input.poll();

        GLFWgamepadstate gamepadState = {};
        {
            input.getJoystickNames(gui.mJoystickMappings, gui.mJoystickNames);
            auto activeJoystickIndex = gui.mCurrentJoystickIndex;
            if (activeJoystickIndex >= 0 && activeJoystickIndex < gui.mJoystickMappings.size()) {
                gamepadState = input.getJoystickState(gui.mJoystickMappings[activeJoystickIndex]);
            }
        }
        gui.mActiveGamepadState = gamepadState;

        const auto stopServer = input.key(GLFW_KEY_X);
        if (stopServer) {
            mEngineRendererComm.setSendStopServer();
        }
        const auto reloadVoxelShader = input.key(GLFW_KEY_R);
        if (reloadVoxelShader) {
            this->mChunksMeshes.reloadShader(mAssetPath);
        }

        mouseLockMonostable.update(input.key(GLFW_KEY_Q));
        if (mouseLockMonostable.state()) {
            if (mouseLocked) {
                glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            } else {
                glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            }
            mouseLocked = !mouseLocked;
        }

        lMouseButton.update(input.button(0));
        rMouseButton.update(input.button(1));

        AGamepadButton.update(gamepadState.buttons[0] == GLFW_PRESS);
        BGamepadButton.update(gamepadState.buttons[1] == GLFW_PRESS);

        const std::array<bool, 6> keys{
            input.key(GLFW_KEY_U), input.key(GLFW_KEY_J), input.key(GLFW_KEY_K),
            input.key(GLFW_KEY_H), input.key(GLFW_KEY_L), input.key(GLFW_KEY_SPACE)
        };
        glm::vec2 mouseDelta = input.cursorDelta();
        if (!mouseLocked) {
            mouseDelta.x = 0.0f;
            mouseDelta.y = 0.0f;
        }
        cameraControl.setSpeed(gui.mCameraSpeed);
        cameraControl.update(dt, keys, mouseDelta, gamepadState);

        int frameBufferWidth = 0;
        int frameBufferHeight = 0;
        glfwGetFramebufferSize(mWindow, &frameBufferWidth, &frameBufferHeight);
        glViewport(0, 0, frameBufferWidth, frameBufferHeight);

        glm::vec3 cameraPositionIntegerFloat;
        const glm::vec3 cameraPositionFraction = glm::modf(cameraControl.getPosition(), cameraPositionIntegerFloat);
        const glm::ivec3 cameraPositionInteger = cameraPositionIntegerFloat;

        if (frameBufferWidth == 0 || frameBufferHeight == 0) {
            frameBufferWidth = 1;
            frameBufferHeight = 1;
        }
        const float aspect = static_cast<float>(frameBufferWidth) / static_cast<float>(frameBufferHeight);
        camera.setAspect(aspect);
        camera.setPosition(cameraPositionFraction);
        camera.setFacing(cameraControl.getYaw(), cameraControl.getPitch());
        const auto viewProjection = camera.getViewProjection();
        const auto view = camera.getView();

        glm::ivec3 centerChunk = cfg::blockPositionToChunkPosition(cameraPositionInteger);
        const auto sceneInfo1 = mChunksMeshes.update(mEngineRendererComm, cameraControl.getPosition(), cameraControl.getFacing());
        gui.mQuadCount = mChunksMeshes.getQuadCount();
        const glm::ivec3 centerBlock = glm::floor(cameraControl.getPosition());
        mEngineRendererComm.setCenterBlock(centerBlock);

        glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        glClear(GL_DEPTH_BUFFER_BIT);

        MeshInfo sceneInfo2{};
        {
            if (gui.mWireframeBlocks)
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            if (gui.mUseMeshShader)
                sceneInfo2 = mChunksMeshes.drawMeshShader(
                    view,
                    viewProjection,
                    -cameraPositionInteger,
                    cameraControl.getPosition(),
                    cameraPositionFraction,
                    gui.mFogColor,
                    gui.mFogStart,
                    gui.mFogTransitionDistance,
                    gui.mAoShadeStrength,
                    gui.mTexturing
                );
            else
                sceneInfo2 = mChunksMeshes.drawPVF(
                    view,
                    viewProjection,
                    -cameraPositionInteger,
                    cameraControl.getPosition(),
                    cameraPositionFraction,
                    gui.mFogColor,
                    gui.mFogStart,
                    gui.mFogTransitionDistance,
                    gui.mAoShadeStrength,
                    gui.mTexturing
                );

            if (gui.mWireframeBlocks)
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if (DRAW_ENTITIES)
            mEntities.draw(viewProjection, -cameraPositionInteger, mEngineRendererComm);

        const auto lb = lMouseButton.state();
        const auto rb = rMouseButton.state();
        const auto ab = AGamepadButton.state();
        const auto bb = BGamepadButton.state();
        EngineRendererComm::Ray ray;
        ray.origin = cameraControl.getPosition();
        ray.direction = cameraControl.getFacing();
        if (rb || bb) {
            EngineRendererComm::RayPlace rayPlace;
            rayPlace.ray = ray;
            rayPlace.value = cfg::Block{ 0 };
            rayPlace.position = 0;
            mEngineRendererComm.addRayPlace(rayPlace);
        }
        if (lb || ab) {
            EngineRendererComm::RayPlace rayPlace;
            rayPlace.ray = ray;
            rayPlace.value = static_cast<cfg::Block>(std::rand() % 6 + 1);
            rayPlace.value = gui.mCurrentBlockIndex;
            rayPlace.position = -1;
            mEngineRendererComm.addRayPlace(rayPlace);
        }

        const auto rayResolution = mEngineRendererComm.getLatestInformativeRayResolution();

        if (rayResolution.hit) {
            gui.mHitBlockIndex = std::max<int>(0, std::min<int>(rayResolution.hitValue, gui.mBlockNames.size() - 1));
        }
        else {
            gui.mHitBlockIndex = 0;
        }
        if (rayResolution.hit && gui.mDrawRayHitLocation) {
            lineCube.draw(viewProjection, rayResolution.last - cameraPositionInteger, { 1, 1, 1 });
            lineCube.draw(viewProjection, rayResolution.preLast - cameraPositionInteger, { 1, 1, 1 });
        }
        if (gui.mDrawAxes)
        {
            const auto cameraPosition = cameraControl.getPosition();
            const auto cameraFacing = cameraControl.getFacing();
            static constexpr float CROSS_DISTANCE = 2.0f;
            const auto crossPosition = -cameraFacing * CROSS_DISTANCE;
            const auto V = glm::lookAt(crossPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
            const auto P = camera.getProjection();

            GLint previousDepthFunc = -1;
            glGetIntegerv(GL_DEPTH_FUNC, &previousDepthFunc);
            glDepthFunc(GL_ALWAYS);

            lineAxis.draw(P * V);

            glDepthFunc(previousDepthFunc);
        }
        mSkyBox.draw(camera.getView(), camera.getProjection(), cameraControl.getPosition());

        static constexpr glm::vec3 CENTER_MARKER_SIZE{ 0.02f, 0.02f, 0.02f };
        const glm::vec3 actualMarkerSize = CENTER_MARKER_SIZE * glm::vec3{ 1.0f, static_cast<float>(aspect), 1.0f };
        lineCube.draw(glm::mat4{ 1.0f }, -actualMarkerSize / 2.0f, actualMarkerSize);

#if defined(SHOW_GROUND_THING)
        {
            camera.setPosition(cameraControl.getPosition());

            if (gui.mWireframeBlocks)
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            ground.draw(camera.getViewProjection());
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            camera.setPosition(cameraPositionFraction);
        }
#endif

        MeshInfo sceneInfo;
        sceneInfo.meshesInMemory = std::max(sceneInfo1.meshesInMemory, sceneInfo2.meshesInMemory);
        sceneInfo.meshesUploaded = std::max(sceneInfo1.meshesUploaded, sceneInfo2.meshesUploaded);
        sceneInfo.meshesRendered = std::max(sceneInfo1.meshesRendered, sceneInfo2.meshesRendered);
        sceneInfo.meshesDeleted = std::max(sceneInfo1.meshesDeleted, sceneInfo2.meshesDeleted);

        ++frameCounter;
        lastInfoUpdate += dt;
        meshesUpdated += sceneInfo.meshesUploaded;
        meshesDeleted += sceneInfo.meshesDeleted;

        if (lastInfoUpdate > infoUpdatePeriod) {
            auto pos = cameraPositionInteger;
            pos.x -= (cameraPositionFraction.x < 0.0f);
            pos.y -= (cameraPositionFraction.y < 0.0f);
            pos.z -= (cameraPositionFraction.z < 0.0f);
            gui.mFPS = float(frameCounter) / lastInfoUpdate;
            gui.mPosition = std::to_string(pos.x) + '/' + std::to_string(pos.y) + '/' + std::to_string(pos.z);
            gui.mMeshesInMemory = std::to_string(sceneInfo.meshesInMemory);
            gui.mMeshesRendered = std::to_string(sceneInfo.meshesRendered);
            gui.mMeshesUploaded = std::to_string(meshesUpdated);
            gui.mMeshesDeleted = std::to_string(meshesDeleted);
            
            meshesUpdated = 0;
            lastInfoUpdate = 0.0f;
            meshesDeleted = 0;

            gui.mTx = std::to_string(txCapacity / frameCounter) + " | " + std::to_string(txSize / frameCounter);
            gui.mRx = std::to_string(rxCapacity / frameCounter) + " | " + std::to_string(rxSize / frameCounter);

            gui.mChunksCached = std::to_string(mEngineRendererComm.getCachedChunkCount());
            gui.mObservedMeshesCached = std::to_string(mEngineRendererComm.getCachedObservedMeshCount());

            if (rayResolution.hit) {
                gui.mLookingAt = std::to_string(rayResolution.last.x) + '/' + std::to_string(rayResolution.last.y) + '/' + std::to_string(rayResolution.last.z);
            } else {
                gui.mLookingAt = "?/?/?";
            }

            txSize = 0;
            rxSize = 0;
            txCapacity = 0;
            rxCapacity = 0;

            frameCounter = 0;
        }

        const glutil::viewport vp1 = {
            0, frameBufferHeight / 2, frameBufferWidth / 2, frameBufferHeight - frameBufferHeight / 2
        };

        glEndQuery(GL_TIME_ELAPSED);

        gui.mFramebufferWidth = frameBufferWidth;
        gui.mFramebufferHeight = frameBufferHeight;
        gui.draw();

        glfwSwapBuffers(mWindow);
    }

    glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}
