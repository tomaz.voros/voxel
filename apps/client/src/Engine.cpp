#include "Engine.hpp"

#include <cstring>
#include <algorithm>

#include "Ray.hpp"
#include "Log.hpp"
#include "compression.hpp"
#include "trace.hpp"

#include <util.hpp>

namespace {
    [[deprecated("Directly generate the new message type.")]]
    std::vector<std::byte> convertMessage(const std::pair<cfg::Message::Header, std::vector<std::byte>>& message) {
        std::vector<std::byte> outMessage;
        outMessage.resize(message.second.size() + 1);
        outMessage[0] = static_cast<std::byte>(message.first.id);
        std::copy(message.second.begin(), message.second.end(), outMessage.begin() + 1);
        return outMessage;
    }
}

/*
    // TODO: generally: instead of iterating over this, one can iterate over std::array<glm::ivec3, 8>
    glm::ivec3 i;
    for (i.z = -1; i.z < 1; ++i.z)
    for (i.y = -1; i.y < 1; ++i.y)
    for (i.x = -1; i.x < 1; ++i.x) {
*/

Engine::Core::Core(
    const boost::asio::ip::tcp::endpoint & endpoint,
    EngineRendererComm & engineRendererComm,
    std::string_view userName,
    const VoxelMetaData & voxelTileInfo,
    std::string_view host,
    std::string_view target,
    const boost::asio::ip::tcp::endpoint & monitorAcceptorEndpoint
) :
    mChunksMeshes{ engineRendererComm },
    mEngineRendererComm{ engineRendererComm },
    mVoxelTileInfo{ voxelTileInfo },
    mServerConnection{
        mIoContext,
        endpoint,
        host,
        target,
        std::chrono::seconds{10}
    }
{
    // TODO: refactor this (out?)
    mEngineRendererComm.setNotifyRayPlaceUpdateFunction(
        [this] (EngineRendererComm::RayPlace rayPlace) {
            std::unique_lock lock{ mPendingPlaceRaysMutex };
            mPendingPlaceRays.push(rayPlace);
        }
    );

    mEngineRendererComm.setTouchNetworkingLoopFunction([this]() { touchNetworkingLoop(); });

    std::pair<cfg::Message::Header, std::vector<std::byte>> authenticationMessage;
    authenticationMessage.first.id = cfg::Message::Id::AUTHENTICATE;
    authenticationMessage.first.size = 16;
    authenticationMessage.second.resize(authenticationMessage.first.size);

    const auto userNameLength = std::min<std::size_t>(userName.size(), authenticationMessage.first.size);
    std::fill_n(authenticationMessage.second.begin() + userNameLength, authenticationMessage.first.size - userNameLength, std::byte{ 0 });
    std::copy_n(userName.data(), userNameLength, reinterpret_cast<char*>(authenticationMessage.second.data()));
    if (userNameLength == 0)
        authenticationMessage.second[0] = static_cast<std::byte>('a');
    mServerConnection.sendMessage(convertMessage(authenticationMessage));

    std::pair<cfg::Message::Header, std::vector<std::byte>> awareAreaMessage;
    awareAreaMessage.first.id = cfg::Message::Id::MOVE_AWARE_AREA;
    awareAreaMessage.first.size = 24;
    awareAreaMessage.second.resize(awareAreaMessage.first.size);
    auto awareAreaMessagePtr = awareAreaMessage.second.data();
    awareAreaMessagePtr = util::writeType(awareAreaMessagePtr, std::numeric_limits<std::int32_t>::min() + 100);
    awareAreaMessagePtr = util::writeType(awareAreaMessagePtr, std::numeric_limits<std::int32_t>::min() + 100);
    awareAreaMessagePtr = util::writeType(awareAreaMessagePtr, std::numeric_limits<std::int32_t>::min() + 100);
    awareAreaMessagePtr = util::writeType(awareAreaMessagePtr, std::numeric_limits<std::int32_t>::max() - 100);
    awareAreaMessagePtr = util::writeType(awareAreaMessagePtr, std::numeric_limits<std::int32_t>::max() - 100);
    awareAreaMessagePtr = util::writeType(awareAreaMessagePtr, std::numeric_limits<std::int32_t>::max() - 100);
    mServerConnection.sendMessage(convertMessage(awareAreaMessage));

    mLoopRunning = true;
    mThread = std::thread(&Engine::Core::coreLoop, this);
}

void Engine::Core::touchNetworkingLoop() {
    // this could be a race condition during initialization
    if (!mLoopRunning)
        return;

    // this is also a race condition but as long as it does not happen too often it's fine
    const auto alreadyScheduled = mNetworkingLoopTouchScheduled.exchange(true);
    if (!alreadyScheduled)
        boost::asio::post(mIoContext, [this] () { mNetworkingLoopTouchScheduled.store(false); });
}

void Engine::Core::coreLoop() {
    trace::init("trace-client-engine.json");
    const auto startTime = Clock::now();

    while (mLoopRunning) {
        const auto loopEvent = trace::event_complete("loop-iteration");

        // TODO: proper timing
        const auto timeNow = Clock::now();

        {
            trace::event_instant("enter-networking");
            bool oneNetworkLoopExecuted = false;
            while (mLoopRunning) {
                bool rayPending = mEngineRendererComm.informativeRayNew();
                bool meshesPending = mChunksMeshes.meshUpdatesPending();
                bool workPending = rayPending || meshesPending;
                if (workPending) {
                    if (oneNetworkLoopExecuted)
                        break;
                    const auto ev = trace::event_complete("socket-io-poll-one", "network");
                    const auto handlersProcessed = mIoContext.poll_one();
                } else {
                    const auto ev = trace::event_complete("socket-io-run-one", "network");
                    const auto handlersProcessed = mIoContext.run_one();
                }
                oneNetworkLoopExecuted = true;
            }
            trace::event_instant("leave-networking");
        }
     
        const auto centerBlock = mEngineRendererComm.getCenterBlock();

        // CHUNKS
        {
            const auto ev = trace::event_complete("compile-load-unload-chunk-list", "loading");
            mChunksMeshes.moveCenterBlock(centerBlock);

            const auto chunkRequestTickLatency = mChunksMeshes.chunkRequestTickLatency(mThisTick);
            float requestQueueSizeDivider = 1.0f;
            // this is a crappy control system
            // TODO: name the magic numbers
            if (chunkRequestTickLatency != -1 && chunkRequestTickLatency > 9) {
                requestQueueSizeDivider = std::pow(2.0f, float(chunkRequestTickLatency - 8) / 2.0f);
            }
            const auto chunkRequestQueueSizeLimit = std::clamp<std::int64_t>(
                float(cfg::MAX_PENDING_CHUNK_REQUESTS) / requestQueueSizeDivider,
                cfg::MIN_PENDING_CHUNK_REQUESTS,
                cfg::MAX_PENDING_CHUNK_REQUESTS);
            trace::event_counter("chunk-requesting-limit", "chunk-request-queue-size-limit", chunkRequestQueueSizeLimit);
            trace::event_counter("chunk-requesting-latency", "chunk-request-latency", chunkRequestTickLatency);
            const auto newChunkRequests = mChunksMeshes.iterate(chunkRequestQueueSizeLimit, mThisTick);
            if (newChunkRequests.size() > 0) {
                cfg::Message::Header header;
                header.id = cfg::Message::Id::CHUNK_REQUEST_DATA;
                header.size = newChunkRequests.size() * sizeof(newChunkRequests[0]);
                std::vector<std::byte> body;
                body.resize(header.size);
                std::copy(
                    reinterpret_cast<const std::byte *>(newChunkRequests.data()),
                    reinterpret_cast<const std::byte *>(newChunkRequests.data() + newChunkRequests.size()),
                    body.data());
                moveMessageToQueue(header, std::move(body));
            }
        }

        // MESHES
        {
            const auto ev = trace::event_complete("generate-meshes", "meshing");
            const auto generatedMeshes = mChunksMeshes.genMeshes(centerBlock, mVoxelTileInfo);
        }

        // MESSAGES
        const auto startingMessageCount = mServerConnection.receivedMessageCount();
        // leave messages that come after the start of the loop for later
        for (std::size_t i = 0; i < startingMessageCount; ++i) {
            const auto message = mServerConnection.getReceivedMessage();
            messageHandler(message);
        }

        // RAY
        if (mEngineRendererComm.informativeRayNew()) {
            const auto ray = mEngineRendererComm.getInformativeRay();
            const auto rayResolution = mChunksMeshes.castRay(ray);
            mEngineRendererComm.setInformativeRayResolution(rayResolution);
        }

        std::size_t pendingRayCount;
        {
            std::unique_lock lock{mPendingPlaceRaysMutex};
            pendingRayCount = mPendingPlaceRays.size();
        }

        // arbitrary constant (10)
        std::size_t numberOfRaysToBeProcessed = 10;
        if (pendingRayCount > numberOfRaysToBeProcessed) {
            log::warning("Warning, not keeping up with block placement ray casts.");
        } else {
            numberOfRaysToBeProcessed = pendingRayCount;
        }

        {
            const auto rayCastEvent = trace::event_complete("ray-casts");
            for (std::size_t i = 0; i < numberOfRaysToBeProcessed; ++i) {
                EngineRendererComm::RayPlace rayPlace;
                {
                    std::unique_lock lock{mPendingPlaceRaysMutex};
                    assert(mPendingPlaceRays.size() > 0);
                    rayPlace = mPendingPlaceRays.front();
                    mPendingPlaceRays.pop();
                }
                glm::ivec3 blockPosition = {0, 0, 0};
                cfg::Block blockValue{0};
                const auto rayResolution = mChunksMeshes.castRay(rayPlace.ray);
                if (rayResolution.hit == false)
                    continue;
                if (rayPlace.position == 0) {
                    blockPosition = rayResolution.last;
                    blockValue = rayPlace.value;
                } else if (rayPlace.position == -1) {
                    blockPosition = rayResolution.preLast;
                    blockValue = rayPlace.value;
                } else {
                    assert(false && "rayPlace.position other than 0 and -1 not supported.");
                }

                cfg::Message::Header header;
                header.id = cfg::Message::Id::BLOCK_DATA;
                header.size = sizeof(cfg::Message::Body::BlockData);

                std::vector<std::byte> body;
                body.resize(sizeof(cfg::Message::Body::BlockData));
                cfg::Message::Body::BlockData * message = reinterpret_cast<cfg::Message::Body::BlockData *>(body.data());
                message->position = blockPosition;
                message->value = blockValue;

                moveMessageToQueue(header, std::move(body));
            }
        }

        const auto sendServerStop = mEngineRendererComm.getAndClearSendStopServer();
        if (sendServerStop) {
            std::pair<cfg::Message::Header, std::vector<std::byte>> stopMessage;
            stopMessage.first.id = cfg::Message::Id::STOP_SERVER;
            stopMessage.first.size = 4;
            stopMessage.second.resize(stopMessage.first.size);
            stopMessage.second[0] = std::byte{'s'};
            stopMessage.second[1] = std::byte{'t'};
            stopMessage.second[2] = std::byte{'o'};
            stopMessage.second[3] = std::byte{'p'};
            mServerConnection.sendMessage(convertMessage(stopMessage));
        }
    }
    trace::finish();
}

Engine::Core::~Core() {
    log::info("~Core()");
    mLoopRunning = false;
    mThread.join();
}

void Engine::Core::moveMessageToQueue(const cfg::Message::Header & header, std::vector<std::byte> && body) {
    // TODO: make sure that this does not perform copies
    mServerConnection.sendMessage(convertMessage(std::make_pair(header, std::move(body))));
}

bool Engine::Core::messageHandler(gsl::span<const std::byte> message) {
    auto roundDown = [](std::size_t x, std::size_t y) {
        static_assert(std::is_unsigned<decltype(x)>::value);
        static_assert(std::is_integral<decltype(x)>::value);
        static_assert(std::is_same<decltype(x), decltype(y)>::value);
        return x / y * y;
    };

    const auto message_id = static_cast<cfg::Message::Id>(message[0]);
    const auto message_content = message.subspan(1);

    if (message_id < cfg::Message::Id::ID_COUNT) {
        trace::event_instant(cfg::Message::MESSAGE_NAMES[static_cast<std::size_t>(message_id)]);
    } else {
        trace::event_instant("received-invalid-message-ID");
    }

    switch (message_id) {
    case cfg::Message::Id::CHUNK_DATA: {
            if (message_content.size() < sizeof(cfg::Message::Body::ChunkDataPosition)) {
                log::warning("(1) Invalid message body length from server.");
                return false;
            }
            cfg::Message::Body::ChunkDataPosition chunkDataPosition;
            std::memcpy(&chunkDataPosition, message_content.data(), sizeof(chunkDataPosition));
            bool scheduleLoader = false;

            std::array<std::byte, cfg::CHUNK_SIZE_BYTES> chunkBuffer;
            const auto chunkBufferSize = compression::chunk::decompress(
                message_content.data() + sizeof(cfg::Message::Body::ChunkDataPosition),
                message_content.size() - sizeof(cfg::Message::Body::ChunkDataPosition),
            chunkBuffer.data());
            if (chunkBufferSize != chunkBuffer.size()) {
                log::warning("Invalid chunk received from server or decompression error.");
                return false;
            }
        const auto success =
            mChunksMeshes.handleChunkData(chunkDataPosition, {chunkBuffer.data(), chunkBuffer.size()}, scheduleLoader);
            if (!success) {
                log::warning("(2) Invalid message body length from server.");
                return false;
            }
    } break;
    case cfg::Message::Id::ENTITY_DATA: {
            // TODO: implement correctly
        const std::size_t dataSize = roundDown(message_content.size(), sizeof(cfg::Message::Body::EntityData));

            if (dataSize != message_content.size()) {
            // Log::print(Log::Type::WARNING, "(3) Invalid message body length from server.");
                return false;
            }

            mEntities.handleEntityData(
            {reinterpret_cast<const cfg::Message::Body::EntityData *>(message_content.data()),
             message_content.size() / sizeof(cfg::Message::Body::EntityData)},
            mEngineRendererComm);
    } break;
    case cfg::Message::Id::CHUNK_REQUEST_DATA: {
            log::warning("Unsupported server message (CHUNK_REQUEST_DATA).");
            return false;
    } break;
    case cfg::Message::Id::BLOCK_DATA: {
        const std::size_t dataSize = roundDown(message_content.size(), sizeof(cfg::Message::Body::BlockData));

            if (dataSize != message_content.size()) {
                log::warning("(4) Invalid message body length from server.");
                return false;
            }

            bool scheduleLoader = false;
            mChunksMeshes.handleBlockData(
            {reinterpret_cast<const cfg::Message::Body::BlockData *>(message_content.data()),
             message_content.size() / sizeof(cfg::Message::Body::BlockData)},
            scheduleLoader);

            // TODO: schedule mesh update
    } break;
    case cfg::Message::Id::REASSIGN_ENTITY_ID: {
        if (message_content.size() == 1)
            break; // actually refactor protocol instead of this
        const std::size_t dataSize = roundDown(message_content.size(), sizeof(cfg::Message::Body::ReassignEntityId));

            if (dataSize != message_content.size()) {
                log::warning("(5) Invalid message body length from server.");
                return false;
            }

        mEntities.handleReassignEntityId(
            {reinterpret_cast<const cfg::Message::Body::ReassignEntityId *>(message_content.data()),
             message_content.size() / sizeof(cfg::Message::Body::ReassignEntityId)});
    } break;
    case cfg::Message::Id::DEAD_ENTITIES: {
        const std::size_t dataSize = roundDown(message_content.size(), sizeof(cfg::Message::Body::DeadEntities));

            if (dataSize != message_content.size()) {
                log::warning("(6) Invalid message body length from server.");
                return false;
            }

        mEntities.handleDeadEntities(
            {reinterpret_cast<const cfg::Message::Body::DeadEntities *>(message_content.data()),
             message_content.size() / sizeof(cfg::Message::Body::DeadEntities)});
    } break;
    case cfg::Message::Id::NEW_TICK: {
            static_assert(std::is_same<decltype(mThisTick), std::int32_t>::value);
        static_assert(std::is_same<decltype(mThisTick), cfg::tick_type>::value);
            if (message_content.size() != sizeof(mThisTick)) {
                log::warning("Bad message content of message with ID {}.", int(message_id));
                return false;
            }
        cfg::tick_type newTickValue;
        std::memcpy(&newTickValue, message_content.data(), sizeof(newTickValue));
        if (newTickValue <= mThisTick) {
            log::warning("New tick value is <= than the current ({} <= {}).", newTickValue, mThisTick);
            return false;
        }
        mThisTick = newTickValue;
        trace::event_instant("new-tick");
    } break;
        default:
            log::warning("Invalid or unknown message from server with ID {}.", int(message_id));
            return false;
    }

    return true;
}

void Engine::Entities::handleEntityData(
    Span<const cfg::Message::Body::EntityData> entityData, EngineRendererComm & engineRendererComm) {
    // TODO: refactor
    // TODO: what about empty update message? still need to extrapolate
    std::size_t entityVectorIndex = 0;
    decltype(mEntities) entitiesTmp;
    for (const auto i : entityData) {
        // entities should always be sent sorted by increasing id (to allow O(n) update)
        while (mEntities.size() != entityVectorIndex && mEntities[entityVectorIndex].id < i.id) {
            mEntities[entityVectorIndex].positionOld = mEntities[entityVectorIndex].position;
            entitiesTmp.push_back(mEntities[entityVectorIndex]);
            ++entityVectorIndex;
        }

        if (mEntities.size() == entityVectorIndex) {
            // => could also mean server error (maybe?)
            EngineRendererComm::Entity entity;
            entity.id = i.id;
            entity.position = i.position;
            entity.positionOld = i.position;
            mEntities.push_back(entity); // dummy
            entitiesTmp.push_back(entity);
            ++entityVectorIndex;
        } else if (mEntities[entityVectorIndex].id == i.id) {
            auto & entity = mEntities[entityVectorIndex];
            entity.positionOld = entity.position;
            entity.position = i.position;
            entitiesTmp.push_back(entity);
            ++entityVectorIndex;
        } else {
            log::error("Missing entities? 3");
            EngineRendererComm::Entity entity;
            entity.id = i.id;
            entity.position = i.position;
            entity.positionOld = i.position;
            // mEntities.push_back(entity);
            entitiesTmp.push_back(entity);
        }
    }

    // entities should always be sent sorted by increasing id (to allow O(n) update)
    while (mEntities.size() != entityVectorIndex) {
        mEntities[entityVectorIndex].positionOld = mEntities[entityVectorIndex].position;
        entitiesTmp.push_back(mEntities[entityVectorIndex]);
        ++entityVectorIndex;
    }

    mEntities = std::move(entitiesTmp);
    mSnapshotTime = std::chrono::steady_clock::now();

    engineRendererComm.copyStateIn(mEntities, mSnapshotTime);
}

void Engine::Entities::handleReassignEntityId(Span<const cfg::Message::Body::ReassignEntityId> reassignEntityId) {
    // TODO: refactor
    std::size_t entityVectorIndex = 0;
    for (const auto i : reassignEntityId) {
        // entities should always be sent sorted by increasing id (to allow O(n) update)
        while (mEntities.size() != entityVectorIndex && mEntities[entityVectorIndex].id < i.oldID)
            ++entityVectorIndex;
        if (mEntities.size() == entityVectorIndex) {
            log::error("Missing entities?");
        } else if (mEntities[entityVectorIndex].id == i.oldID)
            mEntities[entityVectorIndex].id = i.newID;
        else {
            // server error?
            log::error("Missing entities? 2");
        }
    }
}

void Engine::Entities::handleDeadEntities(Span<const cfg::Message::Body::DeadEntities> deadEntities) {
    // TODO: refactor
    // because we collectively hate std::vector<bool>
    std::vector<std::int8_t> deleteList;
    deleteList.resize(mEntities.size(), 0);
    std::size_t entityVectorIndex = 0;
    for (const auto id : deadEntities) {
        while (mEntities.size() != entityVectorIndex && mEntities[entityVectorIndex].id < id)
            ++entityVectorIndex;
        if (mEntities.size() == entityVectorIndex) {
            log::error("Missing entities? 4");
        } else if (mEntities[entityVectorIndex].id == id)
            deleteList[entityVectorIndex] = 1;
        else {
            log::error("Missing entities? 5");
        }
    }

    entityVectorIndex = 0;
    auto isDead = [&entityVectorIndex, &deleteList](const EngineRendererComm::Entity & e) {
        return deleteList[entityVectorIndex++] == 1;
    };

    mEntities.erase(std::remove_if(mEntities.begin(), mEntities.end(), isDead), mEntities.end());
}

Engine::ChunksMeshes::ChunksMeshes(EngineRendererComm & engineRendererComm) : mEngineRendererComm{engineRendererComm} {
    mChunkIteratorNew.moveCenter({0, 0, 0});
}

EngineRendererComm::RayResolution Engine::ChunksMeshes::castRay(const EngineRendererComm::Ray & inRay) {
    // TODO: refactor
    static constexpr float MAX_RAY_LENGTH{10.0f};
    glm::ivec3 beforeSelectedBlock, selectedBlock;
    cfg::Block hitBlockValue{ 0 };
    Ray<float, int> ray{inRay.origin, inRay.direction};
    decltype(ray)::State rayState = ray.next();
    beforeSelectedBlock = rayState.block_position;
    glm::ivec3 chunkPosition = cfg::blockPositionToChunkPosition(rayState.block_position);
    const cfg::Block * chunk = nullptr;
    if (mChunks.exists(chunkPosition)) {
        chunk = mChunks.get(chunkPosition).blocks.data();
    } else {
        log::warning("Missing chunk during ray cast!");
    }
    bool blockHit = false;
    while (rayState.distance <= MAX_RAY_LENGTH && chunk != nullptr) {
        const auto blockIndex = cfg::blockPositionToBlockIndex(rayState.block_position);
        const cfg::Block & block = chunk[blockIndex];
        if (block != cfg::Block{0}) {
            blockHit = true;
            hitBlockValue = block;
            break;
        }
        const auto thisBlock = rayState.block_position;
        rayState = ray.next();
        if (rayState.distance > MAX_RAY_LENGTH) {
            // unclean hack (reverts)
            rayState.block_position = thisBlock;
            break;
        }
        beforeSelectedBlock = thisBlock;
        const auto new_chunk_position = cfg::blockPositionToChunkPosition(rayState.block_position);
        if (!glm::all(glm::equal(new_chunk_position, chunkPosition))) {
            chunkPosition = new_chunk_position;
            if (mChunks.exists(chunkPosition)) {
                chunk = mChunks.get(chunkPosition).blocks.data();
            } else {
                log::warning("Missing chunk during ray cast!");
            }
        }
    }
    selectedBlock = rayState.block_position;

    EngineRendererComm::RayResolution result;
    result.last = selectedBlock;
    result.preLast = beforeSelectedBlock;
    result.hit = blockHit;
    result.hitValue = hitBlockValue;

    return result;
}

void Engine::ChunksMeshes::moveCenterBlock(const position_type & newCenterBlock) {
    mChunkIteratorNew.moveCenter(cfg::blockPositionToChunkPosition(newCenterBlock));
}

std::vector<glm::ivec3> Engine::ChunksMeshes::iterate(std::int64_t runningChunkRequestLimit, cfg::tick_type tickValue) {
    using op = Iterator::Cuboid3<>::operation;

    Iterator::Cuboid3<>::next_type nextTask;
    if (mCachedTask.has_value()) {
        nextTask = mCachedTask.value();
        mCachedTask.reset();
    } else {
        nextTask = mChunkIteratorNew.next();
    }

    if (nextTask.second == op::UNLOAD && mRunningChunkRequests.size() > 0) {
        // unloading only possible if all requested chunks received
        mCachedTask.emplace(nextTask);
        return {};
    }
    
    // unload
    while (nextTask.second == op::UNLOAD) {
        {
            glm::ivec3 i;
            decltype(Mesh::chunksLoaded) chunkI{1};
            for (i.z = -1; i.z < 1; ++i.z)
            for (i.y = -1; i.y < 1; ++i.y)
            for (i.x = -1; i.x < 1; ++i.x) {
                const auto meshPosition = nextTask.first + i;
                bool isCached{false};
                auto & meshInfo = mMeshes.emplace_get(meshPosition, isCached);
                assert(isCached);
                assert((meshInfo.chunksLoaded & chunkI) == chunkI && "Chunk not loaded.");
                meshInfo.chunksLoaded &= ~chunkI;
                // TODO: avoid 2x map read
                if (mOpenMeshGenerations.exists(meshPosition)) {
                    mOpenMeshGenerations.erase(meshPosition);
                }
                // TODO: avoid 2x map read
                if (mUrgentMeshUpdates.exists(meshPosition)) {
                    mUrgentMeshUpdates.erase(meshPosition);
                }
                if (meshInfo.chunksLoaded == 0x0u) {
                    if (!meshInfo.empty) {
                        EngineRendererComm::Mesh mesh;
                        mesh.position = meshPosition;
                        mesh.mesh = {};
                        mEngineRendererComm.moveMeshIn(std::move(mesh), false);
                    }
                    // TODO: reuse iterator for faster erase
                    mMeshes.erase(meshPosition);
                }

                chunkI <<= 1u;
            }
        }

        assert(mChunks.exists(nextTask.first));
        mChunks.erase(nextTask.first);
        nextTask = mChunkIteratorNew.next();
    }

    // load
    std::vector<glm::ivec3> newRequests;
    while (nextTask.second == op::LOAD && mRunningChunkRequests.size() < runningChunkRequestLimit) {
        assert(!mChunks.exists(nextTask.first));
#ifndef NDEBUG
        assert(mRunningChunkRequestsDEBUG.find(nextTask.first) == mRunningChunkRequestsDEBUG.end());
        assert(mRunningChunkRequestsDEBUG.emplace(nextTask.first).second == true);
#endif
        newRequests.push_back(nextTask.first);
        ChunkRequestInfo chunkRequestInfo{};
        chunkRequestInfo.position = nextTask.first;
        chunkRequestInfo.tick = tickValue;
        mRunningChunkRequests.push(chunkRequestInfo);

        nextTask = mChunkIteratorNew.next();
    }

    if (nextTask.second != op::DONE) {
        assert(!mCachedTask.has_value());
        mCachedTask.emplace(nextTask);
    }

    mEngineRendererComm.setCachedChunkCount(static_cast<std::int32_t>(mChunks.size()));
    return newRequests;
}

#if 0
void Engine::ChunksMeshes::removeOutOfRangeMeshes(const glm::ivec3 & centerBlock) {
    const auto centerMesh = cfg::blockPositionToChunkPosition(centerBlock - MESH_OFFSET);
    // TODO: if center chunk unchanged and all chunks and meshes loaded then return

    // TODO: pause & resume unloading (mMeshes iterators always stay valid)
    // unload out of range meshes
    for (auto it = mMeshes.begin(), end = mMeshes.end(); it != end;) {
        const auto meshPosition = it->first;

        std::size_t loadedChunkCount{ 0 };
        glm::ivec3 i;
        for (i.z = 0; i.z < 2; ++i.z)
        for (i.y = 0; i.y < 2; ++i.y)
        for (i.x = 0; i.x < 2; ++i.x) {
            const auto chunkPosition = meshPosition + i;
            if (mChunks.exists(chunkPosition))
                ++loadedChunkCount;
        }

        if (loadedChunkCount != 8) {
            if (!it->second.empty) {
                // unload
                EngineRendererComm::Mesh mesh;
                mesh.position = meshPosition;
                mesh.mesh = {};
                mEngineRendererComm.moveMeshIn(std::move(mesh), false);
            }
            it = mMeshes.erase(it);
        } else {
            ++it;
        }

#if 0
        const auto meshDistanceFromCenter = meshPosition - centerMesh;
        if (glm::any(glm::greaterThanEqual(glm::abs(meshDistanceFromCenter), mMeshUnloadDistance))) {
            if (!it->second.empty) {
                // unload
                EngineRendererComm::Mesh mesh;
                mesh.position = meshPosition;
                mesh.mesh = {};
                mEngineRendererComm.moveMeshIn(std::move(mesh), false);
            }
            it = mMeshes.erase(it);
        } else {
            ++it;
        }
#endif
    }
}
#endif

bool Engine::ChunksMeshes::genMeshes(const glm::ivec3 & centerBlock, const VoxelMetaData & voxelTileInfo) {
    bool generatedMeshes = false;
#if 1
    // TODO:
    /*
        * loader only finds next chunk(s) to request and requests them
        * when hunk is received, it is checked (in different callback from loader) wether a mesh can be generated
        * if yes, mesh generation(s) is/are queued
        * iterator should be adjusted to correspond to chunks and not meshes
    */

    // TODO: schedule individual mesh generations instead of doing multiple in this function

    // const auto loaderWasScheduled = mLoaderScheduled.exchange(false);
    // assert(loaderWasScheduled == true);

    const auto centerMesh = cfg::blockPositionToChunkPosition(centerBlock - MESH_OFFSET);
    // TODO: if center chunk unchanged and all chunks and meshes loaded then return

    // TODO: pause & resume urgent mesh updates (iterators stay valid)
    // do all urgent meshes
    for (auto it = mUrgentMeshUpdates.begin(), end = mUrgentMeshUpdates.end(); it != end;) {
        const auto meshPosition = it->first;
        // erase even if not mesh is generated
        it = mUrgentMeshUpdates.erase(it);

        std::array<const cfg::Block *, 8> chunks;
        std::size_t chunksIndex{0};
        
        glm::ivec3 i;
        for (i.z = 0; i.z < 2; ++i.z)
        for (i.y = 0; i.y < 2; ++i.y)
        for (i.x = 0; i.x < 2; ++i.x) {
            const auto chunkPosition = meshPosition + i;
            // TODO: assert instead of hope for
            if (mChunks.exists(chunkPosition)) {
                chunks[chunksIndex] = mChunks.get(chunkPosition).blocks.data();
                ++chunksIndex;
            }
        }

        if (chunksIndex != chunks.size())
            continue;

        const auto vertices = mChunkMesher.createMesh(chunks, voxelTileInfo);
        generatedMeshes = true;

        bool isCached{false};
        auto & m = mMeshes.emplace_get(meshPosition, isCached);
        bool newIsEmpty = vertices.size() == 0;
        if (!newIsEmpty || (isCached && !m.empty)) {
            EngineRendererComm::Mesh mesh;
            mesh.position = meshPosition;
            mesh.mesh = std::move(vertices);
            mEngineRendererComm.moveMeshIn(std::move(mesh), true);
        }
        m.empty = newIsEmpty;
    }
#endif

    while (mOpenMeshGenerations.size() > 0) {
        const auto & meshPositionIt = mOpenMeshGenerations.begin();
        const auto & meshPosition = meshPositionIt->first;

        std::array<const cfg::Block *, 8> chunks;
        std::size_t chunksIt{0};
        glm::ivec3 i;
        for (i.z = 0; i.z < 2; ++i.z)
        for (i.y = 0; i.y < 2; ++i.y)
        for (i.x = 0; i.x < 2; ++i.x) {
            const auto chunkPosition = meshPosition + i;
            assert(mChunks.exists(chunkPosition));
            // TODO: redesign, this assert can fail (and will)
                    //       because between mOpenMeshGenerations.push() and now, necessary chunks can be unloaded if
                    //       center was moved too far away
                    // assert(mChunks.exists(chunkPosition) && "This could be allowed in future but needs to be
                    // handled.");
                    // if (!mChunks.exists(chunkPosition)) {
                    // continue;
            //}
            chunks[chunksIt++] = mChunks.get(chunkPosition).blocks.data();
        }
        assert(chunksIt == 8);
        auto vertices = mChunkMesher.createMesh(chunks, voxelTileInfo);
        generatedMeshes = true;

        assert(mMeshes.exists(meshPosition));
        auto & m = mMeshes.get(meshPosition);
        bool newIsEmpty = vertices.size() == 0;

        if (!newIsEmpty || !m.empty) {
            EngineRendererComm::Mesh mesh;
            mesh.position = meshPosition;
            // TODO: fix warning: don't use move on constant variables
            mesh.mesh = std::move(vertices);
            mEngineRendererComm.moveMeshIn(std::move(mesh), false);
        }
        m.empty = newIsEmpty;

        mOpenMeshGenerations.erase(meshPositionIt);
    }

    mEngineRendererComm.setCachedObservedMeshCount(static_cast<std::int32_t>(mMeshes.size()));

    return generatedMeshes;
}

#if 0
void Engine::ChunksMeshes::removeOutOfRangeChunks(const glm::ivec3 & centerBlock) {
    const auto centerChunk = cfg::blockPositionToChunkPosition(centerBlock);

    // TODO: pause & resume unloading (mChunks iterators always stay valid)
    // TODO: smart caching
    //       cache:
    //           near chunks,
    //           chunks whose meshes have not been generated yet,
    //           LRU chunks
    // unload out of range chunks
    for (auto it = mChunks.begin(), end = mChunks.end(); it != end;) {
        const auto chunkPosition = it->first;
        // assuming meshX is in chunkX and chunkX+1
        const auto chunkDistanceFromCenter = chunkPosition - centerChunk;
        const auto lowOut = glm::any(glm::lessThanEqual(chunkDistanceFromCenter, -mChunkUnloadDistance));
        const auto highOut = glm::any(glm::greaterThanEqual(chunkDistanceFromCenter, mChunkUnloadDistance + 1));
        if (lowOut || highOut) {
            it = mChunks.erase(it);
#if 0
            {
                glm::ivec3 i;
                decltype(Mesh::chunksLoaded) chunkI{ 1 };
                for (i.z = -1; i.z < 1; ++i.z)
                for (i.y = -1; i.y < 1; ++i.y)
                for (i.x = -1; i.x < 1; ++i.x) {
                    const auto meshPosition = chunkPosition + i;
                    //assert(mMeshes.exists(meshPosition));
                    if (mMeshes.exists(meshPosition)) {
                        auto & meshInfo = mMeshes.get(meshPosition);
                        assert((meshInfo.chunksLoaded & chunkI) != 0u && "Chunk already loaded. Allow in future and handle case.");
                        meshInfo.chunksLoaded &= ~chunkI;
                        chunkI <<= 1u;
                    }
                }
            }
#endif
        } else {
            ++it;
        }
    }
}
#endif

#if 0
std::vector<glm::ivec3> Engine::ChunksMeshes::findChunksToLoad(const glm::ivec3 & centerBlock) {
    const auto centerChunk = cfg::blockPositionToChunkPosition(centerBlock);

    std::vector<glm::ivec3> newRequests;

    for (
        std::size_t i = 0;
        i < mChunkIterator.size() && mRunningChunkRequests.size() < MAX_RUNNING_CHUNK_REQUESTS;
        ++i
    ) {
        const auto currentChunkPosition = mChunkIterator[i] + centerChunk;
        if (
            mChunks.exists(currentChunkPosition)
            ||
            mRunningChunkRequests.find(currentChunkPosition) != mRunningChunkRequests.end()
        ) {
            continue;
        }

        newRequests.push_back(currentChunkPosition);
        const auto successfulInsert = mRunningChunkRequests.insert(currentChunkPosition).second;
        assert(successfulInsert == true);
    }

    mEngineRendererComm.setCachedChunkCount(static_cast<std::int32_t>(mChunks.size()));

    return newRequests;
}
#endif

bool Engine::ChunksMeshes::handleChunkData(
    const cfg::Message::Body::ChunkDataPosition & chunkDataPosition,
    Span<const std::byte> chunkData,
    bool & scheduleLoader
) {
    // TODO: fix the fact that a malicious server can make the client go out of memory
    scheduleLoader = false;
    // only uncompressed supported for now
    if (chunkData.size != cfg::CHUNK_SIZE_BYTES) {
        return false;
    }
    bool isCached{false};
    Chunk & chunk = mChunks.emplace_get(chunkDataPosition, isCached);
    std::copy(chunkData.begin(), chunkData.end(), reinterpret_cast<std::byte *>(chunk.blocks.data()));
    const auto chunkExpected = mRunningChunkRequests.front().position == chunkDataPosition;
    if (!chunkExpected) {
        log::warning("Received unwanted chunk.");
    } else {
#ifndef NDEBUG
        assert(mRunningChunkRequestsDEBUG.erase(chunkDataPosition) == 1);
#endif
        mRunningChunkRequests.pop();
        if (mRunningChunkRequests.size() <= RERUN_LOADER_LOWER_THRESHOLD) {
            // TODO: a way to schedule mesh generation before a bunch of new chunks are received
            if (!mLoaderScheduled.exchange(true))
                scheduleLoader = true;
        }
    }

#if 0
    {
        glm::ivec3 i;
        for (i.z = -1; i.z < 1; ++i.z)
        for (i.y = -1; i.y < 1; ++i.y)
        for (i.x = -1; i.x < 1; ++i.x) {
            const auto meshPosition = chunkDataPosition + i;

            glm::ivec3 j;
            std::size_t loadedChunks{ 0 };
            // this is just an approximation, the situation (what chunks are loaded) can change between now and actual mesh generation
            for (j.z = 0; j.z < 2; ++j.z)
            for (j.y = 0; j.y < 2; ++j.y)
            for (j.x = 0; j.x < 2; ++j.x) {
                const auto chunkPosition2 = meshPosition + j;
                if (mChunks.exists(chunkPosition2))
                    ++loadedChunks;
            }
            if (loadedChunks == 8)
                mOpenMeshGenerations.push(meshPosition);
        }
    }
#else
    {
        glm::ivec3 i;
        decltype(Mesh::chunksLoaded) chunkI{1};
        for (i.z = -1; i.z < 1; ++i.z)
        for (i.y = -1; i.y < 1; ++i.y)
        for (i.x = -1; i.x < 1; ++i.x) {
            const auto meshPosition = chunkDataPosition + i;
                    bool isCachedDummy{false};
            auto & meshInfo = mMeshes.emplace_get(meshPosition, isCachedDummy);
            assert((meshInfo.chunksLoaded & chunkI) == 0 && "Chunk already loaded.");
            meshInfo.chunksLoaded |= chunkI;
            if (meshInfo.chunksLoaded == 0b1111'1111u) {
                mOpenMeshGenerations.emplace_get(meshPosition, isCachedDummy, false);
                assert(!isCachedDummy);
            }
            chunkI <<= 1u;
        }
    }
#endif

    return true;
}

void Engine::ChunksMeshes::handleBlockData(Span<const cfg::Message::Body::BlockData> blockData, bool & scheduleLoader) {
    // update chunks
    for (const auto & block : blockData) {
        const auto chunkPosition = cfg::blockPositionToChunkPosition(block.position);
        if (mChunks.exists(chunkPosition)) {
            bool isCached{false};
            Chunk & chunk = mChunks.get(chunkPosition);
            chunk.blocks[cfg::blockPositionToBlockIndex(block.position)] = block.value;
        } else {
            log::warning("Chunk for block update not found");
        }
    }

    for (const auto & block : blockData) {
        glm::ivec3 i;
        for (i.z = -1; i.z <= 1; ++i.z)
        for (i.y = -1; i.y <= 1; ++i.y)
        for (i.x = -1; i.x <= 1; ++i.x) {
            // get all potential neighbors
            // for blocks, only up to 6 sides + center is needed, for ambient occlusion up to 3x3 are needed
                    const auto meshPosition = cfg::blockPositionToChunkPosition(block.position + i - MESH_OFFSET);

                    bool isCachedDummy{false};
            mUrgentMeshUpdates.emplace_get(meshPosition, isCachedDummy, false);
        }
    }

    scheduleLoader = !mLoaderScheduled.exchange(true);
}
