#pragma once

#include <vector>
#include <queue>
#include <thread>
#include <unordered_set>
#include <chrono>
#include <optional>
#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include "cfg.hpp"
#include "Span.hpp"
#include "ListMap.hpp"
#include "EngineRendererComm.hpp"
#include "Iterator.hpp"
#include "ChunkMesher.hpp"

#include <glm/vec3.hpp>

// TODO: benchmark different hash functions
//       hash function results must be very different
//       from each other for coordinates nearby (inside of an AABB)
#include "ivec3hash.hpp"

#include "VoxelInfo.hpp"
#include "WebSocketConnection.hpp"

namespace Engine {
    class Entities {
    public:
        void handleEntityData(Span<const cfg::Message::Body::EntityData> entityData, EngineRendererComm & engineRendererComm);
        void handleReassignEntityId(Span<const cfg::Message::Body::ReassignEntityId> reassignEntityId);
        void handleDeadEntities(Span<const cfg::Message::Body::DeadEntities> deadEntities);

    private:

        std::vector<EngineRendererComm::Entity> mEntities;
        std::chrono::steady_clock::time_point mSnapshotTime;

    };

    class ChunksMeshes {
    public:
        using position_type = glm::tvec3<std::int32_t>;

        ChunksMeshes(EngineRendererComm & engineRendererComm);
        bool handleChunkData(const cfg::Message::Body::ChunkDataPosition & chunkDataPosition, Span<const std::byte> chunkData, bool & scheduleLoader);
        void handleBlockData(Span<const cfg::Message::Body::BlockData> blockData, bool & scheduleLoader);
        // TODO: template, so that lambda can be possibly optimized out
        void loader(std::function<void(const cfg::Message::Header &, std::vector<std::byte> &&)> moveMessageToQueue);
        EngineRendererComm::RayResolution castRay(const EngineRendererComm::Ray & ray);

        [[nodiscard]] std::vector<glm::ivec3> findChunksToLoad(const glm::ivec3 & centerBlock);
        void removeOutOfRangeChunks(const glm::ivec3 & centerBlock);
        [[nodiscard]] bool genMeshes(const glm::ivec3 & centerBlock, const VoxelMetaData & voxelTileInfo);
        void removeOutOfRangeMeshes(const glm::ivec3 & centerBlock);

        void moveCenterBlock(const position_type & newCenterBlock);
        std::vector<glm::ivec3> iterate(std::int64_t runningChunkRequestLimit, cfg::tick_type tickValue);

        std::size_t runningChunkRequestQueueSize() const { return mRunningChunkRequests.size(); }
        // -1 means "can not determine latency due to no data flow"
        std::int64_t chunkRequestTickLatency(cfg::tick_type currentTick) const {
            if (mRunningChunkRequests.size() == 0)
                return -1;
            assert(currentTick >= mRunningChunkRequests.front().tick);
            return std::int64_t(currentTick) - std::int64_t(mRunningChunkRequests.front().tick);
        }

        bool meshUpdatesPending() const {
            return mUrgentMeshUpdates.size() > 0 || mOpenMeshGenerations.size() > 0;
        }

    private:
        // chunk
        struct Chunk {
            std::array<cfg::Block, cfg::CHUNK_VOLUME> blocks;
        };
        ListMap<glm::ivec3, Chunk> mChunks; // TODO: try replace with octree
        struct ChunkRequestInfo {
            glm::ivec3 position;
            cfg::tick_type tick;
        };
        std::queue<ChunkRequestInfo> mRunningChunkRequests;
#ifndef NDEBUG
        std::unordered_set<glm::ivec3> mRunningChunkRequestsDEBUG;
#endif
        static constexpr std::size_t MAX_RUNNING_CHUNK_REQUESTS{ cfg::MAX_PENDING_CHUNK_REQUESTS };
        static constexpr std::size_t RERUN_LOADER_LOWER_THRESHOLD{ MAX_RUNNING_CHUNK_REQUESTS / 8 };
        std::atomic_bool mLoaderScheduled = true; // assuming it is scheduled in the beginning

        Iterator::Cuboid3<> mChunkIteratorNew{
            {0, 0, 0},
            {
                cfg::LOAD_AND_RENDER_RADIUS_X * 2 + 1,
                cfg::LOAD_AND_RENDER_RADIUS_Y * 2 + 1,
                cfg::LOAD_AND_RENDER_RADIUS_Z * 2 + 1
            }
        };
        std::optional<Iterator::Cuboid3<>::next_type> mCachedTask;

        std::size_t mCurrentIteratorIndex{ 0 };
        glm::ivec3 mCenterMesh{ 0, 0, 0 };
        struct Mesh {
            bool empty{ true };
#if 1
            std::uint8_t chunksLoaded{ 0 };
            static_assert(std::numeric_limits<decltype(chunksLoaded)>::max() >= 255u);
#endif
        };
        ListMap<glm::ivec3, Mesh> mMeshes;

         // dummy bool because too lazy to implement 'ListSet'
        ListMap<glm::ivec3, bool> mOpenMeshGenerations;

        // dummy bool because too lazy to implement 'ListSet'
        ListMap<glm::ivec3, bool> mUrgentMeshUpdates;

        // communication
        EngineRendererComm & mEngineRendererComm;

        // other
        ChunkMesher mChunkMesher;

        static constexpr glm::ivec3 MESH_OFFSET{ cfg::CHUNK_SIZE.x / 2, cfg::CHUNK_SIZE.y / 2, cfg::CHUNK_SIZE.z / 2 };
    };

    class Core {
    public:
        using Clock = std::chrono::steady_clock;

        Core(
            const boost::asio::ip::tcp::endpoint & endpoint,
            EngineRendererComm & engineRendererComm,
            std::string_view userName,
            const VoxelMetaData & voxelTileInfo,
            std::string_view host,
            std::string_view target,
            const boost::asio::ip::tcp::endpoint & monitorAcceptorEndpoint
        );
        ~Core();

        // posts a NOOP handler into the networking executor if not already present
        void touchNetworkingLoop();

    private:
        void coreLoop();

        boost::asio::io_context mIoContext;

        // TODO: thread for lightweight tasks (ray cast, collision...) and threads for heavy lifting (mesh gen, chunk decompression...)
        std::thread mThread;
        std::atomic_bool mLoopRunning;
        Entities mEntities;
        ChunksMeshes mChunksMeshes;
        EngineRendererComm & mEngineRendererComm;
        std::atomic_flag mInformativeRayCastScheduled = ATOMIC_FLAG_INIT;
        const VoxelMetaData & mVoxelTileInfo;
        WebSocketConnection mServerConnection;

        std::queue<EngineRendererComm::RayPlace> mPendingPlaceRays;
        std::mutex mPendingPlaceRaysMutex;
        cfg::tick_type mThisTick = 0;

        std::atomic_bool mNetworkingLoopTouchScheduled{false};

        // return false if there was an issue with message parsing
        bool messageHandler(gsl::span<const std::byte> message);
        void moveMessageToQueue(const cfg::Message::Header & header, std::vector<std::byte> && body);

    };

}
