#include "Ground.hpp"

#include <iostream>
#include <vector>
#include <utility>
#include <random>
#include <numbers>

#include <nlohmann/json.hpp>
#include <earcut/earcut.hpp>

#include <stb/stb_image.h>

#include <glm/gtc/type_ptr.hpp>

#include <util.hpp>
#include <algebra.hpp>


using namespace algebra;

Ground::Ground(
    const std::filesystem::path & assetPath,
    const std::filesystem::path & inFilePath
) {
    const auto groundJson = nlohmann::json::parse(
        util::loadFile<char>(inFilePath)
    );

    auto degToRad = [] (Vec2 deg) -> Vec2 {
        return deg * (std::numbers::pi / 180.0);
    };

    auto loadPoly = [degToRad] (const nlohmann::json & polygon, Mat34 view)
        -> std::pair<std::vector<Vec3>, std::vector<uint32_t>>
    {
        std::vector<Vec2> polygonWGS84;
        polygonWGS84.reserve(polygon.size());
        for (auto& p : polygon)
            polygonWGS84.emplace_back(degToRad({p[0], p[1]}));

        std::vector<Vec3> polygonECEF;
        polygonECEF.reserve(polygonWGS84.size());
        for (auto& p : polygonWGS84)
            polygonECEF.emplace_back(WGS84toECEF::position(p));

        std::vector<Vec3> polygonLocal;
        polygonLocal.reserve(polygonECEF.size());
        for (auto& p : polygonECEF)
            polygonLocal.emplace_back(mul(view, p));

        std::vector<std::vector<std::array<double, 2>>> rings;
        rings.emplace_back();
        for (auto& p : polygonLocal)
            rings[0].push_back({ p.x, p.y });

        auto indices = mapbox::earcut<uint32_t>(rings);

        return std::make_pair(polygonLocal, indices);
    };

    auto & p0 = groundJson["features"][0]["geometry"]["coordinates"][0][0];
    Vec2 centerLatLon{p0[0], p0[1]};

    Mat34 view = WGS84toECEF::view(degToRad(centerLatLon));

    std::vector<float> vertexData;
    std::vector<uint32_t> indices;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);

    int texId = 0;

    for (auto & p : groundJson["features"]) {
        if (p["properties"]["info"] == "bbox")
            continue;

        auto res = loadPoly(p["geometry"]["coordinates"][0], view);

        indices.reserve(indices.size() + res.second.size());
        auto vertexBase = vertexData.size() / 8;
        for (auto & i : res.second)
            indices.emplace_back(i + vertexBase);

        vertexData.reserve(vertexData.size() + 8 * res.first.size());

        Vec3 color{dis(gen), dis(gen), dis(gen)};

        for (auto & p : res.first) {
            // change coordinate system
            // TODO: maybe modify the wgs84 to ecef function to work in thi coordinate system
            vertexData.emplace_back(p.y);
            vertexData.emplace_back(p.z);
            vertexData.emplace_back(p.x);
            vertexData.emplace_back(1.0f);

            vertexData.emplace_back(color.x);
            vertexData.emplace_back(color.y);
            vertexData.emplace_back(color.z);
            vertexData.emplace_back(float(texId));
        }
        texId = ++texId % 3;
    }

    mElementCount = indices.size();

    mShader.reload(assetPath);

    glProgramUniform1i(mShader.name(), mShader.uTexture, TEX_UNIT);

    glCreateBuffers(1, &mVBO);
    glNamedBufferData(mVBO, vertexData.size() * sizeof(vertexData[0]), vertexData.data(), GL_STATIC_DRAW);

    glCreateBuffers(1, &mEBO);
    glNamedBufferData(mEBO, indices.size() * sizeof(indices[0]), indices.data(), GL_STATIC_DRAW);

    glCreateVertexArrays(1, &mVAO);
    glVertexArrayVertexBuffer(mVAO, 0, mVBO, 0, sizeof(float) * 8);
    glVertexArrayElementBuffer(mVAO, mEBO);

    glEnableVertexArrayAttrib(mVAO, mShader.aPosition);
    glEnableVertexArrayAttrib(mVAO, mShader.aColor);
    glVertexArrayAttribFormat(mVAO, mShader.aPosition, 4, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribFormat(mVAO, mShader.aColor, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4);

    glVertexArrayAttribBinding(mVAO, mShader.aPosition, 0);
    glVertexArrayAttribBinding(mVAO, mShader.aColor, 0);

    mVertexCount = vertexData.size() / 3;

    loadTexture(assetPath);
}

void Ground::loadTexture(const std::filesystem::path & assetPath) {
    static constexpr std::array<const char *, 3> TEXTURES{{
        "grass.png",
        "pavement.png",
        "dirt.png"
    }};
    static constexpr int textureSize = 512;
    static constexpr int textureMipMaps = 10;

    glCreateSamplers(1, &mSampler);
    glSamplerParameteri(mSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(mSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(mSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(mSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameteri(mSampler, GL_TEXTURE_MAX_ANISOTROPY, 16.0f);

    glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &mTexture);
    glTextureStorage3D(mTexture, textureMipMaps, GL_RGBA8, textureSize, textureSize, TEXTURES.size());

    for (GLint index = 0; index < TEXTURES.size(); index++) {
        auto path = assetPath / TEXTURES[index];
        int w, h, c;
        auto image = stbi_load(path.string().c_str(), &w, &h, &c, 4);
        if (image == nullptr)
            throw std::runtime_error("Can not load tile texture.");
        if (w != textureSize || h != textureSize) {
            stbi_image_free(image);
            throw std::runtime_error("Invalid tile texture dimensions.");
        }
        glTextureSubImage3D(mTexture, 0, 0, 0, index, textureSize, textureSize, 1, GL_RGBA, GL_UNSIGNED_BYTE, image);
        stbi_image_free(image);
    }
    glGenerateTextureMipmap(mTexture);
}

Ground::~Ground() {
    glDeleteBuffers(1, &mVBO);
    glDeleteBuffers(1, &mEBO);
    glDeleteVertexArrays(1, &mVAO);
    glDeleteSamplers(1, &mSampler);
    glDeleteTextures(1, &mTexture);
}

void Ground::draw(const glm::mat4 & viewProjection) {
    glBindSampler(TEX_UNIT, mSampler);
    glBindTextureUnit(TEX_UNIT, mTexture);

    glUseProgram(mShader.name());
    glUniformMatrix4fv(mShader.uVP, 1, GL_FALSE, glm::value_ptr(viewProjection));
    glBindVertexArray(mVAO);
    glDrawElements(GL_TRIANGLES, mElementCount, GL_UNSIGNED_INT, nullptr);

    glBindSampler(TEX_UNIT, 0);
    glBindTextureUnit(TEX_UNIT, 0);
}
