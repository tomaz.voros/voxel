#include "CameraControl.hpp"
#include <cmath>
#include <glm/glm.hpp>

constexpr float CameraControl::MAX_PITCH;

CameraControl::CameraControl() :
    mPosition{ 0.0f, 0.0f, 0.0f },
    mYaw{ 1.57f },
    mPitch{ 0.0f },
    mFacing{ 0.0f, 0.0f, 1.0f },
    mFront{ 0.0f, 0.0f, 1.0f },
    mRight{ 0.0f, 0.0f, 1.0f },
    mUp{ 0.0f, 1.0f, 0.0f },
    mInverseMouseSensitivity{ 300.0f },
    mSpeed{ 20.0f }
{
    const float dummyDt = 0.0f;
    const std::array<bool, 6> dummyKeys{ { false, false, false, false, false, false } };
    const glm::vec2 dummyMouseDelta{ 0.0f, 0.0f };
    GLFWgamepadstate dummyGamepadState{};

    update(dummyDt, dummyKeys, dummyMouseDelta, dummyGamepadState);
}

void CameraControl::update(float dt, const std::array<bool, 6> & keys, glm::vec2 mouseDelta, const GLFWgamepadstate & gamepadState) {
    // TODO: limit max value to something less than 1.0 (also up down speed max should be the same as horizontal speed)
    glm::vec3 velocity{ 0.0f, 0.0f, 0.0f };
    if (keys[0] != keys[1]) {
        if (keys[0]) velocity += mFront;
        else velocity -= mFront;
    }
    if (keys[2] != keys[3]) {
        if (keys[2]) velocity += mRight;
        else velocity -= mRight;
    }
    if (keys[4] != keys[5]) {
        if (keys[4]) velocity += mUp;
        else velocity -= mUp;
    }

    auto adjustResponseCurve = [&] (float value) -> float {
        return std::copysign(std::pow(std::abs(value), INPUT_POWER), value);
    };

    auto thresholdedAxis = [&] (glm::vec2 vector) -> glm::vec2 {
        float length = glm::length(vector);
        if (length <= AXIS_THRESHOLD)
            vector = glm::vec2{0.0f, 0.0f};
        else
            vector *= 1.0f - AXIS_THRESHOLD / length;
        return vector;
    };

    auto thresholdedTrigger = [&] (float scalar) -> float {
        scalar = scalar * 0.5f + 0.5f;
        if (scalar <= TRIGGER_THRESHOLD)
            scalar = 0.0f;
        else
            scalar -= TRIGGER_THRESHOLD;
        return scalar;
    };

    { // gamepad
        // TODO: scale by dt
        auto direction = thresholdedAxis({ gamepadState.axes[0], gamepadState.axes[1] });
        direction.x = adjustResponseCurve(direction.x);
        direction.y = adjustResponseCurve(direction.y);
        velocity += mRight * direction.x - mFront * direction.y;
        auto up = thresholdedTrigger(gamepadState.axes[5]);
        auto down = thresholdedTrigger(gamepadState.axes[4]);
        up = adjustResponseCurve(up);
        down = adjustResponseCurve(down);
        velocity += mUp * (up - down);
    }
    
    mPosition += velocity * dt * mSpeed;

    mouseDelta.y = -mouseDelta.y;
    mouseDelta /= mInverseMouseSensitivity;
    mYaw += mouseDelta.x;
    mPitch += mouseDelta.y;

    { // gamepad
        // TODO: scale by dt
        auto direction = thresholdedAxis({ gamepadState.axes[2], gamepadState.axes[3] });
        direction.x = adjustResponseCurve(direction.x);
        direction.y = adjustResponseCurve(direction.y);
        direction /= RIGHT_THUMB_SENSITIVITY;
        mYaw += direction.x;
        mPitch -= direction.y;
    }

    mYaw = std::fmod(mYaw, 2.0f * 3.14159);
    mPitch = std::min(std::max(mPitch, -MAX_PITCH), MAX_PITCH);

    const float cp = std::cos(mPitch);
    const float sp = std::sin(mPitch);
    const float cy = std::cos(mYaw);
    const float sy = std::sin(mYaw);
    mFacing = glm::normalize(glm::vec3{ cp * cy, sp, cp * sy });
    mRight = glm::normalize(glm::cross(mFacing, mUp));
    mFront = glm::normalize(glm::vec3{ mFacing.x, 0.0f, mFacing.z });
}
