#pragma once

#include <string_view>
#include <vector>
#include <deque>
#include <optional>
#include <chrono>
#include <string>

#include <boost/asio.hpp>
#include <boost/beast.hpp>

class WebSocketConnection {
public:
    using tcp_socket_type = boost::beast::tcp_stream;
    using websocket_type = boost::beast::websocket::stream<tcp_socket_type, false>;
    using endpoint_type = tcp_socket_type::endpoint_type;
    using message_type = std::vector<std::byte>;

    WebSocketConnection(
        boost::asio::io_context & ioContext,
        const endpoint_type & endpoint,
        std::string_view host,
        std::string_view target,
        const std::chrono::seconds & timeout
    );

    WebSocketConnection(
        boost::asio::ip::tcp::socket && socket,
        boost::beast::role_type roleType,
        std::string_view host,
        std::string_view target
    );

    bool connected() const noexcept { return mConnected; }

    std::size_t receivedMessageCount() const noexcept {
        return mRxQueue.size() - static_cast<std::size_t>(mRxBuffer.has_value());
    }

    std::size_t sendingMessageCount() const noexcept {
        return mTxQueue.size();
    }

    std::vector<std::byte> getReceivedMessage();
    void sendMessage(std::vector<std::byte> && message);

private:
    using message_buffer_type = boost::asio::dynamic_vector_buffer<message_type::value_type, message_type::allocator_type>;
    websocket_type mWebSocket;
    std::string mHost;
    std::string mTarget;
    bool mConnected = false;
    std::deque<message_type> mTxQueue;
    std::deque<message_type> mRxQueue;
    std::optional<message_buffer_type> mRxBuffer;

    void onConnect(boost::beast::error_code error, boost::beast::role_type roleType);
    void onHandshake(boost::beast::error_code error);
    void onRead(const boost::system::error_code & error, std::size_t bytesTransferred);
    void onWrite(const boost::system::error_code & error, std::size_t bytesTransferred);

};
