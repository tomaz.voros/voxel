#include "VoxelTexture.hpp"

#include <type_traits>

#include <util.hpp>
#include "Log.hpp"

void VoxelTexture::updateSampler(
    GLint magnifyingFilter,
    GLint minifyingFilter,
    GLint minMipmap,
    GLint maxMipmap,
    GLfloat anisotropyLevel
) {
    glSamplerParameteri(mSampler, GL_TEXTURE_MAG_FILTER, magnifyingFilter);
    glSamplerParameteri(mSampler, GL_TEXTURE_MIN_FILTER, minifyingFilter);
    glSamplerParameteri(mSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(mSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glSamplerParameterf(mSampler, GL_TEXTURE_MIN_LOD, float(minMipmap));
    glSamplerParameterf(mSampler, GL_TEXTURE_MAX_LOD, float(maxMipmap));
    glSamplerParameteri(mSampler, GL_TEXTURE_MAX_ANISOTROPY, anisotropyLevel);
}

void VoxelTexture::bind(GLuint textureUnit) const {
    glBindSampler(textureUnit, mSampler);
    glBindTextureUnit(textureUnit, mTexture);
}
void VoxelTexture::unbind(GLuint textureUnit) const {
    glBindSampler(textureUnit, 0);
    glBindTextureUnit(textureUnit, 0);
}

VoxelTexture::~VoxelTexture() {
    glDeleteSamplers(1, &mSampler);
    glDeleteTextures(1, &mTexture);
}

VoxelTexture::VoxelTexture(const VoxelTextureAtlas & voxelTileAtlas) {
    using acc_type = std::uint32_t;
    using channel_type = std::uint8_t;
    static_assert(std::is_same<std::remove_cv<std::remove_reference<decltype(voxelTileAtlas.data[0])>::type>::type, channel_type>::value, "Unsupported atlas data type.");

    if (!util::ispow2(voxelTileAtlas.tileWidth))
        throw std::runtime_error("Tiles are not a power of two.");
    if (!util::ispow2(voxelTileAtlas.tilesPerRow))
        throw std::runtime_error("Tile row count is not a power of two.");
    if (voxelTileAtlas.tileWidth * voxelTileAtlas.tilesPerRow != voxelTileAtlas.width)
        throw std::runtime_error("Bad atlas data.");
    if (voxelTileAtlas.width * voxelTileAtlas.width * voxelTileAtlas.channelCount != voxelTileAtlas.data.size())
        throw std::runtime_error("Bad atlas data.");
    if (voxelTileAtlas.channelCount != 4)
        throw std::runtime_error("Unsupported atlas channel count.");

    mMaxMipLevel = util::log2p1(voxelTileAtlas.tileWidth);
    mMaxMipLevel -= 1;

    glCreateSamplers(1, &mSampler);
    updateSampler(GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, 0, mMaxMipLevel, 16.0f);

    glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &mTexture);

    glTextureParameteri(mTexture, GL_TEXTURE_BASE_LEVEL, 0);
    glTextureParameteri(mTexture, GL_TEXTURE_MAX_LEVEL, mMaxMipLevel);

    std::size_t width = voxelTileAtlas.width;
    glTextureStorage3D(mTexture, mMaxMipLevel + 1, GL_RGBA8, width, width, 1);

    const auto & atlas = voxelTileAtlas.data;

    glTextureSubImage3D(mTexture, 0, 0, 0, 0, width, width, 1, GL_RGBA, GL_UNSIGNED_BYTE, atlas.data());

    // 4096^2 * 255 < 2^32-1
    if (voxelTileAtlas.tileWidth > 4096)
        throw std::runtime_error("Tiles too large for mipmapping algorithm. Could overflow accumulator.");

    std::vector<acc_type> accumulatedMip;
    if (mMaxMipLevel > 0) {
        accumulatedMip.resize(atlas.size());
        std::transform(atlas.begin(), atlas.end(), accumulatedMip.begin(), [] (auto v) { return static_cast<acc_type>(v); });
    }

    for (std::size_t mipLayer = 1; mipLayer < mMaxMipLevel + 1; ++mipLayer) {
        const auto newWidth = width / 2;
        std::vector<acc_type> newAccumulatedMip;
        newAccumulatedMip.resize(newWidth * newWidth * voxelTileAtlas.channelCount);
        util::mipSum2D<acc_type, VoxelTextureAtlas::channelCount>(accumulatedMip.data(), newAccumulatedMip.data(), width, width);

        const acc_type dividerPower = mipLayer * 2;
        const acc_type lowMask = (1 << dividerPower) - 1;
        const acc_type roundingThreshold = (1 << dividerPower) / 2;
        std::vector<channel_type> atlasLayer;
        atlasLayer.resize(newAccumulatedMip.size());
        std::transform(
            newAccumulatedMip.begin(), newAccumulatedMip.end(), atlasLayer.begin(),
            [dividerPower, lowMask, roundingThreshold] (auto v) {
                const auto lowBits = v & lowMask;
                return (v >> dividerPower) + acc_type(lowBits >= roundingThreshold);
            }
        );

        glTextureSubImage3D(mTexture, mipLayer, 0, 0, 0, newWidth, newWidth, 1, GL_RGBA, GL_UNSIGNED_BYTE, atlasLayer.data());
        width = newWidth;
        accumulatedMip = std::move(newAccumulatedMip);
    }
}
