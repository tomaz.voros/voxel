#pragma once

#include <vector>
#include <cstdint>
#include <filesystem>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <array>

struct VoxelTextureAtlas {
    static constexpr std::size_t channelCount{4};
    std::vector<std::uint8_t> data;
    std::size_t width{};
    std::size_t tilesPerRow{};
    std::size_t tileWidth{};
};

struct VoxelLookupData {
    // TODO: leave missing sides at 0 (meaning does not exist)
    std::array<std::uint32_t, 6> sides{};
    // TODO: set base offset in position parameter, later just add the voxel scaled position
    std::array<std::uint32_t, 2> model{};
};

using VoxelQuad = std::array<std::uint32_t, 3>;
static_assert(sizeof(VoxelQuad) == 12);

struct VoxelMetaData {
    // TODO: bitfield of face transparencies instead of relying on lookup table having 0 entries
    static constexpr std::size_t maxNameLength{63};
    std::vector<VoxelLookupData> lookupTable;
    std::vector<std::array<char, maxNameLength + 1>> names;
    std::vector<VoxelQuad> quadData;
};

class VoxelInfo {
public:
    using integer_type = std::int64_t;

    static inline constexpr integer_type MIN_TILES_PER_ROW = 1;
    static inline constexpr integer_type MAX_TILES_PER_ROW = 256; // TODO: figure out why this was 254
    static inline constexpr integer_type TILE_GRANULARITY = 16; // TODO: merge with cfg::FULL_QUAD_SIZE
    static inline constexpr integer_type MIN_RESOLUTION = 1; // pixels per step
    static inline constexpr integer_type MAX_RESOLUTION = 32;
    static inline constexpr integer_type MIN_TILE_WIDTH = MIN_RESOLUTION * TILE_GRANULARITY;
    static inline constexpr integer_type MAX_TILE_WIDTH = MAX_RESOLUTION * TILE_GRANULARITY;

    VoxelInfo(const std::filesystem::path & assetPath) {
        // TODO: serialize the output (during compilation? / asset gen step?) to disk and load directly
        // TODO: handle JSON parsing exceptions
        processAssetMetaFile(assetPath);
    }

    VoxelTextureAtlas extractVoxelTextureAtlas() {
        const auto atlas = std::move(mAtlas);
        mAtlas = {};
        return atlas;
    }

    VoxelMetaData extractVoxelData() {
        const auto data = std::move(mData);
        mData = {};
        return data;
    }

private:
    VoxelTextureAtlas mAtlas;
    VoxelMetaData mData;

    static inline constexpr integer_type INVALID_INDEX_VALUE = 0;
    static inline constexpr integer_type MIN_VOXEL_INDEX = 1;
    static inline constexpr integer_type MAX_VOXEL_INDEX = 65'535;
    static inline constexpr integer_type MIN_VOXEL_NAME_SIZE = 1;

    using texture_offset_type = std::array<integer_type, 2>;
    using AtlasPackingData = std::unordered_map<std::string, texture_offset_type>;

    void processAssetMetaFile(const std::filesystem::path & assetPath);
    AtlasPackingData packTextureAtlas(
        const std::string & defaultTextureFileName,
        const std::unordered_set<std::string> & textureFileNames,
        const std::filesystem::path & assetPath,
        integer_type tileWidth,
        const std::filesystem::path & pathPrefix
    );

};
