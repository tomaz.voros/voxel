#pragma once

struct GLFWwindow;
#include <filesystem>
#include <optional>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "Shader.hpp"
#include "ProgramVoxel.hpp"
#include "EngineRendererComm.hpp"
#include "QuadEBO.hpp"
#include "VoxelTexture.hpp"
#include <unordered_map>
#include "SkyBox.hpp"
// TODO: benchmark different vector hash functions
#include "ivec3hash.hpp"
#include "VoxelInfo.hpp"
#include "ProgramVoxelPVF.hpp"
#include "ProgramVoxelMeshShader.hpp"
#include "Gui.hpp"
#include "VoxelAlloc.hpp"

namespace Renderer {
    class Entities {
    public:
        Entities(const std::filesystem::path & assetPath);
        void draw(const glm::mat4 & viewProjection, const glm::ivec3 & offset, EngineRendererComm & engineRendererComm);

    private:
        std::vector<EngineRendererComm::Entity> mEntities;
        std::chrono::steady_clock::time_point mSnapshotTime;

        Shader mEntityShader;
        GLint mPositionLocation;
        GLint mVPLocation;
        GLuint mVAO;
        GLuint mVBO;
    };

    struct MeshInfo {
        int meshesInMemory;
        int meshesUploaded;
        int meshesRendered;
        int meshesDeleted;
        std::size_t quadsRendered = 0;
    };

    class ChunksMeshes {
    public:
        ChunksMeshes(
            const VoxelTextureAtlas & voxelTileAtlas,
            const std::filesystem::path & assetPath
        );
        MeshInfo drawPVF(
            const glm::mat4 & view,
            const glm::mat4 & viewProjection,
            const glm::ivec3 & offset,
            const glm::vec3 & cameraPosition,
            const glm::vec3 & cameraPositionFraction,
            const std::array<float, 4> & fogColor,
            float fogStart,
            float fogTransitionDistance,
            float aoShadeStrength,
            Texturing & texturingParams
        );
        MeshInfo drawMeshShader(
            const glm::mat4 & view,
            const glm::mat4 & viewProjection,
            const glm::ivec3 & offset,
            const glm::vec3 & cameraPosition,
            const glm::vec3 & cameraPositionFraction,
            const std::array<float, 4> & fogColor,
            float fogStart,
            float fogTransitionDistance,
            float aoShadeStrength,
            Texturing & texturingParams
        );
        MeshInfo update(EngineRendererComm & engineRendererComm, const glm::vec3 & cameraPosition,  const glm::vec3 & cameraDirection);
        void reloadShader(const std::filesystem::path & assetPath) {
            mBlockShader.reload(assetPath);
            mBlockShaderPVF.reload(assetPath);
            mBlockShaderMeshShader.reload(assetPath);
        }

        std::size_t getQuadCount() const { return mQuadCount; }

        const VoxelTexture & texture() const { return mVoxelTexture; }

    private:
        std::size_t mQuadCount{0};

        static constexpr std::size_t MAX_MESH_UPDATES_PER_UPDATE_CALL{ 100 };

        QuadEBO mQuadEBO;
        ProgramVoxel mBlockShader;
        ProgramVoxelPVF mBlockShaderPVF;
        ProgramVoxelMeshShader mBlockShaderMeshShader;
        VoxelTexture mVoxelTexture;

        struct VoxelMeshHeader {
            uint32_t bufferOffset = 0;
            uint32_t count = 0;
            float offsetX = 0.0f;
            float offsetY = 0.0f;
            float offsetZ = 0.0f;
            static_assert(VoxelAlloc::BUFFER_SIZE - 1 <= std::numeric_limits<decltype(bufferOffset)>::max());
            static_assert(VoxelAlloc::BUFFER_SIZE - 1 <= std::numeric_limits<decltype(count)>::max());
        };

        struct VoxelMesh {
            // old stuff
            glm::ivec3 offset;
            GLuint VAO, VBO;
            GLsizei elementCount;
            // new stuff
            VoxelMeshHeader header;
            GLuint VBONew = 0; // TODO: rename
        };

        std::unordered_map<glm::ivec3, VoxelMesh> mMeshes;

        VoxelAlloc mVoxelAlloc;

    };

    class Core {
    public:
        Core(
            EngineRendererComm & engineRendererComm,
            GLFWwindow * window,
            const glm::ivec3 & initialCenterBlock,
            const VoxelTextureAtlas & voxelTileAtlas,
            const std::filesystem::path & assetPath
        );
        void run(const VoxelMetaData & voxelTileInfo, const std::filesystem::path & userPath);
        ~Core();

        static constexpr auto DRAW_ENTITIES = false;

    private:
        GLFWwindow * mWindow;
        Entities mEntities;
        ChunksMeshes mChunksMeshes;
        EngineRendererComm & mEngineRendererComm;
        glm::ivec3 mInitCenterBlock; // TODO: remove this somehow
        SkyBox mSkyBox;
        const std::filesystem::path mAssetPath;
        const VoxelTextureAtlas & mVoxelTileAtlas;
        static constexpr std::size_t TIMER_QUERY_COUNT = 16;
        std::array<GLuint, TIMER_QUERY_COUNT> mGpuTimeQueries;
        std::array<bool, TIMER_QUERY_COUNT> mGpuTimeQueriesPending;
        std::size_t mNextQueryIndex = 0;
        static_assert(TIMER_QUERY_COUNT > 0);
    };
}
