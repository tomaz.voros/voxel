#pragma once

#include "ProgramBase.hpp"

class ProgramVoxelPVF : public ProgramBase {
public:
    static constexpr GLuint bufVertexData = 0;

    static constexpr GLint uV = 0;
    static constexpr GLint uVP = 1;
    static constexpr GLint uOffset = 2;
    static constexpr GLint uAoOffset = 3;
    static constexpr GLint uAoScale = 4;
    // static constexpr GLint uWorldSpeceOffset = 5;

    static constexpr GLint uCameraViewSpacePosition = 6;
    static constexpr GLint uCameraWorldSpacePosition = 7;
    static constexpr GLint uVoxelTexture = 8;
    static constexpr GLint uFogColor = 9;
    static constexpr GLint uFogStart = 10;
    static constexpr GLint uFogTransitionDistance = 11;
    static constexpr GLint uAoShadeStrength = 12;

    void reload(const std::filesystem::path & assetPath) {
        ProgramBase::ShaderSourcePaths shaderSourcePaths;
        shaderSourcePaths.vert = assetPath / "shaders/voxelPVF.vert";
        shaderSourcePaths.frag = assetPath / "shaders/voxelPVF.frag";
        ProgramBase::reload(shaderSourcePaths, "ProgramVoxelPVF");
    }

};
