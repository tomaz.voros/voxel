#pragma once

#include <cstddef>
#include <array>
#include <type_traits>

namespace alloc {
    // TODO: figure out if this is defined behaviour (MemoryBlock* -> void* -> T* -> void*)
    /**
     * \tparam Size Size of a single element.
     * \tparam Count Number of elements.
     * \tparam Align Memory alignment of each element.
     */
    template <std::size_t Size, std::size_t Count, std::size_t Align>
    class Pool {
    public:
        static constexpr std::size_t element_size = Size;
        static constexpr std::size_t element_count = Count;
        static constexpr std::size_t element_alignment = Align;
        Pool() {
            mTop = Count;
            for (std::size_t i = 0; i < Count; ++i)
                mFree[i] = &mData[i];
        }

        void * allocate() {
            if (mTop == 0)
                return nullptr;
            return mFree[--mTop];
        }

        void deallocate(void * p) {
            if (p == nullptr) return;
            assert(mTop < Size);
            mFree[mTop++] = p;
        }

    private:
        using MemoryBlock = typename std::aligned_storage<Size, Align>::type;
        std::size_t mTop;
        std::array<void *, Count> mFree;
        std::array<MemoryBlock, Count> mData;

    };

    template<typename T, typename Allocator>
    struct Deleter {
        Deleter(Allocator * allocator) : mAllocator{ allocator } {}
        void operator () (T * ptr) const {
            ptr->~T();
            mAllocator->deallocate(ptr);
        }
    private:
        Allocator * mAllocator;
    };

}
