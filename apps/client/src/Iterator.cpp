#include "Iterator.hpp"
#include <stdexcept>
#include <algorithm>

Iterator::Sphere::Sphere(glm::ivec3::value_type radius) {
    radius = std::max(0, radius);
    if (radius != 0 && radius > std::numeric_limits<glm::ivec3::value_type>::max() / 3 / radius)
        throw std::runtime_error("VoxelIterator::VoxelIterator: Computation would overflow.");
    
    const auto squareRadius = radius * radius;
    auto squareLength = [] (const auto & v) {
        return v.x * v.x + v.y * v.y + v.z * v.z;
    };

    glm::ivec3 iterator;
    for (iterator.z = -radius; iterator.z <= radius; ++iterator.z)
    for (iterator.y = -radius; iterator.y <= radius; ++iterator.y)
    for (iterator.x = -radius; iterator.x <= radius; ++iterator.x)
        if (squareLength(iterator) <= squareRadius)
            mOffsets.push_back(iterator);

    // stable sort for consistency
    std::stable_sort(
        std::begin(mOffsets), std::end(mOffsets),
        [squareLength] (const auto & a, const auto & b) {
            return squareLength(a) < squareLength(b);
        }
    );
}

Iterator::Cuboid::Cuboid(glm::ivec3 radius) {
    if (radius.x < 0) radius.x = 0;
    if (radius.y < 0) radius.y = 0;
    if (radius.z < 0) radius.z = 0;
    const auto largestElement = std::max(std::max(radius.x, radius.y), radius.z);
    if (largestElement != 0 && largestElement > std::numeric_limits<glm::ivec3::value_type>::max() / 3 / largestElement)
        throw std::runtime_error("VoxelIterator::VoxelIterator: Computation might overflow.");

    glm::ivec3 iterator;
    for (iterator.z = -radius.z; iterator.z <= radius.z; ++iterator.z)
    for (iterator.y = -radius.y; iterator.y <= radius.y; ++iterator.y)
    for (iterator.x = -radius.x; iterator.x <= radius.x; ++iterator.x)
        mOffsets.push_back(iterator);

    auto squareLength = [] (const auto & v) {
        return v.x * v.x + v.y * v.y + v.z * v.z;
    };

    // stable sort for consistency
    std::stable_sort(
        std::begin(mOffsets), std::end(mOffsets),
        [squareLength] (const auto & a, const auto & b) {
            return squareLength(a) < squareLength(b);
        }
    );
}
