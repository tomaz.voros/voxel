#include "Iterator.hpp"

#include <unordered_set>

#include <catch2/catch.hpp>

using cub_t = Iterator::Cuboid3<>;
using ele_t = cub_t::position_element_type;
using pos_t = cub_t::position_type;
using op_t = cub_t::operation;

TEST_CASE("empty iterator", "[Cuboid3]") {
    cub_t cube;
    const auto i = cube.next();
    REQUIRE(i.second == op_t::DONE);
}

TEST_CASE("basic iteration", "[Cuboid3]") {
    constexpr auto center = pos_t{1, 0, -1};
    constexpr auto diameter = pos_t{3, 2, 4};
    constexpr std::array<pos_t, 24> expected{{
        // center
        center,
        // -x
        center + pos_t{-1, 0, 0},
        // +x
        center + pos_t{1, 0, 0},
        // -y
        center + pos_t{-1, -1, 0},
        center + pos_t{0, -1, 0},
        center + pos_t{1, -1, 0},
        // -z
        center + pos_t{-1, -1, -1},
        center + pos_t{0, -1, -1},
        center + pos_t{1, -1, -1},
        center + pos_t{-1, 0, -1},
        center + pos_t{0, 0, -1},
        center + pos_t{1, 0, -1},
        // +z
        center + pos_t{-1, -1, 1},
        center + pos_t{0, -1, 1},
        center + pos_t{1, -1, 1},
        center + pos_t{-1, 0, 1},
        center + pos_t{0, 0, 1},
        center + pos_t{1, 0, 1},
        // -z
        center + pos_t{-1, -1, -2},
        center + pos_t{0, -1, -2},
        center + pos_t{1, -1, -2},
        center + pos_t{-1, 0, -2},
        center + pos_t{0, 0, -2},
        center + pos_t{1, 0, -2},
    }};

    cub_t cube{center, diameter};

    SECTION("loading and done") {
        for (const auto & expect : expected) {
            const auto i = cube.next();
            REQUIRE(i.first.x == expect.x);
            REQUIRE(i.first.y == expect.y);
            REQUIRE(i.first.z == expect.z);
            REQUIRE(i.second == op_t::LOAD);
        }
        for (std::size_t n = 0; n < 3; ++n) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::DONE);
        }
    }

    cube = cub_t{center, pos_t{1, 1, 1}};

    SECTION("loading and done of size 1x1x1") {
        {
            const auto i = cube.next();
            REQUIRE(i.first.x == center.x);
            REQUIRE(i.first.y == center.y);
            REQUIRE(i.first.z == center.z);
            REQUIRE(i.second == op_t::LOAD);
        }
        {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::DONE);
        }
    }
}

TEST_CASE("iteration and movement", "[Cuboid3]") {
    constexpr auto center = pos_t{1, 0, -1};
    constexpr auto centerShift = pos_t{10, 10, 10};
    constexpr auto diameter = pos_t{3, 2, 4};
    constexpr std::array<pos_t, 24> expected{{
        // center
        center,
        // -x
        center + pos_t{-1, 0, 0},
        // +x
        center + pos_t{1, 0, 0},
        // -y
        center + pos_t{-1, -1, 0},
        center + pos_t{0, -1, 0},
        center + pos_t{1, -1, 0},
        // -z
        center + pos_t{-1, -1, -1},
        center + pos_t{0, -1, -1},
        center + pos_t{1, -1, -1},
        center + pos_t{-1, 0, -1},
        center + pos_t{0, 0, -1},
        center + pos_t{1, 0, -1},
        // +z
        center + pos_t{-1, -1, 1},
        center + pos_t{0, -1, 1},
        center + pos_t{1, -1, 1},
        center + pos_t{-1, 0, 1},
        center + pos_t{0, 0, 1},
        center + pos_t{1, 0, 1},
        // -z
        center + pos_t{-1, -1, -2},
        center + pos_t{0, -1, -2},
        center + pos_t{1, -1, -2},
        center + pos_t{-1, 0, -2},
        center + pos_t{0, 0, -2},
        center + pos_t{1, 0, -2},
    }};

    constexpr std::array<pos_t, 24> expectedUnload{{
        // -z
        center + pos_t{1, 0, -2},
        center + pos_t{0, 0, -2},
        center + pos_t{-1, 0, -2},
        center + pos_t{1, -1, -2},
        center + pos_t{0, -1, -2},
        center + pos_t{-1, -1, -2},
        // -z
        center + pos_t{1, 0, -1},
        center + pos_t{0, 0, -1},
        center + pos_t{-1, 0, -1},
        center + pos_t{1, -1, -1},
        center + pos_t{0, -1, -1},
        center + pos_t{-1, -1, -1},
        // -y
        center + pos_t{1, -1, 1},
        center + pos_t{0, -1, 1},
        center + pos_t{-1, -1, 1},
        center + pos_t{1, -1, 0},
        center + pos_t{0, -1, 0},
        center + pos_t{-1, -1, 0},
        // -x
        center + pos_t{-1, 0, 1},
        center + pos_t{-1, 0, 0},
        // -z
        center + pos_t{1, 0, 0},
        center + pos_t{0, 0, 0},
        // -y
        center + pos_t{1, 0, 1},
        center + pos_t{0, 0, 1},
    }};

    cub_t cube{center, diameter};

    {
        for (const auto & expect : expected) {
            const auto i = cube.next();
            REQUIRE(i.first.x == expect.x);
            REQUIRE(i.first.y == expect.y);
            REQUIRE(i.first.z == expect.z);
            REQUIRE(i.second == op_t::LOAD);
        }
        for (std::size_t n = 0; n < 3; ++n) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::DONE);
        }
    }

    cube.moveCenter(center + centerShift);

    SECTION("unloading loading done") {
        for (const auto & expect : expectedUnload) {
            const auto i = cube.next();
            REQUIRE(i.first.x == expect.x);
            REQUIRE(i.first.y == expect.y);
            REQUIRE(i.first.z == expect.z);
            REQUIRE(i.second == op_t::UNLOAD);
        }
        for (const auto & expect : expected) {
            const auto i = cube.next();
            const auto expectNew = expect + centerShift;
            REQUIRE(i.first.x == expectNew.x);
            REQUIRE(i.first.y == expectNew.y);
            REQUIRE(i.first.z == expectNew.z);
            REQUIRE(i.second == op_t::LOAD);
        }
        for (std::size_t n = 0; n < 3; ++n) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::DONE);
        }
    }
}

TEST_CASE("reference counted test", "[Cuboid3]") {
    constexpr auto center = pos_t{1, 0, -1};
    constexpr auto centerShift = pos_t{20, 0, -1};
    constexpr auto diameter = pos_t{3, 2, 4};
    constexpr auto exactHalfDiameterZ = 2;
    constexpr auto secondCenterShift = centerShift + pos_t{0, 0, exactHalfDiameterZ};
    constexpr std::ptrdiff_t volume = diameter.x * diameter.y * diameter.z;

    cub_t cube{center, diameter};

    struct pos_hash {
        std::size_t operator()(const pos_t & pos) const {
            return std::size_t(pos.x) ^ std::size_t(pos.y) ^ std::size_t(pos.z);
        }
    };

    std::unordered_set<pos_t, pos_hash> loadedSet;

    SECTION("movement") {
        std::ptrdiff_t refCount = 0;

        // load half
        while (refCount < volume / 2) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::LOAD);
            ++refCount;
            const auto inserted = loadedSet.insert(i.first).second;
            REQUIRE(inserted);
        }
        // move out
        cube.moveCenter(center + centerShift);
        while (refCount > 0) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::UNLOAD);
            --refCount;
            const auto erased = loadedSet.erase(i.first) == 1;
            REQUIRE(erased);
        }
        // load all
        while (refCount < volume) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::LOAD);
            ++refCount;
            const auto inserted = loadedSet.insert(i.first).second;
            REQUIRE(inserted);
        }
        // check if done
        for (std::size_t j = 0; j < 42; ++j) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::DONE);
        }
        // move a bit (half still inside)
        cube.moveCenter(center + secondCenterShift);
        // unload outside
        while (refCount > volume / 2) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::UNLOAD);
            --refCount;
            const auto erased = loadedSet.erase(i.first) == 1;
            REQUIRE(erased);
        }
        // load missing
        while (refCount < volume) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::LOAD);
            ++refCount;
            const auto inserted = loadedSet.insert(i.first).second;
            REQUIRE(inserted);
        }
        // check if done
        for (std::size_t j = 0; j < 42; ++j) {
            const auto i = cube.next();
            REQUIRE(i.second == op_t::DONE);
        }

        REQUIRE(refCount == volume);
        REQUIRE(loadedSet.size() == volume);

        const auto from = center + secondCenterShift - (diameter >> 1);
        const auto to = from + diameter;
        pos_t i;
        for (i.z = from.z; i.z < to.z; ++i.z)
            for (i.y = from.y; i.y < to.y; ++i.y)
                for (i.x = from.x; i.x < to.x; ++i.x) {
                    // check if correct ones are loaded
                    REQUIRE(loadedSet.count(i) == 1);
                }
    }
}
