#include "LineAxis.hpp"

#include <array>

#include <glm/gtc/type_ptr.hpp>

LineAxis::LineAxis(const std::filesystem::path & assetPath) {
    mProgram.reload(assetPath);

    struct Vertex {
        uint8_t x, y, z, w;
        uint8_t r, g, b, a;
    };

    constexpr std::array<Vertex, 6> VERTICES { {
        {   0,   0,   0, 255, 255,   0,   0, 255 },
        { 255,   0,   0, 255, 255,   0,   0, 255 },
        {   0,   0,   0, 255,   0, 255,   0, 255 },
        {   0, 255,   0, 255,   0, 255,   0, 255 },
        {   0,   0,   0, 255,   0,   0, 255, 255 },
        {   0,   0, 255, 255,   0,   0, 255, 255 },
    } };

    glCreateBuffers(1, &mVBO);
    glNamedBufferStorage(mVBO, sizeof(VERTICES), VERTICES.data(), 0);

    glCreateVertexArrays(1, &mVAO);
    glVertexArrayAttribFormat(mVAO, 0, sizeof(uint8_t) * 4, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(Vertex, x));
    glVertexArrayAttribFormat(mVAO, 1, sizeof(uint8_t) * 4, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(Vertex, r));
    glEnableVertexArrayAttrib(mVAO, 0);
    glEnableVertexArrayAttrib(mVAO, 1);
    glVertexArrayAttribBinding(mVAO, 0, 0);
    glVertexArrayAttribBinding(mVAO, 1, 0);
    glVertexArrayVertexBuffer(mVAO, 0, mVBO, 0, sizeof(Vertex));
}

LineAxis::~LineAxis() {
    glDeleteBuffers(1, &mVBO);
    glDeleteVertexArrays(1, &mVAO);
}

void LineAxis::draw(const glm::mat4 & MVP) {
    glUseProgram(mProgram.name());
    glProgramUniformMatrix4fv(mProgram.name(), mProgram.uVP, 1, GL_FALSE, glm::value_ptr(MVP));
    glBindVertexArray(mVAO);
    glDrawArrays(GL_LINES, 0, 6);
    glBindVertexArray(0);
}
