#pragma once

#include "ProgramBase.hpp"

class ProgramColoredLine : public ProgramBase {
public:
    static constexpr GLuint aPosition = 0;
    static constexpr GLuint aColor = 1;
    static constexpr GLint uVP = 0;
    static constexpr GLint uTexture = 1;

    void reload(const std::filesystem::path & assetPath) {
        ProgramBase::ShaderSourcePaths shaderSourcePaths;
        shaderSourcePaths.vert = assetPath / "shaders/coloredLine.vert";
        shaderSourcePaths.frag = assetPath / "shaders/coloredLine.frag";
        ProgramBase::reload(shaderSourcePaths, "ProgramColoredLine");
    }

};
