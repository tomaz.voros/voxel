#pragma once

template <template <typename> typename S>
class FloodFill {
public:
    void addNode(const glm::ivec3 & newNode) {
        mContainer.push(newNode);
    }

    void next() {

    }

    // reference implementation (TODO: remove)
    void loop() {
        // dummy function
        auto alreadyProcessed = [] (const glm::ivec3 & point) -> bool {
            return true;
        };

        auto process = [] (const glm::ivec3 & point) {
            return;
        };

        static constexpr std::array<glm::ivec3, 6> OFFSETS{ {
            {  1,  0,  0 },
            {  0,  1,  0 },
            {  0,  0,  1 },
            { -1,  0,  0 },
            {  0, -1,  0 },
            {  0,  0, -1 }
        } };

        const auto targetNode1 = mContainer.front();
        mContainer.pop();
        if (alreadyProcessed(targetNode1))
            return;
        process(targetNode1);
        mContainer.push(targetNode1);
        while (mContainer.size() > 0) {
            const auto n = mContainer.front();
            mContainer.pop();
            for (const auto & offset : OFFSETS) {
                const auto nNew = n + offset;
                if (!alreadyProcessed(nNew)) {
                    process(nNew);
                    mContainer.push(nNew);
                }
            }
        }

    }

    // reference implementation (TODO: remove)
    void loop2() {
        static constexpr std::array<glm::ivec3, 6> OFFSETS{ {
            {  1,  0,  0 },
            {  0,  1,  0 },
            {  0,  0,  1 },
            { -1,  0,  0 },
            {  0, -1,  0 },
            {  0,  0, -1 }
        } };

        auto processed = [] (const glm::ivec3 & point) -> bool {
            return true;
        };

        auto process = [] (const glm::ivec3 & point) -> void {
            return;
        };

        while (mContainer.size() > 0) {
            const auto & node = mContainer.front();
            if (!processed(node)) {
                process(node);
                for (const auto & offset : OFFSETS)
                    mContainer.push(node + offset);
            }
            mContainer.pop();
        }
    }



private:
    S<glm::ivec3> mContainer;

};
