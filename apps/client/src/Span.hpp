#pragma once

#include <cstddef>

// TODO: replace with std::span when supported by compiler
template <typename T>
struct Span {
    T * data = nullptr;
    std::size_t size = 0;
    T * begin() { return data; }
    T * end() { return data + size; }
};
