#include <string>
#include <filesystem>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fmt/format.h>

#include "Renderer.hpp"
#include "Engine.hpp"
#include "EngineRendererComm.hpp"
#include "Log.hpp"
#include "VoxelInfo.hpp"
#include "stb/stb_truetype.h"
#include <util.hpp>
#include "stb/stb_image_write.h"
#include "Iterator.hpp"

#if 1
inline constexpr auto DEBUG_OPENGL = true;
inline constexpr auto DEBUG_GLFW = true;
inline constexpr auto SHOW_CMD = true;
#else
inline constexpr auto DEBUG_OPENGL = false;
inline constexpr auto DEBUG_GLFW = false;
inline constexpr auto SHOW_CMD = false;
#endif

inline constexpr auto V_SYNC = true;

inline constexpr int MSAA_SAMPLE_COUNT = 0; // 0 means no MSAA

void GLAPIENTRY theOpenglDebugMessageCallback(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar * message,
    const void * userParam
) {
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
        log::error("OpenGL: {}", message);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        log::info("OpenGL: {}", message);
        break;
    default:
        log::warning("OpenGL: {}", message);
        break;
    }
}

void theGlfwErrorCallback(int code, const char * description) {
    log::error("GLFW: {}", description);
}


int mainMain(int argc, char * argv[]) {
#ifdef _WIN32
    ShowWindow(GetConsoleWindow(), SHOW_CMD ? SW_SHOW : SW_HIDE);
#endif

    // TODO: cache the generated assets in filesystem

    if (argc < 2) {
        log::error("Missing asset folder argument.");
        return 0;
    }

    //Log::doLogging(ENABLE_LOGGING);

    const std::filesystem::path assetPath{ argv[1] };

    const std::filesystem::path userPath{argc > 2 ? argv[2] : "."};

    const boost::asio::ip::address address{ boost::asio::ip::address::from_string("::1") };
    const unsigned short port{ 50'000u };
    const boost::asio::ip::tcp::endpoint endpoint{ address, port };
    const boost::asio::ip::tcp::endpoint monitorAcceptorEndpoint{
        boost::asio::ip::address::from_string("::"),
        port + 1
    };

    const auto glfwInitResult = glfwInit();
    if (glfwInitResult != GLFW_TRUE) return 0;

    if constexpr (DEBUG_GLFW) {
        glfwSetErrorCallback(theGlfwErrorCallback);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_LOSE_CONTEXT_ON_RESET);
    glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, GLFW_RELEASE_BEHAVIOR_NONE);
    if constexpr (DEBUG_OPENGL) {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
        glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_FALSE);
    } else {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_FALSE);
        glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_TRUE);
    }
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_SAMPLES, MSAA_SAMPLE_COUNT);

    GLFWwindow * window = glfwCreateWindow(1280, 720, "Voxel", nullptr, nullptr);
    if (window == nullptr) return 0;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(V_SYNC ? 1 : 0);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        log::error("Failed to initialize OpenGL context");
        glfwDestroyWindow(window);
        glfwTerminate();
        window = nullptr;
        return 0;
    }

    if constexpr (MSAA_SAMPLE_COUNT > 0)
        glEnable(GL_MULTISAMPLE);
    else
        glDisable(GL_MULTISAMPLE);

    if constexpr (DEBUG_OPENGL) {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(theOpenglDebugMessageCallback, nullptr);
    }

    GLint contextFlags = 0;
    glGetIntegerv(GL_CONTEXT_FLAGS, &contextFlags);

    log::info(
        "\n"
        "GLFW:\n"
        "  Version : {}.{}.{}\n"
        "OpenGL:\n"
        "  Version  : {}\n"
        "  GLSL     : {}\n"
        "  Vendor   : {}\n"
        "  Renderer : {}\n"
        "Context:\n"
        "  Forward Compatible: {}\n"
        "  Debug             : {}\n"
        "  Robust Access     : {}\n"
        "  No Error          : {}",
        GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION,
        glGetString(GL_VERSION),
        glGetString(GL_SHADING_LANGUAGE_VERSION),
        glGetString(GL_VENDOR),
        glGetString(GL_RENDERER),
        (contextFlags & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT) != 0,
        (contextFlags & GL_CONTEXT_FLAG_DEBUG_BIT) != 0,
        (contextFlags & GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT) != 0,
        (contextFlags & GL_CONTEXT_FLAG_NO_ERROR_BIT) != 0
    );

    const std::string userName{ "test123" };
    const std::string host{ "localhost" };
    const std::string target{ "/" };

    {
        VoxelInfo voxelInfo{ assetPath };
        // TODO: implement hot-reload for assets
        const auto tileInfo = voxelInfo.extractVoxelData();
        const auto tileAtlas = voxelInfo.extractVoxelTextureAtlas();
        glm::ivec3 initialCenterBlock{ 0, 0, 0 };
        // TODO: rework the hierarchy, having two objects at the same level communicate with one another leads
        //       to spaghetti code and it does not scale and is hard to test. Have a super class that contains
        //       both engine and renderer or something.
        EngineRendererComm engineRendererComm{ initialCenterBlock };
        Engine::Core engineCore{endpoint, engineRendererComm, userName, tileInfo, host, target, monitorAcceptorEndpoint};
        Renderer::Core renderer{ engineRendererComm, window, initialCenterBlock, tileAtlas, assetPath };
        renderer.run(tileInfo, userPath);
    }

    if (window != nullptr) {
        glfwDestroyWindow(window);
    }
    glfwTerminate();

    return 0;
}

int main(int argc, char * argv[]) {
    mainMain(argc, argv);
}
