#include "Camera.hpp"
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera() :
    mProjection{ 1.0f },
    mView{ 1.0f },
    mViewProjection{ 1.0f },
    mUp{ 0.0f, 1.0f, 0.0f },
    mAspect{ 1.0f },
    mNear{ 1.0f },
    mFar{ 10.0f },
    mFov{ 1.0f },
    mPosition{ 0.0f, 0.0f, 0.0f },
    mFacing{ 0.0f, 0.0f, -1.0f },
    mViewDirty{ true },
    mProjectionDirty{ true },
    mViewProjectionDirty{ true }
{}

void Camera::setUp(const glm::vec3 & up) {
    mUp = up;
    mViewDirty = true;
    mViewProjectionDirty = true;
}

void Camera::setAspect(float aspect) {
    mAspect = aspect;
    mProjectionDirty = true;
    mViewProjectionDirty = true;
}

void Camera::setNear(float near) {
    mNear = near;
    mProjectionDirty = true;
    mViewProjectionDirty = true;
}

void Camera::setFar(float far) {
    mFar = far;
    mProjectionDirty = true;
    mViewProjectionDirty = true;
}

void Camera::setFov(float fov) {
    mFov = fov;
    mProjectionDirty = true;
    mViewProjectionDirty = true;
}

void Camera::setPosition(const glm::vec3 & position) {
    mPosition = position;
    mViewDirty = true;
    mViewProjectionDirty = true;
}

void Camera::setFacing(float yaw, float pitch) {
    mFacing.x = std::cos(pitch) * std::cos(yaw);
    mFacing.y = std::sin(pitch);
    mFacing.z = std::cos(pitch) * std::sin(yaw);

    mFacing = glm::normalize(mFacing);

    mViewDirty = true;
    mViewProjectionDirty = true;
}

const glm::mat4 & Camera::getView() {
    if (mViewDirty) {
        mView = glm::lookAt(mPosition, mPosition + mFacing, mUp);
        mViewDirty = false;
    }
    return mView;
}

const glm::mat4 & Camera::getProjection() {
    if (mProjectionDirty) {
        mProjection = glm::perspective(mFov, mAspect, mNear, mFar);
        mProjectionDirty = false;
    }
    return mProjection;
}

const glm::mat4 & Camera::getViewProjection() {
    if (mViewProjectionDirty) {
        mViewProjection = getProjection() * getView();
        mViewProjectionDirty = false;
    }
    return mViewProjection;
}
