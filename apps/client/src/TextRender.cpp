#include "TextRender.hpp"

#include <array>
#include <cstddef>

#include "stb/stb_truetype.h"

#include "Log.hpp"

namespace {
    struct TextVertex {
        float posX;
        float posY;
        float texX;
        float texY;
    };
}

TextRender::TextRender(const std::filesystem::path & resource_path) {
    setupTexture(resource_path);
    mProgramName = glutil::programFromVertFrag(
        resource_path / "shaders/text.vert",
        resource_path / "shaders/text.frag"
    );
    glCreateVertexArrays(1, &mVAO);
    glEnableVertexArrayAttrib(mVAO, 0);
    glEnableVertexArrayAttrib(mVAO, 1);
    glVertexArrayAttribBinding(mVAO, 0, 0);
    glVertexArrayAttribBinding(mVAO, 1, 0);
    glVertexArrayAttribFormat(mVAO, 0, 2, GL_FLOAT, GL_FALSE, offsetof(TextVertex, posX));
    glVertexArrayAttribFormat(mVAO, 1, 2, GL_FLOAT, GL_FALSE, offsetof(TextVertex, texX));

    glCreateBuffers(1, &mVBO);
    glVertexArrayVertexBuffer(mVAO, 0, mVBO, 0, sizeof(TextVertex));

    glProgramUniform1i(mProgramName, TEXTURE_LOCATION, TEXTURE_UNIT);
}

void TextRender::updateText(std::string_view text) {
    float posX = 0.0f;
    float posY = 0.0f;
    std::vector<TextVertex> vertex_data;
    vertex_data.reserve(text.size() * 6);

    for (int c : text) {
        if (c == '\n') {
            posX = 0.0f;
            posY -= 1.0f;
            continue;
        }
        const int c2 = (c >= CHAR_FROM && c <= CHAR_TO ? c : CHAR_DUMMY) - CHAR_FROM;
        const float texX = (c2 % CHARS_PER_LINE) * TEX_SCALE_X;
        const float texY = (c2 / CHARS_PER_LINE) * TEX_SCALE_Y;
        std::array<TextVertex, 4> tmp_vertex_data;
        tmp_vertex_data[0] = { posX       , posY - 1.0f, texX              , texY + TEX_SCALE_Y };
        tmp_vertex_data[1] = { posX + 1.0f, posY - 1.0f, texX + TEX_SCALE_X, texY + TEX_SCALE_Y };
        tmp_vertex_data[2] = { posX + 1.0f, posY       , texX + TEX_SCALE_X, texY               };
        tmp_vertex_data[3] = { posX       , posY       , texX              , texY               };
        vertex_data.push_back(tmp_vertex_data[0]);
        vertex_data.push_back(tmp_vertex_data[1]);
        vertex_data.push_back(tmp_vertex_data[2]);
        vertex_data.push_back(tmp_vertex_data[2]);
        vertex_data.push_back(tmp_vertex_data[3]);
        vertex_data.push_back(tmp_vertex_data[0]);
        posX += 1.0f;
    }
    glNamedBufferData(mVBO, vertex_data.size() * sizeof(TextVertex), vertex_data.data(), GL_STREAM_DRAW);
    mVertexCount = vertex_data.size();
}

void TextRender::draw(const glutil::viewport & viewport, float scale) {
    const float scaleX = 2.0f * CHAR_SIZE_X / static_cast<float>(viewport.width) * scale;
    const float scaleY = 2.0f * CHAR_SIZE_Y / static_cast<float>(viewport.height) * scale;
    glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
    glUseProgram(mProgramName);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBindTextureUnit(TEXTURE_UNIT, mTextureName);
    glProgramUniform2f(mProgramName, SCALE_LOCATION, scaleX, scaleY);
    glProgramUniform2f(mProgramName, OFFSET_LOCATION, -1.0f, 1.0f);
    glProgramUniform4f(mProgramName, FONT_COLOR_LOCATION, 0.0f, 0.0f, 0.0f, 1.0f);
    glProgramUniform4f(mProgramName, BACKGROUND_COLOR_LOCATION, 1.0f, 1.0f, 1.0f, 0.3f);
    glBindVertexArray(mVAO);
    glDrawArrays(GL_TRIANGLES, 0, mVertexCount);
}

void TextRender::cleanup() noexcept {
    glDeleteTextures(1, &mTextureName);
    glDeleteProgram(mProgramName);
    glDeleteVertexArrays(1, &mVAO);
    glDeleteBuffers(1, &mVBO);
    mTextureName = 0;
    mProgramName = 0;
    mVAO = 0;
    mVBO = 0;
    mVertexCount = 0;
}

void TextRender::setupTexture(const std::filesystem::path & resource_path) {
    // TODO: mip mapped atlas

    const std::vector<uint8_t> atlas = generateFontAtlas(resource_path);
    constexpr std::array<uint8_t, 4> BLACK{ 0, 0, 0, 255 };

    glCreateTextures(GL_TEXTURE_2D, 1, &mTextureName);
    glTextureStorage2D(mTextureName, 1, GL_R8, ATLAS_WIDTH, ATLAS_HEGHT);

    glTextureParameteri(mTextureName, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mTextureName, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(mTextureName, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(mTextureName, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if (atlas.size() != ATLAS_WIDTH * ATLAS_HEGHT) {
        glClearTexImage(mTextureName, 0, GL_RED, GL_UNSIGNED_BYTE, BLACK.data());
    } else {
        glTextureSubImage2D(mTextureName, 0, 0, 0, ATLAS_WIDTH, ATLAS_HEGHT, GL_RED, GL_UNSIGNED_BYTE, atlas.data());
    }
}

std::vector<uint8_t> TextRender::generateFontAtlas(const std::filesystem::path & resource_path) {
    std::vector<uint8_t> result(ATLAS_WIDTH * ATLAS_HEGHT, 0);
    const auto ttfFile = util::loadFile<unsigned char>(resource_path / "fonts/DejaVuSansMono/DejaVuSansMono.ttf");
    if (ttfFile.size() == 0) {
        log::error("Failed to open font file.");
        return result;
    }
    log::warning("Using stb_truetype to load font. This library will hapily perform out of bounds memory access on invalid font files.");
    stbtt_fontinfo fontHandle = {};
    const auto fontInitResult = stbtt_InitFont(&fontHandle, ttfFile.data(), 0);
    if (fontInitResult == 0) {
        log::error("Failed to parse font file.");
        return result;
    }
    const float scale = stbtt_ScaleForPixelHeight(&fontHandle, CHAR_SIZE_Y);
    int ascent;
    stbtt_GetFontVMetrics(&fontHandle, &ascent, nullptr, nullptr);
    ascent *= scale;
    int x = 0;
    int rowIndex = 0;
    int lineIndex = 0;
    for (int c = CHAR_FROM;  c <= CHAR_TO; ++c, ++rowIndex) {
        if (rowIndex == CHARS_PER_LINE) {
            x = 0;
            ++lineIndex;
            rowIndex = 0;
        }
        int x1, y1, x2, y2;
        stbtt_GetCodepointBitmapBox(&fontHandle, c, scale, scale, &x1, &y1, &x2, &y2);
        const int y = ascent + y1 + lineIndex * CHAR_SIZE_Y;
        const int byteOffset = x + (y * ATLAS_WIDTH);
        stbtt_MakeCodepointBitmap(&fontHandle, result.data() + byteOffset, x2 - x1, y2 - y1, ATLAS_WIDTH, scale, scale, c);
        int advanceWidth;
        stbtt_GetCodepointHMetrics(&fontHandle, c, &advanceWidth, nullptr);
        const int xBefore = x;
        x += advanceWidth * scale;
        const int kern = stbtt_GetCodepointKernAdvance(&fontHandle, c, c + 1);
        x += kern * scale;
        if (x - xBefore != CHAR_SIZE_X) {
            const auto ratio = static_cast<float>(CHAR_SIZE_X) / static_cast<float>(CHAR_SIZE_Y);
            log::warning("Incompatible font. Only monospace font with width / height ratio of {} is supported.", ratio);
            break;
        }
    }
    return result;
}
