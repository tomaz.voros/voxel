#pragma once

#include <cstddef>
#include <gsl/span>

/**
 * Ring buffer with in-place storage.
 * \tparam N Maximum number of elements in the ring buffer.
 * \tparam T Type of the elements in the ring buffer.
 */
template <std::size_t N, typename T = std::byte>
class RingBuffer {
public:
    using size_type  = decltype(N);
    using value_type = T;
    static_assert(N > 0);
    static_assert(N < std::numeric_limits<size_type>::max() / 2, "mFront + count can overflow.");

    using Buffers = std::array<gsl::span<value_type>, 2>;

    /**
     * Constructor. Allocates #N elements.
     */
    constexpr RingBuffer() noexcept :
        mFront{ 0 },
        mSize{ 0 }
    {}

    /**
     * Returns 1 or 2 buffers to the next #count unused elements.
     * @param[in] count Number of elements to return.
     * @return One or two buffers to requested data. The elements might
     *         have to be split between two buffers. If there is
     *         less unused elements than #count, the returned
     *         buffers will be of size 0.
     */
    constexpr Buffers allocate(size_type count) noexcept {
        Buffers result = {};
        if (
            mSize + count < count || // unsigned integer wrap around
            mSize + count > N        // not enough space
        )
            return result;

        result[0] = { mRing + mFront, std::min<size_type>(count, N - mFront) };
        if (std::size(result[0]) < count)
            result[1] = { mRing, count - std::size(result[0]) };
        mFront = (mFront + count) % N;
        mSize += count;

        return result;
    }

    /**
     * @return One or two buffers to all in use elements as one or two buffers.
     */
    constexpr Buffers allocated() noexcept {
        Buffers result = {};
        if (mFront >= mSize) {
            result[0] = { mRing + mFront - mSize, mSize };
        } else {
            result[0] = { mRing + N - mSize + mFront, mSize - mFront };
            result[1] = { mRing, mFront };
        }
        return result;
    }

    /**
     * @return Number of in use elements.
     */
    constexpr size_type size() const noexcept {
        return mSize;
    }

    /**
     * @return The maximum number of elements that can be allocated at any time.
     */
    constexpr size_type capacity() const noexcept {
        return N;
    }

    /**
     * Return #count elements to unused.
     * @param[in] count Number of elements to return. If the number of
     *                  elements in use is smaller than the given parameter,
     *                  all the data is marked unused.
     */
    constexpr void deallocate(size_type count) noexcept {
        if (mSize < count)
            mSize = 0;
        else
            mSize -= count;
    }

private:
    size_type mFront;
    size_type mSize;
    value_type mRing[N];

};
