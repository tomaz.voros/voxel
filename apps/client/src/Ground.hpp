#pragma once

#include <filesystem>
#include <optional>

#include <glad/glad.h>

#include "ProgramColoredLine.hpp"
#include "VoxelTexture.hpp"

#include <glm/mat4x4.hpp>

class Ground {
public:
    Ground(
        const std::filesystem::path & assetPath,
        const std::filesystem::path & inFilePath
    );
    ~Ground();
    void draw(const glm::mat4 & viewProjection);

private:
    GLuint mVAO = 0;
    GLuint mVBO = 0;
    GLuint mEBO = 0;
    GLuint mTexture = 0;
    GLuint mSampler = 0;
    GLsizei mVertexCount = 0;
    GLsizei mElementCount = 0;
    ProgramColoredLine mShader;
    static constexpr GLint TEX_UNIT = 0;

    void loadTexture(const std::filesystem::path & assetPath);
};
