#pragma once

#include <filesystem>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glad/glad.h>
#include "ProgramColoredLine.hpp"

class LineAxis {
public:
    LineAxis(const std::filesystem::path & assetPath);
    ~LineAxis();
    void draw(const glm::mat4 & MVP);

private:
    ProgramColoredLine mProgram;
    GLuint mVAO{0};
    GLuint mVBO{0};

};
