#pragma once

#include <vector>
#include <utility>
#include <array>
#include <optional>
#include <type_traits>

#include <glm/vec3.hpp>
#include <glm/glm.hpp>

namespace Iterator {
    class Sphere {
    public:
        Sphere(glm::ivec3::value_type radius);
        const glm::ivec3 & operator [] (std::size_t i) const {
            assert(i < mOffsets.size());
            return mOffsets[i];
        }
        std::size_t size() const { return mOffsets.size(); }

    private:
        std::vector<glm::ivec3> mOffsets;

    };

    class Cuboid {
    public:
        Cuboid(glm::ivec3 radius);
        const glm::ivec3 & operator [] (std::size_t i) const {
            assert(i < mOffsets.size());
            return mOffsets[i];
        }
        std::size_t size() const { return mOffsets.size(); }

    private:
        std::vector<glm::ivec3> mOffsets;

    };

    template <typename T>
    class CuboidLinear {
    public:
        using position_element_type = T;
        using position_type = glm::tvec3<position_element_type>;

        static constexpr auto one_element_value = position_element_type{1};

        CuboidLinear() = default;

        // inclusive from inclusive to
        CuboidLinear(
            const position_type & from,
            const position_type & to,
            const position_type & start
        ) :
            mLo{from}, mHi{to}, mCurrent{start}
        {
            assert(glm::all(glm::lessThanEqual(from, to)));
            assert(glm::all(glm::greaterThanEqual(start, from)));
            assert(glm::all(glm::lessThanEqual(start, to)));
        }

        CuboidLinear(
            const position_type & from,
            const position_type & to,
            bool startAtFrom = true
        ) :
            CuboidLinear(from, to, startAtFrom ? from : to)
        {}

        std::pair<position_type, bool> next() {
            auto result = std::make_pair(mCurrent, false);
            if (mCurrent.x == mHi.x) {
                mCurrent.x = mLo.x;
                if (mCurrent.y == mHi.y) {
                    mCurrent.y = mLo.y;
                    if (mCurrent.z == mHi.z) {
                        mCurrent.z = mLo.z;
                        result.second = true;
                    } else {
                        mCurrent.z += one_element_value;
                    }
                } else {
                    mCurrent.y += one_element_value;
                }
            } else {
                mCurrent.x += one_element_value;
            }
            return result;
        }

        std::pair<position_type, bool> prev() {
            auto result  = std::make_pair(mCurrent, false);
            if (mCurrent.x == mLo.x) {
                mCurrent.x = mHi.x;
                if (mCurrent.y == mLo.y) {
                    mCurrent.y = mHi.y;
                    if (mCurrent.z == mLo.z) {
                        mCurrent.z = mHi.z;
                        result.second = true;
                    } else {
                        mCurrent.z -= one_element_value;
                    }
                } else {
                    mCurrent.y -= one_element_value;
                }
            } else {
                mCurrent.x -= one_element_value;
            }
            return result;
        }

    private:
        position_type mLo{0, 0, 0};
        position_type mHi{0, 0, 0};
        position_type mCurrent{0, 0, 0};

    };
    
    template <typename T = std::int32_t> class Cuboid3 {
        // TODO: validate algorithm
        // TODO: reverse unloading so that if the currently-being-loaded area is moved out of range, the unloading can be initiated immediately
        // TODO: try to merge chooseNextUnloadTaskZeroSizeTarget(), chooseNextUnloadTask() and chooseNextLoadTask() in to one function
        // TODO: generally try to simplify after validating algorithm
    public:
        using position_element_type = T;
        using absolute_distance_element_type = typename std::make_unsigned<position_element_type>::type;
        using position_type = glm::tvec3<position_element_type>;
        static constexpr auto zero_element_value = position_element_type{0};
        static constexpr auto one_element_value = position_element_type{1};
        static constexpr auto zero_position_value = position_type{zero_element_value, zero_element_value, zero_element_value};
        static constexpr auto one_position_value = position_type{one_element_value, one_element_value, one_element_value};
        static constexpr auto max_element_value = std::numeric_limits<position_element_type>::max();
        static constexpr auto min_element_value = std::numeric_limits<position_element_type>::min();
        static constexpr auto max_position_value = position_type{max_element_value, max_element_value, max_element_value};
        static constexpr auto min_position_value = position_type{min_element_value, min_element_value, min_element_value};
        enum class operation : position_element_type { UNLOAD, LOAD, DONE };
        using next_type = std::pair<position_type, operation>;

        Cuboid3() = default;

        Cuboid3(const position_type & center, const position_type & diameter) :
            mTargetSize{diameter}
        {
            assert(glm::all(glm::greaterThan(diameter, zero_position_value)));
            assert(glm::all(glm::greaterThanEqual(center, min_position_value + (diameter >> one_position_value))));
            assert(glm::all(glm::lessThanEqual(center - (diameter >> one_position_value), max_position_value - diameter + one_position_value)));
            moveCenter(center);
        }

        next_type next() {
            while (true) {
                switch (mCurrentTask) {
                    case internal_task::UNLOAD: {
                        const auto nextToUnload = mActive.prev();
                        if (nextToUnload.second) {
                            unloadDone();
                            mCurrentTask = internal_task::CHOOSE_NEXT_TASK;
                        }
                        return std::make_pair(nextToUnload.first, operation::UNLOAD);
                    } case internal_task::LOAD: {
                        const auto nextToLoad = mActive.next();
                        if (nextToLoad.second){
                            loadDone();
                            mCurrentTask = internal_task::CHOOSE_NEXT_TASK;
                        }
                        return std::make_pair(nextToLoad.first, operation::LOAD);
                    } case internal_task::DONE: {
                        return std::make_pair<position_type>({0, 0, 0}, operation::DONE);
                    } case internal_task::CHOOSE_NEXT_TASK: {
                        chooseNextTask();
                    }
                }
            }
        }

        // TODO: find better name (setCenter?) because move kind of implies delta movement
        void moveCenter(const position_type & newCenter) {
            assert(glm::all(glm::greaterThanEqual(newCenter, min_position_value + (mTargetSize >> one_position_value))));
            assert(glm::all(glm::lessThanEqual(newCenter - (mTargetSize >> one_position_value), max_position_value - mTargetSize + one_position_value)));
            const auto newTargetBegin = newCenter - (mTargetSize >> one_position_value);
            if (glm::all(glm::equal(mTargetBegin, newTargetBegin)))
                return;
            mTargetBegin = newTargetBegin;
            if (mCurrentTask == internal_task::DONE)
                mCurrentTask = internal_task::CHOOSE_NEXT_TASK;
        }

    private:
        enum class internal_task : position_element_type {
            UNLOAD,
            LOAD,
            DONE,
            CHOOSE_NEXT_TASK
        };

        // TODO: pack mCurrentSide and mCurrentTask together
        internal_task mCurrentTask{internal_task::CHOOSE_NEXT_TASK};
        position_type mCurrentBegin{zero_position_value};
        position_type mCurrentSize{zero_position_value};
        position_type mTargetBegin{zero_position_value};
        position_type mTargetSize{zero_position_value};
        CuboidLinear<position_element_type> mActive{};
        std::size_t mCurrentSide{0}; //lowX, highX, lowY, highY, lowZ, highZ, center

        void chooseNextTask() {
            const auto targetIsEmpty = mTargetSize.x == zero_element_value;
            const auto currentIsEmpty = mCurrentSize.x == zero_element_value;
            assert(!currentIsEmpty || glm::all(glm::equal(mCurrentSize, zero_position_value)));

            if (targetIsEmpty && currentIsEmpty) {
                // everything unloaded
                mCurrentTask = internal_task::DONE;
            } else if (targetIsEmpty) {
                // unloading everything
                mCurrentTask = internal_task::UNLOAD;
                chooseNextUnloadTaskZeroSizeTarget();
            } else if (currentIsEmpty) {
                // initialize loading
                mCurrentTask = internal_task::LOAD;
                mCurrentBegin = mTargetBegin + (mTargetSize >> one_position_value);
                mCurrentSize = zero_position_value;
                mActive = CuboidLinear<position_element_type>{mCurrentBegin, mCurrentBegin};
                mCurrentSide = 6;
            } else {
                // unload or load
                const auto unloadingTaskFound = chooseNextUnloadTask();
                if (unloadingTaskFound) {
                    mCurrentTask = internal_task::UNLOAD;
                } else {
                    const auto loadingTaskFound = chooseNextLoadTask();
                    if (loadingTaskFound) {
                        mCurrentTask = internal_task::LOAD;
                    } else {
                        mCurrentTask = internal_task::DONE;
                    }
                }
            }
        }

        static absolute_distance_element_type absoluteDistance(position_element_type a, position_element_type b) {
            if (a < b)
                return static_cast<absolute_distance_element_type>(b) - static_cast<absolute_distance_element_type>(a);
            else
                return static_cast<absolute_distance_element_type>(a) - static_cast<absolute_distance_element_type>(b);
        }
    
        void chooseNextUnloadTaskZeroSizeTarget() {
            assert(glm::all(glm::equal(mTargetSize, zero_position_value)));
            assert(glm::all(glm::greaterThan(mCurrentSize, zero_position_value)));
            assert(glm::all(glm::lessThanEqual(mCurrentBegin, max_position_value - (mCurrentSize - one_position_value))));

            const auto targetCenter = mTargetBegin;
            const auto currentEnd = mCurrentBegin + (mCurrentSize - one_position_value);

            const std::array<absolute_distance_element_type, 6> sideDistances{
                absoluteDistance(mCurrentBegin.x, targetCenter.x),
                absoluteDistance(currentEnd.x,    targetCenter.x),
                absoluteDistance(mCurrentBegin.y, targetCenter.y),
                absoluteDistance(currentEnd.y,    targetCenter.y),
                absoluteDistance(mCurrentBegin.z, targetCenter.z),
                absoluteDistance(currentEnd.z,    targetCenter.z),
            };

            std::ptrdiff_t maxDistanceSideIndex = 5;
            for (std::ptrdiff_t i = 4; i >= 0; --i)
                if (sideDistances[i] > sideDistances[maxDistanceSideIndex])
                    maxDistanceSideIndex = i;

            auto beginRange = mCurrentBegin;
            auto endRange = currentEnd;

            switch (maxDistanceSideIndex) {
                case 0: endRange.x   = beginRange.x; break;
                case 1: beginRange.x = endRange.x;   break;
                case 2: endRange.y   = beginRange.y; break;
                case 3: beginRange.y = endRange.y;   break;
                case 4: endRange.z   = beginRange.z; break;
                case 5: beginRange.z = endRange.z;   break;
            }

            mActive = CuboidLinear<position_element_type>(beginRange, endRange, false);

            auto isLastBlock = false;
            switch (maxDistanceSideIndex) {
                case 0: case 1: isLastBlock = mCurrentSize.x == one_element_value;break;
                case 2: case 3: isLastBlock = mCurrentSize.y == one_element_value;break;
                case 4: case 5: isLastBlock = mCurrentSize.z == one_element_value;break;
            }

            if (isLastBlock)
                mCurrentSide = 6;
            else
                mCurrentSide = maxDistanceSideIndex;
        }

        bool chooseNextUnloadTask() {
            assert(glm::all(glm::greaterThan(mTargetSize, zero_position_value)));
            assert(glm::all(glm::greaterThan(mCurrentSize, zero_position_value)));
            assert(glm::all(glm::lessThanEqual(mTargetBegin, max_position_value - (mTargetSize - one_position_value))));
            assert(glm::all(glm::lessThanEqual(mCurrentBegin, max_position_value - (mCurrentSize - one_position_value))));
            assert(glm::all(glm::lessThanEqual(mTargetBegin, max_position_value - mTargetSize >> one_position_value)));

            const auto targetEnd = mTargetBegin + (mTargetSize - one_position_value);
            const auto currentEnd = mCurrentBegin + (mCurrentSize - one_position_value);

            // TODO: cache in bitfield and only update on change
            const std::array<bool, 6> sideNeedsUnloading{
                mCurrentBegin.x < mTargetBegin.x,
                currentEnd.x    > targetEnd.x,
                mCurrentBegin.y < mTargetBegin.y,
                currentEnd.y    > targetEnd.y,
                mCurrentBegin.z < mTargetBegin.z,
                currentEnd.z    > targetEnd.z,
            };

            std::ptrdiff_t i = 5;
            for (; i >= 0; --i)
                if (sideNeedsUnloading[i])
                    break;
            if (i == -1)
                return false;

            const auto targetCenter = mTargetBegin + (mTargetSize >> one_position_value);
            auto maxDistanceSideIndex = i--;

            const std::array<absolute_distance_element_type, 6> sideDistances{
                absoluteDistance(mCurrentBegin.x, targetCenter.x),
                absoluteDistance(currentEnd.x,    targetCenter.x),
                absoluteDistance(mCurrentBegin.y, targetCenter.y),
                absoluteDistance(currentEnd.y,    targetCenter.y),
                absoluteDistance(mCurrentBegin.z, targetCenter.z),
                absoluteDistance(currentEnd.z,    targetCenter.z),
            };
            for (; i >= 0; --i)
                if (sideNeedsUnloading[i] && sideDistances[i] > sideDistances[maxDistanceSideIndex])
                    maxDistanceSideIndex = i;

            auto beginRange = mCurrentBegin;
            auto endRange = currentEnd;

            switch (maxDistanceSideIndex) {
                case 0: endRange.x   = beginRange.x; break;
                case 1: beginRange.x = endRange.x;   break;
                case 2: endRange.y   = beginRange.y; break;
                case 3: beginRange.y = endRange.y;   break;
                case 4: endRange.z   = beginRange.z; break;
                case 5: beginRange.z = endRange.z;   break;
            }

            mActive = CuboidLinear<position_element_type>(beginRange, endRange, false);

            auto isLastBlock = false;
            switch (maxDistanceSideIndex) {
                case 0: case 1: isLastBlock = mCurrentSize.x == one_element_value;break;
                case 2: case 3: isLastBlock = mCurrentSize.y == one_element_value;break;
                case 4: case 5: isLastBlock = mCurrentSize.z == one_element_value;break;
            }

            if (isLastBlock)
                mCurrentSide = 6;
            else
                mCurrentSide = maxDistanceSideIndex;

            return true;
        }

        bool chooseNextLoadTask() {
            assert(glm::all(glm::greaterThan(mTargetSize, zero_position_value)));
            assert(glm::all(glm::greaterThan(mCurrentSize, zero_position_value)));
            assert(glm::all(glm::lessThanEqual(mTargetBegin, max_position_value - (mTargetSize - one_position_value))));
            assert(glm::all(glm::lessThanEqual(mCurrentBegin, max_position_value - (mCurrentSize - one_position_value))));
            assert(glm::all(glm::lessThanEqual(mTargetBegin, max_position_value - mTargetSize >> one_position_value)));

            const auto targetEnd = mTargetBegin + (mTargetSize - one_position_value);
            const auto currentEnd = mCurrentBegin + (mCurrentSize - one_position_value);

            // TODO: cache in bitfield and only update on change
            const std::array<bool, 6> sideNeedsLoading{
                mCurrentBegin.x > mTargetBegin.x,
                currentEnd.x    < targetEnd.x,
                mCurrentBegin.y > mTargetBegin.y,
                currentEnd.y    < targetEnd.y,
                mCurrentBegin.z > mTargetBegin.z,
                currentEnd.z    < targetEnd.z,
            };

            std::size_t i = 0;
            for (; i < 6; ++i)
                if (sideNeedsLoading[i])
                    break;
            if (i == 6)
                return false;

            const auto targetCenter = mTargetBegin + (mTargetSize >> one_position_value);
            auto minDistanceSideIndex = i++;

            const std::array<absolute_distance_element_type, 6> sideDistances{
                absoluteDistance(mCurrentBegin.x, targetCenter.x),
                absoluteDistance(currentEnd.x,    targetCenter.x),
                absoluteDistance(mCurrentBegin.y, targetCenter.y),
                absoluteDistance(currentEnd.y,    targetCenter.y),
                absoluteDistance(mCurrentBegin.z, targetCenter.z),
                absoluteDistance(currentEnd.z,    targetCenter.z),
            };
            for (; i < 6; ++i)
                if (sideNeedsLoading[i] && sideDistances[i] < sideDistances[minDistanceSideIndex])
                    minDistanceSideIndex = i;

            auto beginRange = mCurrentBegin;
            auto endRange = currentEnd;

            switch (minDistanceSideIndex) {
                case 0: beginRange.x -= one_element_value; endRange.x   = beginRange.x; break;
                case 1: endRange.x   += one_element_value; beginRange.x = endRange.x;   break;
                case 2: beginRange.y -= one_element_value; endRange.y   = beginRange.y; break;
                case 3: endRange.y   += one_element_value; beginRange.y = endRange.y;   break;
                case 4: beginRange.z -= one_element_value; endRange.z   = beginRange.z; break;
                case 5: endRange.z   += one_element_value; beginRange.z = endRange.z;   break;
            }

            mActive = CuboidLinear<position_element_type>(beginRange, endRange);
            mCurrentSide = minDistanceSideIndex;

            return true;
        }

        void unloadDone() {
            switch (mCurrentSide) {
                case 0:
                    assert(mCurrentBegin.x < std::numeric_limits<position_element_type>::max());
                    mCurrentBegin.x += one_element_value;
                case 1:
                    assert(mCurrentSize.x > 0);
                    mCurrentSize.x -= one_element_value;
                    break;
                case 2:
                    assert(mCurrentBegin.y < std::numeric_limits<position_element_type>::max());
                    mCurrentBegin.y += one_element_value;
                case 3:
                    assert(mCurrentSize.y > 0);
                    mCurrentSize.y -= one_element_value;
                    break;
                case 4:
                    assert(mCurrentBegin.z < std::numeric_limits<position_element_type>::max());
                    mCurrentBegin.z += one_element_value;
                case 5:
                    assert(mCurrentSize.z > 0);
                    mCurrentSize.z -= one_element_value;
                    break;
                case 6:
                    // TODO: actually one element to 0 is enough (can be set to case 1 for example (case 0,2,4 could overflow))
                    assert(glm::all(glm::greaterThan(mCurrentSize, zero_position_value)));
                    assert(glm::any(glm::equal(mCurrentSize, one_position_value)));
                    mCurrentSize = zero_position_value;
                    break;
            }
        }

        void loadDone() {
            switch (mCurrentSide) {
                case 0:
                    assert(mCurrentBegin.x > std::numeric_limits<position_element_type>::min());
                    mCurrentBegin.x -= one_element_value;
                case 1:
                    assert(mCurrentSize.x < std::numeric_limits<position_element_type>::max());
                    mCurrentSize.x += one_element_value;
                    break;
                case 2:
                    assert(mCurrentBegin.y > std::numeric_limits<position_element_type>::min());
                    mCurrentBegin.y -= one_element_value;
                case 3:
                    assert(mCurrentSize.y < std::numeric_limits<position_element_type>::max());
                    mCurrentSize.y += one_element_value;
                    break;
                case 4:
                    assert(mCurrentBegin.z > std::numeric_limits<position_element_type>::min());
                    mCurrentBegin.z -= one_element_value;
                case 5:
                    assert(mCurrentSize.z < std::numeric_limits<position_element_type>::max());
                    mCurrentSize.z += one_element_value;
                    break;
                case 6:
                    // TODO: actually one element to 0 is enough (can be set to case 1 for example (case 0,2,4 could overflow))
                    assert(glm::all(glm::equal(mCurrentSize, zero_position_value)));
                    mCurrentSize = one_position_value;
                    break;
            }
        }
    };
}
