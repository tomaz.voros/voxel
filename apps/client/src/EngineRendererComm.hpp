#pragma once

#include <vector>
#include <mutex>
#include <atomic>
#include <queue>
#include <functional>
#include <glm/vec3.hpp>
#include "cfg.hpp"

class EngineRendererComm {
public:
    EngineRendererComm(const glm::ivec3 & initialCenterBlock) :
        mEntitiesNew{ false },
        mCenterBlock{ initialCenterBlock },
        mInformativeRayNew{ false }
    {
        mInformativeRayResolution.last = { 0, 0, 0 };
        mInformativeRayResolution.preLast = { 0, 0, 0 };
        mInformativeRayResolution.hit = false;
    }

    struct Entity {
        std::uint32_t id;
        glm::vec3 position;
        glm::vec3 positionOld;
    };

#if 0
    struct Vertex {
        std::uint8_t x, y, z, t, a0, a1, a2, a3;
    };
#elif 0
    struct Vertex {
        uint8_t position_x;
        uint8_t position_y;
        uint8_t position_z;
        uint8_t ambient_occlusion;
        uint8_t texture_x;
        uint8_t texture_y;
    };
#else
    // TODO: rename since this is not really a vertex anymore
    using Vertex = std::array<std::uint32_t, 3>;
    static_assert(sizeof(Vertex) == 12);
#endif

    struct Mesh {
        glm::ivec3 position;
        std::vector<Vertex> mesh;
    };

    // called by renderer
    void setSendStopServer() {
        touchNetworkingLoop();
        mSendStopServer.store(true);
    }

    // called by engine
    bool getAndClearSendStopServer() {
        return mSendStopServer.exchange(false);
    }

    // called by engine
    void copyStateIn(const std::vector<Entity> & entities, const std::chrono::steady_clock::time_point & snapshotTime) {
        std::unique_lock<std::mutex> lock{ mEntitiesMutex };
        mEntities = entities;
        mSnapshotTime = snapshotTime;
        mEntitiesNew = true;
    }

    // called by renderer
    void moveStateOut(std::vector<Entity> & entities, std::chrono::steady_clock::time_point & snapshotTime) {
        std::unique_lock<std::mutex> lock{ mEntitiesMutex };
        entities = std::move(mEntities);
        snapshotTime = std::move(mSnapshotTime);
        mEntitiesNew = false;
    }

    // called by renderer
    bool entitiesNew() {
        std::unique_lock<std::mutex> lock{ mEntitiesMutex };
        return mEntitiesNew;
    }

    // called by engine
    glm::ivec3 getCenterBlock() {
        std::unique_lock<std::mutex> lock{ mCenterBlockMutex };
        return mCenterBlock;
    }

    // called by renderer
    void setCenterBlock(const glm::ivec3 & newCenterBlock) {
        touchNetworkingLoop();
        std::unique_lock<std::mutex> lock{ mCenterBlockMutex };
        mCenterBlock = newCenterBlock;
    }

    // called by renderer
    std::vector<Mesh> getMeshes(std::size_t maxMeshesToGet) {
        std::unique_lock<std::mutex> lock{ mMeshesMutex };
        std::vector<Mesh> meshes;

        while (mUrgentMeshes.size() > 0 && maxMeshesToGet > 0) {
            meshes.push_back(std::move(mUrgentMeshes.front()));
            mUrgentMeshes.pop();
            --maxMeshesToGet;
        }

        while (mMeshes.size() > 0 && maxMeshesToGet > 0) {
            meshes.push_back(std::move(mMeshes.front()));
            mMeshes.pop();
            --maxMeshesToGet;
        }

        return meshes;
    }

    std::pair<std::size_t, std::size_t> meshQueueSizes() {
        std::unique_lock<std::mutex> lock{ mMeshesMutex };
        return { mUrgentMeshes.size(), mMeshes.size() };
    }

    // called by engine
    void moveMeshIn(Mesh && mesh, bool urgent) {
        std::unique_lock<std::mutex> lock{ mMeshesMutex };
        if (urgent) {
            mUrgentMeshes.push(std::move(mesh));
        } else {
            mMeshes.push(std::move(mesh));
        }
    }

    // called by renderer
    std::int32_t getCachedChunkCount() {
        return mCachedChunkCount.load();
    }

    // called by engine
    void setCachedChunkCount(std::int32_t cachedChunkCount) {
        mCachedChunkCount.store(cachedChunkCount);
    }

    // called by renderer
    std::int32_t getCachedObservedMeshCount() {
        return mCachedObservedMeshesCount.load();
    }

    // called by engine
    void setCachedObservedMeshCount(std::int32_t cachedCount) {
        mCachedObservedMeshesCount.store(cachedCount);
    }

    struct Ray {
        glm::vec3 direction;
        glm::vec3 origin;
    };

    struct RayResolution {
        glm::ivec3 last;
        glm::ivec3 preLast;
        bool hit;
        cfg::Block hitValue;
    };

    // called by renderer
    void updateInformativeRay(const Ray & ray) {
        touchNetworkingLoop();
        {
            std::unique_lock<std::mutex> lock{ mInformativeRayMutex };
            mInformativeRay = ray;
            mInformativeRayNew = true;
        }
        if (mNotifyInformativeRayUpdateIsSet)
            mNotifyInformativeRayUpdate();
    }

    void setNotifyInformativeRayUpdateFunction(std::function<void()> && notifyInformativeRayUpdate) {
        mNotifyInformativeRayUpdate = std::move(notifyInformativeRayUpdate);
        mNotifyInformativeRayUpdateIsSet = true;
    }

    // called by engine
    bool informativeRayNew() {
        std::unique_lock<std::mutex> lock{ mInformativeRayMutex };
        return mInformativeRayNew;
    }

    // called by engine
    Ray getInformativeRay() {
        std::unique_lock<std::mutex> lock{ mInformativeRayMutex };
        mInformativeRayNew = false;
        return mInformativeRay;
    }

    // called by renderer
    RayResolution getLatestInformativeRayResolution() {
        std::unique_lock<std::mutex> lock{ mInformativeRayResolutionMutex };
        return mInformativeRayResolution;
    }

    // called by engine
    void setInformativeRayResolution(const RayResolution & informativeRayResolution) {
        std::unique_lock<std::mutex> lock{ mInformativeRayResolutionMutex };
        mInformativeRayResolution = informativeRayResolution;
    }

    struct RayPlace {
        Ray ray;
        cfg::Block value;
        // 0 is last position
        // -1 is the position before last
        std::int32_t position;
    };

    // called by engine
    void setNotifyRayPlaceUpdateFunction(std::function<void(RayPlace)> && notifyRayPlaceUpdate) {
        mNotifyRayPlaceUpdate = std::move(notifyRayPlaceUpdate);
        mNotifyRayPlaceUpdateIsSet = true;
    }

    // called by renderer
    void addRayPlace(const RayPlace & rayPlace) {
        touchNetworkingLoop();
        if (mNotifyRayPlaceUpdateIsSet)
            mNotifyRayPlaceUpdate(rayPlace);
    }

    // called by the engine
    void setTouchNetworkingLoopFunction(std::function<void()> && touchNetworkingLoop) {
        mTouchNetworkingLoop = std::move(touchNetworkingLoop);
        mTouchNetworkingLoopIsSet = true;
    }

    // called internally or by the renderer
    void touchNetworkingLoop() {
        if (mTouchNetworkingLoopIsSet)
            mTouchNetworkingLoop();
    }

private:
    std::vector<Entity> mEntities;
    std::chrono::steady_clock::time_point mSnapshotTime;
    std::mutex mEntitiesMutex;
    bool mEntitiesNew;

    glm::ivec3 mCenterBlock;
    std::mutex mCenterBlockMutex;

    std::mutex mMeshesMutex;
    std::queue<Mesh> mMeshes;
    std::queue<Mesh> mUrgentMeshes;

    std::atomic_int32_t mCachedChunkCount{ 0 };
    std::atomic_int32_t mCachedObservedMeshesCount{ 0 };

    std::mutex mInformativeRayMutex;
    Ray mInformativeRay;
    bool mInformativeRayNew;
    std::function<void()> mNotifyInformativeRayUpdate;
    std::atomic_bool mNotifyInformativeRayUpdateIsSet = false;
    std::mutex mInformativeRayResolutionMutex;
    RayResolution mInformativeRayResolution;

    std::function<void(RayPlace)> mNotifyRayPlaceUpdate;
    std::atomic_bool mNotifyRayPlaceUpdateIsSet = false;

    std::atomic_bool mSendStopServer = false;

    std::function<void()> mTouchNetworkingLoop;
    std::atomic_bool mTouchNetworkingLoopIsSet = false;

};
