#pragma once

#include <cassert>
#include <atomic>
#include <array>
#include <gsl/span>

class spsc_ring_buffer_base {
public:
    using index_type = std::ptrdiff_t;
    template <typename T> using buffers_type = std::array<gsl::span<T>, 2>;
    static constexpr std::size_t hardware_destructive_interference_size = 64;
    static_assert(std::is_signed<index_type>::value);

protected:
    static index_type read_available(index_type write_index, index_type read_index, index_type capacity) {
        assert(capacity > 0);
        assert(write_index >= 0 && write_index < capacity);
        assert(read_index >= 0 && read_index < capacity);

        auto available = write_index - read_index;
        if (available < 0)
            available += capacity;
        return available;
    }

    static index_type write_available(index_type write_index, index_type read_index, index_type capacity) {
        assert(capacity > 0);
        assert(write_index >= 0 && write_index < capacity);
        assert(read_index >= 0 && read_index < capacity);

        auto available = read_index - write_index - 1;
        if (available < 0)
            available += capacity;
        return available;
    }

    index_type read_available(index_type capacity) const {
        const auto read_index = m_read_index.load(std::memory_order_relaxed);
        const auto write_index = m_write_index.load(std::memory_order_acquire);
        return read_available(write_index, read_index, capacity);
    }

    index_type write_available(index_type capacity) const {
        const auto write_index = m_write_index.load(std::memory_order_relaxed);
        const auto read_index = m_read_index.load(std::memory_order_acquire);
        return write_available(write_index, read_index, capacity);
    }

    template <typename T>
    buffers_type<T> readable_buffers(T * ring_buffer, index_type capacity) const {
        const auto read_index = m_read_index.load(std::memory_order_relaxed);
        const auto write_index = m_write_index.load(std::memory_order_acquire);
        const auto available = read_available(write_index, read_index, capacity);
        buffers_type<T> buffers;
        buffers[0] = {
            ring_buffer + read_index,
            std::min<index_type>(available, capacity - read_index)
        };
        buffers[1] = {
            ring_buffer,
            available - buffers[0].size()
        };
        return buffers;
    }

    template <typename T>
    buffers_type<T> writable_buffers(T * ring_buffer, index_type capacity) const {
        const auto write_index = m_write_index.load(std::memory_order_relaxed);
        const auto read_index = m_read_index.load(std::memory_order_acquire);
        const auto available = write_available(write_index, read_index, capacity);
        buffers_type<T> buffers;
        buffers[0] = {
            ring_buffer + write_index,
            std::min<index_type>(available, capacity - write_index)
        };
        buffers[1] = {
            ring_buffer,
            available - buffers[0].size()
        };
        return buffers;
    }

    void commit_read(index_type count, index_type capacity) {
        assert(capacity > 0);
        assert(count >= 0 && count <= read_available(capacity));

        auto read_index = m_read_index.load(std::memory_order_relaxed);
        read_index += count;
        if (count >= capacity)
            count -= capacity;
        m_read_index.store(read_index, std::memory_order_release);
    }

    void commit_write(index_type count, index_type capacity) {
        assert(capacity > 0);
        assert(count >= 0 && count <= write_available(capacity));

        auto write_index = m_write_index.load(std::memory_order_relaxed);
        write_index += count;
        if (count >= capacity)
            count -= capacity;
        m_write_index.store(write_index, std::memory_order_release);
    }

private:
    alignas(hardware_destructive_interference_size) std::atomic<index_type> m_write_index{ 0 };
    alignas(hardware_destructive_interference_size) std::atomic<index_type> m_read_index{ 0 };

};

template <typename T, spsc_ring_buffer_base::index_type N>
class spsc_ring_buffer_static : private spsc_ring_buffer_base {
private:
    using spsc_ring_buffer_base::readable_buffers;
    using spsc_ring_buffer_base::writable_buffers;
    using spsc_ring_buffer_base::commit_read;
    using spsc_ring_buffer_base::commit_write;

public:
    /**
     * Can only be called by thread A.
     */
    buffers_type<T> readable_buffers() {
        return readable_buffers<T>(m_buffer.data(), m_buffer.size());
    }

    /**
     * Can only be called by thread B.
     */
    buffers_type<T> writable_buffers() {
        return writable_buffers<T>(m_buffer.data(), m_buffer.size());
    }

    /**
     * Can only be called by thread A.
     */
    void commit_read(index_type count) {
        commit_read(count, m_buffer.size());
    }

    /**
     * Can only be called by thread B.
     */
    void commit_write(index_type count) {
        commit_write(count, m_buffer.size());
    }

private:
    static_assert(N < std::numeric_limits<index_type>::max());

    std::array<T, N + 1> m_buffer;

};
