#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

class Camera {
public:
    Camera();

    void setUp(const glm::vec3 & up);
    void setAspect(float aspect);
    void setNear(float near);
    void setFar(float far);
    void setFov(float fov);

    void setPosition(const glm::vec3 & position);
    void setFacing(float yaw, float pitch);

    const glm::mat4 & getView();
    const glm::mat4 & getProjection();
    const glm::mat4 & getViewProjection();

private:
    glm::mat4 mProjection;
    glm::mat4 mView;
    glm::mat4 mViewProjection;
    
    glm::vec3 mUp;
    float mAspect;
    float mNear;
    float mFar;
    float mFov;

    glm::vec3 mPosition;
    glm::vec3 mFacing;

    bool mViewDirty;
    bool mProjectionDirty;
    bool mViewProjectionDirty;

};
