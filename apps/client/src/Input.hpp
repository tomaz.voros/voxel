#pragma once

#include <map>
#include <array>
#include <vector>
#include <glm/vec2.hpp>
#include <GLFW/glfw3.h>

class Input {
public:
    Input(GLFWwindow * window);
    void poll();

    bool key(int key) const;
    bool button(int button) const;
    glm::dvec2 cursorDelta();
    glm::dvec2 scrollDelta();

    static void getJoystickNames(std::vector<int> & mappings, std::vector<const char *> & names);

    static GLFWgamepadstate getJoystickState(int joystick_id);

private:
    std::array<bool, 256> mKeys;
    std::array<bool, 8> mButtons;

    glm::dvec2 mCursor;
    glm::dvec2 mScroll;

    glm::dvec2 mLastCursor;

    struct JoystickData {
        const char * name;
        GLFWgamepadstate state;
    };

    static std::map<int, JoystickData> sJoysticks;

    static void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods);
    static void mouseButtonCallback(GLFWwindow * window, int button, int action, int mods);
    static void cursorPosCallback(GLFWwindow * window, double x_pos, double y_pos);
    static void scrollCallback(GLFWwindow * window, double x_offset, double y_offset);
    static void joystickCallback(int joystick_id, int event);

    static void updateJoystick(int joystick_id, GLFWgamepadstate & outState);

};
