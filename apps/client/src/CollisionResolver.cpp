#include "CollisionResolver.hpp"

#include <cmath>

#include <glm/glm.hpp>

#include "Log.hpp"

glm::vec3 CollisionResolver::resolve(
    const glm::vec3 & startingPosition,
    const glm::vec3 & directionIn,
    std::function<bool(const glm::ivec3 &)>  isSolid
) {
    auto signDir = [] (const glm::vec3 & v) -> glm::vec3 {
        return glm::vec3{
            float(v.x != 0.0f) * (std::signbit(v.x) ? -1.0f : 1.0f),
            float(v.y != 0.0f) * (std::signbit(v.y) ? -1.0f : 1.0f),
            float(v.z != 0.0f) * (std::signbit(v.z) ? -1.0f : 1.0f)
        };
    };

    glm::vec3 position{ startingPosition };
    glm::vec3 direction{ directionIn };
    glm::vec3 directionMask{ 1.0f, 1.0f, 1.0f };

    int loopCount = 0;
    while (true) {
        ++loopCount;
        const glm::vec3 stepDirection{ signDir(direction * directionMask) };
        const glm::ivec3 stepDirectionI{ stepDirection };
        const glm::ivec3 blockPosition{ glm::floor(position) };

        auto getNextPlane = [&isSolid] (int stepDirection, int blockPositionD, bool nextIsSolid) -> float {
            float next = std::numeric_limits<glm::vec3::value_type>::infinity();
            if (stepDirection == 1) {
                if (nextIsSolid) {
                    next = std::nextafter(float(blockPositionD + 1), float(blockPositionD));
                } else {
                    next = float(blockPositionD + 1);
                }
            } else if (stepDirection == -1) {
                if (nextIsSolid) {
                    next = float(blockPositionD);
                } else {
                    next = std::nextafter(float(blockPositionD), float(blockPositionD - 1));
                }
            }
            return next;
        };

        glm::vec3 next{
            getNextPlane(
                stepDirectionI.x, blockPosition.x,
                isSolid(blockPosition + glm::ivec3{ stepDirectionI.x, 0, 0 })
            ),
            getNextPlane(
                stepDirectionI.y, blockPosition.y,
                isSolid(blockPosition + glm::ivec3{ 0, stepDirectionI.y, 0 })
            ),
            getNextPlane(
                stepDirectionI.z, blockPosition.z,
                isSolid(blockPosition + glm::ivec3{ 0, 0, stepDirectionI.z })
            )
        };

        const glm::vec3 collisionTime = (next - position) / (direction * directionMask);

        std::size_t i = 2;
        if (collisionTime.x < collisionTime.y) {
            if (collisionTime.x < collisionTime.z) {
                i = 0;
            }
        } else {
            if (collisionTime.y < collisionTime.z) {
                i = 1;
            }
        }

        const float dt = std::max(std::min(collisionTime[i], 1.0f), 0.0f);

        glm::vec3 newPosition = position + (direction * directionMask) * dt;

        // avoid issues from rounding errors (this was unnecessary in my testing though)
        if (dt != 1.0f) {
            newPosition[i] = next[i];
        }

        glm::ivec3 blockOffsetMask{ 0 };
        blockOffsetMask[i] = 1;
        const auto collided = isSolid(blockPosition + stepDirectionI * blockOffsetMask);
        auto newDirectionMask = directionMask;
        const glm::vec3 newDirection = direction * (1.0f - dt);

        if (collided) {
            if (direction[i] < 0.0f)
                newDirectionMask[i] = -0.0f;
            else
                newDirectionMask[i] = 0.0f;
        }

        // reset when leaving block
        const glm::ivec3 newBlockPosition{ glm::floor(newPosition) };
        if (glm::any(glm::notEqual(newBlockPosition, blockPosition))) {
            newDirectionMask = glm::vec3{ 1.0f };
        }

        position = newPosition;
        direction = newDirection;
        directionMask = newDirectionMask;
        if (dt == 1.0f) break;
        if (loopCount >= 20) {
            Log::print(Log::Type::WARNING, "Calcelling collision resolution after 20 iterations!!!");
            break;
        }
    }
    return position;
}
