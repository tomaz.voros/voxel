#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <span>

class VoxelAlloc {
public:
    struct Allocation {
        GLuint vbo = 0;
        size_t offBot = 0;
        size_t offTop = 0;
    };

    struct Buffer {
        GLuint vbo = 0;
        size_t allocations = 0;
    };

    static constexpr size_t BUFFER_SIZE = 16 * 1024 * 1024;

    Allocation allocate(size_t amountBot, size_t amountTop) {
        if (amountBot < 1 || amountTop < 1)
            throw std::runtime_error("Zero allocation.");
        size_t amountTotal = amountBot + amountTop;
        if (amountTotal < amountBot || amountTotal > BUFFER_SIZE)
            throw std::runtime_error("Allocation too large.");
        size_t remainingSpace = mTop - mBot;
        if (remainingSpace < amountTotal) {
            mBuffers.emplace_back();
            glCreateBuffers(1, &mBuffers.back().vbo);
            glNamedBufferData(mBuffers.back().vbo, BUFFER_SIZE, nullptr, GL_STATIC_DRAW);
            mBot = 0;
            mTop = BUFFER_SIZE;
            mBuffers.back().allocations = 0;
        }
        mBuffers.back().allocations++;
        Allocation allocation;
        allocation.vbo = mBuffers.back().vbo;
        allocation.offBot = mBot;
        mBot += amountBot;
        mTop -= amountTop;
        allocation.offTop = mTop;
        return allocation;
    }

    void deallocate(GLuint vbo) {
        // TODO: O(1) deallocation (which is a lot more involved)
        for (auto & buffer : mBuffers) {
            if (buffer.vbo != vbo)
                continue;
            if (buffer.allocations < 1)
                throw std::runtime_error("Deallocating from empty buffer.");
            buffer.allocations--;
            if (buffer.allocations == 0) {
                // TODO: deallocate or recycle
            }
            return;
        }
        throw std::runtime_error("VBO not found.");
    }

    ~VoxelAlloc() {
        for (auto& buffer : mBuffers)
            glDeleteBuffers(1, &buffer.vbo);
    }

    std::span<const Buffer> buffers() const { return mBuffers; }

private:
    std::vector<Buffer> mBuffers;
    size_t mTop = 0;
    size_t mBot = 0;

};