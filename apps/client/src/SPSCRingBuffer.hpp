#pragma once

#include <cstddef>
#include <mutex>

#include "Log.hpp"

// TODO: verify correctness
// TODO: Implement lock+wait free version (probably easy if you transform (mSize, mFront) to (mFront, mBack))

/**
 * Ring buffer with in-place storage. This container is thread
 * safe as long as only one thread is adding data and one thread
 * is consuming data TODO: better explanation on what functions can be called by what thread
 * \tparam N Maximum number of elements in the ring buffer.
 * \tparam T Type of the elements in the ring buffer.
 */
template <std::size_t N, typename T = std::byte>
class SPSCRingBuffer {
public:
    using size_type  = decltype(N);
    using value_type = T;
    static_assert(N > 0);
    static_assert(N < std::numeric_limits<size_type>::max() / 2, "mFront + count can overflow.");

    struct Buffers {
        struct {
            value_type * data;
            size_type    size;
        } first;
        struct {
            value_type * data;
            size_type    size;
        } second;
    };

    /**
     * Constructor. Allocates #N elements.
     */
    constexpr SPSCRingBuffer() noexcept :
        mFront{ 0 },
        mSize{ 0 }
    {}

    /**
     * Returns 1 or 2 buffers to the unused elements.
     * @return One or two buffers to writable data. The elements might
     *         have to be split between two buffers.
     */
    constexpr Buffers writable() noexcept {
        std::unique_lock<std::mutex> l{ mMutex };
        const auto s = mSize;
        const auto f = mFront;
        l.unlock();
        const size_type count = N - s;
        Buffers result{};
        result.first.data = mRing + f;
        result.first.size = std::min<size_type>(count, N - f);
        result.second.data = mRing;
        result.second.size = count - result.first.size;
        return result;
    }

    /**
     * @return One or two buffers to all in use elements as one or two buffers.
     */
    constexpr Buffers readable() noexcept {
        std::unique_lock<std::mutex> l{ mMutex };
        const auto s = mSize;
        const auto f = mFront;
        l.unlock();
        Buffers result{};
        if (f >= s) {
            result.first.data = mRing + f - s;
            result.first.size = s;
        } else {
            result.first.data = mRing + N - s + f;
            result.first.size = s - f;
            result.second.data = mRing;
            result.second.size = f;
        }
        return result;
    }

    /**
     * Add #count elements to in use.
     * @param[in] size Number of elements to set readable. If the number of
     *                 unused elements is smaller than the given parameter,
     *                 all the data is marked in use and an error message is printed.
     */
    constexpr void push(size_type count) noexcept {
        bool countTooLarge = false;
        std::unique_lock<std::mutex> l{ mMutex };
        if (
            mSize + count < count || // unsigned integer wrap around
            mSize + count > N        // not enough space
        ) {
            count = N - mSize;
            countTooLarge = true;
        }
        mFront = (mFront + count) % N;
        mSize += count;
        l.unlock();
        if (countTooLarge)
            Log::print(Log::Type::ERROR, "Pushing too much data into ring buffer.");
    }

    /**
     * Return #count elements to unused.
     * @param[in] count Number of elements to return. If the number of
     *                  elements in use is smaller than the given parameter,
     *                  all the data is marked unused.
     */
    constexpr void pop(size_type count) noexcept {
        std::unique_lock<std::mutex> l{ mMutex };
        if (mSize < count)
            mSize = 0;
        else
            mSize -= count;
    }

    /**
     * @return Number of in use elements at the point of the function call.
     */
    constexpr size_type size() const noexcept {
        std::unique_lock<std::mutex> l{ mMutex };
        return mSize;
    }

    /**
     * @return The maximum number of elements that can be allocated at any time.
     */
    constexpr size_type capacity() const noexcept {
        return N;
    }

private:
    std::mutex mMutex;
    size_type mFront;
    size_type mSize;
    value_type mRing[N];

};
