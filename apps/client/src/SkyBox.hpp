#pragma once

#include "Shader.hpp"
#include <glm/mat4x4.hpp>

class SkyBox {
public:
    SkyBox(
        const std::vector<std::filesystem::path> & fileNames, size_t width, size_t height,
        const std::vector<Shader::Source> & shaderSource
    );

    void draw(const glm::mat4 & view, const glm::mat4 & projection, const glm::vec3 & cameraPosition);

private:
    Shader mShader;
    GLuint mVAO = 0;
    GLuint mVBO = 0;
    GLuint mTexture = 0;
    GLint mVP = -1;
    GLint mSkyBox = -1;
    GLint mCameraWorldSpacePosition = -1;


};
