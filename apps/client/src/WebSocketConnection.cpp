#include "WebSocketConnection.hpp"

#include <cassert>

#include "Log.hpp"
#include "cfg.hpp"


WebSocketConnection::WebSocketConnection(
    boost::asio::io_context & ioContext,
    const endpoint_type & endpoint,
    std::string_view host,
    std::string_view target,
    const std::chrono::seconds & timeout
) :
    mWebSocket{ ioContext },
    mHost{ host },
    mTarget{ target }
{
    auto & lowest_layer = boost::beast::get_lowest_layer(mWebSocket);

    lowest_layer.expires_after(timeout);
    lowest_layer.async_connect(
        endpoint,
        [this] (boost::beast::error_code error) {
            onConnect(error, boost::beast::role_type::client);
        }
    );
}

WebSocketConnection::WebSocketConnection(
    boost::asio::ip::tcp::socket && socket,
    boost::beast::role_type roleType,
    std::string_view host,
    std::string_view target
) :
    mWebSocket{ std::move(socket) },
    mHost{ host },
    mTarget{ target }
{
    onConnect(boost::beast::error_code{}, roleType);
}

std::vector<std::byte> WebSocketConnection::getReceivedMessage() {
    assert(receivedMessageCount() > 0);
    const auto message = std::move(mRxQueue.front());
    mRxQueue.pop_front();
    return message;
}

void WebSocketConnection::onConnect(boost::beast::error_code error, boost::beast::role_type roleType) {
    if (error) {
        log::info("{}", error.message());
        return;
        // TODO: proper error handling
    }

    auto & lowest_layer = boost::beast::get_lowest_layer(mWebSocket);

    lowest_layer.expires_never();

    // TODO: set other options

    lowest_layer.socket().set_option(
        boost::asio::ip::tcp::no_delay{ true }
    );

    mWebSocket.set_option(boost::beast::websocket::stream_base::timeout::suggested(roleType));

    if (roleType == boost::beast::role_type::server) {
        mWebSocket.async_accept(
            [this] (boost::beast::error_code error) { onHandshake(error); }
        );
    } else {
        mWebSocket.async_handshake(
            mHost,
            mTarget,
            [this] (boost::beast::error_code error) { onHandshake(error); }
        );
    }
}

void WebSocketConnection::onHandshake(boost::beast::error_code error) {
    if (error) {
        log::info("{}", error.message());
        return;
        // TODO: proper error handling
    }

    mWebSocket.binary(true);
    mWebSocket.read_message_max(cfg::MAX_MESSAGE_SIZE);
    mWebSocket.auto_fragment(false);

    mConnected = true;

    mRxBuffer.emplace(boost::asio::dynamic_buffer(mRxQueue.emplace_back()));

    mWebSocket.async_read(
        mRxBuffer.value(),
        [this] (
            const boost::system::error_code & error,
            std::size_t bytesTransferred
        ) { onRead(error, bytesTransferred); }
    );

    if (mTxQueue.size() > 1) {
        mWebSocket.async_write(
            boost::asio::buffer(mTxQueue.front()),
            [this](
                const boost::system::error_code& error,
                std::size_t bytesTransferred
                ) { onWrite(error, bytesTransferred); }
        );
    }
}

void WebSocketConnection::onRead(const boost::system::error_code & error, std::size_t bytesTransferred) {
    bool messageGood = true;

    if (!mConnected) {
        messageGood = false;
    } else if (error) {
        log::warning("Reading message error: {}", error.message());
        messageGood = false;
    } else if (!mWebSocket.got_binary()) {
        log::warning("Server sent non-binary data.");
        messageGood = false;
    } else if (bytesTransferred < cfg::MIN_MESSAGE_SIZE || bytesTransferred > cfg::MAX_MESSAGE_SIZE) {
        log::warning("Server sent data with out of bounds size.");
        messageGood = false;
    }

    if (!messageGood) {
        mRxBuffer.reset();
        mRxQueue.pop_back();
        mConnected = false;
        return;
    }

    mRxBuffer.emplace(boost::asio::dynamic_buffer(mRxQueue.emplace_back()));

    mWebSocket.async_read(
        mRxBuffer.value(),
        [this] (
            const boost::system::error_code & error,
            std::size_t bytesTransferred
        ) { onRead(error, bytesTransferred); }
    );
}


void WebSocketConnection::sendMessage(std::vector<std::byte> && message) {
    mTxQueue.push_back(std::move(message));

    if (mTxQueue.size() > 1 || !mConnected) {
        // write already queued
        return;
    }
    mWebSocket.async_write(
        boost::asio::buffer(mTxQueue.front()),
        [this] (
            const boost::system::error_code & error,
            std::size_t bytesTransferred
        ) { onWrite(error, bytesTransferred); }
    );
}

void WebSocketConnection::onWrite(const boost::system::error_code & error, std::size_t bytesTransferred) {
    bool messageGood = true;

    if (!mConnected) {
        messageGood = false;
    } else if (error) {
        log::warning("Reading message error: {}", error.message());
        messageGood = false;
    }

    if (!messageGood) {
        mConnected = false;
        return;
    }

    mTxQueue.pop_front();

    if (mTxQueue.size() == 0) {
        // nothing to send
        return;
    }

    mWebSocket.async_write(
        boost::asio::buffer(mTxQueue.front()),
        [this] (
            const boost::system::error_code & error,
            std::size_t bytesTransferred
        ) { onWrite(error, bytesTransferred); }
    );
}
