#pragma once

#include <glad/glad.h>

#include "VoxelInfo.hpp"
#include "Gui.hpp"

class VoxelTexture {
public:
    VoxelTexture(const VoxelTextureAtlas & voxelTileAtlas);
    ~VoxelTexture();
    void bind(GLuint textureUnit) const;
    void unbind(GLuint textureUnit) const;
    void updateSampler(
        GLint magnifyingFilter,
        GLint minifyingFilter,
        GLint minMipmap,
        GLint maxMipmap,
        GLfloat anisotropyLevel
    );

    int maxMipLevel() const noexcept { return mMaxMipLevel; }

private:
    GLuint mTexture = 0;
    GLuint mSampler = 0;
    int mMaxMipLevel = 0;

};
