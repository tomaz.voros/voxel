#include "VoxelInfo.hpp"

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <algorithm>
#include <limits>

#include <nlohmann/json.hpp>
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>

#include <util.hpp>

void VoxelInfo::processAssetMetaFile(const std::filesystem::path & assetPath) {
    // TODO: merge sides and model
    // TODO: assign default values to as many parameters as possible
    const auto assetMetaJson = nlohmann::json::parse(
        util::loadFile<char>(assetPath / "voxels.json")
    );

    const integer_type resolution = assetMetaJson.at("resolution");
    if (resolution < MIN_RESOLUTION || resolution > MAX_RESOLUTION)
        throw std::runtime_error("Unsupported tile resolution.");
    const auto tileWidth = resolution * TILE_GRANULARITY;
    if (tileWidth < MIN_TILE_WIDTH || tileWidth > MAX_TILE_WIDTH || !util::ispow2(tileWidth))
        throw std::runtime_error("Unsupported tiles per row count.");

    auto voxelNameCharactersValid = [] (const std::string & name) -> bool {
        for (const auto & c : name) {
            const auto lowerCase = c >= 'a' && c <= 'z';
            const auto upperCase = c >= 'A' && c <= 'Z';
            const auto number = c >= '0' && c <= '9';
            const auto space = c == ' ';
            // TODO: remove underscore
            const auto underscore = c == '_';
            const auto valid = lowerCase || upperCase || number || space || underscore;
            if (!valid)
                return false;
        }
        return true;
    };

    std::unordered_set<std::string> textureFileNames;
    integer_type maxVoxelIndex = 0;
    std::unordered_set<integer_type> voxelIndices;
    std::unordered_set<std::string> voxelNames;
    const std::string defaultTextureFileName = assetMetaJson.at("default_file_name");
    for (const auto & voxel : assetMetaJson.at("voxels")) {
        // index
        const integer_type voxelIndex = voxel.at("index");
        if (voxelIndex < MIN_VOXEL_INDEX || voxelIndex > MAX_VOXEL_INDEX)
            throw std::runtime_error("Out of bounds voxel index.");
        maxVoxelIndex = std::max(maxVoxelIndex, voxelIndex);
        const auto indexInsertionResult = voxelIndices.insert(voxelIndex);
        if (!indexInsertionResult.second)
            throw std::runtime_error("Duplicate voxel index.");
        // name
        std::string voxelName = voxel.at("name");
        if (voxelName.size() < MIN_VOXEL_NAME_SIZE || voxelName.size() > mData.maxNameLength)
            throw std::runtime_error("Voxel name too long or too short.");
        if (!voxelNameCharactersValid(voxelName))
            throw std::runtime_error("Invalid characters in voxel name.");
        const auto nameInsertionResult = voxelNames.insert(std::move(voxelName));
        if (!nameInsertionResult.second)
            throw std::runtime_error("Duplicate voxel name.");
        // sides
        const auto & sides = voxel.at("sides"); // TODO: make sides element optional
        std::array<bool, 6> sidesPresent{};
        std::fill_n(sidesPresent.begin(), sidesPresent.size(), false);
        for (const auto & side : sides) {
            const integer_type sideIndex = side.at("index");
            if (sideIndex < 0 || sideIndex > 5)
                throw std::runtime_error("Invalid voxel side index.");
            if (sidesPresent[sideIndex])
                throw std::runtime_error("Duplicate voxel side index.");
            sidesPresent[sideIndex] = true;
        }
        const auto & model = voxel.find("model");
        // textures
        for (const auto & side : sides) {
            std::string fileName = side.at("file_name");
            textureFileNames.insert(std::move(fileName));
        }
        if (model != voxel.end()) {
            for (const auto & quad : *model) {
                std::string fileName = quad.at("file_name");
                textureFileNames.insert(std::move(fileName));
            }
        }
        // model
        if (model == voxel.end())
            continue;
        for (const auto & quad : *model) {
            const integer_type x = quad.at("x");
            const integer_type y = quad.at("y");
            const integer_type z = quad.at("z");
            const integer_type width = quad.at("width");
            const integer_type height = quad.at("height");
            const integer_type u = quad.value("u", integer_type{0});
            const integer_type v = quad.value("v", integer_type{0});
            const integer_type sideIndex = quad.at("side");
            // TODO: more appropriate name than file_name (also see sides)
            const std::string fileName = quad.at("file_name");

            const auto validXYZ =
                x <= TILE_GRANULARITY &&
                y <= TILE_GRANULARITY &&
                z <= TILE_GRANULARITY &&
                x >= 0 && y >= 0 && z >= 0;
            const auto validDimensions =
                width <= TILE_GRANULARITY &&
                height <= TILE_GRANULARITY &&
                width >= 1 && height >= 1;
            const auto validUV =
                u < TILE_GRANULARITY && v < TILE_GRANULARITY &&
                u >= 0 && v >= 0;
            const auto validSide = sideIndex >= 0 && sideIndex <= 5;

            bool validRange = false;
            bool validUVRange = false;
            // TODO: document and check which is width and which is height and when
            switch (sideIndex) {
                // TODO: correct checking, consider rotate and mirror when checking ranges
                // TODO: also check if uv are in valid range
                // TODO: do the same for sides
            case 0: case 1:
                validRange = y + width <= TILE_GRANULARITY && z + height <= TILE_GRANULARITY;
                break;
            case 2: case 3:
                validRange = x + width <= TILE_GRANULARITY && z + height <= TILE_GRANULARITY;
                break;
            case 4: case 5:
                validRange = x + width <= TILE_GRANULARITY && y + height <= TILE_GRANULARITY;
                break;
            }
            if (!validXYZ || !validDimensions || !validSide || !validRange)
                throw std::runtime_error("Invalid voxel model.");
        }
    }

    voxelIndices = {};
    voxelNames = {};

    const auto pathPrefix = assetPath / std::string{ assetMetaJson.at("file_prefix") };
    const auto atlasMetaData = packTextureAtlas(defaultTextureFileName, textureFileNames, assetPath, tileWidth, pathPrefix);

    textureFileNames = {};

    mData.lookupTable.resize(maxVoxelIndex + 1, {});
    for (auto & quad : mData.lookupTable) {
        std::fill_n(quad.sides.begin(), quad.sides.size(), 0);
        std::fill_n(quad.model.begin(), quad.model.size(), 0);
    }

    mData.names.resize(maxVoxelIndex + 1);
    for (auto & name : mData.names)
        std::fill_n(name.begin(), name.size(), '\0');

    mData.quadData.push_back({}); // insert dummy at position 0

    for (const auto & voxel : assetMetaJson.at("voxels")) {
        const integer_type voxelIndex = voxel.at("index");
        assert(voxelIndex >= MIN_VOXEL_INDEX && voxelIndex < mData.lookupTable.size());
        auto & lookupTable = mData.lookupTable[voxelIndex];

        const std::string name = voxel.at("name");
        assert(name.size() >= MIN_VOXEL_NAME_SIZE && name.size() <= mData.names[voxelIndex].size());
        std::copy_n(name.begin(), name.size(), mData.names[voxelIndex].begin());
        mData.names[voxelIndex][name.size()] = '\0';

        // TODO: make sides not required
        const auto & sides = voxel.at("sides");
        for (const auto & side : sides) {
            // TODO: unify naming sides::index vs model::side
            const integer_type sideIndex = side.at("index");
            assert(sideIndex >= 0 && sideIndex <= 5);
            const integer_type rotationAmount = side.value("rotate", 0);
            if (rotationAmount < 0 || rotationAmount > 3)
                throw std::runtime_error("Out of bounds rotation amount.");
            const bool doTranspose = side.value("transpose", false);
            // TODO: more appropriate name than file_name (also see model)
            const std::string fileName = side.at("file_name");

            assert(mData.quadData.size() <= std::numeric_limits<std::remove_reference<decltype(lookupTable.sides[0])>::type>::max());
            lookupTable.sides[sideIndex] = mData.quadData.size();

            const auto texture = atlasMetaData.find(fileName);
            assert(texture != atlasMetaData.end());

            auto vertexNotPacked = util::VertexNotPacked::makeDefault();
            vertexNotPacked.transpose = doTranspose ? 1 : 0;
            vertexNotPacked.rotate_by = rotationAmount;
            vertexNotPacked.side_index = sideIndex;
            vertexNotPacked.size_x = TILE_GRANULARITY - 1;
            vertexNotPacked.size_y = TILE_GRANULARITY - 1;
            // // will be added to the voxel offset
            // if (sideIndex == integer_type{1})
            //     vertexNotPacked.position_x = cfg::FULL_QUAD_SIZE[0];
            // // will be added to the voxel offset
            // if (sideIndex == integer_type{3})
            //     vertexNotPacked.position_y = cfg::FULL_QUAD_SIZE[1];
            // // will be added to the voxel offset
            // if (sideIndex == integer_type{5})
            //     vertexNotPacked.position_z = cfg::FULL_QUAD_SIZE[2];
            vertexNotPacked.texture_position_x = texture->second[0];
            vertexNotPacked.texture_position_y = texture->second[1];
            assert(vertexNotPacked.hasValidValues());

            VoxelQuad quadData{};
            util::packQuad(quadData.data(), vertexNotPacked);
            mData.quadData.push_back(quadData);
        }

        const auto & model = voxel.find("model");
        if (model != voxel.end()) {
            assert(mData.quadData.size() <= std::numeric_limits<std::remove_reference<decltype(lookupTable.model[0])>::type>::max());
            lookupTable.model[0] = mData.quadData.size();
            for (const auto & quad : *model) {
                // TODO: texture offset (also update in json schema and above check code)
                // TODO: texture transpose (also update in json schema and above check code)
                // TODO: texture rotate (also update in json schema and above check code)
                const integer_type x = quad.at("x");
                const integer_type y = quad.at("y");
                const integer_type z = quad.at("z");
                // TODO: maybe call it size_x
                const integer_type width = quad.at("width");
                // TODO: maybe call it size_y
                const integer_type height = quad.at("height");
                const integer_type u = quad.value("u", integer_type{0});
                const integer_type v = quad.value("v", integer_type{0});
                const integer_type sideIndex = quad.at("side");
                // TODO: more appropriate name than file_name (also see sides)
                const std::string fileName = quad.at("file_name");

                const integer_type rotationAmount = quad.value("rotate", 0);
                if (rotationAmount < 0 || rotationAmount > 3)
                    throw std::runtime_error("Out of bounds rotation amount.");
                const bool doTranspose = quad.value("transpose", false);

                const auto texture = atlasMetaData.find(fileName);
                assert(texture != atlasMetaData.end());

                auto vertexNotPacked = util::VertexNotPacked::makeDefault();
                vertexNotPacked.transpose = doTranspose ? 1 : 0;
                vertexNotPacked.rotate_by = rotationAmount;
                vertexNotPacked.texture_position_x = texture->second[0] + u;
                vertexNotPacked.texture_position_y = texture->second[1] + v;
                assert(width > 0 && height > 0);
                vertexNotPacked.size_x = width - 1;
                vertexNotPacked.size_y = height - 1;
                // will be added to the voxel offset
                vertexNotPacked.position_x = x;
                // will be added to the voxel offset
                vertexNotPacked.position_y = y;
                // will be added to the voxel offset
                vertexNotPacked.position_z = z;
                vertexNotPacked.side_index = sideIndex;
                assert(vertexNotPacked.hasValidValues());

                VoxelQuad quadData{};
                util::packQuad(quadData.data(), vertexNotPacked);
                mData.quadData.push_back(quadData);
            }
            assert(mData.quadData.size() <= std::numeric_limits<std::remove_reference<decltype(lookupTable.model[1])>::type>::max());
            lookupTable.model[1] = mData.quadData.size();
        }
    }
}

VoxelInfo::AtlasPackingData VoxelInfo::packTextureAtlas(
    const std::string & defaultTextureFileName,
    const std::unordered_set<std::string> & textureFileNames,
    const std::filesystem::path & assetPath,
    integer_type tileWidth,
    const std::filesystem::path & pathPrefix
) {
    AtlasPackingData result{};

    assert(tileWidth >= MIN_TILE_WIDTH && tileWidth <= MAX_TILE_WIDTH && util::ispow2(tileWidth));
    // TODO: support for different size textures

    // TODO: texture atlas ARRAY to reduce the amount of wasted memory (an atlas could be half empty)

    const auto tileCount = textureFileNames.size();
    if (tileCount < 1)
        throw std::runtime_error("Empty texture atlas packer input.");

    auto tilesPerRow = static_cast<integer_type>(std::ceil(std::sqrt(static_cast<double>(tileCount))));
    if (tilesPerRow * tilesPerRow < tileCount || (tilesPerRow - 1) * (tilesPerRow - 1) >= tileCount)
        throw std::runtime_error("Unexpected float computation issue.");
    // round up to next power of 2
    tilesPerRow = util::ceil2(tilesPerRow);
    if (tilesPerRow > MAX_TILES_PER_ROW)
        throw std::runtime_error("Too many tiles to pack.");

    mAtlas.tilesPerRow = tilesPerRow;
    mAtlas.tileWidth = tileWidth;
    mAtlas.width = mAtlas.tilesPerRow * mAtlas.tileWidth;

    mAtlas.data.resize(mAtlas.width * mAtlas.width * mAtlas.channelCount);
    std::fill_n(mAtlas.data.begin(), mAtlas.data.size(), 0);

    std::array<integer_type, 2> tileOffset{};
    tileOffset[0] = 0;
    tileOffset[1] = 0;

    // const auto path = assetPath / pathPrefix;
    const auto path = pathPrefix;

    auto addTexture = [&] (auto & textureFileName) {
        assert(tileOffset[1] < mAtlas.tilesPerRow);
        const auto filePath = path / textureFileName;
        int w, h, c;
        auto image = stbi_load(filePath.string().c_str(), &w, &h, &c, mAtlas.channelCount);
        if (image == nullptr)
            throw std::runtime_error("Can not load tile texture.");
        if (w != mAtlas.tileWidth || h != mAtlas.tileWidth) {
            stbi_image_free(image);
            throw std::runtime_error("Invalid tile texture dimensions.");
        }

        texture_offset_type textureOffset{};
        textureOffset[0] = tileOffset[0] * TILE_GRANULARITY;
        textureOffset[1] = tileOffset[1] * TILE_GRANULARITY;

        result.emplace(textureFileName, textureOffset);

        util::blit2D<std::uint8_t, VoxelTextureAtlas::channelCount>(
            image,
            mAtlas.data.data(),
            mAtlas.tileWidth,
            mAtlas.tileWidth,
            mAtlas.width,
            mAtlas.width,
            0, 0,
            static_cast<std::size_t>(tileOffset[0]) * mAtlas.tileWidth,
            static_cast<std::size_t>(mAtlas.tilesPerRow - 1 - tileOffset[1]) * mAtlas.tileWidth,
            mAtlas.tileWidth,
            mAtlas.tileWidth
        );

        stbi_image_free(image);

        tileOffset[0] += 1;
        if (tileOffset[0] == mAtlas.tilesPerRow) {
            tileOffset[0] = 0;
            tileOffset[1] += 1;
        }
    };

    addTexture(defaultTextureFileName);
    for (const auto & textureFileName : textureFileNames) {
        addTexture(textureFileName);
    }

#if 0
    stbi_write_png("test.png", mAtlas.width, mAtlas.width, mAtlas.channelCount, mAtlas.data.data(), 0);
#endif
    util::flipYInPlace<std::uint8_t, VoxelTextureAtlas::channelCount>(mAtlas.data.data(), mAtlas.width, mAtlas.width);
#if 0
    stbi_write_png("test_post_flip.png", mAtlas.width, mAtlas.width, mAtlas.channelCount, mAtlas.data.data(), 0);
#endif

    return result;
}
