#pragma once

#include <cassert>
#include <cstdint>
#include <algorithm>

// TODO: template size
class FullOctree {
public:
    FullOctree() {
        constexpr size_t STORAGE_SIZE = 299592; // 8/7*(8^N-1)
        constexpr size_t BYTE_COUNT = STORAGE_SIZE / 8;
        static_assert(STORAGE_SIZE % 8 == 0);
        constexpr size_t DEPTH = 6;

        mOcTree.resize(BYTE_COUNT, 0);

    }

    static size_t firstChildIndex(size_t index) {
        return (nodeIndex + 1) * 8;
    }

    static size_t parentIndex(size_t index) {
        return index / 8 - 1;
    }

private:
    std::vector<uint8_t> mOcTree;

};

namespace tree {
    using int_type = std::int32_t;

    inline constexpr int_type NODE_SIZE_X = 4;
    inline constexpr int_type NODE_SIZE_Y = 4;
    inline constexpr int_type NODE_SIZE_Z = 4;
    inline constexpr int_type NODE_VOLUME = NODE_SIZE_X * NODE_SIZE_Y * NODE_SIZE_Z;

    static_assert(NODE_SIZE_X > 0 && NODE_SIZE_Y > 0 && NODE_SIZE_Z > 0);

    template <typename T>
    struct node {
        node * parent;
        union {
            // TODO: maybe index into allocated block to reduce memory consumption (pointer == 8 bytes, index == 2, 3 or 4 bytes)

            // TODO: rename to leaf
            node * child[NODE_VOLUME];
            T * leaf[NODE_VOLUME];
        };
    };

    template <typename T>
    class iterator {
    public:
        // TODO: maybe raii with pointers and move node<T> inside of this class

        using node_type = node<T>;

        // getters

        int_type getLevel() const {
            return current_level;
        }

        node_type * getParent() const {
            assert(current_node != nullptr);
            return current_node->parent;
        }

        node_type * getCurrent() const {
            assert(current_node != nullptr);
            return current_node;
        }

        node_type * getChild(int_type x, int_type y, int_type z) const {
            assert(current_node != nullptr);
            assert(current_level > 0);
            return current_node->child[indexFromRelativePosition(x, y, z)];
        }

        T * getLeaf(int_type x, int_type y, int_type z) const {
            assert(current_node != nullptr);
            assert(current_level == 0);
            return current_node->leaf[indexFromRelativePosition(x, y, z)];
        }

        // setters

        void setAndInitParent(node_type * newParent) {
            assert(current_node != nullptr);
            assert(current_node->parent == nullptr);
            current_node->parent = newParent;
            newParent->parent = nullptr;
            std::fill_n(newParent->child, NODE_VOLUME, nullptr);
            newParent->child[indexInParentFromCurrentPosition(current_x, current_y, current_z)] = current_node;
        }

        void setAndInitCurrent(node_type * newCurrent, int_type newLevel, int_type x, int_type y, int_type z) {
            assert(current_node == nullptr);
            current_node = newCurrent;
            current_level = newLevel;
            current_x = x;
            current_y = y;
            current_z = z;
            newCurrent->parent = nullptr;
            if (newLevel > 0)
                std::fill_n(newCurrent->child, NODE_VOLUME, nullptr);
            else
                std::fill_n(newCurrent->leaf, NODE_VOLUME, nullptr);
        }

        void setAndInitChild(node_type * newChild, int_type x, int_type y, int_type z) {
            assert(current_node != nullptr);
            assert(current_level > 0);
            assert(current_node->child[indexFromRelativePosition(x, y, z)] == nullptr);
            current_node->child[indexFromRelativePosition(x, y, z)] = newChild;
            newChild->parent = current_node;
            if (current_level > 1)
                std::fill_n(newCurrent->child, NODE_VOLUME, nullptr);
            else
                std::fill_n(newCurrent->leaf, NODE_VOLUME, nullptr);
        }

        void setLeaf(T * newLeaf, int_type x, int_type y, int_type z) {
            assert(current_node != nullptr);
            assert(current_level == 0);
            assert(current_node->leaf[indexFromRelativePosition(x, y, z)] == nullptr);
            current_node->leaf[indexFromRelativePosition(x, y, z)] = newLeaf;
        }

        // advancing

        void stepIn(int_type x, int_type y, int_type z) {
            assert(current_node != nullptr);
            assert(current_level > 0);
            assert(current_node->child[indexFromRelativePosition(x, y, z)] != nullptr);
            current_node = current_node->child[indexFromRelativePosition(x, y, z)];
            current_level = current_level - 1;
            // TODO: overflow checks
            current_x = current_x * NODE_SIZE_X + x;
            current_y = current_y * NODE_SIZE_Y + y;
            current_z = current_z * NODE_SIZE_Z + z;
        }

        void stepOut() {
            assert(current_node != nullptr);
            assert(current_node->parent != nullptr);
            current_node = current_node->parent;
            current_level = current_level + 1;
            current_x = (current_x + static_cast<int_type>(current_x < 0)) / NODE_SIZE_X - static_cast<int_type>(current_x < 0);
            current_y = (current_y + static_cast<int_type>(current_y < 0)) / NODE_SIZE_Y - static_cast<int_type>(current_y < 0);
            current_z = (current_z + static_cast<int_type>(current_z < 0)) / NODE_SIZE_Z - static_cast<int_type>(current_z < 0);
        }

        // safe advancing

        bool stepInCreate(node_type * allocatedNode, int_type x, int_type y, int_type z) {
            assert(current_node != nullptr);
            assert(current_level > 0);
            node_type * * childNode = current_node->child + indexFromRelativePosition(x, y, z);
            const bool nodeAdded = *childNode == nullptr;
            if (nodeAdded) {
                assert(allocatedNode != nullptr);
                *childNode = allocatedNode;
                allocatedNode->parent = current_node;
                if (current_level > 1)
                    std::fill_n(allocatedNode->child, NODE_VOLUME, nullptr);
                else
                    std::fill_n(allocatedNode->leaf, NODE_VOLUME, nullptr);
            }
            current_node = *childNode;
            current_level = current_level - 1;
            // TODO: overflow checks
            current_x = current_x * NODE_SIZE_X + x;
            current_y = current_y * NODE_SIZE_Y + y;
            current_z = current_z * NODE_SIZE_Z + z;
            return nodeAdded;
        }

        bool stepOutCreate(node_type * allocatedNode) {
            assert(current_node != nullptr);
            const bool nodeAdded = current_node->parent == nullptr;
            if (nodeAdded) {
                assert(allocatedNode != nullptr);
                current_node->parent = allocatedNode;
                allocatedNode->parent = nullptr;
                std::fill_n(allocatedNode->child, NODE_VOLUME, nullptr);
                allocatedNode->child[indexInParentFromCurrentPosition(current_x, current_y, current_z)] = current_node;
            }
            current_node = current_node->parent;
            current_level = current_level + 1;
            current_x = (current_x + static_cast<int_type>(current_x < 0)) / NODE_SIZE_X - static_cast<int_type>(current_x < 0);
            current_y = (current_y + static_cast<int_type>(current_y < 0)) / NODE_SIZE_Y - static_cast<int_type>(current_y < 0);
            current_z = (current_z + static_cast<int_type>(current_z < 0)) / NODE_SIZE_Z - static_cast<int_type>(current_z < 0);
        }

        // defragmenting helpers

        node_type moveCurrent(node_type * target) {
            assert(target != nullptr);
            assert(current_node != nullptr);
            *target = *current_node;
            current_node = target;
            if (current_level > 0)
                std::for_each_n(current_node->child, NODE_VOLUME, [target] (auto & c) { c->parent = target; });
        }

        // TODO: current_x/y/z shifting

        // TODO: current_x/y/z getter

    private:
        node_type * current_node = nullptr;
        int_type current_level = 0;
        int_type current_x = 0;
        int_type current_y = 0;
        int_type current_z = 0;

        // TODO: specialize index computation for 2^N dimensions (possible with bitwise operations)

        static int_type indexFromRelativePosition(int_type x, int_type y, int_type z) {
            assert(x >= 0 && x < NODE_SIZE_X);
            assert(y >= 0 && y < NODE_SIZE_Y);
            assert(z >= 0 && z < NODE_SIZE_Z);
            return (NODE_SIZE_Y * NODE_SIZE_X) * z + NODE_SIZE_X * y + x;
        }

        static int_type indexInParentFromCurrentPosition(int_type x, int_type y, int_type z) {
            x = x % NODE_SIZE_X;
            y = y % NODE_SIZE_Y;
            z = z % NODE_SIZE_Z;
            if (x < 0) x += NODE_SIZE_X;
            if (y < 0) y += NODE_SIZE_Y;
            if (z < 0) z += NODE_SIZE_Z;
            return (NODE_SIZE_Y * NODE_SIZE_X) * z + NODE_SIZE_X * y + x;
        }

    };

}
