#pragma once

#include <array>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <GLFW/glfw3.h>

class CameraControl {
public:
    CameraControl();

    void setSpeed(float speed) { mSpeed = speed; }
    // keys: forwards, backwards, right, left, up, down
    void update(float dt, const std::array<bool, 6> & keys, glm::vec2 mouseDelta, const GLFWgamepadstate & gamepadState);

    float getPitch() const { return mPitch; }
    float getYaw() const { return mYaw; }
    const glm::vec3 & getPosition() const { return mPosition; }
    const glm::vec3 & getFacing() const { return mFacing; }

private:
    glm::vec3 mPosition;
    float mYaw;
    float mPitch;
    glm::vec3 mFacing;
    glm::vec3 mFront;
    glm::vec3 mRight;
    glm::vec3 mUp;

    float mInverseMouseSensitivity;
    float mSpeed;

    static constexpr float MAX_PITCH{ 3.14159f / 2.0f - 0.001f };
    static constexpr float AXIS_THRESHOLD{0.25f};
    static constexpr float TRIGGER_THRESHOLD{0.05f};
    static constexpr float RIGHT_THUMB_SENSITIVITY{20.0f};
    static constexpr float INPUT_POWER{2.0f};
    static_assert(AXIS_THRESHOLD > 0.0f);
    static_assert(RIGHT_THUMB_SENSITIVITY > 0.0f);
    static_assert(TRIGGER_THRESHOLD > 0.0f);

};
