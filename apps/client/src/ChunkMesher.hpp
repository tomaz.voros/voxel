#pragma once

#include <vector>
#include <array>
#include "EngineRendererComm.hpp"
#include "cfg.hpp"
#include "VoxelInfo.hpp"

struct BlockFaceData {
    cfg::Block block;
    uint8_t side;
    uint8_t occlusion;
};

class ChunkMesher {
public:
    std::vector<EngineRendererComm::Vertex> createMesh(
        const std::array<const cfg::Block *, 8> & chunks,
        const VoxelMetaData & voxelTileInfo
    );

};
