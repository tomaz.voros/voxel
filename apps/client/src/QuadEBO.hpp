#pragma once

#include <glad/glad.h>
#include <vector>

class QuadEBO
{
public:
    QuadEBO() :
        mIndices{ 0 },
        mEBO{ 0 }
    {}

    void bind() {
        if (mEBO == 0) glGenBuffers(1, &mEBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
    }

    void resize(GLsizeiptr newElementCount) {
        if (newElementCount <= mIndices) return;
        // get next multiple of 6
        newElementCount = ((newElementCount + 5) / 6) * 6;
        std::vector<GLuint> indices;
        indices.reserve(newElementCount);
        // generate
        for (GLuint a = 0; indices.size() < static_cast<std::size_t>(newElementCount); a += 4) {
            indices.emplace_back(a + 0); indices.emplace_back(a + 1); indices.emplace_back(a + 2);
            indices.emplace_back(a + 2); indices.emplace_back(a + 3); indices.emplace_back(a + 0);
        }
        // upload
        bind();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), indices.data(), GL_STATIC_DRAW);
        mIndices = newElementCount;
    }

    GLenum type() { return GL_UNSIGNED_INT; }
    GLsizeiptr size() { return mIndices; }

private:
    GLsizeiptr mIndices;
    GLuint mEBO;

};
