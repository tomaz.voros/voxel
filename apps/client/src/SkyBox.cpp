#include "SkyBox.hpp"
#include <vector>
#include <string>
#include "stb/stb_image.h"
#include "Log.hpp"
#include <array>
#include <glm/gtc/type_ptr.hpp>

static constexpr std::array<int8_t, 6 * 6 * 3> SKYBOX_VERTICES{ {
    -1,  1, -1,
    -1, -1, -1,
     1, -1, -1,
     1, -1, -1,
     1,  1, -1,
    -1,  1, -1,

    -1, -1,  1,
    -1, -1, -1,
    -1,  1, -1,
    -1,  1, -1,
    -1,  1,  1,
    -1, -1,  1,

     1, -1, -1,
     1, -1,  1,
     1,  1,  1,
     1,  1,  1,
     1,  1, -1,
     1, -1, -1,

    -1, -1,  1,
    -1,  1,  1,
     1,  1,  1,
     1,  1,  1,
     1, -1,  1,
    -1, -1,  1,

    -1,  1, -1,
     1,  1, -1,
     1,  1,  1,
     1,  1,  1,
    -1,  1,  1,
    -1,  1, -1,

    -1, -1, -1,
    -1, -1,  1,
     1, -1, -1,
     1, -1, -1,
    -1, -1,  1,
     1, -1,  1
} };

SkyBox::SkyBox(
    const std::vector<std::filesystem::path> & fileNames, size_t width, size_t height,
    const std::vector<Shader::Source> & shaderSource
) {
    // vertices
    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    glBindVertexArray(mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, SKYBOX_VERTICES.size() * sizeof(SKYBOX_VERTICES[0]), SKYBOX_VERTICES.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_BYTE, GL_TRUE, 3 * sizeof(SKYBOX_VERTICES[0]), (void*)0);

    // shader
    mShader.reload(shaderSource);
    mVP = glGetUniformLocation(mShader.id(), "uVP");
    mSkyBox = glGetUniformLocation(mShader.id(), "uSkyBox");
    mCameraWorldSpacePosition = glGetUniformLocation(mShader.id(), "uCameraWorldSpacePosition");

    // texture
    glGenTextures(1, &mTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, mTexture);

    for (size_t i = 0; i < 6; ++i) {
        int w = -1, h = -1, c = -1;
        unsigned char * data = stbi_load(fileNames[i].string().c_str(), &w, &h, &c, 3);
        if (data != nullptr && w == width && h == height) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        } else {
            log::error("Error loading cubemap side: {}", fileNames[i].string());
        }
        if (data != nullptr)
            stbi_image_free(data);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void SkyBox::draw(const glm::mat4 & view, const glm::mat4 & projection, const glm::vec3 & cameraPosition) {
    GLint depthFunc = -1;
    glGetIntegerv(GL_DEPTH_FUNC, &depthFunc);
    glDepthFunc(GL_LEQUAL);
    mShader.use();
    const auto VP = projection * glm::mat4{ glm::mat3{ view } };
    glUniformMatrix4fv(mVP, 1, GL_FALSE, glm::value_ptr(VP));
    glUniform3f(mCameraWorldSpacePosition, cameraPosition.x, cameraPosition.y, cameraPosition.z);
    glBindVertexArray(mVAO);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, mTexture);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glDepthFunc(depthFunc);
}
