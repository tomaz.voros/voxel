#include "Input.hpp"

#include <GLFW/glfw3.h>

#include "Log.hpp"

std::map<int, Input::JoystickData> Input::sJoysticks;

Input::Input(GLFWwindow * window) :
    mCursor{ 0.0, 0.0 },
    mScroll{ 0.0, 0.0 },
    mLastCursor{ 0.0, 0.0 }
{
    std::fill(std::begin(mKeys), std::end(mKeys), false);
    std::fill(std::begin(mButtons), std::end(mButtons), false);

    glfwSetWindowUserPointer(window, this);

    glfwSetKeyCallback(window, Input::keyCallback);
    glfwSetMouseButtonCallback(window, Input::mouseButtonCallback);
    glfwSetCursorPosCallback(window, Input::cursorPosCallback);
    glfwSetScrollCallback(window, Input::scrollCallback);

    for (int joystick_id = GLFW_JOYSTICK_1; joystick_id <= GLFW_JOYSTICK_LAST; joystick_id++) {
        const auto present = glfwJoystickPresent(joystick_id);
        if (present == GLFW_FALSE)
            continue;
        joystickCallback(joystick_id, GLFW_CONNECTED);
    }
    glfwSetJoystickCallback(Input::joystickCallback);

    double xPosition, yPosition;
    glfwGetCursorPos(window, &xPosition, &yPosition);
    Input::cursorPosCallback(window, xPosition, yPosition);
}

void Input::poll() {
    glfwPollEvents();
    for (auto & joystickData : sJoysticks)
        updateJoystick(joystickData.first, joystickData.second.state);
}

bool Input::key(int key) const {
    if (key < 0 || key >= mKeys.size()) return false;
    return mKeys[key];
}

bool Input::button(int button) const {
    if (button < 0 || button >= mButtons.size()) return false;
    return mButtons[button];
}

glm::dvec2 Input::cursorDelta() {
    const auto delta = mCursor - mLastCursor;
    mLastCursor = mCursor;
    return delta;
}

glm::dvec2 Input::scrollDelta() {
    const auto delta = mScroll;
    mScroll.x = 0.0;
    mScroll.y = 0.0;
    return delta;
}

void Input::keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    Input * input = static_cast<Input *>(glfwGetWindowUserPointer(window));

    if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GL_TRUE);
        return;
    }

    if (key < 0 || key >= input->mKeys.size()) return;

    if (action == GLFW_PRESS)
        input->mKeys[key] = true;
    else if (action == GLFW_RELEASE)
        input->mKeys[key] = false;
}

void Input::mouseButtonCallback(GLFWwindow * window, int button, int action, int mods) {
    Input * input = static_cast<Input *>(glfwGetWindowUserPointer(window));
    if (button < 0 || button >= input->mButtons.size()) return;
    if (action == GLFW_PRESS)
        input->mButtons[button] = true;
    else if (action == GLFW_RELEASE)
        input->mButtons[button] = false;
}

void Input::cursorPosCallback(GLFWwindow * window, double xPos, double yPos) {
    Input * input = static_cast<Input *>(glfwGetWindowUserPointer(window));
    input->mCursor.x = xPos;
    input->mCursor.y = yPos;
}

void Input::scrollCallback(GLFWwindow * window, double xOffset, double yOffset) {
    Input * input = static_cast<Input *>(glfwGetWindowUserPointer(window));
    input->mScroll.x += xOffset;
    input->mScroll.y += yOffset;
}

void Input::joystickCallback(int joystick_id, int event) {
    if (event == GLFW_CONNECTED) {
        log::info("Joystick {} connected.", joystick_id);
    } else if (event == GLFW_DISCONNECTED) {
        log::info("Joystick {} disconnected.", joystick_id);
    }
    auto & joystickData = sJoysticks[joystick_id];
    joystickData = {};
    joystickData.name = glfwGetGamepadName(joystick_id);
    if (joystickData.name == nullptr) {
        joystickData.name = "";
        return;
    }
    updateJoystick(joystick_id, joystickData.state);
}

void Input::updateJoystick(int joystick_id, GLFWgamepadstate & outState) {
    const auto updateResult = glfwGetGamepadState(joystick_id, &outState);
    if (updateResult == GLFW_FALSE) {
        outState = {};
        log::warning("Failed to retrieve joystick {} state.", joystick_id);
    }
}

void Input::getJoystickNames(std::vector<int> & mappings, std::vector<const char *> & names) {
    names.resize(0);
    names.reserve(sJoysticks.size());
    mappings.resize(0);
    mappings.reserve(sJoysticks.size());
    for (const auto & joystick : sJoysticks) {
        names.push_back(joystick.second.name);
        mappings.push_back(joystick.first);
    }
}

GLFWgamepadstate Input::getJoystickState(int joystick_id) {
    const auto joystick = sJoysticks.find(joystick_id);
    if (joystick == sJoysticks.end())
        return {};
    return joystick->second.state;
}
