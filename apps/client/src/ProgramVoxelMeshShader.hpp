#pragma once

#include "ProgramBase.hpp"

class ProgramVoxelMeshShader : public ProgramBase {
public:
    static constexpr GLuint bufVertexData = 0;

    // static constexpr GLint uQuadCount = 0;
    static constexpr GLint uVP = 1;
    static constexpr GLint uOffset = 2;
    static constexpr GLint uCameraViewSpacePosition = 3;
    static constexpr GLint uVoxelTexture = 4;
    static constexpr GLint uFogColor = 5;
    static constexpr GLint uFogStart = 6;
    static constexpr GLint uFogTransitionDistance = 7;
    static constexpr GLint uV = 8;
    static constexpr GLint uAoShadeStrength = 9;

    void reload(const std::filesystem::path & assetPath) {
        ProgramBase::ShaderSourcePaths shaderSourcePaths;
        shaderSourcePaths.task = assetPath / "shaders/voxelMeshShader.task";
        shaderSourcePaths.mesh = assetPath / "shaders/voxelMeshShader.mesh";
        shaderSourcePaths.frag = assetPath / "shaders/voxelMeshShader.frag";
        ProgramBase::reload(shaderSourcePaths, "ProgramVoxelMeshShader");
    }

};
