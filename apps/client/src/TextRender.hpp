#pragma once

#include <filesystem>
#include <string_view>

#include <glad/glad.h>

#include "glutil.hpp"
#include <util.hpp>

class TextRender {
public:
    TextRender(const std::filesystem::path & resource_path);
    TextRender(const TextRender &) = delete;
    TextRender & operator = (const TextRender &) = delete;
    TextRender(TextRender && other) noexcept {
        std::swap(mTextureName, other.mTextureName);
        std::swap(mProgramName, other.mProgramName);
        std::swap(mVAO, other.mVAO);
        std::swap(mVBO, other.mVBO);
        std::swap(mVertexCount, other.mVertexCount);
    }
    TextRender & operator = (TextRender && other) noexcept {
        cleanup();
        std::swap(mTextureName, other.mTextureName);
        std::swap(mProgramName, other.mProgramName);
        std::swap(mVAO, other.mVAO);
        std::swap(mVBO, other.mVBO);
        std::swap(mVertexCount, other.mVertexCount);
        return *this;
    }
    ~TextRender() noexcept {
        cleanup();
    }

    void updateText(std::string_view text);
    void draw(const glutil::viewport & viewport, float scale);

private:
    inline static constexpr GLuint TEXTURE_UNIT = 0;
    inline static constexpr GLint SCALE_LOCATION = 0;
    inline static constexpr GLint OFFSET_LOCATION = 1;
    inline static constexpr GLint FONT_COLOR_LOCATION = 2;
    inline static constexpr GLint BACKGROUND_COLOR_LOCATION = 3;
    inline static constexpr GLint TEXTURE_LOCATION = 4;
    inline static constexpr float TEX_SCALE_X = 1.0f / 16.0f;
    inline static constexpr float TEX_SCALE_Y = 1.0f / 8.0f;
    inline static constexpr float CHAR_SIZE_X =  8.0f;
    inline static constexpr float CHAR_SIZE_Y = 16.0f;
    inline static constexpr int CHARS_PER_LINE = 16;
    inline static constexpr int CHAR_FROM = ' ';
    inline static constexpr int CHAR_TO = '~';
    inline static constexpr int CHAR_DUMMY = '?';
    inline static constexpr GLsizei ATLAS_WIDTH = CHARS_PER_LINE * CHAR_SIZE_X;
    inline static constexpr GLsizei ATLAS_HEGHT = ATLAS_WIDTH;

    static_assert(CHAR_DUMMY >= CHAR_FROM && CHAR_DUMMY <= CHAR_TO);

    GLuint mTextureName = 0;
    GLuint mProgramName = 0;
    GLuint mVAO = 0;
    GLuint mVBO = 0;
    GLsizei mVertexCount = 0;

    void cleanup() noexcept;
    void setupTexture(const std::filesystem::path & resource_path);
    std::vector<uint8_t> generateFontAtlas(const std::filesystem::path & resource_path);

};
