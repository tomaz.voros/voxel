#pragma once

#include <filesystem>

#include "glutil.hpp"
#include <util.hpp>

class PlotRender {
public:
	PlotRender(const std::filesystem::path& resource_path) {
		mProgramName = glutil::programFromVertFrag(
			resource_path / "shaders/line.vert",
			resource_path / "shaders/line.frag"
		);
		glCreateVertexArrays(1, &mVAO);
		glEnableVertexArrayAttrib(mVAO, 0);
		glVertexArrayAttribBinding(mVAO, 0, 0);
		glVertexArrayAttribFormat(mVAO, 0, 2, GL_FLOAT, GL_FALSE, offsetof(PlotVertex, posX));

		glCreateBuffers(1, &mVBO);
		glVertexArrayVertexBuffer(mVAO, 0, mVBO, 0, sizeof(PlotVertex));
	}

	PlotRender(const PlotRender&) = delete;

	PlotRender& operator = (const PlotRender&) = delete;

	PlotRender(PlotRender&& other) noexcept {
		std::swap(mProgramName, other.mProgramName);
		std::swap(mVAO, other.mVAO);
		std::swap(mVBO, other.mVBO);
		std::swap(mVertexCount, other.mVertexCount);
	}

	PlotRender& operator = (PlotRender&& other) noexcept {
		cleanup();
		std::swap(mProgramName, other.mProgramName);
		std::swap(mVAO, other.mVAO);
		std::swap(mVBO, other.mVBO);
		std::swap(mVertexCount, other.mVertexCount);
		return *this;
	}

	~PlotRender() {
		cleanup();
	}

	void setMinMax(float min, float max) {
		mMin = min;
		mMax = max;
	}

	void updatePlot(const float* data_x, const float * data_y, std::size_t length) {
		// TODO: clamping and stuff and scaling and stuff
		std::vector<PlotVertex> vertex_data;
		vertex_data.reserve(length + 5);

		for (std::size_t i = 0; i < length; ++i) {
			vertex_data.push_back({ data_x[i], std::clamp(data_y[i], mMin, mMax) });
		}

		vertex_data.push_back({ static_cast<float>(length > 1 ? length - 1 : 1), mMax });
		vertex_data.push_back({ 0.0f, mMax });
		vertex_data.push_back({ 0.0f, mMin });
		vertex_data.push_back({ static_cast<float>(length > 1 ? length - 1 : 1), mMin });
		if (length > 0) {
			vertex_data.push_back(*(vertex_data.end() - 5));
		} else {
			vertex_data.push_back(*(vertex_data.end() - 4));
		}

		glNamedBufferData(mVBO, vertex_data.size() * sizeof(PlotVertex), vertex_data.data(), GL_STREAM_DRAW);
		mVertexCount = vertex_data.size();
	}

	void draw(const glutil::viewport & viewport, float scale) {
		const float scaleX = 2.0f / static_cast<float>(viewport.width) * scale;
		const float scaleY = 2.0f / static_cast<float>(viewport.height) * scale;
		glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
		glUseProgram(mProgramName);
		glProgramUniform2f(mProgramName, SCALE_LOCATION, scaleX, scaleY);
		glProgramUniform2f(mProgramName, OFFSET_LOCATION, -1.0f, -1.0f);
		glProgramUniform4f(mProgramName, COLOR_LOCATION, 0.7f, 0.5f, 0.6f, 1.0f);
		glBindVertexArray(mVAO);
		glDrawArrays(GL_LINE_STRIP, 0, mVertexCount);
	}

private:
	inline static constexpr GLint SCALE_LOCATION = 0;
	inline static constexpr GLint OFFSET_LOCATION = 1;
	inline static constexpr GLint COLOR_LOCATION = 2;
	GLuint mTextureName = 0;
	GLuint mProgramName = 0;
	GLuint mVAO = 0;
	GLuint mVBO = 0;
	GLsizei mVertexCount = 0;
	float mMin = 0.0;
	float mMax = 1.0;

	struct PlotVertex {
		float posX;
		float posY;
	};

	void cleanup() {
		glDeleteProgram(mProgramName);
		glDeleteVertexArrays(1, &mVAO);
		glDeleteBuffers(1, &mVBO);
		mProgramName = 0;
		mVAO = 0;
		mVBO = 0;
		mVertexCount = 0;
	}

};