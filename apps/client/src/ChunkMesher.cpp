#include "ChunkMesher.hpp"

#include <cassert>

#include <glm/glm.hpp>

#include <util.hpp>

#include "Log.hpp"
#include "EngineRendererComm.hpp"

// TODO: refactor
namespace {
    inline constexpr bool isAir(cfg::Block b) {
        // TODO: improve this function to take more info into account
        return b == 0;
    }

    static_assert(cfg::CHUNK_SIZE_X > 0 && cfg::CHUNK_SIZE_X % 2 == 0, "Chunk size must be even.");
    static_assert(cfg::CHUNK_SIZE_Y > 0 && cfg::CHUNK_SIZE_Y % 2 == 0, "Chunk size must be even.");
    static_assert(cfg::CHUNK_SIZE_Z > 0 && cfg::CHUNK_SIZE_Z % 2 == 0, "Chunk size must be even.");
    // TODO: figure out wether jumb ochunk only needs to be larger by 1 for meshing (aka. no high coordinate border)
    static constexpr int32_t JUMBO_CHUNK_SIZE_X = cfg::CHUNK_SIZE_X + 2;
    static constexpr int32_t JUMBO_CHUNK_SIZE_Y = cfg::CHUNK_SIZE_Y + 2;
    static constexpr int32_t JUMBO_CHUNK_SIZE_Z = cfg::CHUNK_SIZE_Z + 2;
    static constexpr int32_t JUMBO_CHUNK_VOLUME = JUMBO_CHUNK_SIZE_X * JUMBO_CHUNK_SIZE_Y * JUMBO_CHUNK_SIZE_Z;

    void makeJumboChunk(
        const std::array<const cfg::Block *, 8> & chunks,
        std::array<cfg::Block, JUMBO_CHUNK_VOLUME> & jumboChunk
    ) {
        static constexpr std::array<glm::tvec3<int32_t>, 8> srcOffs{ {
            { cfg::CHUNK_SIZE_X / 2 - 1, cfg::CHUNK_SIZE_Y / 2 - 1, cfg::CHUNK_SIZE_Z / 2 - 1 },
            {                         0, cfg::CHUNK_SIZE_Y / 2 - 1, cfg::CHUNK_SIZE_Z / 2 - 1 },
            { cfg::CHUNK_SIZE_X / 2 - 1,                         0, cfg::CHUNK_SIZE_Z / 2 - 1 },
            {                         0,                         0, cfg::CHUNK_SIZE_Z / 2 - 1 },
            { cfg::CHUNK_SIZE_X / 2 - 1, cfg::CHUNK_SIZE_Y / 2 - 1,                         0 },
            {                         0, cfg::CHUNK_SIZE_Y / 2 - 1,                         0 },
            { cfg::CHUNK_SIZE_X / 2 - 1,                         0,                         0 },
            {                         0,                         0,                         0 },
        } };
        static constexpr std::array<glm::tvec3<int32_t>, 8> dstOffs{ {
            {                      0,                      0,                      0 },
            { JUMBO_CHUNK_SIZE_X / 2,                      0,                      0 },
            {                      0, JUMBO_CHUNK_SIZE_Y / 2,                      0 },
            { JUMBO_CHUNK_SIZE_X / 2, JUMBO_CHUNK_SIZE_Y / 2,                      0 },
            {                      0,                      0, JUMBO_CHUNK_SIZE_Z / 2 },
            { JUMBO_CHUNK_SIZE_X / 2,                      0, JUMBO_CHUNK_SIZE_Z / 2 },
            {                      0, JUMBO_CHUNK_SIZE_Y / 2, JUMBO_CHUNK_SIZE_Z / 2 },
            { JUMBO_CHUNK_SIZE_X / 2, JUMBO_CHUNK_SIZE_Y / 2, JUMBO_CHUNK_SIZE_Z / 2 },
        } };
        for (std::size_t i = 0; i < 8; ++i)
            util::blit3D<cfg::Block, 1>(
                chunks[i], jumboChunk.data(),
                cfg::CHUNK_SIZE_X, cfg::CHUNK_SIZE_Y, cfg::CHUNK_SIZE_Z,
                JUMBO_CHUNK_SIZE_X, JUMBO_CHUNK_SIZE_Y, JUMBO_CHUNK_SIZE_Z,
                srcOffs[i].x, srcOffs[i].y, srcOffs[i].z,
                dstOffs[i].x, dstOffs[i].y, dstOffs[i].z,
                JUMBO_CHUNK_SIZE_X / 2, JUMBO_CHUNK_SIZE_Y / 2, JUMBO_CHUNK_SIZE_Z / 2
            );
    }
}

std::vector<EngineRendererComm::Vertex> ChunkMesher::createMesh(
    const std::array<const cfg::Block *, 8> & chunks,
    const VoxelMetaData & voxelTileInfo
)  {
    using int_type = int32_t;
    using pos_vec_type = glm::tvec3<int_type>;

    static constexpr bool UNKNOWN_TO_DEFAULT = true;
    static constexpr bool UNKNOWN_TO_ONE = true;

    static_assert(cfg::CHUNK_SIZE_X == cfg::CHUNK_SIZE_Y && cfg::CHUNK_SIZE_X == cfg::CHUNK_SIZE_Z);
    static_assert(JUMBO_CHUNK_SIZE_X == JUMBO_CHUNK_SIZE_Y && JUMBO_CHUNK_SIZE_X == JUMBO_CHUNK_SIZE_Z);
    static_assert(JUMBO_CHUNK_SIZE_X == cfg::CHUNK_SIZE_X + 2);
    static_assert(JUMBO_CHUNK_SIZE_Y == cfg::CHUNK_SIZE_Y + 2);
    static_assert(JUMBO_CHUNK_SIZE_Z == cfg::CHUNK_SIZE_Z + 2);

    static constexpr int_type FROM{ 1 };
    static constexpr int_type TO{ JUMBO_CHUNK_SIZE_X - 1 };

    static constexpr pos_vec_type from{FROM, FROM, FROM};
    static constexpr pos_vec_type to{TO, TO, TO};

    static constexpr std::array<pos_vec_type, 6> NEIGHBOR_OFFSETS{{
        {-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}, {0, 0, -1}, {0, 0, 1},
    }};

    static constexpr pos_vec_type SCALE{ 1, JUMBO_CHUNK_SIZE_X, JUMBO_CHUNK_SIZE_X * JUMBO_CHUNK_SIZE_X };
    static constexpr pos_vec_type JUMBO_CHUNK_SIZE{JUMBO_CHUNK_SIZE_X, JUMBO_CHUNK_SIZE_Y, JUMBO_CHUNK_SIZE_Z};

    static constexpr std::array<std::uint32_t, 3> JUMBO_OFFSET{ { 1, 1, 1 } };

    static constexpr std::array<std::array<glm::tvec3<int32_t>, 8>, 6> AOS_OFFSETS{ {
        { { { -1, -1, -1 }, { -1,  0, -1 }, { -1,  1, -1 }, { -1, -1,  0 }, { -1,  1,  0 }, { -1, -1,  1 }, { -1,  0,  1 }, { -1,  1,  1 } } },
        { { {  1, -1, -1 }, {  1,  0, -1 }, {  1,  1, -1 }, {  1, -1,  0 }, {  1,  1,  0 }, {  1, -1,  1 }, {  1,  0,  1 }, {  1,  1,  1 } } },

        { { { -1, -1, -1 }, {  0, -1, -1 }, {  1, -1, -1 }, { -1, -1,  0 }, {  1, -1,  0 }, { -1, -1,  1 }, {  0, -1,  1 }, {  1, -1,  1 } } },
        { { { -1,  1, -1 }, {  0,  1, -1 }, {  1,  1, -1 }, { -1,  1,  0 }, {  1,  1,  0 }, { -1,  1,  1 }, {  0,  1,  1 }, {  1,  1,  1 } } },

        { { { -1, -1, -1 }, {  0, -1, -1 }, {  1, -1, -1 }, { -1,  0, -1 }, {  1,  0, -1 }, { -1,  1, -1 }, {  0,  1, -1 }, {  1,  1, -1 } } },
        { { { -1, -1,  1 }, {  0, -1,  1 }, {  1, -1,  1 }, { -1,  0,  1 }, {  1,  0,  1 }, { -1,  1,  1 }, {  0,  1,  1 }, {  1,  1,  1 } } },
    } };

    static constexpr std::array<std::array<std::array<std::uint8_t, 3>, 4>, 6> AO_OFFSETS{{
        {{ {{1, 3, 0}}, {{3, 6, 5}}, {{1, 4, 2}}, {{4, 6, 7}} }},
        {{ {{1, 3, 0}}, {{1, 4, 2}}, {{3, 6, 5}}, {{4, 6, 7}} }},

        {{ {{1, 3, 0}}, {{1, 4, 2}}, {{3, 6, 5}}, {{4, 6, 7}} }},
        {{ {{1, 3, 0}}, {{3, 6, 5}}, {{1, 4, 2}}, {{4, 6, 7}} }},

        {{ {{1, 3, 0}}, {{3, 6, 5}}, {{1, 4, 2}}, {{4, 6, 7}} }},
        {{ {{1, 3, 0}}, {{1, 4, 2}}, {{3, 6, 5}}, {{4, 6, 7}} }},
    }};

    static constexpr std::array<std::uint32_t, 11> RANDOM_11{{
        0x9c163083,
        0x54ff0b43,
        0x84424c7c,
        0x7461d2c7,
        0x923f635a,
        0xa31145f2,
        0xc1e25516,
        0x5af35593,
        0x9afc4e35,
        0x27f8394c,
        0xc62d20f1,
    }};

    std::array<cfg::Block, JUMBO_CHUNK_VOLUME> jumboChunk;
    makeJumboChunk(chunks, jumboChunk);

    std::vector<EngineRendererComm::Vertex> result;

    if (voxelTileInfo.lookupTable.size() < 1)
        throw std::runtime_error("No voxel tile info received.");

    auto aoVal = [] (bool a, bool b, bool c) -> std::uint8_t {
        c = c || (a && b);
        return static_cast<std::uint8_t>(c) + static_cast<std::uint8_t>(a) + static_cast<std::uint8_t>(b);
    };

    auto checkJumboBounds = [&] (pos_vec_type pos) -> bool {
        const auto lo = glm::greaterThanEqual(pos, pos_vec_type{0, 0, 0});
        const auto hi = glm::lessThan(pos, JUMBO_CHUNK_SIZE);
        return glm::all(lo) && glm::all(hi);
    };

    auto getAo = [&] (const pos_vec_type & pos, int32_t side) -> std::uint8_t {
        std::array<bool, 8> solid;
        for (std::int32_t i = 0; i < 8; ++i)
            solid[i] = !isAir(jumboChunk[util::dot(pos + AOS_OFFSETS[side][i], SCALE)]);
        std::uint8_t result = 0;
        for (std::int32_t corner = 0; corner < 4; ++corner)
            result |= aoVal(solid[AO_OFFSETS[side][corner][0]], solid[AO_OFFSETS[side][corner][1]], solid[AO_OFFSETS[side][corner][2]]) << (corner * 2);
        return result;
    };

    auto sanitizeBlock = [&] (cfg::Block b) -> cfg::Block {
        if constexpr (UNKNOWN_TO_DEFAULT) {
            assert(voxelTileInfo.lookupTable.size() > 0);
            if (b >= voxelTileInfo.lookupTable.size() || b < 0) {
                if constexpr (UNKNOWN_TO_ONE) {
                    assert(voxelTileInfo.lookupTable.size() > 1);
                    b = 1;
                } else {
                    b = 0;
                }
            }
        } else {
            assert(b >= 0 && b < voxelTileInfo.lookupTable.size());
        }
        return b;
    };

    auto addSide = [&] (const pos_vec_type & thisPos, int_type thisSide) {
        // TODO: current block data is recomputed at least 3x (3 calls to addSide) optimize that
        assert(checkJumboBounds(thisPos));
        const auto thisIndex = util::dot(thisPos, SCALE);
        // TODO: guarantee in bounds before calling this function and remove this clamping
        const auto thisValue = sanitizeBlock(jumboChunk[thisIndex]);
        const auto thisDataLocation = voxelTileInfo.lookupTable[thisValue].sides[thisSide];
// if (thisDataLocation == 0)
//     return;
        const auto neighborSide = thisSide ^ int_type{1}; // actually "+ 1" should be fine since input is only 0, 2 or 4
        const auto neighborPos = thisPos + NEIGHBOR_OFFSETS[thisSide];
        assert(checkJumboBounds(neighborPos));
        const auto neighborIndex = util::dot(neighborPos, SCALE);
        // TODO: guarantee in bounds before calling this function and remove this clamping
        const auto neighborValue = sanitizeBlock(jumboChunk[neighborIndex]);
        const auto neighborDataLocation = voxelTileInfo.lookupTable[neighborValue].sides[neighborSide];
// if (neighborDataLocation != 0)
//     return;

        const auto noThis = thisDataLocation == 0;
        const auto noNeighbor = neighborDataLocation == 0;
        if (noThis == noNeighbor)
            return;
        
        const auto actualSide = noThis ? neighborSide : thisSide;
        const auto actualDataLocation = noThis ? neighborDataLocation : thisDataLocation;
        const auto actualPos = noThis ? neighborPos : thisPos;

        util::VertexNotPacked vertexNotPacked = util::VertexNotPacked::makeDefault();

        const pos_vec_type off{actualSide == 1, actualSide == 3, actualSide == 5};

        vertexNotPacked.position_x = (actualPos.x + off.x - JUMBO_OFFSET[0]);
        vertexNotPacked.position_y = (actualPos.y + off.y - JUMBO_OFFSET[1]);
        vertexNotPacked.position_z = (actualPos.z + off.z - JUMBO_OFFSET[2]);

        vertexNotPacked.position_x *= cfg::FULL_QUAD_SIZE[0];
        vertexNotPacked.position_y *= cfg::FULL_QUAD_SIZE[1];
        vertexNotPacked.position_z *= cfg::FULL_QUAD_SIZE[2];

        vertexNotPacked.ambient_occlusion = getAo(actualPos, actualSide);

        const auto randomNumber = RANDOM_11[util::position35711(actualPos)]; // TODO: subtract jumbo offset, replace with worldPos
        vertexNotPacked.rotate_by = randomNumber & 0x3u; // rotation demo
        vertexNotPacked.transpose = randomNumber >> 2u & 0x1u; // transpose demo

        assert(vertexNotPacked.hasValidValues());
        auto & theResult = result.emplace_back();
        util::packQuad(theResult.data(), vertexNotPacked);
        const auto & tileData = voxelTileInfo.quadData[actualDataLocation];
        // TODO: somehow verify that this addition did not cause any overflow
        util::addQuad(theResult.data(), tileData.data());
    };

    auto addModel = [&](const pos_vec_type & thisPos) {
        assert(checkJumboBounds(thisPos));
        // TODO: deduplicate the first lines of code with addSide
        const auto thisIndex = util::dot(thisPos, SCALE);
        // TODO: guarantee in bounds before calling this function and remove this clamping
        const auto thisValue = sanitizeBlock(jumboChunk[thisIndex]);
        const auto & modelBeginEnd = voxelTileInfo.lookupTable[thisValue].model;
        if (modelBeginEnd[0] == 0)
            return;

        util::VertexNotPacked vertexNotPacked = util::VertexNotPacked::makeDefault();

        vertexNotPacked.position_x = (thisPos.x - JUMBO_OFFSET[0]);
        vertexNotPacked.position_y = (thisPos.y - JUMBO_OFFSET[1]);
        vertexNotPacked.position_z = (thisPos.z - JUMBO_OFFSET[2]);

        vertexNotPacked.position_x *= cfg::FULL_QUAD_SIZE[0];
        vertexNotPacked.position_y *= cfg::FULL_QUAD_SIZE[1];
        vertexNotPacked.position_z *= cfg::FULL_QUAD_SIZE[2];

        // TODO: implement ambient occlusion
        vertexNotPacked.ambient_occlusion = 0;

        EngineRendererComm::Vertex packedTemplate;
        util::packQuad(packedTemplate.data(), vertexNotPacked);

        for (std::uint32_t i = modelBeginEnd[0]; i < modelBeginEnd[1]; i++) {
            auto & thisResult = result.emplace_back(packedTemplate);
            const auto& tileData = voxelTileInfo.quadData[i];
            util::addQuad(thisResult.data(), tileData.data());
        }
    };

    pos_vec_type it{};
    for (it.z = from.z; it.z < to.z; it.z++)
        for (it.y = from.y; it.y < to.y; it.y++)
            for (it.x = from.x; it.x < to.x; it.x++) {
                for (int_type side = 0; side < 6; side += 2)
                    addSide(it, side);
                addModel(it);
            }

    return result;
}
