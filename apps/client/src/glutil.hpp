#pragma once

#include <glad/glad.h>

#include <util.hpp>

namespace glutil {
    inline GLuint programFromVertFrag(
        const std::filesystem::path& vertexShaderSourcePath,
        const std::filesystem::path& fragmentSahderSourcePath
    ) {
        const auto vertSource = util::loadFile<char>(vertexShaderSourcePath);
        const auto fragSource = util::loadFile<char>(fragmentSahderSourcePath);
        const auto vertName = glCreateShader(GL_VERTEX_SHADER);
        const auto fragName = glCreateShader(GL_FRAGMENT_SHADER);
        const auto vertSourcePtr = vertSource.data();
        const auto fragSourcePtr = fragSource.data();
        const GLint vertSourceLen = vertSource.size();
        const GLint fragSourceLen = fragSource.size();
        glShaderSource(vertName, 1, &vertSourcePtr, &vertSourceLen);
        glShaderSource(fragName, 1, &fragSourcePtr, &fragSourceLen);
        glCompileShader(vertName);
        glCompileShader(fragName);
        const auto program = glCreateProgram();
        glAttachShader(program, vertName);
        glAttachShader(program, fragName);
        glLinkProgram(program);
        glDetachShader(program, vertName);
        glDetachShader(program, fragName);
        glDeleteShader(vertName);
        glDeleteShader(fragName);
        return program;
    }

    struct viewport {
        GLint x;
        GLint y;
        GLsizei width;
        GLsizei height;
    };
}
