#pragma once

#include <vector>
#include <string>
#include <fstream>

#include <nlohmann/json.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <GLFW/glfw3.h>

#include <util.hpp>

// TODO: serialize settings on shutdown and restore on startup

namespace {
    struct Texturing {
    public:
        Texturing() = default;
        Texturing(const nlohmann::json & save) {
            if (!save.is_object())
                return;
#define LOAD_TEXTURING_PARAM(name) name = save.value(#name, name);
            LOAD_TEXTURING_PARAM(magnTexel)
            LOAD_TEXTURING_PARAM(miniTexel)
            LOAD_TEXTURING_PARAM(miniMipmap)
            LOAD_TEXTURING_PARAM(anisotropy)
            LOAD_TEXTURING_PARAM(minMipLevel)
            LOAD_TEXTURING_PARAM(maxMipLevel)
            LOAD_TEXTURING_PARAM(dirty)
#undef LOAD_TEXTURING_PARAM
        }

        void save(nlohmann::json & save) {
#define SAVE_TEXTURING_PARAM(name) save[#name] = name;
            SAVE_TEXTURING_PARAM(magnTexel)
            SAVE_TEXTURING_PARAM(miniTexel)
            SAVE_TEXTURING_PARAM(miniMipmap)
            SAVE_TEXTURING_PARAM(anisotropy)
            SAVE_TEXTURING_PARAM(minMipLevel)
            SAVE_TEXTURING_PARAM(maxMipLevel)
            SAVE_TEXTURING_PARAM(dirty)
#undef SAVE_TEXTURING_PARAM
        }

        void draw() {
            if (!ImGui::CollapsingHeader("Texturing"))
                return;
            ImGui::PushID(0);
                ImGui::Text("Magnification Function");
                dirty = ImGui::RadioButton("Nearest", &magnTexel, 0) || dirty;
                ImGui::SameLine();
                dirty = ImGui::RadioButton("Linear", &magnTexel, 1) || dirty;
            ImGui::PopID();
            ImGui::PushID(1);
                ImGui::Text("Minification Function");
                dirty = ImGui::RadioButton("Nearest", &miniTexel, 0) || dirty;
                ImGui::SameLine();
                dirty = ImGui::RadioButton("Linear", &miniTexel, 1) || dirty;
            ImGui::PopID();
            ImGui::PushID(2);
                ImGui::Text("Mipmap Function");
                dirty = ImGui::RadioButton("Nearest", &miniMipmap, 0) || dirty;
                ImGui::SameLine();
                dirty = ImGui::RadioButton("Linear", &miniMipmap, 1) || dirty;
                dirty = ImGui::DragIntRange2("Range", &minMipLevel, &maxMipLevel, 1, 0, 16, "%d", "%d") || dirty;
            ImGui::PopID();
            ImGui::PushID(3);
                ImGui::Text("Anisotropy Function");
                dirty = ImGui::SliderFloat("Anisotropy Level", &anisotropy, 1.0f, 16.0f, "%.2f") || dirty;
            ImGui::PopID();
        }

        void setDirty() { dirty = true; }
        void clearDirty() { dirty = false; }

        bool changed() const noexcept { return dirty; }

        float anisotropyLevel() const noexcept { return anisotropy; }

        GLenum magnifyingFilter() const noexcept {
            return magnTexel ? GL_LINEAR : GL_NEAREST;
        }

        GLenum minifyingFilter() const noexcept {
            if (miniMipmap)
                return miniTexel ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR;
            else
                return miniTexel ? GL_LINEAR_MIPMAP_NEAREST : GL_NEAREST_MIPMAP_NEAREST;
        }

        std::pair<int, int> mipmapRange() const noexcept {
            return {minMipLevel, maxMipLevel};
        }

    private:
        int magnTexel = 0;
        int miniTexel = 0;
        int miniMipmap = 0;
        float anisotropy = 1.0f;
        int minMipLevel = 0;
        int maxMipLevel = 16;
        bool dirty = true;

    };
}

class Gui {
public:
    std::array<float, 4> mFogColor{0.5f, 0.6f, 0.5f, 1.0f};
    float mCameraSpeed{20.0};
    bool mDrawRayHitLocation{true};
    float mFogStart{300.0f};
    float mFogTransitionDistance{300.0f};
    bool mWireframeBlocks{false};
    bool mDrawAxes{true};
    int mCurrentBlockIndex{0};

    int mFramebufferWidth{1};
    int mFramebufferHeight{1};
    int mHitBlockIndex{0};

    float mFPS{0.0f};

    bool mUseMeshShader{false};

    std::string mPosition;
    std::string mMeshesInMemory;
    std::string mMeshesRendered;
    std::string mMeshesUploaded;
    std::string mMeshesDeleted;

    std::string mTx;
    std::string mRx;

    std::string mChunksCached;
    std::string mObservedMeshesCached;

    std::string mLookingAt;

    std::vector<std::string> mBlockNames;
    std::vector<const char *> mBlockNamesInternal;

    std::vector<int> mJoystickMappings;
    std::vector<const char *> mJoystickNames;
    int mCurrentJoystickIndex{0};
    GLFWgamepadstate mActiveGamepadState{};

    std::size_t mQuadCount{0};

    float mAoShadeStrength{0.25f};

    Texturing mTexturing;

    Gui(GLFWwindow * window, const std::filesystem::path & userPath) :
        mGuiJsonPath{(userPath / "gui.json").string()},
        mImguiIniPath{(userPath / "imgui.ini").string()}
    {
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO & io = ImGui::GetIO();
        io.IniFilename = mImguiIniPath.c_str();
        ImGui::StyleColorsDark();
        ImGui_ImplGlfw_InitForOpenGL(window, true);
        ImGui_ImplOpenGL3_Init("#version 460");
        loadState();
    }

    void loadState() {
        nlohmann::json stateJson;
        try {
            stateJson = nlohmann::json::parse(
                util::loadFile<char>(mGuiJsonPath)
            );
        } catch (...) {}

        if (!stateJson.is_object())
            return;

#define DESERIALIZE_GUI_VAR(name) name = stateJson.value(#name, name);

        DESERIALIZE_GUI_VAR(mFogColor)
        DESERIALIZE_GUI_VAR(mCameraSpeed)
        DESERIALIZE_GUI_VAR(mDrawRayHitLocation)
        DESERIALIZE_GUI_VAR(mFogStart)
        DESERIALIZE_GUI_VAR(mFogTransitionDistance)
        DESERIALIZE_GUI_VAR(mWireframeBlocks)
        DESERIALIZE_GUI_VAR(mDrawAxes)
        DESERIALIZE_GUI_VAR(mCurrentBlockIndex)
        DESERIALIZE_GUI_VAR(mHitBlockIndex)
        DESERIALIZE_GUI_VAR(mAoShadeStrength)
        DESERIALIZE_GUI_VAR(mUseMeshShader)

#undef DESERIALIZE_GUI_VAR

        mTexturing = Texturing(stateJson["texturing"]);
    }

    void storeState() {
        nlohmann::json json;

#define SERIALIZE_GUI_VAR(name) json[#name] = name;

        SERIALIZE_GUI_VAR(mFogColor)
        SERIALIZE_GUI_VAR(mCameraSpeed)
        SERIALIZE_GUI_VAR(mDrawRayHitLocation)
        SERIALIZE_GUI_VAR(mFogStart)
        SERIALIZE_GUI_VAR(mFogTransitionDistance)
        SERIALIZE_GUI_VAR(mWireframeBlocks)
        SERIALIZE_GUI_VAR(mDrawAxes)
        SERIALIZE_GUI_VAR(mCurrentBlockIndex)
        SERIALIZE_GUI_VAR(mHitBlockIndex)
        SERIALIZE_GUI_VAR(mAoShadeStrength)
        SERIALIZE_GUI_VAR(mUseMeshShader)

#undef SERIALIZE_GUI_VAR

        mTexturing.save(json["texturing"]);

        std::ofstream out{mGuiJsonPath, std::ofstream::trunc};
        out << std::setw(4) << json << std::endl;
    }

    void draw() {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        {
            if (mShowDemoWindow)
                ImGui::ShowDemoWindow(&mShowDemoWindow);
        }
        if (ImGui::Begin("Options")) {
            drawBlockSelection();
            drawJoystickSelection();
            drawShaders();
            mTexturing.draw();
            drawControls();
            drawStats();
            ImGui::Checkbox("Show ImGui Demo Window", &mShowDemoWindow);
        }
        ImGui::End();
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

    ~Gui() {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
        storeState();
    }

    void addGpuTime(float gpuTime) {
        // TODO: replace with std::shift_left in C++20
        std::rotate(mGpuTimeBuffer.begin(), mGpuTimeBuffer.begin() + 1, mGpuTimeBuffer.end());
        mGpuTimeBuffer.back() = gpuTime;

    }

private:
    bool mShowDemoWindow{false};
    std::array<float, 64> mGpuTimeBuffer{};

    std::string mGuiJsonPath;
    std::string mImguiIniPath;

    void drawBlockSelection() {
        if (!ImGui::CollapsingHeader("Blocks"))
            return;
        ImGui::PushID("Blocks");

        ImGui::Combo("Active", &mCurrentBlockIndex, mBlockNamesInternal.data(), mBlockNamesInternal.size());

        ImGui::PopID();
    }

    void drawJoystickSelection() {
        if (!ImGui::CollapsingHeader("Gamepads"))
            return;
        ImGui::PushID("Gamepads");

        ImGui::Combo("Active", &mCurrentJoystickIndex, mJoystickNames.data(), mJoystickNames.size());

        std::array<const char*, 15> buttonNames{{
            "A", "B", "X", "Y",
            "Left Bumper", "Right Bumper",
            "Back", "Start", "Guide",
            "Left Thumb", "Right Thumb",
            "Up", "Right", "Down", "Left"
        }};

        std::array<int, buttonNames.size()> lineBreaks{{
            1, 0, 0, 0,
            1, 0,
            1, 0, 0,
            1, 0,
            1, 0, 0, 0
        }};

        for (int b = 0; b < buttonNames.size(); b++) {
            if (lineBreaks[b] == 0)
                ImGui::SameLine();
            bool pressed = mActiveGamepadState.buttons[b] == GLFW_PRESS;
            ImGui::Checkbox(buttonNames[b], &pressed);
        }

        std::array<const char*, 6> sliderNames{{
            "Left X", "Left Y",
            "Right X", "Right Y",
            "Left Trigger", "Right Trigger"
        }};

        for (int s = 0; s < sliderNames.size(); s++) {
            float value = mActiveGamepadState.axes[s];
            ImGui::SliderFloat(sliderNames[s], &value, -1.0f, 1.0f, "%.3f");
        }

        ImGui::PopID();
    }

    void drawShaders() {
        if (!ImGui::CollapsingHeader("Shaders"))
            return;
        ImGui::PushID("Stats");

        ImGui::SliderFloat("AO Shade Strength", &mAoShadeStrength, 0.f, 0.25f, "%.2f");
        ImGui::ColorEdit3("Fog Color", mFogColor.data());
        ImGui::SliderFloat("Fog Start", &mFogStart, 1.0f, 1000.0f, "%.1f");
        ImGui::SliderFloat("Fog Transition Distance", &mFogTransitionDistance, 0.1f, 1000.0f, "%.1f");
        ImGui::Checkbox("Draw Ray Hit Location", &mDrawRayHitLocation);
        ImGui::Checkbox("Draw Axes", &mDrawAxes);
        ImGui::Checkbox("Wireframe Blocks", &mWireframeBlocks);
        ImGui::Checkbox("Use Mesh Shaders", &mUseMeshShader);

        ImGui::PopID();
    }

    void drawControls() {
        if (!ImGui::CollapsingHeader("Controls"))
            return;
        ImGui::PushID("Stats");

        ImGui::SliderFloat("Camera Speed", &mCameraSpeed, 0.1f, 300.0f, "%.2f", 4.0f);

        ImGui::PopID();
    }

    void drawStats() {
        if (!ImGui::CollapsingHeader("Stats"))
            return;
        ImGui::PushID("Stats");
        ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, 0.0f);

        ImGui::Columns(2);

        auto stat = [](const char * l, const char * r) {
            ImGui::Text(l);
            ImGui::NextColumn();
            ImGui::Text(r);
            ImGui::NextColumn();
        };

        ImGui::Separator();
        stat("Framebuffer Width", std::to_string(mFramebufferWidth).c_str());
        stat("Framebuffer Height", std::to_string(mFramebufferHeight).c_str());
        ImGui::Separator();
        stat("FPS", std::to_string(mFPS).c_str());
        stat("GPU ms", std::to_string(mGpuTimeBuffer.back() * 1000.0f).c_str());
        ImGui::Separator();
        stat("Position", mPosition.c_str());
        stat("Meshes In Memory", mMeshesInMemory.c_str());
        stat("Meshes Rendered", mMeshesRendered.c_str());
        stat("Meshes Uploaded", mMeshesUploaded.c_str());
        stat("Meshes Deleted", mMeshesDeleted.c_str());
        stat("Quads on GPU", std::to_string(mQuadCount).c_str());
        ImGui::Separator();
        stat("Tx", mTx.c_str());
        stat("Rx", mRx.c_str());
        ImGui::Separator();
        stat("Chunks Cached", mChunksCached.c_str());
        stat("Observed Meshes Cached", mObservedMeshesCached.c_str());
        ImGui::Separator();
        stat("Looking at", mLookingAt.c_str());
        // TODO: avoid out of bounds access here
        stat("Hit block type", mBlockNames.at(mHitBlockIndex).c_str());

        ImGui::Separator();
        ImGui::Columns(1);

        ImGui::PlotHistogram("GPU time", mGpuTimeBuffer.data(), mGpuTimeBuffer.size(), 0.0f, nullptr, 0.0f, 0.016f, ImVec2{0.0f, 80.0f});

        ImGui::PopStyleVar();
        ImGui::PopID();
    }
};
