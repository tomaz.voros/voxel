#pragma once

#include <glm/vec3.hpp>
#include <functional>
#include <array>

class CollisionResolver {
public:
    static glm::vec3 resolve(
        const glm::vec3 & startingPosition,
        const glm::vec3 & directionIn,
        std::function<bool(const glm::ivec3 &)>  isSolid
    );

private:

};
