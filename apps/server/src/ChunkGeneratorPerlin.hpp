#pragma once

#include "IChunkGenerator.hpp"

class ChunkGeneratorPerlin : public IChunkGenerator {
public:
    void generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) override;

};
