#include <memory>
#include <optional>
#include <cstring>

#include "Engine.hpp"
#include "Network.hpp"
#include "ChunkServer.hpp"
#include "ChunkGenerator2.hpp"
#include "EngineBase.hpp"
#include "trace.hpp"

struct program_arguments {
    std::optional<boost::asio::ip::tcp::endpoint> endpoint;
    std::optional<Network::ssl_file_names> ssl_certificate_file;
    std::optional<const char *> save_folder;
    bool no_encryption = false;
};

program_arguments parse_program_arguments(int argc, char * argv[]) {
    program_arguments args;

    auto require_true = [] (bool condition) {
        if (!condition)
            throw std::runtime_error("Broken program arguments.");
    };

    auto get_endpoint = [&args, argc, argv, require_true] (int i) -> int {
        // ip 0-65535

        require_true(i <= argc - 2);
        require_true(!args.endpoint.has_value());

        boost::system::error_code error;
        const auto address = boost::asio::ip::make_address(argv[i], error);
        require_true(!error);

        const auto port = std::strtoll(argv[i + 1], nullptr, 10);
        require_true(port > 0 && port <= 65535);

        boost::asio::ip::tcp::endpoint endpoint;
        endpoint.address(address);
        endpoint.port(static_cast<unsigned short>(port));

        args.endpoint.emplace(endpoint);

        return i + 2;
    };

    auto get_certificate_file_names = [&args, argc, argv, require_true] (int i) -> int {
        // certificate private_key diffie_hellman

        require_true(i <= argc - 3);
        require_true(!args.ssl_certificate_file.has_value());

        Network::ssl_file_names file_names;
        file_names.certificate = argv[i];
        file_names.private_key = argv[i + 1];
        file_names.diffie_hellman = argv[i + 2];

        args.ssl_certificate_file.emplace(file_names);

        return i + 3;
    };

    auto get_save_folder = [&args, argc, argv, require_true] (int i) -> int {
        // world_folder

        require_true(i <= argc - 1);
        require_true(!args.save_folder.has_value());

        args.save_folder.emplace(argv[i]);

        return i + 1;
    };

    for (int i = 1; i < argc;) {
        if (std::strcmp(argv[i], "-p") == 0) {
            i = get_endpoint(i + 1);
        } else if (std::strcmp(argv[i], "-c") == 0) {
            i = get_certificate_file_names(i + 1);
        } else if (std::strcmp(argv[i], "-w") == 0) {
            i = get_save_folder(i + 1);
        } else if (std::strcmp(argv[i], "-no-encryption") == 0) {
            i = i + 1;
            args.no_encryption = true;
        } else {
            require_true(false);
        }
    }

    return args;
}

void print_help() {
    log::info(
        "Example usage:\n"
        "./server -p :: 51000 -w world/ -c cert/certificate.crt cert/cert.key cert/dh.pem\n"
        "    -p <ipv4/ipv6> <port>\n"
        "    -w <save_folder>\n"
        "    -c <certificate> <private_key> <diffie_hellman>\n"
        "    -no-encryption\n"
    );
}

int main(int argc, char * argv[]) {
    program_arguments args;

    try {
        args = parse_program_arguments(argc, argv);
    } catch (...) {
        log::error("Broken program arguments.");
        print_help();
        return 0;
    }

    if (argc == 1) {
        print_help();
        return 0;
    }

    if (!args.endpoint.has_value()) {
        log::error("Endpoint not specified.");
        print_help();
        return 0;
    }

    if (!args.save_folder.has_value()) {
        log::error("Save folder not specified.");
        print_help();
        return 0;
    }

    // do not remove this line or the return inside!
    if (args.no_encryption == args.ssl_certificate_file.has_value()) {
        if (args.no_encryption)
            log::error("No encryption specified but certificate file specified.");
        else
            log::error("Certificate file not specified.");
        print_help();
        return 0;
    }

    if (!args.ssl_certificate_file.has_value()) {
        log::warning("Not using encryption.");
    }

    trace::init("trace-server-main.json");
    {
        auto engine = std::make_unique<Engine>(
            std::make_unique<ChunkServer>(args.save_folder.value()),
            std::make_unique<ChunkGenerator2>(),
            args.save_folder.value()
        );

        auto server = std::make_unique<Network::Server>();

        engine->setServerProxy(server.get());

        const auto firstTickTime = std::chrono::steady_clock::now() + cfg::TICK_DT;
        server->serve(args.endpoint.value(), args.ssl_certificate_file, engine.get(), firstTickTime);
    }
    trace::finish();
}
