#pragma once

#include "EntityBase.hpp"
#include <util.hpp>

class EntityBlob : public EntityBase {
public:
    EntityBlob(
        cfg::entity_id_type id,
        glm::vec3 position,
        glm::vec3 velocity,
        float timeToLive
    ) :
        EntityBase{ id },
        mPosition{ position },
        mVelocity{ velocity },
        mTimeToLive{ timeToLive }
    {}

    cfg::EntityType type() const { return cfg::EntityType::BLOB; }

    glm::vec3 position() const override { return mPosition; }
    bool alive() const override { return mTimeToLive > 0.0f; }
    void update(float dt) override {
        mPosition += mVelocity * dt;
        if (alive())
            mTimeToLive -= dt;
    }
    void serialize_update(std::vector<std::byte> & out) const override {
        util::push(out, mPosition.x);
        util::push(out, mPosition.y);
        util::push(out, mPosition.z);
    }

private:
    glm::vec3 mPosition;
    glm::vec3 mVelocity;
    float mTimeToLive;

};
