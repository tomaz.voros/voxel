#include "ChunkGenerator2.hpp"

#include <cmath>

#include <glm/gtc/noise.hpp>

void ChunkGenerator2::generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) {
    using int_type = std::int32_t;
    using ipos_type = glm::tvec3<int_type>;
    using float_type = float;
    using fpos_type = glm::tvec3<float>;
    using fpos2_type = glm::tvec2<float>;

    auto toIndex = [] (ipos_type p) -> int_type {
        static constexpr ipos_type SCALE{
            int_type{1},
            cfg::CHUNK_SIZE.x,
            cfg::CHUNK_SIZE.y * cfg::CHUNK_SIZE.x
        };
        p *= SCALE;
        return p.x + p.y + p.z;
    };

    const ipos_type from{ 0 };
    const ipos_type to{ cfg::CHUNK_SIZE };
    const ipos_type worldOffset{chunkPosition * cfg::CHUNK_SIZE};

    static constexpr fpos2_type XZ_SCALE{1.0f / 30.0f, 1.0f / 30.0f};
    static constexpr float_type Y_SCALE{1.0f / 15.0f};

    static constexpr int_type DIRT_CEIL{-4};
    static constexpr int_type SAND_CEIL{6};
    static constexpr int_type POUND_CEIL{-6};

    ipos_type i;
    ipos_type w;
    for (i.z = from.z; i.z < to.z; ++i.z) {
        for (i.x = from.x; i.x < to.x; ++i.x) {
            w.x = i.x + worldOffset.x;
            w.z = i.z + worldOffset.z;
            const float_type noiseValue = glm::perlin(fpos2_type{w.x, w.z} * XZ_SCALE);
            for (i.y = from.y; i.y < to.y; ++i.y) {
                cfg::Block & block = blocks[toIndex(i)];
                w.y = i.y + worldOffset.y;
                if (noiseValue > float_type(w.y) * Y_SCALE) {
                    if (w.y < DIRT_CEIL)
                        block = cfg::Block{6};
                    else if (w.y < SAND_CEIL)
                        block = cfg::Block{5};
                    else
                        block = cfg::Block{8};
                } else {
                    if (w.y < POUND_CEIL)
                        block = cfg::Block{11};
                    else
                        block = cfg::Block{0};
                }
            }
        }
    }
}
