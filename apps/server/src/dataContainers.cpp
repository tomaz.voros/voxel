#include "dataContainers.hpp"

#include <cassert>
#include <cstring>

#include "Log.hpp"
#include <util.hpp>

bool ClientRxBuffer::processMessage(
    gsl::span<const std::byte> message_full
) {
    nothingReceived = false;
    // TODO: refactor
    assert(size(message_full) <= cfg::MAX_MESSAGE_SIZE);
    assert(size(message_full) >= cfg::MIN_MESSAGE_SIZE);
    // TODO: do not assume that message id is same size as std::byte
    assert(size(message_full) > 0);
    const auto message_id = static_cast<cfg::Message::Id>(message_full[0]);
    const auto message = message_full.subspan(1);

    // TODO: validate that all the types have correct size and format
    // TODO: enforce little endian (check that the target system is little endian)

    const auto msg = data(message);
    const auto message_size = size(message);
    switch (message_id) {
    case cfg::Message::Id::CHUNK_REQUEST_DATA: {
        if (message_size % sizeof(cfg::Message::Body::ChunkRequestDataPosition) != 0)
            return false;
        for (
            // TODO: fix issues resulting from unaligned access => util::read()
            auto m = reinterpret_cast<const cfg::Message::Body::ChunkRequestDataPosition *>(msg),
            end = m + message_size / sizeof(cfg::Message::Body::ChunkRequestDataPosition);
            m < end; ++m
        ) {
            if (chunkRequests.size() >= cfg::MAX_PENDING_CHUNK_REQUESTS) {
                log::info("Client tried to request more chunks than allowed in one tick.");
                return false;
            }
            chunkRequests.push_back(*m);
        }
    } break;
    case cfg::Message::Id::BLOCK_DATA: {
        // TODO: correct message size constant in cfg for all messages
        // TODO: get rid of the 3 byte padding in the BLOCK_DATA message
        if (message_size % 16 != 0)
            return false;
        for (auto m = msg, end = m + message_size; m < end; m += 16) {
            if (blockPlacements.size() >= cfg::MAX_BLOCK_PLACEMENTS_PER_TICK) {
                log::info("Client tried to place more blocks than allowed in one tick.");
                return false;
            }
            // TODO: fix undefined behaviour of reading potentially unaligned memory
            cfg::Message::Body::BlockData blockUpdate;
            util::read({ m, 16 }, 0, blockUpdate.position.x);
            util::read({ m, 16 }, 4, blockUpdate.position.y);
            util::read({ m, 16 }, 8, blockUpdate.position.z);
            util::read({ m, 16 }, 12, blockUpdate.value);
            blockPlacements.push_back(blockUpdate);
        }
    } break;
    case cfg::Message::Id::STOP_SERVER: {
        if (message_size != 4)
            return false;
        static_assert(sizeof(msg[0]) == sizeof(char));
        const auto correctMessage = std::strncmp(reinterpret_cast<const char *>(msg), "stop", 4);
        if (correctMessage == 0)
            stopRequested = true;
        else
            return false;
    } break;
    case cfg::Message::Id::CLIENT_DISCONNECTED: {
        disconnected = true;
    } break;
    case cfg::Message::Id::PLAYER_ORIENTATION: {
        if (message_size != 20)
            return false;
        if (orientation.updated)
            return false; // only allow once per tick
        static_assert(sizeof(float) == 4);
        util::read(message, 0, orientation.x);
        util::read(message, 4, orientation.y);
        util::read(message, 8, orientation.z);
        util::read(message, 12, orientation.yaw);
        util::read(message, 16, orientation.pitch);
        orientation.updated = true;
    } break;
    case cfg::Message::Id::MOVE_AWARE_AREA: {
        if (message_size != 6 * 4)
            return false;
        if (move_aware_area.has_value())
            return false;
        util::box<> new_aware_area = {};
        util::read(message, 0, new_aware_area.from.x);
        util::read(message, 4, new_aware_area.from.y);
        util::read(message, 8, new_aware_area.from.z);
        util::read(message, 12, new_aware_area.to.x);
        util::read(message, 16, new_aware_area.to.y);
        util::read(message, 20, new_aware_area.to.z);
        const auto valid =
            new_aware_area.from.x <= new_aware_area.to.x &&
            new_aware_area.from.y <= new_aware_area.to.y &&
            new_aware_area.from.z <= new_aware_area.to.z;
        if (!valid)
            return false;
        move_aware_area.emplace(new_aware_area);
    } break;
    default: {
        return false;
    } break;
    }
    return true;
}
