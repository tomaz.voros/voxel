#pragma once

#include <cassert>
#include <list>
#include <unordered_map>
#include "Log.hpp"

template <typename Key, typename Val>
class LeastRecentlyUsed {
public:
    /**
     * Prints warnings when the container is not empty on destruction.
     */
    ~LeastRecentlyUsed() {
        if (mUnusedList.size() != 0)
            log::warning("LRU unused list was not empty on destruction.");
        if (mList.size() != 0)
            log::warning("LRU in use list was not empty on destruction.");
    }

    /**
     * Emplaces a new node. The node is marked as in use. If a node
     * with the same key already exists, the behaviour is undefined.
     * @return Reference to the value of the newly constructed node.
     */
    template <typename ... Args>
    Val & emplace(const Key & key, Args && ... args) {
        assert(!exists(key));
        mList.emplace_front(
            std::piecewise_construct,
            std::forward_as_tuple(key),
            std::forward_as_tuple(std::forward<Args>(args)...)
        );
        mMap.emplace(key, mList.begin());
        return mList.front().second;
    }

    /**
     * If a node with the given key
     * does not exists, the behaviour is undefined. The node is
     * moved to the in use list. If the node is already in the
     * in use list, the behaviour is undefined.
     * @return Value with the given key.
     */
    Val & get(const Key & key) {
        assert(exists(key));
        const auto i = mMap.find(key);
        mList.splice(mList.begin(), mUnusedList, i->second);
        return i->second->second;
    }

    /**
     * The node with the given key is moved from in use list to the unused list.
     * If a node with the given key does not exist, the behaviour is undefined.
     * If the node with the given key is already in the unused list, the
     * behaviour is undefined.
     */
    void release(const Key & key) {
        assert(exists(key));
        // TODO: assert node in in use list
        mUnusedList.splice(mUnusedList.begin(), mList, mMap.find(key)->second);
    }

    /**
     * The returned node is moved to in use list. If the node is already in the
     * in use list, the behaviour is undefined.
     * @param[out] isCached True if the returned value was taken out of the cache.
     *                      False if a new node is constructed because a node
     *                      with the given key was not found.
     * @return Reference to the value of the found or the newly constructed node.
     */
    template <typename ... Args>
    Val & emplace_get(const Key & key, bool & isCached, Args && ... args) {
        isCached = exists(key);
        if (isCached) {
            return get(key);
        } else {
            return emplace(key, std::forward<Args>(args)...);
        }
    }

    /**
     * @return True if a node with the given key esists. False if not.
     */
    bool exists(const Key & key) const {
        return mMap.find(key) != mMap.end();
    }

    /**
     * @return Reference to a key value pair of the least recently used node.
     *         That is from the unused list.
     *         This will be the node that will be removed if the next function
     *         call is #pop(). If the unused list is empty the call to this function
     *         has undefined behaviour.
     */
    std::pair<Key, Val> & lru() {
        assert(mUnusedList.size() > 0);
        return mUnusedList.back();
    }

    /**
     * Removes the least recently used node from the unused list. If the list
     * is empty, the behaviour is undefined.
     */
    void pop() {
        assert(mUnusedList.size() > 0);
        mMap.erase(mUnusedList.back().first);
        mUnusedList.pop_back();
    }

    /**
     * @return The number of nodes in the unused list.
     */
    std::size_t unusedSize() const { return mUnusedList.size(); }

    /**
     * @return The number of nodes in the in use list.
     */
    std::size_t usedSize() const { return mList.size(); }

private:
    /**
     * In use list.
     */
    std::list<std::pair<Key, Val>> mList;
    /**
     * Unused list.
     */
    std::list<std::pair<Key, Val>> mUnusedList;
    std::unordered_map<Key, typename decltype(mList)::iterator> mMap;

};
