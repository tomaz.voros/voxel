#pragma once

#include "EntityBase.hpp"

class EntityPlayer final : public EntityBase {
public:
    EntityPlayer(
        cfg::entity_id_type id,
        glm::vec3 position,
        float yaw,
        float pitch,
        cfg::tick_type tick
    ) :
        EntityBase{ id },
        mPosition{ position, position },
        mYaw{ yaw, yaw },
        mPitch{ pitch, pitch },
        mTick{ tick, tick }
    {}

    cfg::EntityType type() const { return cfg::EntityType::PLAYER; }


    // TODO: serialize to/from disc ...
    // TODO: serialize update to/from network ...
    glm::vec3 position() const override { return mPosition[1]; }
    float yaw() const { return mYaw[1]; }
    float pitch() const { return mPitch[1]; }
    float tick() const { return mTick[1]; }
    bool alive() const override { return mAlive; }
    void kill() { mAlive = false; }
    void update(float dt) override {
        //
    }
    void serialize_update(std::vector<std::byte> & out) const override {
        util::push(out, mPosition[1].x);
        util::push(out, mPosition[1].y);
        util::push(out, mPosition[1].z);
        util::push(out, mYaw[1]);
        util::push(out, mPitch[1]);
    }

    void pushOrientation(
        const glm::vec3 & newPosition,
        float newYaw,
        float newPitch,
        cfg::tick_type newTick
    ) {
        mPosition[0] = mPosition[1];
        mYaw[0] = mYaw[1];
        mPitch[0] = mPitch[1];
        mTick[0] = mTick[1];

        mPosition[1] = newPosition;
        mYaw[1] = newYaw;
        mPitch[1] = newPitch;
        mTick[1] = newTick;
    }

private:
    glm::vec3 mPosition[2];
    float mYaw[2];
    float mPitch[2];
    cfg::tick_type mTick[2];
    bool mAlive = true;

};
