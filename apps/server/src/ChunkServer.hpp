#pragma once

#include <fstream>
#include <array>
#include <filesystem>
#include <cstdint>

#include <glm/vec3.hpp>

#include "LeastRecentlyUsed.hpp"
#include "cfg.hpp"
#include "ivec3hash.hpp"

class IChunkGenerator;

class ChunkServer {
    // TODO: rework. The current implementation needs at least reference counting for chunks and regions to be more easily usable.
    // TODO: region files should always be in valid state:
    //       - append chunk data to file, then update lookup table
    //       - when defragmenting create new file instead of overwriting
    //       - when overwriting, move old version to the end before overwriting
    // TODO: try async file io: poke() to load
public:
    ChunkServer(const std::filesystem::path & worldFolder);
    ~ChunkServer();
    const cfg::Block * get(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator);
    cfg::Block * getWritable(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator);
    void release(const glm::ivec3 & chunkPosition);
    // TODO: implement
    /**
     * Writes a compressed version of the chunk to #out. For compression type see namespace Config.
     * @param[out] out Must be a buffer to at least #Config::MAX_COMPRESSED_CHUNK_SIZE_BYTES bytes.
     * @return Size of the compressed data or 0 on failure.
     */
    std::size_t getCompressedCopy(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator, std::byte * out);

private:
    struct Region {
        /**
        region file structure:
            [uint32:data_end],
            [uint32:garbage],
            Config::REGION_VOLUME x [uint32:position][uint32:size],
            Nx[raw_chunk_bytes]
        
        data_end: index of the past-the-end byte in the file
        garbage: amount of garbage bytes in the file (used for determining whether defragmentation is needed)
        position: location (byte index) of the chunk in file (relative to beginning)
        size: size of chunk data in bytes
        */

        // TODO: consider storing the look-up table for chunks in this struct
        std::fstream file;
        std::uint32_t garbage;
        std::uint32_t end;
        bool dirty;

        /**
         * Reads consecutive bytes from the open region file.
         * @param[out] buffer Destination for the read bytes.
         * @param[in] count Number of bytes to read.
         * @param[in] position Location in the file of the first byte to read.
         */
        void read(void * buffer, std::size_t count, std::size_t position);
        /**
         * Writes consecutive bytes to the open region file.
         * @param[in] buffer Source of the written bytes.
         * @param[in] count Number of bytes to write.
         * @param[in] position Location in the file for the first byte to write.
         */
        void write(const void * buffer, std::size_t count, std::size_t position);
        /**
         * Opens a new region file and closes the open region file
         * if a region file is already open. If file to be opened does not exist,
         * a new uninitialized file is created.
         * @param[in] filePath Path of the file to open.
         * @return True if the file is new. False otherwise.
         */
        bool openFile(const std::filesystem::path & filePath);
        /**
         * Closes the open region file if a region file is already open.
         */
        void closeFile();

        ~Region() {
            if (dirty)
                log::warning("Destroying region that was not saved.");
        }

    };

    struct Chunk {
        // TODO: store compressed versions when only sending
        // TODO: send compressed chunks, send meshes instead of chunks for far away chunks (of mesh smaller than chunk, also instead of real mesh just send faces from which the client can reconstruct the mesh)
        union {
            // TODO: to save memory, store compressed and uncompressed chunks separately
            std::array<cfg::Block, cfg::CHUNK_VOLUME> blocks;
            std::array<std::byte, cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES> compressedBlocks;
        } content;
        // 0 means chunk is not compressed
        std::size_t compressedSize{0};
        bool dirty;
        ~Chunk() {
            if (dirty)
                log::warning("Destroying chunk that was not saved.");
        }
    };

    LeastRecentlyUsed<glm::ivec3, Chunk> mChunks;
    LeastRecentlyUsed<glm::ivec3, Region> mRegions;
    std::filesystem::path mWorldFolder;

    Region & getRegion(const glm::ivec3 & regionPosition);
    Chunk & getChunk(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator);
    void saveRegion(const glm::ivec3 & regionPosition, Region & region);
    void saveChunk(const glm::ivec3 & chunkPosition, Chunk & chunk);
    // TODO: replace with a RAII mechanism (also see returnChunk)
    void returnRegion(const glm::ivec3 & regionPosition);
    // TODO: replace with a RAII mechanism (also see returnRegion)
    void returnChunk(const glm::ivec3 & chunkPosition);
    void defragmentRegion(Region & region, const glm::ivec3 & regionPosition);
    std::filesystem::path constructRegionFilePath(const glm::ivec3 & regionPosition) const;

};
