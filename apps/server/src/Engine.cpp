#include "Engine.hpp"

#include <algorithm>
#include <fstream>
#include <sstream>
#include <chrono>

#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "CollisionResolver.hpp"
#include "ServerBase.hpp"
#include "EntityPlayer.hpp"
#include "EntityBlob.hpp"
#include "trace.hpp"

Engine::Engine(
    std::unique_ptr<ChunkServer> chunkServer,
    std::unique_ptr<IChunkGenerator> chunkGenerator,
    const char * world_folder
) :
    mChunkServer{ std::move(chunkServer) },
    mChunkGenerator{ std::move(chunkGenerator) }
{
    mClients.reserve(cfg::MAX_CONNECTED_CLIENT_COUNT);

    mUserFileLocation = std::string{ world_folder } + std::string{ "/users" };

    std::ifstream user_file{ mUserFileLocation, std::ifstream::binary | std::ifstream::ate };
    if (!user_file.good()) {
        log::warning("Broken user file, starting from scratch.");
    } else {
        cfg::index_type user_file_size = user_file.tellg();
        user_file.seekg(0);
        static_assert(sizeof(user_map_node) == 40);
        if (user_file_size % sizeof(user_map_node) != 0) {
            log::warning("Broken user file, starting from scratch.");
        } else {
            const auto user_count = user_file_size / sizeof(user_map_node);
            for (cfg::index_type u = 0; u < user_count; ++u) {
                user_map_node user;
                user_file.read(reinterpret_cast<char *>(&user), sizeof(user));
                // TODO: check data validity / integrity
                const auto insert_result = mUserDatabase.emplace(user);
                if (insert_result.second == false) {
                    log::warning("Broken user file, starting from scratch.");
                    mUserDatabase.clear();
                    break;
                }
            }
        }
    }
}

void Engine::setServerProxy(ServerBase * server_proxy) {
    mServerProxy = server_proxy;
}

void Engine::tick() {
    static constexpr bool PRINT_CLIENT_INFO = false;
    static constexpr bool PRINT_TIMING_INFO = false;

    const auto tickEvent = trace::event_complete("tick");

    {
        if constexpr (PRINT_TIMING_INFO) {
            const auto secondsSinceStart = std::chrono::duration_cast<std::chrono::duration<double>>(
                std::chrono::steady_clock::now() - mTickTime
            ).count();
            log::debug("Seconds since start: {}", secondsSinceStart);
        }

        bool clientRequestedStop = false;
        for (const auto & c : mClients)
            if (c->rxBuffer.stopRequested) {
                clientRequestedStop = true;
                break;
            }

        static constexpr float DT = std::chrono::duration<float>{ cfg::TICK_DT }.count();

        // TODO: flow control
        // TODO: congestion control
        struct MessageBuilder {
            void newMessage(cfg::Message::Id id) {
                message.clear();
                message.push_back(static_cast<std::byte>(id));
            }

            void appendMessage(gsl::span<const std::byte> messagePiece) {
                message.insert(end(message), begin(messagePiece), end(messagePiece));
            }

            gsl::span<const std::byte> getMessageView() {
                return message;
            }

        private:
            std::vector<std::byte> message;

        };

        MessageBuilder messageBuilder;

        for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
            auto & c = mClients[clientIndex];
            if (!c->is_new)
                continue;
            c->is_new = false;

            auto c_db = mUserDatabase.find(c->name_container);
            if (c_db == mUserDatabase.end()) {
                // new
                if (mUserDatabase.size() >= cfg::MAX_REGISTERED_USER_COUNT) {
                    log::warning("Registered user count limit reached, refusing clients.");
                    c->clientBad = true;
                    continue;
                } else {
                    log::info("New user: {}", c->name);
                    const auto insertion = mUserDatabase.emplace(std::make_pair(
                        c->name_container,
                        UserDB{}
                    ));
                    assert(insertion.second == true);
                    c_db = insertion.first;
                }
            }

            // load from database
            assert(c_db != mUserDatabase.end());

            if (mNextID == std::numeric_limits<decltype(mNextID)>::max())
                throw std::runtime_error("MAX entity id reached. Gonna fail now.");
            const auto id = mNextID++;
            auto e = std::make_unique<EntityPlayer>(
                    id,
                    c_db->second.position,
                    c_db->second.yaw,
                    c_db->second.pitch,
                    c_db->second.tick
            );

            c->entity_reference = e.get();

            mEntities.push_back(std::move(e));
            ++mPlayerCounter;
            assert(mPlayerCounter + mBlobCounter == mEntities.size());

            messageBuilder.newMessage(cfg::Message::Id::PLAYER_ORIENTATION);
            struct player_orientation {
                glm::vec3 position;
                float yaw;
                float pitch;
                cfg::entity_id_type id;
            };
            static_assert(sizeof(player_orientation) == 24);
            player_orientation po;
            po.position = c->entity_reference->position();
            po.yaw = c->entity_reference->yaw();
            po.pitch = c->entity_reference->pitch();
            po.id = c->entity_reference->id();
            // not clean, should use serialization functions
            messageBuilder.appendMessage({ reinterpret_cast<const std::byte *>(&po), sizeof(po) });
            mServerProxy->sendMessage(clientIndex, messageBuilder.getMessageView());
        }

        assert(mUserDatabase.size() <= cfg::MAX_REGISTERED_USER_COUNT);

        for (const auto & c : mClients) {
            auto & orr = c->rxBuffer.orientation;
            if (orr.updated) {
                c->entity_reference->pushOrientation(
                    {
                        c->rxBuffer.orientation.x,
                        c->rxBuffer.orientation.y,
                        c->rxBuffer.orientation.z
                    },
                    c->rxBuffer.orientation.yaw,
                    c->rxBuffer.orientation.pitch,
                    mTickNumber
                );
                orr.updated = false;
            }
        }

        // TODO: determine if it is possible (and fix if yes) for the client
        //       to miss wanted updates when changing avtive, aware regions
        for (const auto & c : mClients) {
            auto & rx_aware = c->rxBuffer.move_aware_area;
            if (rx_aware.has_value()) {
                c->aware_area = rx_aware.value();
                rx_aware.reset();
            }
        }

        // 0 means either actually 0 or some previous message could not have been written due to lacking space
        std::array<cfg::index_type, cfg::MAX_CONNECTED_CLIENT_COUNT> clientSendBufferSpace;
        for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
            clientSendBufferSpace[clientIndex] = mServerProxy->sendBufferSpace(clientIndex);
            // TODO: send "OUT_OF_SYNC_MESSAGE" when buffer full
        }

        { // send tick index
            messageBuilder.newMessage(cfg::Message::Id::NEW_TICK);
            static_assert(std::is_same<decltype(mTickNumber), std::int32_t>::value);
            messageBuilder.appendMessage({ reinterpret_cast<const std::byte *>(&mTickNumber), sizeof(mTickNumber) });
            const auto msg = messageBuilder.getMessageView();
            for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
/*
                if (clientSendBufferSpace[clientIndex] < msg.size() + cfg::PER_MESSAGE_BUFFER_OVERHEAD) {
                    clientSendBufferSpace[clientIndex] = 0;
                    continue;
                }
                clientSendBufferSpace[clientIndex] -= msg.size() + cfg::PER_MESSAGE_BUFFER_OVERHEAD;
*/
                mServerProxy->sendMessage(clientIndex, msg);
            }
        }
        {
            updateEntities(DT);
        }

        { // add/remove entities
            for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
                auto & client = mClients[clientIndex];
                std::vector<cfg::entity_id_type> remove_ids;
                // TODO: refactor
                struct id_and_type {
                    cfg::entity_id_type id = 0;
                    cfg::entity_type_type type = 0; // TODO: pack date better
                };
                std::vector<id_and_type> add_ids;
                auto & entities_aware = client->entities_aware;

                auto aware_i = entities_aware.begin();

                // TODO: try to abstract this onto an algorithm
                for (const auto & entity : mEntities) {
                    while (
                        aware_i != entities_aware.end() &&
                        *aware_i < entity->id()
                    ) {
                        // this entity does not exist anymore
                        remove_ids.push_back(*aware_i);
                        ++aware_i;
                    }

                    const auto distance = glm::distance(entity->position(), client->entity_reference->position());
                    const auto this_id_and_type = id_and_type{ entity->id(), static_cast<cfg::entity_type_type>(entity->type()) };
                    if (aware_i == entities_aware.end()) {
                        if (distance <= cfg::MAX_VISIBLE_ENTITY_DISTANCE) {
                            add_ids.push_back(this_id_and_type);
                        } else {
                            // noop
                        }
                    } else if (*aware_i > entity->id()) {
                        if (distance <= cfg::MAX_VISIBLE_ENTITY_DISTANCE) {
                            add_ids.push_back(this_id_and_type);
                        } else {
                            // noop
                        }
                    } else {
                        assert(*aware_i == entity->id());
                        if (distance <= cfg::MAX_VISIBLE_ENTITY_DISTANCE) {
                            // noop
                        } else {
                            remove_ids.push_back(entity->id());
                        }
                        ++aware_i;
                    }
                }

                while (aware_i != entities_aware.end()) {
                    remove_ids.push_back(*aware_i);
                    ++aware_i;
                }
                
                if (remove_ids.size() > 0) {
                    log::debug("removing entities from player: {}", remove_ids.size());
                    messageBuilder.newMessage(cfg::Message::Id::REMOVE_ENTITIES);
                    messageBuilder.appendMessage({ reinterpret_cast<const std::byte *>(remove_ids.data()), static_cast<ptrdiff_t>(sizeof(remove_ids[0]) * remove_ids.size()) });
                    // TODO: handle buffer exhaustion
                    mServerProxy->sendMessage(clientIndex, messageBuilder.getMessageView());
                }
                if (add_ids.size() > 0) {
                    log::debug("adding entities to player: {}", add_ids.size());
                    messageBuilder.newMessage(cfg::Message::Id::ADD_ENTITIES);
                    messageBuilder.appendMessage({ reinterpret_cast<const std::byte *>(add_ids.data()), static_cast<ptrdiff_t>(sizeof(add_ids[0]) * add_ids.size()) });
                    // TODO: handle buffer exhaustion
                    mServerProxy->sendMessage(clientIndex, messageBuilder.getMessageView());
                }

                // this is actually in-place std::set_difference(...)
                entities_aware.erase(
                    std::remove_if(
                        entities_aware.begin(),
                        entities_aware.end(),
                        [i = remove_ids.begin(), end = remove_ids.end()] (auto entity_aware_id) mutable {
                            while (i != end && *i < entity_aware_id) {
                                ++i;
                            }
                            if (i != end && *i == entity_aware_id) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    ),
                    entities_aware.end()
                );

                std::vector<cfg::entity_id_type> tmp_add_ids;
                std::vector<cfg::entity_id_type> result_ids;
                tmp_add_ids.reserve(add_ids.size());
                for (const auto & id : add_ids)
                    tmp_add_ids.push_back(id.id);
                result_ids.resize(entities_aware.size() + tmp_add_ids.size());
                auto merge_end = std::merge(
                    tmp_add_ids.begin(), tmp_add_ids.end(),
                    entities_aware.begin(), entities_aware.end(),
                    result_ids.begin()
                );
                assert(merge_end == result_ids.end());
                entities_aware = std::move(result_ids);
            }
        }
        { // send entity updates to clients (selectively only for client->entities_aware)
            std::vector<std::byte> entity_message_buffer(64);
            for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
                auto & client = mClients[clientIndex];
                auto entity_it = mEntities.cbegin();
                
                messageBuilder.newMessage(cfg::Message::Id::ENTITY_DATA);
                // TODO: handle buffer exhaustion
                for (const auto & entity_id : client->entities_aware) {
                    while (entity_it != mEntities.end()) {
                        if ((*entity_it)->id() == entity_id)
                            break;
                        ++entity_it;
                    }

                    entity_message_buffer.clear();
                    (*entity_it)->serialize_update(entity_message_buffer);

                    messageBuilder.appendMessage({
                        entity_message_buffer.data(),
                        static_cast<ptrdiff_t>(entity_message_buffer.size())
                    });
                }
                mServerProxy->sendMessage(clientIndex, messageBuilder.getMessageView());

            }
        }
/*
        { // hacky: send all player positions instead of entities
            renderData.data.clear();
            renderData.data.reserve(mClients.size());
            int id = 0;
            for (const auto & c : mClients) {
                RenderData::RenderEntity e;
                e.id = id++;
                if (
                    c->mOrientation[0].second == c->mOrientation[1].second ||
                    c->mOrientation[0].second + 1 == c->mOrientation[1].second
                
                ) {
                    // nothing to interpolate
                    e.position.x = c->mOrientation[1].first.x;
                    e.position.y = c->mOrientation[1].first.y;
                    e.position.z = c->mOrientation[1].first.z;
                } else {
                    const float snapshot_0_time = 0.0f;
                    const float snapshot_1_time = (c->mOrientation[1].second - c->mOrientation[0].second) * DT;
                    const float this_tick_time = (mTickNumber - c->mOrientation[0].second) * DT;

                    const auto s_0 = glm::vec3{ c->mOrientation[0].first.x, c->mOrientation[0].first.y, c->mOrientation[0].first.z };
                    const auto s_1 = glm::vec3{ c->mOrientation[1].first.x, c->mOrientation[1].first.y, c->mOrientation[1].first.z };
                    const auto v = (s_1 - s_0) / snapshot_1_time;

                    const auto pos = s_0 + v * this_tick_time;

                    e.position.x = pos.x;
                    e.position.y = pos.y;
                    e.position.z = pos.z;
                }
                renderData.data.push_back(e);
            }
        }
        { // entities
            if (renderData.data.size() > 0) {
                const std::byte * body = reinterpret_cast<const std::byte *>(renderData.data.data());
                messageBuilder.newMessage(cfg::Message::Id::ENTITY_DATA);
                messageBuilder.appendMessage({ body, static_cast<ptrdiff_t>(sizeof(renderData.data[0]) * renderData.data.size()) });
                const auto msg = messageBuilder.getMessageView();
                for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
                    // TODO: handle buffer exhaustion
                    mServerProxy->sendMessage(clientIndex, msg);
                }
            }
        }
*/
        { // blocks
            // TODO: check if there were too many block update requests and kill client if yes (see chunk requests, but block requests must all be processed in one tick)
            std::vector<cfg::Message::Body::BlockData> blockUpdates;
            // compile block updates
            for (auto & c : mClients) {
                auto & bp = c->rxBuffer.blockPlacements;
                blockUpdates.insert(blockUpdates.end(), bp.begin(), bp.end());
                bp.clear();
            }

            // update blocks
            for (const auto & bu : blockUpdates) {
                log::info("BUD processed");
                const auto chunkPosition = cfg::blockPositionToChunkPosition(bu.position);
                auto * data = mChunkServer->getWritable(chunkPosition, *mChunkGenerator.get());
                data[cfg::blockPositionToBlockIndex(bu.position)] = bu.value;
                mChunkServer->release(chunkPosition);
            }

            // send block updates
            // TODO: batch (all block updates in one message)
            // TODO: only send in-range block updates (+ implement client ranges)
            for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
                const auto & c = mClients[clientIndex];
                for (const auto & bu : blockUpdates) {
                    if (!util::block_in_chunk_range(bu.position, c->aware_area))
                        continue;
                    cfg::Message::Body::BlockData blockData;
                    blockData.position.x = bu.position.x;
                    blockData.position.y = bu.position.y;
                    blockData.position.z = bu.position.z;
                    // TODO: careful with the size of the struct and padding
                    blockData.value = bu.value;
                    messageBuilder.newMessage(cfg::Message::Id::BLOCK_DATA);
                    messageBuilder.appendMessage({ reinterpret_cast<const std::byte *>(&blockData), sizeof(blockData) });
                    // TODO: handle buffer exhaustion
                    mServerProxy->sendMessage(clientIndex, messageBuilder.getMessageView());
                }
            }
        }
        std::vector<bool> clientRequestedTooManyChunks(mClients.size(), false);


        std::vector<std::size_t> numberOfChunkRequestsProcessed(mClients.size(), 0);
        { // chunks
            const auto chunkEvent = trace::event_complete("do-chunk-stuff");
            for (std::size_t i = 0; i < mClients.size(); ++i) {
                auto & cpc = mClients[i]->pendingChunkRequests;
                auto & tir = mClients[i]->rxBuffer.chunkRequests;
                const auto space = cfg::MAX_PENDING_CHUNK_REQUESTS - cpc.size();
                if (tir.size() > space) {
                    clientRequestedTooManyChunks[i] = true;
                } else {
                    cpc.insert(cpc.end(), tir.begin(), tir.end());
                }
                tir.clear();
            }


            size_t chunkSendCount = 0;
            bool finishedServingChunks;
            do {
                finishedServingChunks = true;
                for (std::size_t id = 0; id < mClients.size(); ++id) {
                    ++chunkSendCount;
                    assert(numberOfChunkRequestsProcessed[id] <= mClients[id]->pendingChunkRequests.size());
                    if (numberOfChunkRequestsProcessed[id] > mClients[id]->pendingChunkRequests.size())
                        log::error("Too many chunk requests processed.");
                    if (numberOfChunkRequestsProcessed[id] >= mClients[id]->pendingChunkRequests.size()) {
                        continue;
                    } else {
                        const auto & chunkRequest = mClients[id]->pendingChunkRequests[numberOfChunkRequestsProcessed[id]];
                        ++numberOfChunkRequestsProcessed[id];
                        // TODO: do not stall the loop for file IO (get does blocking file IO), use file IO worker thread I guess
                        // TODO: minimize the number of copy operations
                        std::array<std::byte, cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES> compressedChunk;
                        const auto compressedChunkSize = mChunkServer->getCompressedCopy(chunkRequest, *mChunkGenerator.get(), compressedChunk.data());
                        assert(compressedChunkSize > 0 && compressedChunkSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
                        if (compressedChunkSize < 1) throw std::runtime_error("Could not load chunk for some reason. Implement proper handling.");

                        messageBuilder.newMessage(cfg::Message::Id::CHUNK_DATA);
                        messageBuilder.appendMessage({ reinterpret_cast<const std::byte *>(glm::value_ptr(chunkRequest)), sizeof(chunkRequest) });
                        messageBuilder.appendMessage({ compressedChunk.data(), static_cast<std::ptrdiff_t>(compressedChunkSize) });
                        // TODO: handle buffer exhaustion
                        mServerProxy->sendMessage(id, messageBuilder.getMessageView());
                    }

                    const bool moreStuffToDo = numberOfChunkRequestsProcessed[id] < mClients[id]->pendingChunkRequests.size();
                    if (moreStuffToDo)
                        finishedServingChunks = false;
                }

                if (mTickTime + cfg::TICK_DT < std::chrono::steady_clock::now()) {
                    // we have run out of time
                    break;
                }
            } while (!finishedServingChunks);

            for (std::size_t id = 0; id < mClients.size(); ++id) {
                auto & cpc = mClients[id]->pendingChunkRequests;
                cpc.erase(cpc.begin(), cpc.begin() + numberOfChunkRequestsProcessed[id]);
            }
        }

        if constexpr (PRINT_CLIENT_INFO) {
            std::ostringstream infoText;

            infoText << "Index | ID | pending chunk requests | chunk requests processed\n";
            for (std::size_t id = 0; id < mClients.size(); ++id) {
                infoText << id << " | " << mClients[id]->id << " | " << mClients[id]->pendingChunkRequests.size();
                infoText << " | " << numberOfChunkRequestsProcessed[id] << '\n';
            }

            log::info("\n{}", infoText.str());
        }

        assert(clientRequestedTooManyChunks.size() == mClients.size());
        for (std::size_t i = 0; i < mClients.size(); ++i) {
            if (clientRequestedTooManyChunks[i]) {
                mClients[i]->clientBad = true;
            }
        }

        // remove bad clients
        for (cfg::index_type clientIndex = 0; clientIndex < mClients.size();) {
            const auto removeClient = mClients[clientIndex]->clientBad || mClients[clientIndex]->rxBuffer.disconnected;
            if (removeClient) {
                const auto & c = mClients[clientIndex];
                const auto c_db = mUserDatabase.find(c->name_container);
                assert(c_db != mUserDatabase.end());

                // TODO: save actual position, not the extrapolated one (requires storing position data received from client)
                c_db->second.position = c->entity_reference->position();
                c_db->second.yaw = c->entity_reference->yaw();
                c_db->second.pitch = c->entity_reference->pitch();
                c_db->second.tick = c->entity_reference->tick();

                c->entity_reference->kill();

                mServerProxy->eraseClient(clientIndex);
                eraseClient(clientIndex);
            } else {
                ++clientIndex;
            }
        }

        { // timing
            mTickTime += cfg::TICK_DT;
            const auto newCurrentTime = std::chrono::steady_clock::now();
            if (mTickTime < newCurrentTime) {
                log::warning("Lagging, adjusting tick time point.");
                mTickTime = newCurrentTime;
            }
        }

        if (clientRequestedStop) {
            mServerProxy->scheduleTick({});
        } else {
            mServerProxy->scheduleTick(mTickTime);
        }
        if (mTickNumber == std::numeric_limits<decltype(mTickNumber)>::max())
            throw std::runtime_error("Ran out of tick values.");
        mTickNumber += 1;
    }
}

Engine::~Engine() {
    // save database
    for (cfg::index_type clientIndex = 0; clientIndex < mClients.size(); ++clientIndex) {
        const auto & c = mClients[clientIndex];
        const auto c_db = mUserDatabase.find(c->name_container);
        assert(c_db != mUserDatabase.end());
        // TODO: save actual position, not the extrapolated one (requires storing position data received from client)
        c_db->second.position = c->entity_reference->position();
        c_db->second.yaw = c->entity_reference->yaw();
        c_db->second.pitch = c->entity_reference->pitch();
        c_db->second.tick = c->entity_reference->tick();
    }

    // TODO: fix: truncating is dangerous, can cause loss of all data if error occurs during writing
    // ... (the same is true for chunks)
    std::ofstream user_file{ mUserFileLocation, std::ofstream::trunc | std::ofstream::binary };
   
    static_assert(sizeof(user_map_node) == 40);
    // this is not safe lol. define the binary format
    for (const auto & n : mUserDatabase)
        user_file.write(reinterpret_cast<const char *>(&n), sizeof(n));
}

void Engine::updateEntities(float dt) {
    std::for_each(
        mEntities.begin(),
        mEntities.end(),
        [dt] (auto & entity) { entity->update(dt); }
    );

    mEntities.erase(
        std::remove_if(
            mEntities.begin(),
            mEntities.end(),
            [this] (const auto & entity) {
                const auto remove_it = !entity->alive();
                if (remove_it) {
                    switch (entity->type()) {
                    case cfg::EntityType::PLAYER:
                        assert(mPlayerCounter > 0);
                        --mPlayerCounter;
                        break;
                    case cfg::EntityType::BLOB:
                        assert(mBlobCounter > 0);
                        --mBlobCounter;
                        break;
                    default:
                        assert(false && "Invalid entity type.");
                        break;
                    }

                }
                return remove_it;
            }
        ),
        mEntities.end()
    );
    assert(mPlayerCounter + mBlobCounter == mEntities.size());

    // just for demo purposes
    while (mPlayerCounter * 3 > mBlobCounter) {
        if (mNextID == std::numeric_limits<decltype(mNextID)>::max())
            throw std::runtime_error("MAX entity id reached. Gonna fail now.");
        const auto id = mNextID++;
        const auto position = glm::vec3{
            mUniformDistribution(mRng) * 10.0f,
            mUniformDistribution(mRng) * 10.0f,
            mUniformDistribution(mRng) * 10.0f
        };
        auto velocity = glm::vec3{
            mUniformDistribution(mRng) * 2.0f,
            mUniformDistribution(mRng) * 2.0f,
            mUniformDistribution(mRng) * 2.0f
        };
        velocity.x = std::signbit(velocity.x) ? velocity.x - 2.0f : velocity.x + 2.0f;
        velocity.y = std::signbit(velocity.y) ? velocity.y - 2.0f : velocity.y + 2.0f;
        velocity.z = std::signbit(velocity.z) ? velocity.z - 2.0f : velocity.z + 2.0f;
        const auto timeToLive = mUniformDistribution(mRng) * 10.0f + 20.0f;
        mEntities.push_back(
            std::make_unique<EntityBlob>(
                id, position, velocity, timeToLive
            )
        );
        ++mBlobCounter;
    }
    assert(mPlayerCounter + mBlobCounter == mEntities.size());

//    Log::print(Log::Type::INFO, "# of active entities after update: ", mEntities.size());
}

void Engine::processMessage(cfg::index_type clientIndex, gsl::span<const std::byte> message) {
    assert(clientIndex >= 0 && clientIndex < mClients.size());
    mClients[clientIndex]->rxBuffer.processMessage(message);
}

void Engine::newClient(cfg::client_id_type id, cfg::user_name_type name) {
    assert(mClients.size() < cfg::MAX_CONNECTED_CLIENT_COUNT);
    assert(std::none_of(mClients.begin(), mClients.end(), [id] (const auto & c) { return c->id == id; }));
    auto & client = mClients.emplace_back(std::make_unique<Client>());
    client->id = id;
    client->rxBuffer.reserveSpace();
    client->pendingChunkRequests.reserve(cfg::MAX_PENDING_CHUNK_REQUESTS);
    std::copy(name.begin(), name.end(), client->name_container.begin());
    cfg::index_type name_length = 0;
    for (; name_length < cfg::MAX_USER_NAME_LENGTH; ++name_length)
        if (client->name_container[name_length] == '\0')
            break;
    client->name = { client->name_container.data(), static_cast<size_t>(name_length) };
    client->is_new = true;
}

void Engine::eraseClient(cfg::index_type clientIndex) {
    assert(clientIndex >= 0 && clientIndex < mClients.size());
    if (clientIndex != mClients.size() - 1) {
        std::swap(mClients[clientIndex], mClients.back());
    }
    mClients.pop_back();
}
