#pragma once

#include <cstddef>
#include <gsl/span>
#include "cfg.hpp"

class EngineBase {
public:
    virtual void tick() = 0;
    virtual void processMessage(cfg::index_type clientIndex, gsl::span<const std::byte> message) = 0;
    virtual void newClient(cfg::client_id_type id, cfg::user_name_type name) = 0;

protected:
    ~EngineBase() = default;

};
