#include "ChunkGeneratorSine.hpp"

#include <cmath>

void ChunkGeneratorSine::generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) {
    glm::ivec3 i;
    const glm::ivec3 from{ chunkPosition * cfg::CHUNK_SIZE };
    const glm::ivec3 to{ from + cfg::CHUNK_SIZE };

    size_t j = 0;
    for (i.z = from.z; i.z < to.z; ++i.z)
        for (i.y = from.y; i.y < to.y; ++i.y)
            for (i.x = from.x; i.x < to.x; ++i.x) {
                const auto set = std::sin(i.x * 0.1) * std::sin(i.z * 0.1) * 10.0 > static_cast<double>(i.y);
                blocks[j] = set ? std::rand() % 255 + 1 : 0;
                ++j;
            }
}
