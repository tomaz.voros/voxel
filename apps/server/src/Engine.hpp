#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <unordered_map>
#include <random>

#include "ChunkServer.hpp"
#include "IChunkGenerator.hpp"
#include "cfg.hpp"
#include "dataContainers.hpp"
#include "EngineBase.hpp"
#include "EntityPlayer.hpp"
class ServerBase;

struct UserDB {
    glm::vec3 position{ 0.0f, 0.0f, 0.0f };
    float yaw = 0.0f;
    float pitch = 0.0f;
    cfg::tick_type tick = 0;
};

class Engine : public EngineBase {
public:
    Engine(
        std::unique_ptr<ChunkServer> chunkServer,
        std::unique_ptr<IChunkGenerator> chunkGenerator,
        const char * world_folder
    );

    ~Engine();

    void setServerProxy(ServerBase * server_proxy);

    void tick() final;
    void processMessage(cfg::index_type clientIndex, gsl::span<const std::byte> message) final;
    /**
     * Precondition: client with given name is not curently active!
     */
    void newClient(cfg::client_id_type id, cfg::user_name_type name) final;

private:
    void eraseClient(cfg::index_type clientIndex);

    struct Client {
        Client() = default;
        Client(Client &&) = default;
        Client & operator = (Client &&) = default;
        Client(const Client &) = delete;
        Client & operator = (const Client &) = delete;
        static_assert(std::is_signed<cfg::client_id_type>::value);
        // Server id, not entity id
        cfg::client_id_type id = -1;
        // acts as a queue
        std::vector<glm::ivec3> pendingChunkRequests;

        ClientRxBuffer rxBuffer;
        bool clientBad = false;

        cfg::user_name_type name_container;
        std::string_view name;

        bool is_new = true;

        util::box<> aware_area = { { 0, 0, 0 }, { 0, 0, 0 } };

        EntityPlayer * entity_reference = nullptr;

        std::vector<cfg::entity_id_type> entities_aware;

    };

    // TODO: fix: erase is slow because of heavy Client class
    std::vector<std::unique_ptr<Client>> mClients;

    std::unique_ptr<ChunkServer> mChunkServer;
    std::unique_ptr<IChunkGenerator> mChunkGenerator;

    cfg::tick_type mTickNumber = 1; // 0 is reserved for "uninitialized chunk"

    void updateEntities(float dt);

    ServerBase * mServerProxy = nullptr;

    std::chrono::steady_clock::time_point mTickTime;

    std::unordered_map<cfg::user_name_type, UserDB> mUserDatabase;
    using user_map_node = decltype(mUserDatabase)::value_type;

    std::string mUserFileLocation;

    // TODO: Custom allocator for Entities
    // TODO: abstract entity counting and creation away
    std::vector<std::unique_ptr<EntityBase>> mEntities;
    cfg::entity_id_type mNextID = 1;
    cfg::index_type mPlayerCounter = 0;
    cfg::index_type mBlobCounter = 0;
    
    std::mt19937 mRng; // default seed
    std::uniform_real_distribution<float> mUniformDistribution{ -1.0f, std::nextafter(1.0f, 2.0f) };

};
