#include "Network.hpp"

#include <boost/asio.hpp>
#include <boost/beast/websocket/ssl.hpp>

#include "cfg.hpp"
#include "Log.hpp"
#include "EngineBase.hpp"
#include <util.hpp>

/**
 * 
 * TODO: coroutines for networking to make everything 10x easier to understand
 * 
 */

Network::Client::Client(
    boost::asio::ip::tcp::socket && inSocket,
    std::optional<boost::asio::ssl::context> & ssl_context,
    cfg::index_type clientIndex
) :
    tcp_socket{ std::move(inSocket) },
    index{ clientIndex }
{
    log::debug("Calling Client().");

    if (ssl_context.has_value())
        ws.emplace<1>(tcp_socket, ssl_context.value());
    else
        ws.emplace<2>(tcp_socket);
}

Network::Client::~Client() {
    log::debug("Calling ~Client().");
    if (pendingAsyncCallCount != 0) {
        log::error("~Client(): pendingAsyncCallCount = {}", pendingAsyncCallCount);
    }
}

void Network::Server::handshakeHandler(
    Client * client,
    const boost::system::error_code & error
) {
    assert(client->pendingAsyncCallCount == 1);
    --client->pendingAsyncCallCount;

    if (error || !mRunning) {
        if (error)
            log::info("{}", error.message());
        moveFromAuthenticatingToDisconnecting(client);
        if (!client->closing)
            closeSocket(client);
        if (client->pendingAsyncCallCount == 0) {
            scheduleClientRemoval(client);
        }
        return;
    }

    ++client->pendingAsyncCallCount;

    std::get<1>(client->ws).async_accept(
        [this, client] (const boost::system::error_code & error) {
            acceptHandler(client, error);
        }
    );
}

void Network::Server::acceptHandler(Client * client, const boost::system::error_code & error) {
    assert(client->pendingAsyncCallCount == 1);
    --client->pendingAsyncCallCount;

    if (error || !mRunning) {
        if (error)
            log::info("{}", error.message());
        moveFromAuthenticatingToDisconnecting(client);
        if (!client->closing)
            closeSocket(client);
        if (client->pendingAsyncCallCount == 0) {
            scheduleClientRemoval(client);
        }
        return;
    }

    ++client->pendingAsyncCallCount;

    auto code = [this, client] (auto & ws) {
        ws.text(false);
        ws.binary(true);
        ws.async_read(
            client->rxBuffer,
            [this, client] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                readAuthentication(client, error, bytesTransferred);
            }
        );
    };

    if (mSslContext.has_value()) {
        code(std::get<1>(client->ws));
    } else {
        code(std::get<2>(client->ws));
    }
}

void Network::Server::readAuthentication(
    Client * client,
    const boost::system::error_code & error,
    std::size_t bytesTransferred
) {
    assert(client->pendingAsyncCallCount == 1);
    --client->pendingAsyncCallCount;

    bool messageWrong = false;

    if (bytesTransferred < cfg::MIN_MESSAGE_SIZE || bytesTransferred > cfg::MAX_MESSAGE_SIZE) {
        log::warning("Error in receiving message. Message too small or too large.");
        messageWrong = true;
    } else if (bytesTransferred != client->rxBuffer.size()) {
        log::warning("Error in receiving message. Size mismatch.");
        messageWrong = true;
    } else if (bytesTransferred != cfg::MAX_USER_NAME_LENGTH + 1) {
        log::warning("Wrong message size.");
        messageWrong = true;
    }

    cfg::user_name_type user_name;
    static_assert(user_name.size() > 0);
    user_name[0] = '\0';

    if (!messageWrong) {
        const gsl::span<const std::byte> message = {
            reinterpret_cast<const std::byte *>(client->rxBuffer.data().data()),
            static_cast<cfg::index_type>(client->rxBuffer.size())
        };
        if (message.size() < 1 || message[0] != static_cast<std::byte>(cfg::Message::Id::AUTHENTICATE)) {
            messageWrong = true;
        } else {
            user_name = checkAndSanitizeUserName(message.subspan(1));
            static_assert(user_name.size() > 0);
            messageWrong = user_name[0] == '\0';
        }
    }

    // remove message
    client->rxBuffer.clear();

    if (!messageWrong) {
        const auto duplicate_user = mClientNames.find(user_name) != mClientNames.end();
        messageWrong = messageWrong || duplicate_user;
    }

    if (messageWrong || error || !mRunning) {
        log::info("Duplicate user name, refusing connection.");
        moveFromAuthenticatingToDisconnecting(client);
        if (!client->closing)
            closeSocket(client);
        if (client->pendingAsyncCallCount == 0) {
            scheduleClientRemoval(client);
        }
        return;
    }

    // TODO: set received ID, -1 as dummy for now
    assert(!mFreeIDs.empty());
    const cfg::client_id_type dummy_id = mFreeIDs.top();
    mFreeIDs.pop();
    client->id = dummy_id;

    assert(mClientNames.size() < cfg::MAX_CONNECTED_CLIENT_COUNT);
    mClientNames.insert(user_name);
    std::copy(user_name.begin(), user_name.end(), client->name.begin());
    mEngineProxy->newClient(dummy_id, user_name);

    moveFromAuthenticatingToActive(client);

    ++client->pendingAsyncCallCount;

    auto code = [this, client] (auto & ws) {
        ws.async_read(
            client->rxBuffer,
            [this, client] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                readHandler(client, error, bytesTransferred);
            }
        );
    };
    if (mSslContext.has_value()) {
        code(std::get<1>(client->ws));
    } else {
        code(std::get<2>(client->ws));
    }
}

cfg::user_name_type Network::Server::checkAndSanitizeUserName(gsl::span<const std::byte> user_name) {
    cfg::user_name_type result;
    std::fill(result.begin(), result.end(), '\0');
    // check length
    if (user_name.size() > result.size() || user_name.size() < cfg::MIN_USER_NAME_LENGTH)
        return result;
    // check characters
    cfg::index_type length = 0;
    for (; length < user_name.size(); ++length) {
        const auto c = user_name[length];
        const auto lower_case_letter = c >= std::byte{ 'a' } && c <= std::byte{ 'z' };
        const auto upper_case_letter = c >= std::byte{ 'A' } && c <= std::byte{ 'Z' };
        const auto number = c >= std::byte{ '0' } && c <= std::byte{ '9' };
        const auto underscore = c == std::byte{ '_' };
        const auto valid_character = lower_case_letter || upper_case_letter || number || underscore;
        const auto null = c == std::byte{ '\0' };
        if (null)
            break;
        if (!valid_character)
            return result;
    }
    for (cfg::index_type i = 0; i < length; ++i)
        result[i] = static_cast<char>(user_name[i]);
    return result;
}

void Network::Server::scheduleClientRemoval(Client * client) {
    boost::asio::post(
        mIoContext,
        [this, client] () {
            removeFromDisconnecting(client);
            if (mAcceptCallbackPending)
                return;
            mAcceptCallbackPending = true;
            mAcceptor.async_accept(
                [this] (const boost::system::error_code & error, boost::asio::ip::tcp::socket && socket) {
                    acceptConnectionHandler(error, std::move(socket));
                }
            );
        }
    );
}

void Network::Server::closeSocket(Client * client) {
    if (client->closing) {
        log::warning("Calling closeSocket() again.");
        return;
    }

    client->closing = true;
    boost::system::error_code ec;
    if (mSslContext.has_value()) {
        std::get<1>(client->ws).next_layer().next_layer().shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
        if (ec) log::warning("Error in shutting down socket.");
        std::get<1>(client->ws).next_layer().next_layer().close(ec);
        if (ec) log::warning("Error in closing socket.");
    } else {
        std::get<2>(client->ws).next_layer().shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
        if (ec) log::warning("Error in shutting down socket.");
        std::get<2>(client->ws).next_layer().close(ec);
        if (ec) log::warning("Error in closing socket.");
    }
}

void Network::Server::onClientIssue(Client * client) {
        constexpr cfg::index_type CLOSE_MESSAGE_SIZE = 5;
        static_assert(CLOSE_MESSAGE_SIZE >= cfg::MIN_MESSAGE_SIZE && CLOSE_MESSAGE_SIZE <= cfg::MAX_MESSAGE_SIZE);
        constexpr std::array<std::byte, CLOSE_MESSAGE_SIZE> message{
            static_cast<std::byte>(cfg::Message::Id::CLIENT_DISCONNECTED),
            std::byte{ 'a' },
            std::byte{ 'b' },
            std::byte{ 'c' },
            std::byte{ 'd' }
        };

        if (client->state == Client::State::ACTIVE && !client->closing) {
            mEngineProxy->processMessage(client->index, message);
            closeSocket(client);
        }
        else if (client->state == Client::State::DISCONNECTING && client->pendingAsyncCallCount == 0) {
            scheduleClientRemoval(client);
        } else if (client->state != Client::State::ACTIVE && client->state != Client::State::DISCONNECTING) {
            assert(false && "Invalid state.");
        }
    };

void Network::Server::readHandler(Client * client, const boost::system::error_code & error, std::size_t bytesTransferred) {
    assert(client->pendingAsyncCallCount > 0);
    --client->pendingAsyncCallCount;

    if (client->closing) {
        log::warning("Read handler called on a !okay() client.");
        onClientIssue(client);
        return;
    }
    if (error) {
        log::warning("Error in receiving message. ", error.message());
        onClientIssue(client);
        return;
    }
    bool got_binary = false;
    if (mSslContext.has_value()) {
        got_binary = std::get<1>(client->ws).got_binary();
    } else {
        got_binary = std::get<2>(client->ws).got_binary();
    }
    if (!got_binary) {
        log::warning("Error in receiving message. Message not binary.");
        onClientIssue(client);
        return;
    }
    if (bytesTransferred < cfg::MIN_MESSAGE_SIZE || bytesTransferred > cfg::MAX_MESSAGE_SIZE) {
        log::warning("Error in receiving message. Message too small or too large.");
        onClientIssue(client);
        return;
    }
    if (bytesTransferred != client->rxBuffer.size()) {
        log::warning("Error in receiving message. Size mismatch.");
        onClientIssue(client);
        return;
    }

    const gsl::span<const std::byte> message = {
        reinterpret_cast<const std::byte *>(client->rxBuffer.data().data()),
        static_cast<cfg::index_type>(client->rxBuffer.size())
    };

    static_assert(cfg::MIN_MESSAGE_SIZE > 0);
    if (message[0] == static_cast<std::byte>(cfg::Message::Id::CLIENT_DISCONNECTED)) {
        log::warning("Error in receiving message. Size mismatch.");
        onClientIssue(client);
        return;
    }

    mEngineProxy->processMessage(client->index, message);

    // remove message
    client->rxBuffer.clear();

    ++client->pendingAsyncCallCount;

    auto code = [this, client] (auto & ws) {
        ws.async_read(
            client->rxBuffer,
            [this, client] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                readHandler(client, error, bytesTransferred);
            }
        );
    };
    if (mSslContext.has_value()) {
        code(std::get<1>(client->ws));
    } else {
        code(std::get<2>(client->ws));
    }
}

void Network::Server::writeHandler(Client * client, const boost::system::error_code & error, std::size_t bytesTransferred) {
    assert(client->pendingAsyncCallCount > 0);
    --client->pendingAsyncCallCount;

    if (client->closing) {
        client->txBuffer.consume(client->txBuffer.size());
        log::warning("Send callback called on a !okay() client.");
        onClientIssue(client);
        return;
    }

    if (error) {
        client->txBuffer.consume(client->txBuffer.size());
        log::warning("Error in sending data. {}", error.message());
        onClientIssue(client);
        return;
    }

    // remove sent message
    assert(client->txBuffer.size() >= sizeof(message_size_type));
    assert(bytesTransferred <= client->txBuffer.size() - sizeof(message_size_type));
    const auto buffers = client->txBuffer.data();
    message_size_type message_size = {};
    util::readFromSpanArray(
        util::removeEnd(buffers, client->txBuffer.size() - sizeof(message_size_type)),
        gsl::span<std::byte>{ reinterpret_cast<std::byte *>(&message_size), sizeof(message_size_type) }
    );
    assert(bytesTransferred == message_size);
    client->txBuffer.consume(message_size + sizeof(message_size_type));

    if (client->txBuffer.size() == 0) {
        // all pending data was sent, nothing to do for now
        return;
    }

    assert(client->txBuffer.size() >= sizeof(message_size_type));
    const auto buffers_new = client->txBuffer.data();
    message_size_type message_size_new = {};
    util::readFromSpanArray(
        util::removeEnd(buffers_new, client->txBuffer.size() - sizeof(message_size_type)),
        gsl::span<std::byte>{ reinterpret_cast<std::byte *>(&message_size_new), sizeof(message_size_type) }
    );

    assert(client->txBuffer.size() >= message_size_new + sizeof(message_size_type));
    const auto message_buffers_out = util::removeBegin(
        util::removeEnd(buffers_new, client->txBuffer.size() - sizeof(message_size_type) - message_size_new),
        sizeof(message_size_type)
    );

    ++client->pendingAsyncCallCount;

    auto code = [this, client, &message_buffers_out] (auto & ws) {
        ws.async_write(
            message_buffers_out,
            [this, client] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                writeHandler(client, error, bytesTransferred);
            }
        );
    };

    if (mSslContext.has_value()) {
        code(std::get<1>(client->ws));
    } else {
        code(std::get<2>(client->ws));
    }
}

Network::Server::Server() :
    mIoContext{ 1 },
    mAcceptor{ mIoContext },
    mTimer{ mIoContext }
{
    mClients.reserve(cfg::MAX_CONNECTED_CLIENT_COUNT);

    static_assert(std::numeric_limits<cfg::client_id_type>::max() >= cfg::MAX_CONNECTED_CLIENT_COUNT);
    for (cfg::client_id_type id = 0; id < cfg::MAX_CONNECTED_CLIENT_COUNT; ++id)
        mFreeIDs.push(id);
}

bool Network::Server::setupSsl(const ssl_file_names & ssl_files) {
    try {
        mSslContext.emplace(boost::asio::ssl::context::method::tlsv12_server);
        auto & context = mSslContext.value();

        context.set_password_callback(
            [](std::size_t, boost::asio::ssl::context_base::password_purpose) {
                return "";
            }
        );

        context.set_options(
            boost::asio::ssl::context::default_workarounds |
            boost::asio::ssl::context::no_sslv2 |
            boost::asio::ssl::context::single_dh_use
        );

        context.use_certificate_chain_file(ssl_files.certificate);
        context.use_private_key_file(
            ssl_files.private_key,
            boost::asio::ssl::context::file_format::pem
        );
        context.use_tmp_dh_file(ssl_files.diffie_hellman);
    } catch (...) {
        log::error("Failed to setup ssl.");
        return false;
    }
    return true;
}

void Network::Server::acceptConnectionHandler(
    const boost::system::error_code & error,
    boost::asio::ip::tcp::socket && socket
) {
    static_assert(cfg::MAX_CONNECTED_CLIENT_COUNT > 0);
    assert(mAcceptCallbackPending == true);
    mAcceptCallbackPending = false;
    const auto total_client_count_before = mClients.size();
    assert(total_client_count_before < cfg::MAX_CONNECTED_CLIENT_COUNT);
    assert(mActiveCount + mAuthenticatingCount + mDisconnectingCount == total_client_count_before);

    if (!mRunning) {
        log::info("Discarding incoming client connection because !mRunning.");
        return;
    }

    if (error) {
        log::warning("Refusing client connection due to an error.");
    } else {
        try {
            boost::asio::ip::tcp::no_delay option(true);
            socket.set_option(option);
        } catch (...) {
            log::error("Failed to disable Nagle's algorithm on socket.");
        }
        auto client = mClients.emplace_back(
            std::make_unique<Client>(std::move(socket), mSslContext, mClients.size())
        ).get();

        assert(client->pendingAsyncCallCount == 0);
        ++client->pendingAsyncCallCount;

        // TODO: set option timeout (boost 1.70+)
        if (mSslContext.has_value()) {
            auto & ws = std::get<1>(client->ws);
            ws.read_message_max(cfg::MAX_MESSAGE_SIZE);
            ws.next_layer().async_handshake(
                boost::asio::ssl::stream_base::handshake_type::server,
                [this, client] (const boost::system::error_code & error) { handshakeHandler(client, error); }
            );
        } else {
            auto & ws = std::get<2>(client->ws);
            ws.read_message_max(cfg::MAX_MESSAGE_SIZE);
            ws.async_accept(
                [this, client] (const boost::system::error_code & error) {
                    acceptHandler(client, error);
                }
            );
        }
    }

    const auto total_client_count_after = mClients.size();
    if (total_client_count_after > total_client_count_before) {
        ++mAuthenticatingCount;
    }

    log::info(
        "Server::acceptConnectionHandler(): Client count: {}. Disconnecting count: {}. Authenticating count: {}.",
        mActiveCount,
        mDisconnectingCount,
        mAuthenticatingCount
    );

    if (total_client_count_after < cfg::MAX_CONNECTED_CLIENT_COUNT) {
        mAcceptCallbackPending = true;
        mAcceptor.async_accept(
            [this] (const boost::system::error_code & error, boost::asio::ip::tcp::socket && socket) {
                acceptConnectionHandler(error, std::move(socket));
            }
        );
    }
}

void Network::Server::moveFromAuthenticatingToDisconnecting(Client * client) {
    assert(client->index >= mActiveCount && client->index < mClients.size());
    assert(client->state == Client::State::AUTHENTICATING);
    assert(mAuthenticatingCount > 0);
    assert(mAuthenticatingCount + mActiveCount + mDisconnectingCount == mClients.size());

    client->state = Client::State::DISCONNECTING;
    const auto & client_a = mClients[client->index];
    assert(client_a->index == client->index);

    ++mDisconnectingCount;
    --mAuthenticatingCount;
}

void Network::Server::moveFromAuthenticatingToActive(Client * client) {
    assert(client->index >= mActiveCount && client->index < mClients.size());
    assert(client->state == Client::State::AUTHENTICATING);
    assert(mAuthenticatingCount > 0);
    assert(mAuthenticatingCount + mActiveCount + mDisconnectingCount == mClients.size());

    client->state = Client::State::ACTIVE;

    auto & client_a = mClients[client->index];
    auto & client_b = mClients[mActiveCount];
    assert(client_a->index == client->index && client_b->index == mActiveCount);

    if (client_a != client_b) {
        std::swap(client_a->index, client_b->index);
        std::swap(client_a, client_b);
    }

    ++mActiveCount;
    --mAuthenticatingCount;
}

void Network::Server::moveFromActiveToDisconnecting(Client * client) {
    assert(client->index >= 0 && client->index < mActiveCount);
    assert(client->state == Client::State::ACTIVE);
    assert(mAuthenticatingCount + mActiveCount + mDisconnectingCount == mClients.size());

    client->state = Client::State::DISCONNECTING;

    auto & client_a = mClients[client->index];
    auto & client_b = mClients[mActiveCount - 1];
    assert(client_a->index == client->index && client_b->index == mActiveCount - 1);

    if (client_a != client_b) {
        std::swap(client_a->index, client_b->index);
        std::swap(client_a, client_b);
    }

    ++mDisconnectingCount;
    --mActiveCount;
}

void Network::Server::removeFromDisconnecting(Client * client) {
    assert(client->index >= mActiveCount && client->index < mClients.size());
    assert(client->state == Client::State::DISCONNECTING);
    assert(mDisconnectingCount > 0);
    assert(mAuthenticatingCount + mActiveCount + mDisconnectingCount == mClients.size());
    assert(client->index == mClients[client->index]->index);
    assert(client->pendingAsyncCallCount == 0);

    auto & client_a = mClients[client->index];
    auto & client_b = mClients.back();
    assert(client_a->index == client->index && client_b->index == mClients.size() - 1);
    if (client_a != client_b) {
        std::swap(client_a->index, client_b->index);
        std::swap(client_a, client_b);
    }

    mClients.pop_back();

    --mDisconnectingCount;
}

void Network::Server::scheduleTick(std::optional<std::chrono::steady_clock::time_point> tickTime) {
    assert(mTimerSet == false);
    assert(mRunning == true);

    if (tickTime.has_value()) {
        mTimerSet = true;
        mTimer.expires_at(tickTime.value());
        mTimer.async_wait(
            [this] (const boost::system::error_code & error) { preTick(error); }
        );
    } else {
        mRunning = false;
        for (auto & client : mClients)
            closeSocket(client.get());
        if (mAcceptCallbackPending) {
            boost::system::error_code ec;
            mAcceptor.cancel(ec);
            if (ec)
                log::warning("Error in closing acceptor.");
        }
        mIoContext.get_executor().on_work_finished();
    }
}

void Network::Server::eraseClient(cfg::index_type clientIndex) {
    assert(clientIndex >= 0 && clientIndex < mActiveCount);
    assert(clientIndex < mClients.size());

    auto client = mClients[clientIndex].get();
    assert(client->index == clientIndex);

    mFreeIDs.push(client->id);

    const auto name_iter = mClientNames.find(client->name);
    assert(name_iter != mClientNames.end());
    mClientNames.erase(name_iter);

    moveFromActiveToDisconnecting(client);
    if(!client->closing)
        closeSocket(client);
    if (client->pendingAsyncCallCount == 0) {
        scheduleClientRemoval(client);
    }
}

void Network::Server::preTick(const boost::system::error_code & error) {
    assert(mRunning == true);
    assert(mTimerSet == true);
    mTimerSet = false;
    if (error) {
        log::error("{}", error.message());
        std::this_thread::sleep_for(cfg::TICK_DT);
    }
    mEngineProxy->tick();
}

cfg::index_type Network::Server::sendBufferSpace(cfg::index_type clientIndex) {
    assert(clientIndex >= 0 && clientIndex < mActiveCount);
    assert(clientIndex < mClients.size());
    const auto client = mClients[clientIndex].get();
    return client->txBuffer.capacity() - client->txBuffer.size();
}

void Network::Server::sendMessage(cfg::index_type clientIndex, gsl::span<const std::byte> message) {
    assert(clientIndex >= 0 && clientIndex < mActiveCount);
    assert(clientIndex < mClients.size());

    auto client = mClients[clientIndex].get();

    // TODO: instead of copying message, let the user of this class get the tx_buffer directly

    if (client->closing) {
        log::warning("Calling send on a !okay() client.");
        return;
    }

    assert(message.size() >= cfg::MIN_MESSAGE_SIZE && message.size() <= cfg::MAX_MESSAGE_SIZE);

    const auto tx_buffer_was_empty = client->txBuffer.size() == 0;

    if (client->txBuffer.capacity() - client->txBuffer.size() < sizeof(message_size_type) + message.size()) {
        log::error("Unable to allocate space for the message in TX buffer.");
        closeSocket(client); // read handler initializes the cleanup for the client
        return;
    }

    // save message size
    const message_size_type message_size = message.size();
    const auto size_buffers = client->txBuffer.prepare(sizeof(message_size_type));
    util::writeToSpanArray(
        gsl::span<const std::byte>{
            reinterpret_cast<const std::byte *>(&message_size),
            sizeof(message_size_type)
        },
        size_buffers
    );
    client->txBuffer.commit(sizeof(message_size_type));

    // save message content
    const auto message_buffers = client->txBuffer.prepare(message_size);
    util::writeToSpanArray(message, message_buffers);
    client->txBuffer.commit(message_size);

    if (tx_buffer_was_empty) {
        const auto message_buffers_out = util::removeBegin(client->txBuffer.data(), sizeof(message_size_type));

        ++client->pendingAsyncCallCount;

        // writeHandler needs to be scheduled
        auto code = [this, client, &message_buffers_out] (auto & ws) {
            ws.async_write(
                message_buffers_out,
                [this, client] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                    writeHandler(client, error, bytesTransferred);
                }
            );
        };

        if (mSslContext.has_value()) {
            code(std::get<1>(client->ws));
        } else {
            code(std::get<2>(client->ws));
        }
    }
}

void Network::Server::serve(
    const boost::asio::ip::tcp::endpoint & endpoint,
    const std::optional<ssl_file_names> & ssl_files,
    EngineBase * engineProxy,
    std::chrono::steady_clock::time_point firstTickTime
) {
    if (mServed) {
        log::error("Server::serve() can only be called once.");
        return;
    }
    mServed = true;

    if (cfg::MAX_CONNECTED_CLIENT_COUNT < 1) {
        log::warning("Server::serve() called with cfg::MAX_CONNECTED_CLIENT_COUNT < 1. Returning.");
        return;
    }

    if (ssl_files.has_value()) {
        if (!setupSsl(ssl_files.value())) {
            log::warning("Server::serve() failed to setup ssl.");
            return;
        }
    }

    assert(engineProxy != nullptr);
    mEngineProxy = engineProxy;
    mRunning = true;

    scheduleTick(firstTickTime);

    try {
        mAcceptor.open(endpoint.protocol());
        mAcceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address{ true });
        mAcceptor.bind(endpoint);
        mAcceptor.listen(cfg::ACCEPTOR_BACKLOG_SIZE_HINT);
        if (mClients.size() < cfg::MAX_CONNECTED_CLIENT_COUNT) {
            mAcceptCallbackPending = true;
            mAcceptor.async_accept(
                [this] (const boost::system::error_code & error, boost::asio::ip::tcp::socket && socket) {
                    acceptConnectionHandler(error, std::move(socket));
                }
            );
        }
        mIoContext.get_executor().on_work_started();
        mIoContext.run();
    } catch (const boost::system::system_error & error) {
        log::error("Server loop exited with following boost exception:\n{}", error.what());
    } catch (const std::exception & error) {
        log::error("Server loop exited with following std exception:\n{}", error.what());
    } catch (...) {
        log::error("Server loop exited with an exception.");
    }
}
