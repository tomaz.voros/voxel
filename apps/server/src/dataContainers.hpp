#pragma once

#include <optional>
#include <vector>
#include "cfg.hpp"
#include <util.hpp>

struct ClientRxBuffer {
    ClientRxBuffer() :
        stopRequested{ false }
    {}

    ClientRxBuffer & operator = (ClientRxBuffer &&) = default;
    ClientRxBuffer(ClientRxBuffer &&) = default;
    ClientRxBuffer & operator = (const ClientRxBuffer &) = delete;
    ClientRxBuffer(ClientRxBuffer &) = delete;

    void reserveSpace() {
        blockPlacements.reserve(cfg::MAX_BLOCK_PLACEMENTS_PER_TICK);
        chunkRequests.reserve(cfg::MAX_PENDING_CHUNK_REQUESTS);
    }

    /**
     * Processes the passed in header and message and stores result in the correct
     * container if the message is valid.
     * @param[in] header Header of the message to be processed.
     * @param[in] body Body of the message to be processed.
     * @return True if message was valid and data was added to corresponding container.
     */
    bool processMessage(
        gsl::span<const std::byte> message
    );

    std::vector<cfg::Message::Body::BlockData> blockPlacements;
    std::vector<cfg::Message::Body::ChunkRequestDataPosition> chunkRequests;
    bool stopRequested;

    bool nothingReceived = true;

    bool disconnected = false;

    // TODO: move out / replace with std::optional<...>
    struct Orientation {
        bool updated = false;
        float x, y, z, yaw, pitch;
    } orientation;

    std::optional<util::box<>> move_aware_area;
    bool aware_region_out_of_sync = false;

};
