#include "ChunkServer.hpp"

#include <sstream>

#include "IChunkGenerator.hpp"
#include "Log.hpp"
#include "compression.hpp"
#include "trace.hpp"

ChunkServer::ChunkServer(const std::filesystem::path & worldFolder) :
    mWorldFolder{ worldFolder }
{
    // TODO: error handling when the folder does not exist and can not be created
    std::error_code directoryCreationErrorCode;
    const auto folderCreated = std::filesystem::create_directories(mWorldFolder, directoryCreationErrorCode);
    if (folderCreated) {
        log::info("A new save folder created.");
    }
    if (directoryCreationErrorCode) {
        log::error("Error save folder creation: {}.", directoryCreationErrorCode.message());
    }
}

ChunkServer::~ChunkServer() {
    log::info("Saving chunks.");
    while (mChunks.unusedSize() > 0) {
        auto & positionChunk = mChunks.lru();
        if (positionChunk.second.dirty)
            saveChunk(positionChunk.first, positionChunk.second);
        mChunks.pop();
    }

    log::info("Saving regions.");
    while (mRegions.unusedSize() > 0) {
        auto & positionRegion = mRegions.lru();
        saveRegion(positionRegion.first, positionRegion.second);
        positionRegion.second.closeFile();
        mRegions.pop();
    }
    if (mChunks.usedSize() > 0) {
        log::warning("not all chunks saved.");
    }
    if (mRegions.usedSize() > 0) {
        log::warning("not all regions saved.");
    }
}

void ChunkServer::saveRegion(const glm::ivec3 & regionPosition, Region & region) {
    if (!region.dirty) return;
    const std::array<std::uint32_t, 2> endGarbage{ region.end, region.garbage };
    region.write(endGarbage.data(), 2 * sizeof(std::uint32_t), 0);
    region.dirty = false;
}

void ChunkServer::saveChunk(const glm::ivec3 & chunkPosition, Chunk & chunk) {
    if (!chunk.dirty) return;
    const auto regionPosition = cfg::chunkPositionToRegionPosition(chunkPosition);
    auto & region = getRegion(regionPosition);
    region.dirty = true;
    const auto chunkIndex = cfg::chunkPositionToChunkIndex(chunkPosition);
    std::array<std::uint32_t, 2> positionSize;
    region.read(positionSize.data(), 2 * sizeof(std::uint32_t), (2 + 2 * chunkIndex) * sizeof(std::uint32_t));

    std::vector<std::byte> compressBuffer;
    if (chunk.compressedSize == 0) {
        compressBuffer.resize(cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
        const auto compressedSize = compression::chunk::compress(reinterpret_cast<const std::byte *>(chunk.content.blocks.data()), compressBuffer.data());
        compressBuffer.resize(compressedSize);
    } else {
        assert(chunk.compressedSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
        compressBuffer.resize(chunk.compressedSize);
        std::copy_n(chunk.content.compressedBlocks.begin(), chunk.compressedSize, compressBuffer.begin());
    }

    if (compressBuffer.size() == 0) {
        // TODO: properly handle compression error
        log::error("while compressing chunk.");
    }

    const std::uint32_t newSize = compressBuffer.size();
    const std::int64_t sizeDifference = static_cast<std::int64_t>(positionSize[1]) - static_cast<std::int64_t>(newSize);
    if (sizeDifference < 0) {
        // append to file
        const std::array<std::uint32_t, 2> newPositionSize{ region.end, newSize };
        region.write(newPositionSize.data(), 2 * sizeof(std::uint32_t), (2 + 2 * chunkIndex) * sizeof(std::uint32_t));
        region.write(compressBuffer.data(), newSize, region.end);
        region.end += newSize;
        region.garbage += positionSize[1];
    } else {
        // insert in-place
        // TODO: replace 'uint32: size' with 'uint16: size, uint16: capacity' to be able to use in-place more often
        region.write(compressBuffer.data(), newSize, positionSize[0]);
        region.garbage += static_cast<std::uint32_t>(sizeDifference);
        region.write(&newSize, sizeof(newSize), (2 + 2 * chunkIndex + 1) * sizeof(std::uint32_t));
    }

    chunk.dirty = false;

    // TODO: possibly find a better time to do defragmenting
    if (region.garbage >= cfg::DEFRAGMENT_GARBAGE_THRESHOLD)
        defragmentRegion(region, regionPosition);

    returnRegion(regionPosition);
}

void ChunkServer::defragmentRegion(Region & region, const glm::ivec3 & regionPosition) {
    log::info("Defragmenting region.");
    std::vector<std::uint32_t> chunkLookupTable;
    chunkLookupTable.resize(2 * cfg::REGION_VOLUME);
    region.read(
        chunkLookupTable.data(),
        chunkLookupTable.size() * sizeof(std::uint32_t),
        sizeof(std::uint32_t) * 2
    );

    static constexpr std::size_t END_OF_LUT_IN_BYTES_IN_FILE = (2 * cfg::REGION_VOLUME + 2) * sizeof(std::uint32_t);

    std::vector<std::byte> chunkData;
    assert(region.end >= END_OF_LUT_IN_BYTES_IN_FILE);
    if (region.end < END_OF_LUT_IN_BYTES_IN_FILE) {
        // TODO: see string content
        throw std::runtime_error("Broken file likely. Handle that appropriately. 1.");
    }
    chunkData.resize(region.end - END_OF_LUT_IN_BYTES_IN_FILE);
    region.read(chunkData.data(), chunkData.size() * sizeof(std::byte), END_OF_LUT_IN_BYTES_IN_FILE);

    std::uint32_t fileEnd = END_OF_LUT_IN_BYTES_IN_FILE;
    for (std::size_t i = 0; i < cfg::REGION_VOLUME; ++i) {
        const auto chunkPosition = chunkLookupTable[i * 2];
        const auto chunkSize = chunkLookupTable[i * 2 + 1];
        if (chunkPosition == 0)
            continue;
        
        assert(chunkPosition >= END_OF_LUT_IN_BYTES_IN_FILE);
        if (chunkPosition < END_OF_LUT_IN_BYTES_IN_FILE) {
            // TODO: see string content
            throw std::runtime_error("Broken file likely. Handle that appropriately. 2.");
        }
        const auto chunkOffset = chunkPosition - END_OF_LUT_IN_BYTES_IN_FILE;
        region.write(chunkData.data() + chunkOffset, chunkSize, fileEnd);
        chunkLookupTable[i * 2] = fileEnd;
        fileEnd += chunkSize;
    }

    region.write(
        chunkLookupTable.data(),
        chunkLookupTable.size() * sizeof(std::uint32_t),
        sizeof(std::uint32_t) * 2
    );

    const std::array<std::uint32_t, 2> endGarbage{ fileEnd, 0 };
    region.write(endGarbage.data(), 2 * sizeof(std::uint32_t), 0);

    region.dirty = false;
    region.end = endGarbage[0];
    region.garbage = endGarbage[1];
    // TODO: file IO error handling
    std::filesystem::resize_file(constructRegionFilePath(regionPosition), fileEnd);
}

void ChunkServer::returnRegion(const glm::ivec3 & regionPosition) {
    mRegions.release(regionPosition);
}

void ChunkServer::returnChunk(const glm::ivec3 & chunkPosition) {
    mChunks.release(chunkPosition);
}

ChunkServer::Region & ChunkServer::getRegion(const glm::ivec3 & regionPosition) {
    bool regionCached;
    auto & region = mRegions.emplace_get(regionPosition, regionCached);
    if (!regionCached) {
        const std::filesystem::path filePath = constructRegionFilePath(regionPosition);
        const bool regionIsNew = region.openFile(filePath);
        if (regionIsNew) {
            region.end = (2 * cfg::REGION_VOLUME + 2) * sizeof(std::uint32_t);
            region.garbage = 0;
            region.dirty = true;
            std::filesystem::resize_file(filePath, region.end);
        } else {
            std::array<std::uint32_t, 2> endGarbage;
            region.read(endGarbage.data(), 2 * sizeof(std::uint32_t), 0);
            region.end = endGarbage[0];
            region.garbage = endGarbage[1];
            region.dirty = false;
        }
    }

    // garbage collection
    while (mRegions.unusedSize() > 0 && (mRegions.unusedSize() + mRegions.usedSize()) > cfg::MAX_REGIONS_LOADED) {
        auto & positionRegion = mRegions.lru();
        saveRegion(positionRegion.first, positionRegion.second);
        positionRegion.second.closeFile();
        mRegions.pop();
    }
    if (mRegions.unusedSize() + mRegions.usedSize() > cfg::MAX_REGIONS_LOADED) {
        log::warning("usedRegions + unusedRegions > MAX_REGIONS_LOADED <= {}", mRegions.unusedSize() + mRegions.usedSize());
    }

    return region;
}

std::filesystem::path ChunkServer::constructRegionFilePath(const glm::ivec3 & regionPosition) const {
    std::ostringstream fileNameBuffer;
    fileNameBuffer << regionPosition.x << 'x' << regionPosition.y << 'x' << regionPosition.z;
    return mWorldFolder / fileNameBuffer.str();
}

cfg::Block * ChunkServer::getWritable(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator) {
    auto & chunk = getChunk(chunkPosition, chunkGenerator);
    chunk.dirty = true;
    if (chunk.compressedSize != 0) {
        assert(chunk.compressedSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
        const auto compressEvent = trace::event_complete("decompress-for-writable-chunk", "ChunkServer");
        decltype(chunk.content.blocks) tmpChunkBuffer;
        const auto decompressedSize = compression::chunk::decompress(
            chunk.content.compressedBlocks.data(),
            chunk.compressedSize,
            reinterpret_cast<std::byte *>(tmpChunkBuffer.data())
        );
        assert(decompressedSize == cfg::CHUNK_SIZE_BYTES);
        std::copy_n(tmpChunkBuffer.begin(), tmpChunkBuffer.size(), chunk.content.blocks.begin());
        chunk.compressedSize = 0;
    }

    return chunk.content.blocks.data();
}

const cfg::Block * ChunkServer::get(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator) {
    auto & chunk = getChunk(chunkPosition, chunkGenerator);
    if (chunk.compressedSize != 0) {
        assert(chunk.compressedSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
        const auto compressEvent = trace::event_complete("decompress-for-get-chunk", "ChunkServer");
        decltype(chunk.content.blocks) tmpChunkBuffer;
        const auto decompressedSize = compression::chunk::decompress(
            chunk.content.compressedBlocks.data(),
            chunk.compressedSize,
            reinterpret_cast<std::byte *>(tmpChunkBuffer.data())
        );
        assert(decompressedSize == cfg::CHUNK_SIZE_BYTES);
        std::copy_n(tmpChunkBuffer.begin(), tmpChunkBuffer.size(), chunk.content.blocks.begin());
        chunk.compressedSize = 0;
    }
    return chunk.content.blocks.data();
}

struct TimeSince {
public:
    using Clock = std::chrono::steady_clock;

    TimeSince() : mStart{ Clock::now() } {}

    double secondsPassed() const {
        return std::chrono::duration<double>(Clock::now() - mStart).count();
    }

private:
    Clock::time_point mStart;

};
std::size_t ChunkServer::getCompressedCopy(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator, std::byte * out) {
    // TODO: might be better to read the compressed version directly from the disk or have a compressed version cached somewhere
    // TODO: store both compressed and uncompressed version when both are frequently accessed
    const auto compressEvent = trace::event_complete("get-compressed-copy-chunk", "ChunkServer");
    auto & chunk = getChunk(chunkPosition, chunkGenerator);
    size_t compressedChunkSize = 0;
    if (chunk.compressedSize == 0) {
        const auto compressEvent = trace::event_complete("compress-chunk", "ChunkServer");
        compressedChunkSize = compression::chunk::compress(reinterpret_cast<const std::byte *>(chunk.content.blocks.data()), out);
        assert(compressedChunkSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
        // let's hope there is no rogue thread writing to the buffer at while this function executes
        // TODO: store recompressed version only when necessary
        std::copy_n(out, compressedChunkSize, chunk.content.compressedBlocks.data());
        chunk.compressedSize = compressedChunkSize;
    } else {
        const auto compressEvent = trace::event_complete("copy-compressed-chunk", "ChunkServer");
        assert(chunk.compressedSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
        std::copy_n(chunk.content.compressedBlocks.begin(), chunk.compressedSize, out);
        compressedChunkSize = chunk.compressedSize;
    }
    assert(compressedChunkSize <= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
    returnChunk(chunkPosition);
    return compressedChunkSize;
}


ChunkServer::Chunk & ChunkServer::getChunk(const glm::ivec3 & chunkPosition, IChunkGenerator & chunkGenerator) {
    const auto retrieveChunkEvent = trace::event_complete("retrieve-chunk", "ChunkServer");
    bool chunkCached;
    auto & chunk = mChunks.emplace_get(chunkPosition, chunkCached);
    if (!chunkCached) {
        const auto regionPosition = cfg::chunkPositionToRegionPosition(chunkPosition);
        auto & region = getRegion(regionPosition);
        const auto chunkIndex = cfg::chunkPositionToChunkIndex(chunkPosition);
        std::array<std::uint32_t, 2> positionSize;
        region.read(positionSize.data(), 2 * sizeof(std::uint32_t), (2 + 2 * chunkIndex) * sizeof(std::uint32_t));
        if (positionSize[0] != 0) { // if exists
            if (positionSize[1] > cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES)
                log::warning("Invalid chunk stored in region because the size is {}.", positionSize[1]);
            // TODO: compressed chunk validation (needed at a later stage, maybe even as late as in client)
            region.read(chunk.content.compressedBlocks.data(), positionSize[1], positionSize[0]);
            chunk.compressedSize = positionSize[1];
            chunk.dirty = false;
        } else { // if it does not exist
            const auto generateChunkEvent = trace::event_complete("generate-chunk", "ChunkServer");
            chunkGenerator.generateChunk(chunk.content.blocks.data(), chunkPosition);
            chunk.compressedSize = 0;
            chunk.dirty = true;
        }
        returnRegion(regionPosition);
    }

    // garbage collection
    const auto garbageCollectionEvent = trace::event_complete("defragment-garbage-collect", "ChunkServer");
    while (mChunks.unusedSize() > 0 && (mChunks.unusedSize() + mChunks.usedSize()) > cfg::MAX_CHUNKS_LOADED) {
        auto & positionChunk = mChunks.lru();
        if (positionChunk.second.dirty)
            saveChunk(positionChunk.first, positionChunk.second);
        mChunks.pop();
    }
    if (mChunks.unusedSize() + mChunks.usedSize() > cfg::MAX_CHUNKS_LOADED) {
        log::warning("usedChunks + unusedChunks > MAX_CHUNKS_LOADED <= {}", mChunks.unusedSize() + mChunks.usedSize());
    }

    return chunk;
}

void ChunkServer::release(const glm::ivec3 & chunkPosition) {
    returnChunk(chunkPosition);
}

void ChunkServer::Region::read(void * buffer, std::size_t count, std::size_t position) {
    // pread(file, buffer, count, position);

    // TODO: file IO error handling
    file.seekg(position);
    file.read(reinterpret_cast<char *>(buffer), count);
}

void ChunkServer::Region::write(const void * buffer, std::size_t count, std::size_t position) {
    // pwrite(file, buffer, count, position);

    // TODO: file IO error handling
    file.seekp(position);
    file.write(reinterpret_cast<const char *>(buffer), count);
}

bool ChunkServer::Region::openFile(const std::filesystem::path & filePath) {
    // TODO: file IO error handling
    closeFile();
    const auto fileExists = std::filesystem::exists(filePath);
    if (!fileExists) {
        log::info("Creating new region file.");
        std::ofstream tmpFile{ filePath };
    }
    file.open(
        filePath,
        std::fstream::in | std::fstream::out | std::fstream::binary
    );
    return !fileExists;
}

void ChunkServer::Region::closeFile() {
    // TODO: file IO error handling
    assert(dirty == false);
    if (file.is_open()) {
        file.close();
    }
}
