#pragma once

#include "IChunkGenerator.hpp"

class ChunkGeneratorSine : public IChunkGenerator {
public:
    void generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) override;

};
