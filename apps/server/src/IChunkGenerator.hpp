#pragma once

#include "cfg.hpp"

class IChunkGenerator {
public:
    virtual void generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) = 0;

    virtual ~IChunkGenerator() = default;

};
