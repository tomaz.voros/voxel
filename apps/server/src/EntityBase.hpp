#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include "cfg.hpp"

// TODO: make pure virtual and use EntityBlob as default instantiable object
class EntityBase {
public:
    EntityBase(cfg::entity_id_type id) : m_id{ id } {}
    virtual ~EntityBase() = default;
    virtual cfg::entity_id_type id() const final { return m_id; }
    // must be overridden, bad design choice?
    virtual cfg::EntityType type() const { return cfg::EntityType::BASE; }
    virtual glm::vec3 position() const = 0;
    virtual bool alive() const = 0;
    virtual void update(float dt) = 0;
    virtual void serialize_update(std::vector<std::byte> & out) const = 0;

private:
    const cfg::entity_id_type m_id;

};
