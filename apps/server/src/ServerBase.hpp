#pragma once

#include <optional>
#include <chrono>
#include <cstddef>
#include <gsl/span>
#include "cfg.hpp"

class ServerBase {
public:
    /**
     * Send message to client with given index.
     */
    virtual void sendMessage(cfg::index_type clientIndex, gsl::span<const std::byte> message) = 0;
    /**
     * Erases client with the given index. Client with the largest index gets its index.
     */
    virtual void eraseClient(cfg::index_type clientIndex) = 0;
    /**
     * If tickTime has no value, the server stopping is scheduled instead.
     */
    virtual void scheduleTick(std::optional<std::chrono::steady_clock::time_point> tickTime) = 0;
    /**
     * Returns the amount of available space for messages and message overhead.
     */
    virtual cfg::index_type sendBufferSpace(cfg::index_type clientIndex) = 0;

protected:
    ~ServerBase() = default;

};
