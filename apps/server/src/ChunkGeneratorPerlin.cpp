#include "ChunkGeneratorPerlin.hpp"

#include <cmath>

#include <glm/gtc/noise.hpp>

void ChunkGeneratorPerlin::generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) {
    const glm::ivec3 from{ chunkPosition * cfg::CHUNK_SIZE };
    const glm::ivec3 to{ from + cfg::CHUNK_SIZE };

    size_t j = 0;
    glm::ivec3 i;
    for (i.z = from.z; i.z < to.z; ++i.z)
        for (i.y = from.y; i.y < to.y; ++i.y)
            for (i.x = from.x; i.x < to.x; ++i.x) {
                const auto value = glm::perlin(glm::vec2{ i.x, i.z } / 10.0f);
                if (value > float(i.y + 10) / 10.0f)
                    blocks[j] = 1 + std::rand() % 7;
                else
                    blocks[j] = 0;
                ++j;
            }
}
