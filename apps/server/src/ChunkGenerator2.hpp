#pragma once

#include "IChunkGenerator.hpp"

class ChunkGenerator2 : public IChunkGenerator {
public:
    void generateChunk(cfg::Block * blocks, const glm::ivec3 & chunkPosition) override;

};
