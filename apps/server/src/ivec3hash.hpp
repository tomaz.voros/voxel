#pragma once

#include <cstddef>
#include <functional>
#include <glm/vec3.hpp>

namespace std {
    template <> struct hash<glm::ivec3> {
        std::size_t operator () (const glm::ivec3 & v) const {
            return std::size_t(v.x) ^ std::size_t(v.y) ^ std::size_t(v.z);
        }
    };
}
