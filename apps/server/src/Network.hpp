#pragma once

#include <vector>
#include <cstddef>
#include <chrono>
#include <array>
#include <stack>
#include <set>
#include <variant>
#include <optional>

#include <boost/asio/ssl.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/websocket.hpp>
// TODO: use std::span
#include <gsl/span>

#include "cfg.hpp"
#include "ServerBase.hpp"
class EngineBase;

namespace Network {
struct ssl_file_names {
    const char * certificate = nullptr;
    const char * private_key = nullptr;
    const char * diffie_hellman = nullptr;
};

class Client {
public:
    Client(
        boost::asio::ip::tcp::socket && inSocket,
        std::optional<boost::asio::ssl::context> & ssl_context,
        cfg::index_type clientIndex
    );

    ~Client();

    // TODO: SSL/TLS
    // TODO: boost::asio::ip::tcp::no_delay(true)
    // TODO: boost::asio::ip::tcp::receive_buffer_size
    // TODO: boost::asio::ip::tcp::send_buffer_size
    boost::asio::ip::tcp::socket tcp_socket;
    // boost::beast::async_detect_ssl() can be used in the future
    std::variant<
        std::monostate,
        boost::beast::websocket::stream<boost::asio::ssl::stream<boost::asio::ip::tcp::socket &>>,
        boost::beast::websocket::stream<boost::asio::ip::tcp::socket &>
    > ws;
    bool closing = false;
    cfg::index_type index = -1;
    cfg::index_type pendingAsyncCallCount = 0;
    cfg::client_id_type id = -1;
    boost::beast::static_buffer<cfg::CLIENT_TX_BUFFER_SIZE> txBuffer;
    boost::beast::flat_static_buffer<cfg::MAX_MESSAGE_SIZE> rxBuffer;

    enum class State {
        AUTHENTICATING,
        ACTIVE,
        DISCONNECTING
    };

    State state = State::AUTHENTICATING;

    cfg::user_name_type name;

};

class Server : public ServerBase {
public:
    Server();
    ~Server() = default;

    /**
     * Starts and executes the networking loop.
     */
    void serve(
        const boost::asio::ip::tcp::endpoint & endpoint,
        const std::optional<ssl_file_names> & ssl_files,
        EngineBase * engineProxy,
        std::chrono::steady_clock::time_point firstTickTime
    );

    /**
     * The ServerBase functions do not call any EngineBase functions.
     */
    // TODO: check message pipelining (passing directly to linux tcp buffer), maybe non-blocking non-async write
    //       ISSUE: websockets do not support non-blocking synchronous IO
    void sendMessage(cfg::index_type clientIndex, gsl::span<const std::byte> message) final;
    void scheduleTick(std::optional<std::chrono::steady_clock::time_point> tickTime) final;
    void eraseClient(cfg::index_type clientIndex) final;
    cfg::index_type sendBufferSpace(cfg::index_type clientIndex) final;

private:
    using message_size_type = cfg::message_size_type;
    static_assert(std::numeric_limits<message_size_type>::min() <= cfg::MIN_MESSAGE_SIZE);
    static_assert(std::numeric_limits<message_size_type>::max() >= cfg::MAX_MESSAGE_SIZE);

    void acceptConnectionHandler(const boost::system::error_code & error, boost::asio::ip::tcp::socket && socket);
    void preTick(const boost::system::error_code & e);

    void closeSocket(Client * client);
    // TODO: consider separate thread for send loops that is active during tick update
    void writeHandler(Client * client, const boost::system::error_code & error, std::size_t bytesTransferred);
    // ssl handshake
    void handshakeHandler(Client * client, const boost::system::error_code & error);
    // websocket accept
    void acceptHandler(Client * client, const boost::system::error_code & error);
    void readHandler(Client * client, const boost::system::error_code & error, std::size_t bytesTransferred);
    // user authentication
    void readAuthentication(Client * client, const boost::system::error_code & error, std::size_t bytesTransferred);

    void moveFromAuthenticatingToDisconnecting(Client * client);
    void moveFromAuthenticatingToActive(Client * client);
    void moveFromActiveToDisconnecting(Client * client);
    void removeFromDisconnecting(Client * client);
    void scheduleClientRemoval(Client * client);
    void onClientIssue(Client * client);

    static cfg::user_name_type checkAndSanitizeUserName(gsl::span<const std::byte> user_name);

    bool setupSsl(const ssl_file_names & ssl_files);

    boost::asio::io_context mIoContext;
    boost::asio::ip::tcp::acceptor mAcceptor;
    boost::asio::steady_timer mTimer;
    bool mTimerSet = false;
    EngineBase * mEngineProxy = nullptr;
    bool mRunning = false;
    bool mAcceptCallbackPending = false;

    /**
     * First #mActiveCount are active clients,
     * the rest are other states mixed.
     */
    std::vector<std::unique_ptr<Client>> mClients;
    cfg::index_type mActiveCount = 0;
    cfg::index_type mAuthenticatingCount = 0;
    cfg::index_type mDisconnectingCount = 0;
    std::set<cfg::user_name_type> mClientNames;

    /**
     * TODO: proper authentication system instead of this joke.
     */
    std::stack<cfg::client_id_type> mFreeIDs;

    bool mServed = false;

    std::optional<boost::asio::ssl::context> mSslContext;

};
}
