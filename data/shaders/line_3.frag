#version 450 core

layout (location = 0) out vec4 outColor;
layout (location = 2) uniform vec4 uColor;

void main() {
    outColor = uColor;
}
