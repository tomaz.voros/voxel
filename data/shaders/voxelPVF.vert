#version 460 core

// #define USE_OVERRIDE_TEXTURE

layout (std430, binding = 0) readonly restrict buffer VertexData {
    float offX;
    float offY;
    float offZ;
    uint quadCount;
    uint vertexData[];
} vertexBuffer;

layout (location = 0) uniform mat4 uV;
layout (location = 1) uniform mat4 uVP;
layout (location = 2) uniform vec3 uOffset;
layout (location = 3) uniform float uAoOffset;
layout (location = 4) uniform float uAoScale;
// layout (location = 5) uniform vec3 uWorldSpeceOffset;
layout (location = 12) uniform float uAoShadeStrength;

// TODO: two-sided face: get side which is currently visible as uniform and render that side (flip quad if back side facing towards it)
//                       flipping face is possible thanks to PVF :)

out vec2 aoCoord;
flat out vec4 aoColors;
out vec3 textureCoord;
flat out vec2 textureOff;

// out vec3 worldSpacePos;
out vec3 viewSpacePos;

out vec3 bary;
out vec3 augmentedBary;
flat out vec3 edgeDistances;

out vec4 debugColor;

flat out uint side;

const float TEXTURE_TILES_PER_ROW = 4.0; // TODO: when changing also change the same define in the fragment shader
const float FULL_QUAD_SIZE = 16.0;

vec3 hsvToRgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

uvec3 getFaceData() {
    uint faceOffset = uint(gl_VertexID) / 4u * 3u;
    return uvec3(
        vertexBuffer.vertexData[faceOffset],
        vertexBuffer.vertexData[faceOffset + 1u],
        vertexBuffer.vertexData[faceOffset + 2u]
    );
}

vec3 rotateRight(vec3 vector, uint amount) {
    if (amount == 1)
        vector = vector.zxy;
    else if (amount == 2)
        vector = vector.yzx;
    return vector;
}

uint reverseWinding(uint index) {
    return (index & 0x1u) << 1u ^ index;
}

void main() {
    // TODO: use unsigned % operator or bitwise to avoid bad codegen

    // TODO: determine wether the precise qualifier is necessary if using float instead of int for computation

    uvec3 faceData = getFaceData();
    // TODO: deferred value decoding to save memory
    vec2 inTexturePosition = (uvec2(faceData.x) >> uvec2(20u, 8u) & 0xfffu) / FULL_QUAD_SIZE;
    vec2 inSize = ((uvec2(faceData.x) >> uvec2(4u, 0u) & 0xfu) + 1u) / FULL_QUAD_SIZE;
    vec3 inPosition = (uvec3(faceData.y) >> uvec3(23u, 14u, 5u) & 0x1ffu) / FULL_QUAD_SIZE;
    uint inRotateBy = faceData.y >> 3u & 0x3u;
    uint inSideIndex = faceData.y & 0x7u;
    uint inAmbientOcclusion = faceData.z >> 24u;
    bool inTranspose = bool(faceData.z >> 23u & 0x1u);

#if defined(USE_OVERRIDE_TEXTURE)
    inTexturePosition = vec2(0.0, 0.0);
#endif

    vec2 uv;

    { // compute position and texture position
        uint planeIndex = inSideIndex >> 1u;
        // canonicalize position
        // this could be done when generating
        inPosition = rotateRight(inPosition, 2u - planeIndex);

        bool planeSide = bool(inSideIndex & 0x1u);

        uint cornerIndex = uint(gl_VertexID) & 0x3u;

        if (!planeSide)
            cornerIndex = reverseWinding(cornerIndex);

        bvec2 offsetMask = bvec2(uvec2(cornerIndex) >> uvec2(0u, 1u) & 0x1u);
        offsetMask.x = offsetMask.x != offsetMask.y;
        inPosition.xy += inSize * vec2(offsetMask);

        uint textureVertexIndex = cornerIndex;

        if (planeSide == inTranspose)
            textureVertexIndex = reverseWinding(textureVertexIndex);

        // this could be done when generating
        if (inTranspose)
            inRotateBy = 4 - inRotateBy;

        textureVertexIndex = textureVertexIndex + inRotateBy & 0x3u;

        bool rotationIsOdd = bool(inRotateBy & 0x1u);

        if ((rotationIsOdd == planeSide) != inTranspose)
            inSize = inSize.yx;

        bvec2 textureOffsetMask = bvec2(uvec2(textureVertexIndex) >> uvec2(0u, 1u) & 0x1u);
        textureOffsetMask.x = textureOffsetMask.x != textureOffsetMask.y;

        // inTexturePosition += inSize * vec2(textureOffsetMask);
        uv = inSize * vec2(textureOffsetMask);
        uv /= TEXTURE_TILES_PER_ROW;

        inTexturePosition /= TEXTURE_TILES_PER_ROW;

        // un-canonicalize dimensions
        inPosition = rotateRight(inPosition, planeIndex + 1u);
    }

    // worldSpacePos = inPosition + uWorldSpeceOffset;
    vec3 of = vec3(vertexBuffer.offX, vertexBuffer.offY, vertexBuffer.offZ);
    viewSpacePos = vec3(uV * vec4(inPosition + uOffset + of, 1.0));
    gl_Position = uVP * vec4(inPosition + uOffset + of, 1.0);

    // textureCoord = vec3(inTexturePosition, 0.0);
    textureOff = inTexturePosition;
    textureCoord = vec3(uv, 0.0);

    aoColors = uvec4(inAmbientOcclusion) >> uvec4(0u, 2u, 4u, 6u) & 3u;
    aoColors = 1.0 - uAoShadeStrength * aoColors;

    int hi = (gl_VertexID >> 1) & 1;
    aoCoord = vec2((gl_VertexID & 1) != hi, hi);

    int vertexIndex = gl_VertexID & 0x3;

    bary = vec3(
        vertexIndex == 0,
        vertexIndex == 2,
        float(vertexIndex == 1 || vertexIndex == 3)
    );

    augmentedBary = bary;

    edgeDistances = vec3(
        1.0,
        1.0,
        0.7071
    );

    switch (gl_VertexID % 4) {
        case 0:
            debugColor = vec4(1.0, 0.0, 0.0, 1.0);
            break;
        case 1:
            debugColor = vec4(0.0, 1.0, 0.0, 1.0);
            break;
        case 2:
            debugColor = vec4(0.0, 0.0, 1.0, 1.0);
            break;
        case 3:
            debugColor = vec4(1.0, 1.0, 0.0, 1.0);
            break;
    }

    if (inSideIndex > 5 || inSideIndex < 0)
        debugColor = vec4(0.0, 0.0, 0.0, 1.0);
    else
        debugColor = vec4(hsvToRgb(vec3(float(inSideIndex) / 6.0, 1.0, 1.0)), 1.0);

    side = inSideIndex;
}
