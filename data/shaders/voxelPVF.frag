#version 460 core

// #define USE_DEBUG_TEXTURE
// #define USE_DEBUG_SIDE_COLOR
// #define DEBUG_TRANSPARENCY

const float TEXTURE_TILES_PER_ROW = 4.0; // TODO: when changing also change the same define in the vertex shader

layout (location = 0) out vec4 outColor;

layout (location = 6) uniform vec3 uCameraViewSpacePosition;
layout (location = 7) uniform vec3 uCameraWorldSpacePosition;

layout (location = 8) uniform sampler2DArray uVoxelTexture;

layout (location = 9) uniform vec3 uFogColor;
layout (location = 10) uniform float uFogStart;
layout (location = 11) uniform float uFogTransitionDistance;

in vec2 aoCoord;
flat in vec4 aoColors;
in vec3 textureCoord;
flat in vec2 textureOff;

in vec3 viewSpacePos;
// in vec3 worldSpacePos;
in vec3 bary;
in vec3 augmentedBary;
flat in vec3 edgeDistances;

in vec4 debugColor;

flat in uint side;

float biLerp(float a, float b, float c, float d, float s, float t) {
    float x1 = mix(a, b, s);
    float x2 = mix(c, d, s);
    return mix(x1, x2, t);
}

float wireFrame(vec3 bary) {
	vec3 d = smoothstep(vec3(0.0), fwidth(bary), bary);
	return min(min(d.x, d.y), d.z);
}

ivec2 traceRay(vec3 origin3, vec3 direction3) {
    // TODO: choose the two components based on the block side
    vec2 origin = origin3.xy;
    vec2 direction = direction3.xy;

    vec2 originPosition = floor(origin);
    ivec2 XY = ivec2(originPosition);
    bvec2 positive = greaterThanEqual(XY, vec2(0.0));
    ivec2 stepXY = ivec2(positive) * 2 - 1;
    vec2 tDelta = abs(1.0 / direction);
    vec2 tMax;
    tMax.x = isinf(tDelta.x) ? 1.0 / 0.0 : (positive.x ? float(originPosition.x) - origin.x + 1.0 : origin.x - float(originPosition.x)) * tDelta.x;
    tMax.y = isinf(tDelta.y) ? 1.0 / 0.0 : (positive.y ? float(originPosition.y) - origin.y + 1.0 : origin.y - float(originPosition.y)) * tDelta.y;

    for (int i = 0; i < 16; ++i) {
        if (tMax.x < tMax.y) {
            XY.x += stepXY.x;
            tMax.x += tDelta.x;
        } else {
            XY.y += stepXY.y;
            tMax.y += tDelta.y;
        }
        // TODO: check height aka. hit
    }

    return XY;
}

void main() {
    vec2 dfdx = dFdx(textureCoord.st);
    vec2 dfdy = dFdy(textureCoord.st);

#if 1 // fix uv out of bounds
    vec2 texCoordFixed = min(textureCoord.st, vec2((1.0 - 0.5 / 16.0) / TEXTURE_TILES_PER_ROW));
#else
    vec2 texCoordFixed = textureCoord.st;
#endif
    vec3 texUV = vec3(texCoordFixed, textureCoord.p) + vec3(textureOff, 0.0);

/*
    outColor = debugColor;
    return;
*/
    float interpolatedShade = biLerp(
        aoColors.x, aoColors.y, aoColors.z, aoColors.w,
        aoCoord.s, aoCoord.t
    );

    float fragmentDistance = length(uCameraViewSpacePosition - viewSpacePos);

    //float lod = textureQueryLod(uVoxelTexture, texUV.st).y;

    vec4 texColor = textureGrad(uVoxelTexture, texUV, dfdx, dfdy);
// #if defined(USE_DEBUG_TEXTURE)
//     vec2 debugCoord;
//     switch (side) {
//         case 0:
//         case 1:
//             debugCoord = worldSpacePos.yz;
//             break;
//         case 2:
//         case 3:
//             debugCoord = worldSpacePos.xz;
//             break;
//         case 4:
//         default:
//             debugCoord = worldSpacePos.xy;
//             break;
//     }
//     // TODO: merge fract() call
//     debugCoord = fract(debugCoord);
//     debugCoord /= TEXTURE_TILES_PER_ROW;
//     outColor = textureGrad(uVoxelTexture, vec3(debugCoord, 0.0), dfdx, dfdy);
//     return;
// #endif

#if defined(USE_DEBUG_SIDE_COLOR)
    outColor = debugColor;
    return;
#endif

    //texColor = textureLod(uVoxelTexture, texUV, fragmentDistance / 50.0 + 10.0);
    //outColor = vec4(vec3(interpolatedShade), 1.0) * texColor;
    //outColor.rgb *= clamp(2.0 - lod / 3.0, 0.0, 1.0);

#if defined(DEBUG_TRANSPARENCY)
    if (texColor.a < 0.5)
        texColor = vec4(1.0, 0.0, 1.0, 1.0);
#else
    if (texColor.a < 0.5)
        discard;
#endif

    float fog = (fragmentDistance - uFogStart) / uFogTransitionDistance;
    fog = 1.0 - clamp(fog, 0.0, 1.0);

    outColor = vec4(vec3(mix(0.9, interpolatedShade, fog)), 1.0) * texColor;
    outColor.rgb = mix(uFogColor, outColor.rgb, fog);

    vec2 posdx = dFdx(aoCoord);
    vec2 posdy = dFdy(aoCoord);

    vec2 posdmax = max(abs(posdx), abs(posdy));

    // outColor = vec4(texUV.st, 0.0, 1.0);
/*
    float s = fragmentDistance / 10.0;
    vec2 a  = (aoCoord * 2.0 - 1.0) * s;
    float someDistFromCenderQuad = length(a * a);*/

    //outColor *= clamp(1.0 - someDistFromCenderQuad, 0.0, 1.0);


    //outColor.rgb = vec3(1.0 - posdmax.x * 16.0, 1.0, 1.0);
    //outColor.rgb *= mix(0.0, interpolatedShade, fog);

    /*vec3 col = vec3(0.0, 0.0, 0.0);
    if (lod < 1.0)
        col = vec3(1.0, 0.0, 0.0);
    else if (lod < 2.0)
        col = vec3(0.0, 1.0, 0.0);
    else if (lod < 3.0)
        col = vec3(0.0, 0.0, 1.0);
    else
        col = vec3(0.0, 0.0, 0.0);*/
    //outColor.rgb = lod.rrr / 5.0;
    //outColor.rgb = col;

#if 0
	outColor.rgb = outColor.rgb / 2.0 + debugColor / 2.0;
#endif

/*
	float wire = wireFrame(bary);

	vec3 d3 = augmentedBary;
	float d = min(min(d3.x, d3.y), d3.z);
	float bevel = d < 0.2 ? 0.0 : 1.0;
	float bevel2 = d >= 0.8 && d <= 1.0 ? 0.0 : 1.0;

	float distanceFromVertex = 1.0 - d3.y;
	float bevel3 = distanceFromVertex > 0.8 ? 1.0 : 0.0;
	float bevel4 = distanceFromVertex < 0.2 ? 1.0 : 0.0;
	
	if (wire < 1.0)
		outColor = vec4(1.0);
	else
		outColor = vec4(bevel4, 0.0, bevel3, 1.0);
*/
}
