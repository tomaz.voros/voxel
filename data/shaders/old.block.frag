#version 330 core

layout (location = 0) out vec4 outColor;

uniform sampler2DArray uVoxelTexture;

uniform vec3 uCameraViewSpacePosition;
uniform vec3 uCameraWorldSpacePosition;

in vec2 aoCoord;
flat in vec4 aoColors;
in vec3 textureCoord;
in vec3 viewSpacePos;
in vec3 worldSpacePos;
in vec3 bary;
in vec3 augmentedBary;
flat in vec3 edgeDistances;

float biLerp(float a, float b, float c, float d, float s, float t) {
    float x = mix(a, b, t);
    float y = mix(c, d, t);
    return mix(x, y, s);
}

float wireFrame(vec3 bary) {
	vec3 d = smoothstep(vec3(0.0), fwidth(bary), bary);
	return min(min(d.x, d.y), d.z);
}

void main() {
    float interpolatedShade = biLerp(
        aoColors.x, aoColors.y, aoColors.z, aoColors.w,
        aoCoord.s, aoCoord.t
    );

    outColor = vec4(vec3(interpolatedShade), 1.0) * texture(uVoxelTexture, textureCoord);

/*
	float wire = wireFrame(bary);

	vec3 d3 = augmentedBary;
	float d = min(min(d3.x, d3.y), d3.z);
	float bevel = d < 0.2 ? 0.0 : 1.0;
	float bevel2 = d >= 0.8 && d <= 1.0 ? 0.0 : 1.0;

	float distanceFromVertex = 1.0 - d3.y;
	float bevel3 = distanceFromVertex > 0.8 ? 1.0 : 0.0;
	float bevel4 = distanceFromVertex < 0.2 ? 1.0 : 0.0;
	
	if (wire < 1.0)
		outColor = vec4(1.0);
	else
		outColor = vec4(bevel4, 0.0, bevel3, 1.0);
*/
}
