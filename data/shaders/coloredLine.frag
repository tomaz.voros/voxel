#version 460 core

layout (location = 0) out vec4 Color;

layout (location = 1) uniform sampler2DArray uTexture;

in vec4 color;
in vec3 uv;

void main() {
    Color = color;
    Color = texture(uTexture, uv);
}
