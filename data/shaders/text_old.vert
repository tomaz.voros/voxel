#version 330 core

layout (location = 0) in float inCharacter;
layout (location = 1) in vec2 inPosition;

uniform vec2 uScale;
uniform vec2 uOffset;

flat out float character;

void main() {
    gl_Position = vec4(inPosition * uScale + uOffset, 0.0, 1.0);
    character = inCharacter;
}
