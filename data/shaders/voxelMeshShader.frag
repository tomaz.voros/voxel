#version 460

const float TEXTURE_TILES_PER_ROW = 4.0; // TODO: when changing also change the same define in the vertex shader

layout (location = 0) out vec4 outColor;


layout (location = 3) uniform vec3 uCameraViewSpacePosition;

layout (location = 4) uniform sampler2DArray uVoxelTexture;

layout (location = 5) uniform vec3 uFogColor;
layout (location = 6) uniform float uFogStart;
layout (location = 7) uniform float uFogTransitionDistance;

perprimitiveNV in PerPrimitive {
    vec2 textureOff;
    vec4 aoColors;
} perPrimitive;
in PerVertex {
    vec3 textureCoord;
    vec2 aoCoord;
    vec3 viewSpacePos;
} perVertex;

float biLerp(float a, float b, float c, float d, float s, float t) {
    float x1 = mix(a, b, s);
    float x2 = mix(c, d, s);
    return mix(x1, x2, t);
}

void main() {
    vec2 dfdx = dFdx(perVertex.textureCoord.st);
    vec2 dfdy = dFdy(perVertex.textureCoord.st);

    vec2 texCoordFixed = min(perVertex.textureCoord.st, vec2((1.0 - 0.5 / 16.0) / TEXTURE_TILES_PER_ROW));
    vec3 texUV = vec3(texCoordFixed, perVertex.textureCoord.p) + vec3(perPrimitive.textureOff, 0.0);

    float interpolatedShade = biLerp(
        perPrimitive.aoColors.x, perPrimitive.aoColors.y, perPrimitive.aoColors.z, perPrimitive.aoColors.w,
        perVertex.aoCoord.s, perVertex.aoCoord.t
    );

    float fragmentDistance = length(uCameraViewSpacePosition - perVertex.viewSpacePos);

    vec4 texColor = textureGrad(uVoxelTexture, texUV, dfdx, dfdy);

    if (texColor.a < 0.5)
        discard;

    float fog = (fragmentDistance - uFogStart) / uFogTransitionDistance;
    fog = 1.0 - clamp(fog, 0.0, 1.0);

    outColor = vec4(vec3(mix(0.9, interpolatedShade, fog)), 1.0) * texColor;
    outColor.rgb = mix(uFogColor, outColor.rgb, fog);
}
