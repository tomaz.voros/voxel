#version 450 core

layout (location = 0) in vec2 inCharacterPosition;
layout (location = 1) in vec2 inTexturePosition;

layout (location = 0) uniform vec2 uScale;
layout (location = 1) uniform vec2 uOffset;

out vec2 texturePosition;

void main() {
    gl_Position = vec4(inCharacterPosition * uScale + uOffset, 0.0, 1.0);
    texturePosition = inTexturePosition;
}
