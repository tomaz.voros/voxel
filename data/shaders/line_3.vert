#version 450 core

layout (location = 0) in vec2 inPosition;

layout (location = 0) uniform vec2 uScale;
layout (location = 1) uniform vec2 uOffset;

void main() {
    gl_Position = vec4(inPosition * uScale + uOffset, 0.0, 1.0);
}
