#version 460 core

layout (location = 0) out vec4 outColor;

layout (location = 6) uniform vec3 uCameraViewSpacePosition;
layout (location = 7) uniform vec3 uCameraWorldSpacePosition;

layout (location = 8) uniform sampler2DArray uVoxelTexture;

layout (location = 9) uniform vec3 uFogColor;

in vec2 aoCoord;
flat in vec4 aoColors;
in vec3 textureCoord;
in vec3 viewSpacePos;
in vec3 worldSpacePos;
in vec3 bary;
in vec3 augmentedBary;
flat in vec3 edgeDistances;

in vec3 debugColor;

float biLerp(float a, float b, float c, float d, float s, float t) {
    float x1 = mix(a, b, s);
    float x2 = mix(c, d, s);
    return mix(x1, x2, t);
}

float wireFrame(vec3 bary) {
	vec3 d = smoothstep(vec3(0.0), fwidth(bary), bary);
	return min(min(d.x, d.y), d.z);
}

void main() {
    float interpolatedShade = biLerp(
        aoColors.x, aoColors.y, aoColors.z, aoColors.w,
        aoCoord.s, aoCoord.t
    );

    float fragmentDistance = length(uCameraViewSpacePosition - viewSpacePos);

    float lod = textureQueryLod(uVoxelTexture, textureCoord.st).y;

    vec4 texColor = texture(uVoxelTexture, textureCoord);
    //texColor = textureLod(uVoxelTexture, textureCoord, fragmentDistance / 50.0 + 10.0);
    outColor = vec4(vec3(interpolatedShade), 1.0) * texColor;
    //outColor.rgb *= clamp(2.0 - lod / 3.0, 0.0, 1.0);

    float fog = (clamp((160.0 - fragmentDistance) / 100.0, -1.0, 1.0)  + 1.0) / 2.0;

    outColor = vec4(vec3(mix(0.5, interpolatedShade, fog)), 1.0) * texColor;
    outColor.rgb = mix(uFogColor, outColor.rgb, fog);

    //outColor.rgb = mix(vec3(0.7, 0.8, 0.9), outColor.rgb, fog);

    vec2 posdx = dFdx(aoCoord);
    vec2 posdy = dFdy(aoCoord);

    vec2 posdmax = max(abs(posdx), abs(posdy));


/*
    float s = fragmentDistance / 10.0;
    vec2 a  = (aoCoord * 2.0 - 1.0) * s;
    float someDistFromCenderQuad = length(a * a);*/

    //outColor *= clamp(1.0 - someDistFromCenderQuad, 0.0, 1.0);


    //outColor.rgb = vec3(1.0 - posdmax.x * 16.0, 1.0, 1.0);
    //outColor.rgb *= mix(0.0, interpolatedShade, fog);

    /*vec3 col = vec3(0.0, 0.0, 0.0);
    if (lod < 1.0)
        col = vec3(1.0, 0.0, 0.0);
    else if (lod < 2.0)
        col = vec3(0.0, 1.0, 0.0);
    else if (lod < 3.0)
        col = vec3(0.0, 0.0, 1.0);
    else
        col = vec3(0.0, 0.0, 0.0);*/
    //outColor.rgb = lod.rrr / 5.0;
    //outColor.rgb = col;

#if 0
	outColor.rgb = outColor.rgb / 2.0 + debugColor / 2.0;
#endif

/*
	float wire = wireFrame(bary);

	vec3 d3 = augmentedBary;
	float d = min(min(d3.x, d3.y), d3.z);
	float bevel = d < 0.2 ? 0.0 : 1.0;
	float bevel2 = d >= 0.8 && d <= 1.0 ? 0.0 : 1.0;

	float distanceFromVertex = 1.0 - d3.y;
	float bevel3 = distanceFromVertex > 0.8 ? 1.0 : 0.0;
	float bevel4 = distanceFromVertex < 0.2 ? 1.0 : 0.0;
	
	if (wire < 1.0)
		outColor = vec4(1.0);
	else
		outColor = vec4(bevel4, 0.0, bevel3, 1.0);
*/
}
