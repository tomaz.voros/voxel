#version 460 core

layout (location = 0) in vec4 Position;
layout (location = 1) in vec4 Color;

layout (location = 0) uniform mat4 VP;

out vec4 color;
out vec3 uv;

void main() {
    gl_Position = VP * Position;
    color = Color;
    uv = vec3(Position.xz, Color.a);
}
