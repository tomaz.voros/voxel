#version 330 core

in vec3 texCoord;
out vec4 color;
uniform samplerCube uSkyBox;
uniform vec3 uCameraWorldSpacePosition;
in vec3 worldSpacePos;

void main() {
    color = texture(uSkyBox, texCoord);
}
