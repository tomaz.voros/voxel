#version 460 core

layout (location = 0) in vec3 inPosition;
layout (location = 1) in uint inAO;
layout (location = 2) in vec2 inTexturePosition;

layout (location = 0) uniform mat4 uV;
layout (location = 1) uniform mat4 uVP;
layout (location = 2) uniform vec3 uOffset;
layout (location = 3) uniform float uAoOffset;
layout (location = 4) uniform float uAoScale;
layout (location = 5) uniform vec3 uWorldSpeceOffset;

out vec2 aoCoord;
flat out vec4 aoColors;
out vec3 textureCoord;

out vec3 worldSpacePos;
out vec3 viewSpacePos;

out vec3 bary;
out vec3 augmentedBary;
flat out vec3 edgeDistances;

out vec3 debugColor;

const float AO_SHADE_STRENGTH = 0.25;

void main() {
    worldSpacePos = inPosition + uWorldSpeceOffset;
    viewSpacePos = vec3(uV * vec4(inPosition + uOffset, 1.0));
    gl_Position = uVP * vec4(inPosition + uOffset, 1.0);
    // 1 / 8 = 0.125
    textureCoord = vec3(inTexturePosition * 0.125, 0.0);

    aoColors = vec4(
        inAO >> 0u & 3u,
        inAO >> 2u & 3u,
        inAO >> 6u & 3u,
        inAO >> 4u & 3u
    );
    aoColors = 1.0 - AO_SHADE_STRENGTH * aoColors;

    int hi = (gl_VertexID >> 1) & 1;
    aoCoord = vec2((gl_VertexID & 1) != hi, hi);

    int vertexIndex = gl_VertexID & 0x3;

    bary = vec3(
        vertexIndex == 0,
        vertexIndex == 2,
        float(vertexIndex == 1 || vertexIndex == 3)
    );

    augmentedBary = bary;

    edgeDistances = vec3(
        1.0,
        1.0,
        0.7071
    );

    switch (gl_VertexID % 4) {
        case 0:
            debugColor = vec3(1.0, 0.0, 0.0);
            break;
        case 1:
            debugColor = vec3(0.0, 1.0, 0.0);
            break;
        case 2:
            debugColor = vec3(0.0, 0.0, 1.0);
            break;
        case 3:
            debugColor = vec3(1.0, 1.0, 0.0);
            break;
    }
}
