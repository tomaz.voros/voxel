#version 330 core

layout (location = 0) in vec3 inPosition;

uniform mat4 uVP;
uniform vec3 uPosition;

void main() {
    gl_Position = uVP * vec4(inPosition * 0.2 + uPosition, 1.0);
}
