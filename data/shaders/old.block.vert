#version 330 core

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec4 inAO;
layout (location = 2) in uint inType;

uniform mat4 uV;
uniform mat4 uVP;
uniform vec3 uOffset;
uniform float uAoOffset;
uniform float uAoScale;
uniform vec3 uWorldSpeceOffset;

out vec2 aoCoord;
flat out vec4 aoColors;
out vec3 textureCoord;

out vec3 worldSpacePos;
out vec3 viewSpacePos;

out vec3 bary;
out vec3 augmentedBary;
flat out vec3 edgeDistances;

const uint ATLAS_SIZE = 4u;

void main() {
    worldSpacePos = inPosition + uWorldSpeceOffset;
    viewSpacePos = vec3(uV * vec4(inPosition + uOffset, 1.0));
    gl_Position = uVP * vec4(inPosition + uOffset, 1.0);
    aoCoord = vec2((gl_VertexID & 1) ^ ((gl_VertexID >> 1) & 1), (gl_VertexID >> 1) & 1);
    aoColors = inAO * uAoScale + uAoOffset;

    textureCoord = vec3(
        inType % ATLAS_SIZE,
        inType / ATLAS_SIZE % ATLAS_SIZE,
        inType / ATLAS_SIZE / ATLAS_SIZE
    );

    textureCoord.xy = (textureCoord.xy + aoCoord / 2.0 + 0.25) / vec2(ATLAS_SIZE, ATLAS_SIZE);

	int vertexIndex = gl_VertexID & 0x3;

	bary = vec3(
		vertexIndex == 0,
		vertexIndex == 2,
		float(vertexIndex == 1 || vertexIndex == 3)
	);

	augmentedBary = bary;

	edgeDistances = vec3(
		1.0,
		1.0,
		0.7071
	);

	//augmentedBary.x += 2.0;
	//augmentedBary.y += 2.0;
	//augmentedBary.z += 2.0;
}
