#version 330 core

layout (location = 0) in vec3 inPosition;

out vec3 texCoord;
out vec3 worldSpacePos;

uniform mat4 uVP;

void main() {
    texCoord = inPosition;
    vec4 pos = uVP * vec4(inPosition, 1.0);
    gl_Position = pos.xyww;
    // not actually world space position
    worldSpacePos = normalize(inPosition) * 100.0;
}
