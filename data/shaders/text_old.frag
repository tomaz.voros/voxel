#version 330 core

layout (location = 0) out vec4 outColor;

uniform sampler2DArray uCharacterTexture;
uniform float uTransparency;

flat in float character;

void main() {
    float color = texture(uCharacterTexture, vec3(gl_PointCoord, character)).r;
    outColor = vec4(vec3(color), 1.0 - color * uTransparency);
}
