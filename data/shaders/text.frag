#version 450 core

in vec2 texturePosition;

layout (location = 0) out vec4 outColor;
layout (location = 2) uniform vec4 uFontColor;
layout (location = 3) uniform vec4 uBackgroundColor;
layout (location = 4) uniform sampler2D uCharacterTexture;

void main() {
    float charColor = texture(uCharacterTexture, texturePosition).r;
	outColor = mix(uBackgroundColor, uFontColor, charColor);
}
