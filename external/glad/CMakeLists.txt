set(TARGET glad)

set(H_FILES
    include/glad/glad.h
    include/KHR/khrplatform.h
)

set(C_FILES
    src/glad.c
)

set_source_files_properties(${C_FILES} PROPERTIES LANGUAGE CXX)

add_library(${TARGET} ${H_FILES} ${C_FILES})

target_compile_features(${TARGET} PRIVATE cxx_std_17)

target_include_directories(${TARGET} PUBLIC include)
