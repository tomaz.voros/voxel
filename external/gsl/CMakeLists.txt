set(TARGET gsl)
set(TARGET_NAME gsl-sources)

set(HEADER_FILES
    include/gsl/gsl
    include/gsl/gsl_algorithm
    include/gsl/gsl_assert
    include/gsl/gsl_byte
    include/gsl/gsl_util
    include/gsl/multi_span
    include/gsl/pointers
    include/gsl/span
    include/gsl/string_span
)

add_library(${TARGET} INTERFACE)

target_compile_features(${TARGET} INTERFACE cxx_std_17)

set_source_files_properties(${HEADER_FILES} PROPERTIES LANGUAGE CXX)

add_custom_target(${TARGET_NAME} SOURCES ${HEADER_FILES})

target_include_directories(${TARGET} INTERFACE include)
