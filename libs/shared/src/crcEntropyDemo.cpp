#include <array>
#include <cstdint>
#include <string>
#include <bitset>
#include <iostream>

#include "crc.hpp"

int main() {
    using scalar_type = std::int32_t;
    using vector_type = std::array<scalar_type, 3>;
    using hash_type = std::uint32_t;

    const vector_type begin{{-1234567890, -1234567890, -1234567890}};
    // const vector_type begin{{0, 0, 0}};
    const vector_type size{{4, 4, 4}};
    const vector_type end{{begin[0] + size[0], begin[1] + size[1], begin[2] + size[2]}};

    vector_type it{{0, 0, 0}};
    for (it[2] = begin[2]; it[2] < end[2]; it[2]++)
        for (it[1] = begin[1]; it[1] < end[1]; it[1]++)
            for (it[0] = begin[0]; it[0] < end[0]; it[0]++) {
                const auto input = reinterpret_cast<const std::uint8_t *>(it.data());
                const auto crcResult = crc::crc32c(input, it.size() * sizeof(scalar_type));
                std::cout << std::bitset<32>(crcResult) << std::endl;
        }


    const scalar_type prime = 17;

    auto hash35711 = [] (vector_type v) -> hash_type {
        const hash_type x = v[0];
        const hash_type y = v[1];
        const hash_type z = v[2];
        return (x * 3u ^ y * 5u ^ z * 7u) % 11u;
    };
    
    for (it[2] = begin[2]; it[2] < end[2]; it[2]++)
        for (it[1] = begin[1]; it[1] < end[1]; it[1]++)
            for (it[0] = begin[0]; it[0] < end[0]; it[0]++) {
                const auto hash = hash35711(it);
                std::cout << hash << std::endl;
        }
}