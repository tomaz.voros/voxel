#include "compression.hpp"

#include <type_traits>
#include <cassert>
#include <zlib.h>
#include "Log.hpp"
#include "cfg.hpp"

std::vector<std::byte> compression::zlib::compress(const std::byte * data, std::size_t size) {
    if (size == 0)
        return {};
    if (size > std::numeric_limits<uLongf>::max())
        return {};
    static_assert(sizeof(Bytef) == sizeof(std::byte));

    uLongf compressedSize = compressBound(size);
    std::vector<std::byte> buffer;
    buffer.resize(compressedSize);
    const auto compressionResult = compress2(
        reinterpret_cast<Bytef *>(buffer.data()), &compressedSize,
        reinterpret_cast<const Bytef *>(data), size, Z_BEST_COMPRESSION
    );

    if (compressionResult != Z_OK) {
        log::warning("zlib compression failed.");
        return {};
    }
    assert(compressedSize <= buffer.size());
    buffer.resize(compressedSize);
    return buffer;
}

std::vector<std::byte> compression::deflate::compress(const std::byte * data, std::size_t size) {
    if (size == 0)
        return {};
    if (size > std::numeric_limits<uLongf>::max())
        return {};
    static_assert(sizeof(Bytef) == sizeof(std::byte));

    // fail if this is not enough space
    std::vector<std::byte> buffer(size + std::max<std::size_t>(10, size / 10));
    Bytef * b_out = reinterpret_cast<Bytef *>(buffer.data());
    // why zlib why?
    z_const Bytef * b_in = const_cast<z_const Bytef *>(reinterpret_cast<const Bytef *>(data));

    z_stream stream = {};
    stream.zalloc = nullptr;
    stream.zfree = nullptr;
    stream.opaque = nullptr;
    stream.next_in = b_in;

    const auto init_result = deflateInit2(&stream, Z_BEST_COMPRESSION, Z_DEFLATED, -15, 8, Z_DEFAULT_STRATEGY);
    if (init_result != Z_OK) {
        buffer.clear();
        return buffer;
    }

    stream.next_out = b_out;
    stream.avail_out = 0;
    stream.next_in = b_in;
    stream.avail_in = 0;

    std::size_t out_left = buffer.size();
    std::size_t in_left = size;
    static_assert(std::numeric_limits<decltype(stream.avail_in)>::max() <= std::numeric_limits<std::size_t>::max());
    static_assert(std::numeric_limits<decltype(stream.avail_out)>::max() <= std::numeric_limits<std::size_t>::max());
    constexpr std::size_t IN_MAX = std::numeric_limits<decltype(stream.avail_in)>::max();
    constexpr std::size_t OUT_MAX = std::numeric_limits<decltype(stream.avail_out)>::max();

    int err = Z_OK;

    while (err == Z_OK) {
        if (stream.avail_out == 0) {
            stream.avail_out = out_left > OUT_MAX ? OUT_MAX : out_left;
            out_left -= stream.avail_out;
        }
        if (stream.avail_in == 0) {
            stream.avail_in = in_left > IN_MAX ? IN_MAX : in_left;
            in_left -= stream.avail_in;
        }
        err = ::deflate(&stream, in_left ? Z_NO_FLUSH : Z_FINISH);
    }

    deflateEnd(&stream);

    static_assert(std::numeric_limits<decltype(stream.total_out)>::max() <= std::numeric_limits<std::size_t>::max());
    if (err == Z_STREAM_END)
        buffer.resize(stream.total_out);
    else
        buffer.clear();

    return buffer;
}

std::vector<std::byte> compression::deflate::decompress(const std::byte * data, std::size_t size, std::size_t expectedSize) {
    if (size == 0)
        return {};
    if (size > std::numeric_limits<uLongf>::max())
        return {};
    static_assert(sizeof(Bytef) == sizeof(std::byte));

    // fail if this is not enough space
    std::vector<std::byte> buffer(expectedSize);
    Bytef * b_out = reinterpret_cast<Bytef *>(buffer.data());
    // why zlib why?
    z_const Bytef * b_in = const_cast<z_const Bytef *>(reinterpret_cast<const Bytef *>(data));

    z_stream stream = {};
    stream.zalloc = nullptr;
    stream.zfree = nullptr;
    stream.opaque = nullptr;
    stream.next_in = b_in;
    stream.avail_in = 0;

    const auto init_result = inflateInit2(&stream, -15);
    if (init_result != Z_OK) {
        buffer.clear();
        return buffer;
    }

    stream.next_out = b_out;
    stream.avail_out = 0;
    stream.next_in = b_in;
    stream.avail_in = 0;

    std::size_t out_left = buffer.size();
    std::size_t in_left = size;
    static_assert(std::numeric_limits<decltype(stream.avail_in)>::max() <= std::numeric_limits<std::size_t>::max());
    static_assert(std::numeric_limits<decltype(stream.avail_out)>::max() <= std::numeric_limits<std::size_t>::max());
    constexpr std::size_t IN_MAX = std::numeric_limits<decltype(stream.avail_in)>::max();
    constexpr std::size_t OUT_MAX = std::numeric_limits<decltype(stream.avail_out)>::max();

    int err = Z_OK;

    while (err == Z_OK) {
        if (stream.avail_out == 0) {
            stream.avail_out = out_left > OUT_MAX ? OUT_MAX : out_left;
            out_left -= stream.avail_out;
        }
        if (stream.avail_in == 0) {
            stream.avail_in = in_left > IN_MAX ? IN_MAX : in_left;
            in_left -= stream.avail_in;
        }
        err = inflate(&stream, Z_NO_FLUSH);
    }

    inflateEnd(&stream);

    static_assert(std::numeric_limits<decltype(stream.total_out)>::max() <= std::numeric_limits<std::size_t>::max());
    if (err == Z_STREAM_END)
        buffer.resize(stream.total_out);
    else
        buffer.clear();

    return buffer;
}


std::vector<std::byte> compression::zlib::decompress(const std::byte * data, std::size_t size, std::size_t expectedSize) {
    if (size == 0)
        return {};
    if (size > std::numeric_limits<uLongf>::max())
        return {};
    if (expectedSize > std::numeric_limits<uLongf>::max())
        return {};
    static_assert(sizeof(Bytef) == sizeof(std::byte));

    uLong decompressedSize = expectedSize;
    std::vector<std::byte> buffer;
    buffer.resize(decompressedSize);
    const auto decompressionResult = uncompress(
        reinterpret_cast<Bytef *>(buffer.data()), &decompressedSize,
        reinterpret_cast<const Bytef *>(data), size
    );
    if (decompressionResult != Z_OK || decompressedSize != buffer.size()) {
        log::warning("Decompression failed.");
        return {};
    }
    return buffer;
}

std::size_t compression::chunk::compress(const std::byte * in, std::byte * out) {
    static_assert(cfg::CHUNK_SIZE_BYTES > 0);
    static_assert(sizeof(std::byte) == 1, "Should be guaranteed by the standard.");
    static_assert(sizeof(cfg::CompressionStrategy) == sizeof(std::byte));
    static_assert(cfg::CHUNK_SIZE_BYTES + sizeof(cfg::CompressionStrategy) == cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES);
    // TODO: remove unnecessary copy by passing in output buffer (std::span)
    auto deflateResult = compression::deflate::compress(in, cfg::CHUNK_SIZE_BYTES);
    if (deflateResult.size() == 0 || deflateResult.size() + sizeof(cfg::CompressionStrategy) >= cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES) {
        out[0] = static_cast<std::byte>(cfg::CompressionStrategy::IDENTITY);
        std::copy_n(in, cfg::CHUNK_SIZE_BYTES, out + sizeof(cfg::CompressionStrategy));
        return cfg::MAX_COMPRESSED_CHUNK_SIZE_BYTES;
    } else {
        // TODO: test this part of code by constructing data that can not be compressed with zlib
        out[0] = static_cast<std::byte>(cfg::CompressionStrategy::DEFLATE);
        std::copy(deflateResult.begin(), deflateResult.end(), out + sizeof(cfg::CompressionStrategy));
        return sizeof(cfg::CompressionStrategy) + deflateResult.size();
    }
}

std::size_t compression::chunk::decompress(const std::byte * in, std::size_t size, std::byte * out) {
    static_assert(cfg::CHUNK_SIZE_BYTES > 0);
    static_assert(sizeof(std::byte) == 1, "Should be guaranteed by the standard.");
    static_assert(sizeof(cfg::CompressionStrategy) == sizeof(std::byte));
    if (size < 1) {
        return 0;
    } else if (static_cast<cfg::CompressionStrategy>(in[0]) == cfg::CompressionStrategy::IDENTITY) {
        // TODO: test this part of code by constructing data that can not be compressed with zlib
        assert(cfg::CHUNK_SIZE_BYTES == size - 1);
        if (cfg::CHUNK_SIZE_BYTES != size - 1)
            return 0;
        std::copy(in + 1, in + 1 + cfg::CHUNK_SIZE_BYTES, out);
        return cfg::CHUNK_SIZE_BYTES;
    } else if (static_cast<cfg::CompressionStrategy>(in[0]) == cfg::CompressionStrategy::DEFLATE) {
        const auto deflateResult = compression::deflate::decompress(in + 1, size - 1, cfg::CHUNK_SIZE_BYTES);
        if (deflateResult.size() == 0)
            return 0;
        assert(cfg::CHUNK_SIZE_BYTES == deflateResult.size());
        if (cfg::CHUNK_SIZE_BYTES != deflateResult.size())
            return 0;
        std::copy(deflateResult.begin(), deflateResult.end(), out);
        return cfg::CHUNK_SIZE_BYTES;
    }
    return 0;
}