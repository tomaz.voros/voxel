#include "crc.hpp"

#include <iostream>

#include <catch2/catch.hpp>

TEST_CASE("CRC32C basic", "[CRC32C]") {
    REQUIRE(0x00000000u == crc::crc32c((const uint8_t *)"", 0));
    REQUIRE(0x47b1770du == crc::crc32c((const uint8_t *)"f1234", 5));
    REQUIRE(0x01e6c190u == crc::crc32c((const uint8_t *)"qwerasdf", 8));
    REQUIRE(0xe3069283u == crc::crc32c((const uint8_t *)"123456789", 9));
    REQUIRE(0xe5a85359u == crc::crc32c((const uint8_t *)"asjfgasyrgaetgr2rq2rfyrg342yrgq2f6", 34));
    REQUIRE(0x430f50efu == crc::crc32c((const uint8_t *)"sadohgfarfgr425rq234utuq4qtuihuqggqrghuh3iqu4qiegfuorhgiu3khsgsdkfhfgsakghgksajghiuretgru43r4343", 96));
}
