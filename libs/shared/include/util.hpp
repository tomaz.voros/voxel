#pragma once

#include <cassert>
#include <cstddef>
#include <filesystem>
#include <algorithm>
#include <type_traits>
#include <cstring>
#include <fstream>

#include <gsl/span>

#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <boost/asio.hpp>

#include "cfg.hpp"
#include "crc.hpp"

namespace util {
    /**
     * Writes t to out and returns past the end pointer.
     * out must have enough space to store t.
     */
    template <typename T>
    std::byte * writeType(std::byte * out, const T & src) {
        std::memcpy(out, &src, sizeof(T));
        return out + sizeof(T);
    }

    template <typename Container, typename Value>
    void push(Container & container, const Value & value) {
        static_assert(sizeof(typename Container::value_type) == 1);
        // TODO: check std::endian when supported
        container.resize(container.size() + sizeof(value));
        std::memcpy(
            container.data() + (container.size() - sizeof(value)),
            &value,
            sizeof(value)
        );
    }

    template <typename T = glm::ivec3>
    struct box {
        T from;
        T to;
    };

    template <typename T>
    [[deprecated("use std::ispow2 from C++20")]]
    constexpr bool ispow2(T x) noexcept {
        return x != 0 && (x & (x - 1)) == 0;
    }

    template <typename T>
    [[deprecated("use std::log2p1 from C++20")]]
    constexpr T log2p1(T x) noexcept {
        T result = 1;
        while (x > 1) {
            x >>= 1;
            result++;
        }
        return result;
    }

    template <typename T>[[deprecated("use std::ceil2 from C++20")]] constexpr T ceil2(T x) noexcept {
        T result = 1;
        while (result < x)
            result *= T{2};
        return result;
    }

    template <typename T>
    std::vector<T> loadFile(const std::filesystem::path & filePath) {
        std::vector<T> result;
        std::error_code errorCode;
        const auto fileSize = std::filesystem::file_size(filePath, errorCode);
        if (fileSize <= 0 || fileSize == static_cast<std::uintmax_t>(-1))
            return result;
        if (fileSize > std::numeric_limits<std::size_t>::max())
            return result;
        std::ifstream fileStream{ filePath, std::ifstream::in | std::ifstream::binary };
        if (!fileStream.good())
            return result;
        result.resize(static_cast<std::size_t>(fileSize) / sizeof(T));
        fileStream.read(reinterpret_cast<char *>(result.data()), result.size() * sizeof(T));
        if (!fileStream.good())
            result = {};
        return result;
    }

    inline bool block_in_chunk_range(const glm::ivec3 & block_position, const box<glm::ivec3> & chunk_range) {
        const auto chunk_with_block = cfg::blockPositionToChunkPosition(block_position);
        const auto in_range =
            chunk_with_block.x >= chunk_range.from.x &&
            chunk_with_block.y >= chunk_range.from.y &&
            chunk_with_block.z >= chunk_range.from.z &&
            chunk_with_block.x < chunk_range.to.x &&
            chunk_with_block.y < chunk_range.to.y &&
            chunk_with_block.z < chunk_range.to.z;
        return in_range;

    }

    // for use with boost::asio::mutable_buffer and boost::asio::const_buffer
    template <typename SpanArray>
    inline void writeToSpanArray(
        gsl::span<const std::byte> in,
        SpanArray out
    ) {
        auto out0 = *boost::asio::buffer_sequence_begin(out);
        auto out1 = *(boost::asio::buffer_sequence_begin(out) + 1);
        assert(in.size() == out0.size() + out1.size()); // beware of '+' overflow
        std::copy_n(in.data(), out0.size(), reinterpret_cast<std::byte *>(out0.data()));
        std::copy_n(in.data() + out0.size(), out1.size(), reinterpret_cast<std::byte *>(out1.data()));
    }

    // for use with boost::asio::mutable_buffer and boost::asio::const_buffer
    template <typename SpanArray>
    inline void readFromSpanArray(
        SpanArray in,
        gsl::span<std::byte> out
    ) {
        auto in0 = *boost::asio::buffer_sequence_begin(in);
        auto in1 = *(boost::asio::buffer_sequence_begin(in) + 1);
        assert(out.size() == in0.size() + in1.size()); // beware of '+' overflow
        std::copy_n(reinterpret_cast<const std::byte *>(in0.data()), in0.size(), out.data());
        std::copy_n(reinterpret_cast<const std::byte *>(in1.data()), in1.size(), out.data() + in0.size());
    }

    /**
     * Reduces buffer size by removing the beginning.
     */
    // for use with boost::asio::mutable_buffer and boost::asio::const_buffer
    template <typename SpanArray>
    inline SpanArray removeBegin(SpanArray in, cfg::index_type count) {
        auto in0 = *boost::asio::buffer_sequence_begin(in);
        auto in1 = *(boost::asio::buffer_sequence_begin(in) + 1);
        assert(count <= in0.size() + in1.size());
        if (in0.size() <= count) {
            in0 = in1 + (count - in0.size());
            in1 = {};
        } else {
            in0 = in0 + count;
        }
        return { in0, in1 };
    }

    /**
     * Reduces buffer size by removing the end.
     */
    // for use with boost::asio::mutable_buffer and boost::asio::const_buffer
    template <typename SpanArray>
    inline SpanArray removeEnd(SpanArray in, cfg::index_type count) {
        auto in0 = *boost::asio::buffer_sequence_begin(in);
        auto in1 = *(boost::asio::buffer_sequence_begin(in) + 1);
        assert(count <= in0.size() + in1.size());
        if (in1.size() <= count) {
            in0 = { in0.data(), in0.size() - (count - in1.size()) };
            in1 = {};
        } else {
            in1 = { in1.data(), in1.size() - count };
        }
        return { in0, in1 };
    }

    template <typename T>
    inline void read(const gsl::span<const std::byte> & in, cfg::index_type offset, T & out) {
        assert(offset + sizeof(T) <= in.size());
        std::copy_n(in.data() + offset, sizeof(T), reinterpret_cast<std::byte *>(&out));
    }

    template <typename T, std::size_t N>
    inline void blit2D(
        const T * src, T * dst,
        std::size_t src_size_x, std::size_t src_size_y,
        std::size_t dst_size_x, std::size_t dst_size_y,
        std::size_t src_off_x, std::size_t src_off_y,
        std::size_t dst_off_x, std::size_t dst_off_y,
        std::size_t size_x, std::size_t size_y
    ) {
        // assuming no overflow
        assert(size_x + src_off_x <= src_size_x && size_y + src_off_y <= src_size_y);
        assert(size_x + dst_off_x <= dst_size_x && size_y + dst_off_y <= dst_size_y);
        for (std::size_t y = 0; y < size_y; ++y)
            for (std::size_t x = 0; x < size_x; ++x) {
                const auto src_index = N * (src_size_x * (src_off_y + y) + (src_off_x + x));
                const auto dst_index = N * (dst_size_x * (dst_off_y + y) + (dst_off_x + x));
                for (std::size_t c = 0; c < N; ++c)
                    dst[dst_index + c] = src[src_index + c];
            }
    }

    template <typename T, std::size_t N>
    inline void flipYInPlace(T * img, std::size_t width, std::size_t height) {
        const auto lineSize = width * N;
        for (std::size_t i = 0; i < height / 2; ++i) {
            auto begin = img + lineSize * i;
            auto end = begin + lineSize;
            auto other_begin = img + lineSize * (height - i - 1);
            std::swap_ranges(begin, end, other_begin);
        }
    }

    template <typename T, std::size_t N>
    inline void blit3D(
        const T * src, T * dst,
        std::size_t src_size_x, std::size_t src_size_y, std::size_t src_size_z,
        std::size_t dst_size_x, std::size_t dst_size_y, std::size_t dst_size_z,
        std::size_t src_off_x, std::size_t src_off_y, std::size_t src_off_z,
        std::size_t dst_off_x, std::size_t dst_off_y, std::size_t dst_off_z,
        std::size_t size_x, std::size_t size_y, std::size_t size_z
    ) {
        // assuming no overflow
        assert(size_x + src_off_x <= src_size_x && size_y + src_off_y <= src_size_y && size_z + src_off_z <= src_size_z);
        assert(size_x + dst_off_x <= dst_size_x && size_y + dst_off_y <= dst_size_y && size_z + dst_off_z <= dst_size_z);
        for (std::size_t z = 0; z < size_z; ++z)
            for (std::size_t y = 0; y < size_y; ++y)
                for (std::size_t x = 0; x < size_x; ++x) {
                    const auto src_index = N * (src_size_x * (src_size_y * (src_off_z + z) + (src_off_y + y)) + (src_off_x + x));
                    const auto dst_index = N * (dst_size_x * (dst_size_y * (dst_off_z + z) + (dst_off_y + y)) + (dst_off_x + x));
                    for (std::size_t c = 0; c < N; ++c)
                        dst[dst_index + c] = src[src_index + c];
                }
    }

    template <typename T, std::size_t N>
    inline void mipSum2D(
        const T * src,
        T * dst,
        std::size_t src_size_x,
        std::size_t src_size_y
    ) {
        if (src_size_x % 2 != 0)
            throw std::runtime_error("Non even source width not supported.");
        if (src_size_y % 2 != 0)
            throw std::runtime_error("Non even source height not supported.");
        const std::size_t dst_size_x = src_size_x / 2;
        const std::size_t dst_size_y = src_size_y / 2;

        std::fill_n(dst, dst_size_x * dst_size_y * N, T{ 0 });

        for (std::size_t y = 0; y < dst_size_y; ++y)
            for (std::size_t x = 0; x < dst_size_x; ++x) {
                const auto dst_index = dst_size_x * N * y + N * x;
                for (std::size_t yy = y * 2; yy < y * 2 + 2; ++yy)
                    for (std::size_t xx = x * 2; xx < x * 2 + 2; ++xx) {
                        const auto src_index = src_size_x * N * yy + N * xx;
                            for (std::size_t c = 0; c < N; ++c) {
                                dst[dst_index + c] += src[src_index + c];
                            }
                    }
            }
    }

    inline int32_t dot(const glm::ivec3 & a, const glm::ivec3 & b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    template <class To, class From>
    [[deprecated("use std::bit_cast(...)")]]
    To bit_cast(const From & src) noexcept {
        To dst;
        std::memcpy(&dst, &src, sizeof(To));
        return dst;
    }

    // TODO: something more efficient than writing VertexNotPacked and then packing
    struct VertexNotPacked {
        std::uint32_t texture_position_x;
        std::uint32_t texture_position_y;
        std::uint32_t size_x;
        std::uint32_t size_y;

        std::uint32_t position_x;
        std::uint32_t position_y;
        std::uint32_t position_z;
        std::uint32_t rotate_by;
        std::uint32_t side_index;

        std::uint32_t ambient_occlusion;
        std::uint32_t transpose;

        /*
            |--------------------|-----------|-----------|
            | NAME               | BIT COUNT | COMPONENT |
            |--------------------|-----------|-----------|
            | texture_position_x |        12 |         x |
            | texture_position_y |        12 |         x |
            | size_x             |         4 |         x |
            | size_y             |         4 |         x |
            |--------------------|-----------|-----------|
            | position_x         |         9 |         y |
            | position_y         |         9 |         y |
            | position_z         |         9 |         y |
            | rotate_by          |         2 |         y |
            | side_index         |         3 |         y |
            |--------------------|-----------|-----------|
            | ambient_occlusion  |         8 |         z |
            | transpose          |         1 |         z |
            | **unused**         |        23 |         z |
            |--------------------|-----------|-----------|
        */

        bool hasValidValues() const {
            bool valid{true};

            valid = valid && (texture_position_x < 4096u);
            valid = valid && (texture_position_y < 4096u);
            valid = valid && (size_x < 16u);
            valid = valid && (size_y < 16u);

            valid = valid && (position_x < 512u);
            valid = valid && (position_y < 512u);
            valid = valid && (position_z < 512u);
            valid = valid && (rotate_by < 4u);
            valid = valid && (side_index < 6u); // only 0...5 is valid

            valid = valid && (ambient_occlusion < 256u);
            valid = valid && (transpose < 2u);

            return valid;
        }

        static VertexNotPacked makeDefault() {
            VertexNotPacked result{};
            result.texture_position_x = 0;
            result.texture_position_y = 0;
            result.size_x = 0;
            result.size_y = 0;
            result.position_x = 0;
            result.position_y = 0;
            result.position_z = 0;
            result.rotate_by = 0;
            result.side_index = 0;
            result.ambient_occlusion = 0;
            result.transpose = 0;
            return result;
        }
    };

    inline void packQuad(std::uint32_t * out, const VertexNotPacked & vertex) {
        assert(vertex.hasValidValues());

        out[0] = vertex.texture_position_x << 20u;
        out[0] |= vertex.texture_position_y << 8u;
        out[0] |= vertex.size_x << 4u;
        out[0] |= vertex.size_y;

        out[1] = vertex.position_x << 23u;
        out[1] |= vertex.position_y << 14u;
        out[1] |= vertex.position_z << 5u;
        out[1] |= vertex.rotate_by << 3u;
        out[1] |= vertex.side_index;

        out[2] = vertex.ambient_occlusion << 24u;
        out[2] |= vertex.transpose << 23u;
    }

    inline void bitwiseOrQuad(std::uint32_t * out, const std::uint32_t * in) {
        out[0] |= in[0];
        out[1] |= in[1];
        out[2] |= in[2];
    }

    inline void addQuad(std::uint32_t * out, const std::uint32_t * in) {
        out[0] += in[0];
        out[1] += in[1];
        out[2] += in[2];
    }

    inline std::uint32_t positionToRandom(const glm::tvec3<std::int32_t> & position) {
        const auto b = reinterpret_cast<const std::uint8_t *>(glm::value_ptr(position));
        return crc::crc32c(b, 3 * sizeof(std::int32_t));
    }

    inline std::uint32_t position35711(const glm::tvec3<std::int32_t> & position) {
        const std::uint32_t x = position.x;
        const std::uint32_t y = position.y;
        const std::uint32_t z = position.z;
        return (x * 3u ^ y * 5u ^ z * 7u) % 11u;
    };

}
