#pragma once

#include <vector>

// TODO: use std::span<std::byte>
// TODO: compression strategies should fail if exceeding maximum given space

namespace compression {
    namespace zlib {
        /**
         * Compresses data using zlib Z_BEST_COMPRESSION.
         * @param[in] data Bytes to compress.
         * @param[in] size Number of consecutive bytes to compress.
         * @return Vector of compressed data. If vector of size 0 is returned
         *         either there was no data or there was an error during compression.
         */
        std::vector<std::byte> compress(const std::byte * data, std::size_t size);
        /**
         * Decompresses data that was compressed with zlib.
         * @param[in] data Bytes to decompress.
         * @param[in] size Number of consecutive bytes to decompress.
         * @return Vector of decompressed data. If vector of size 0 is returned
         *         either there was no data or there was an error during decompression.
         */
        std::vector<std::byte> decompress(const std::byte * data, std::size_t size, std::size_t expectedSize);
    }

    namespace deflate {
        std::vector<std::byte> compress(const std::byte * data, std::size_t size);
        std::vector<std::byte> decompress(const std::byte * data, std::size_t size, std::size_t expectedSize);
    }

    namespace chunk {
        /**
         * Compresses data so that the output size does not exceed #Config::MAX_COMPRESSED_CHUNK_SIZE_BYTES.
         * @param[in] in Buffer to #Config::CHUNK_SIZE_BYTES bytes that will be compressed.
         * @param[out] out Buffer to at least #Config::MAX_COMPRESSED_CHUNK_SIZE_BYTES bytes where
         *                 the result will be stored.
         * @return Number of bytes written to #out.
         */
        std::size_t compress(const std::byte * in, std::byte * out);
        /**
         * Decompresses data that was compressed with the corresponding compression function.
         * @param[in] in Bytes that were returned by the corresponding compression function for decompression.
         * @param[in] size Return value of the corresponding call to the corresponding compression function.
         * @param[out] out Buffer to #Config::CHUNK_SIZE_BYTES bytes where the decompressed result will be stored.
         * @return #Config::CHUNK_SIZE_BYTES on success, 0 on error.
         */
        std::size_t decompress(const std::byte * in, std::size_t size, std::byte * out);
    }
}
