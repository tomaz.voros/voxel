#pragma once

namespace algebra {
    //==========================================================================
    //==========================================================================
    using Scalar = double;
    //==========================================================================
    //==========================================================================
    struct Vec2 {
        Vec2() = default;
        Vec2(Scalar x, Scalar y) : x{x}, y{y} {}
        Scalar x{0};
        Scalar y{0};
    };
    struct Vec3 {
        Vec3() = default;
        Vec3(Scalar x, Scalar y, Scalar z) : x{x}, y{y}, z{z} {}
        Vec3(Vec2 xy, Scalar z) : Vec3{xy.x, xy.y, z} {}
        Vec3(Scalar x, Vec2 yz) : Vec3{x, yz.x, yz.y} {}
        Scalar x{0};
        Scalar y{0};
        Scalar z{0};
    };
    struct Vec4 {
        Vec4() = default;
        Vec4(Scalar x, Scalar y, Scalar z, Scalar w) : x{x}, y{y}, z{z}, w{w} {}
        Vec4(Vec2 xy, Scalar z, Scalar w) : Vec4{xy.x, xy.y, z, w} {}
        Vec4(Scalar x, Vec2 yz, Scalar w) : Vec4{x, yz.x, yz.y, w} {}
        Vec4(Vec2 xy, Vec2 zw) : Vec4{xy.x, xy.y, zw.x, zw.y} {}
        Vec4(Scalar x, Scalar y, Vec2 zw) : Vec4{x, y, zw.x, zw.y} {}
        Vec4(Vec3 xyz, Scalar w) : Vec4{xyz.x, xyz.y, xyz.z, w} {}
        Vec4(Scalar x, Vec3 yzw) : Vec4{x, yzw.x, yzw.y, yzw.z} {}
        Scalar x{0};
        Scalar y{0};
        Scalar z{0};
        Scalar w{1};
    };
    //==========================================================================
    //==========================================================================
    struct Mat34 {
        Mat34() = default;
        Mat34(Vec4 a, Vec4 b, Vec4 c) {
            xx = a.x; xy = a.y; xz = a.z; xw = a.w;
            yx = b.x; yy = b.y; yz = b.z; yw = b.w;
            zx = c.x; zy = c.y; zz = c.z; zw = c.w;
        }
        Scalar xx{1}; Scalar xy{0}; Scalar xz{0}; Scalar xw{0};
        Scalar yx{0}; Scalar yy{1}; Scalar yz{0}; Scalar yw{0};
        Scalar zx{0}; Scalar zy{0}; Scalar zz{1}; Scalar zw{0};
    };
    struct Mat23 {
        Mat23() = default;
        Mat23(Vec3 a, Vec3 b) {
            xx = a.x; xy = a.y; xz = a.z;
            yx = b.x; yy = b.y; yz = b.z;
        }
        Scalar xx{1}; Scalar xy{0}; Scalar xz{0};
        Scalar yx{0}; Scalar yy{1}; Scalar yz{0};
    };
    struct Mat12 {
        Mat12() = default;
        Mat12(Vec2 a) {
            xx = a.x; xy = a.y;
        }
        Scalar xx{1}; Scalar xy{0};
    };
    //==========================================================================
    //==========================================================================
    template<typename Op>
    Vec2 elementwise(Vec2 a, Vec2 b, Op op) {
        return {op(a.x, b.x), op(a.y, b.y)};
    }
    template<typename Op>
    Vec3 elementwise(Vec3 a, Vec3 b, Op op) {
        return {op(a.x, b.x), op(a.y, b.y), op(a.z, b.z)};
    }
    template<typename Op>
    Vec4 elementwise(Vec4 a, Vec4 b, Op op) {
        return {op(a.x, b.x), op(a.y, b.y), op(a.z, b.z), op(a.w, b.w)};
    }
    template<typename Op>
    Scalar horizontal(Vec2 a, Op op) {
        return op(a.x, a.y);
    }
    template<typename Op>
    Scalar horizontal(Vec3 a, Op op) {
        return op(op(a.x, a.y), a.z);
    }
    template<typename Op>
    Scalar horizontal(Vec4 a, Op op) {
        return op(op(op(a.x, a.y), a.z), a.w);
    }
    //==========================================================================
    inline Scalar add(Scalar a, Scalar b) { return a + b; }
    inline Scalar sub(Scalar a, Scalar b) { return a - b; }
    inline Scalar mul(Scalar a, Scalar b) { return a * b; }
    inline Scalar div(Scalar a, Scalar b) { return a / b; }
    //==========================================================================
#define algebra_operator_def(o, op)                                                              \
    inline Vec2 operator##o(  Vec2 a,   Vec2 b) { return elementwise( a, b, op); }               \
    inline Vec2 operator##o(  Vec2 a, Scalar s) { return elementwise( a, Vec2{s, s}, op); }      \
    inline Vec2 operator##o(Scalar s,   Vec2 b) { return elementwise(Vec2{s, s}, b, op); }       \
    inline Vec3 operator##o(  Vec3 a,   Vec3 b) { return elementwise(a, b, op); }                \
    inline Vec3 operator##o(  Vec3 a, Scalar s) { return elementwise(a, Vec3{s, s, s}, op); }    \
    inline Vec3 operator##o(Scalar s,   Vec3 b) { return elementwise(Vec3{s, s, s}, b, op); }    \
    inline Vec4 operator##o(  Vec4 a,   Vec4 b) { return elementwise(a, b, op); }                \
    inline Vec4 operator##o(  Vec4 a, Scalar s) { return elementwise(a, Vec4{s, s, s, s}, op); } \
    inline Vec4 operator##o(Scalar s,   Vec4 b) { return elementwise(Vec4{s, s, s, s}, b, op); }
    algebra_operator_def(+, add)
    algebra_operator_def(-, sub)
    algebra_operator_def(*, mul)
    algebra_operator_def(/, div)
    // TODO: assignment operators *= += -= /=
#undef algebra_operator_def
    inline Vec2 operator-(Vec2 a) { return {-a.x, -a.y}; }
    inline Vec3 operator-(Vec3 a) { return {-a.x, -a.y, -a.z}; }
    inline Vec4 operator-(Vec4 a) { return {-a.x, -a.y, -a.z, -a.w}; }
    inline Vec2 operator+(Vec2 a) { return {+a.x, +a.y}; }
    inline Vec3 operator+(Vec3 a) { return {+a.x, +a.y, +a.z}; }
    inline Vec4 operator+(Vec4 a) { return {+a.x, +a.y, +a.z, +a.w}; }
    //==========================================================================
    inline Scalar hadd(Vec2 a) { return horizontal(a, add); }
    inline Scalar hadd(Vec3 a) { return horizontal(a, add); }
    inline Scalar hadd(Vec4 a) { return horizontal(a, add); }
    inline Scalar dot(Vec2 a, Vec2 b) { return hadd(a * b); }
    inline Scalar dot(Vec3 a, Vec3 b) { return hadd(a * b); }
    inline Scalar dot(Vec4 a, Vec4 b) { return hadd(a * b); }
    inline Vec3 cross(Vec3 a, Vec3 b) {
        return {
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x
        };
    }
    inline Scalar length2(Vec3 a) { return dot(a, a); }
    inline Scalar length(Vec3 a) { return std::sqrt(length2(a)); }
    //==========================================================================
    inline Mat34 view(Vec3 t, Vec3 b, Vec3 n, Vec3 p) {
        return {
            Vec4{t, -dot(t, p)},
            Vec4{b, -dot(b, p)},
            Vec4{n, -dot(n, p)}
        };
    }
    inline Mat23 view(Vec2 t, Vec2 b, Vec2 p) {
        return {
            Vec3{t, -dot(t, p)},
            Vec3{b, -dot(b, p)},
        };
    }
    inline Mat12 view(Scalar t, Scalar p) {
        return {
            Vec2{t, -(t * p)},
        };
    }
    //==========================================================================
    inline Mat34 mul(Mat34 a, Mat34 b) {
        auto ax = Vec4{a.xx, a.xy, a.xz, a.xw};
        auto ay = Vec4{a.yx, a.yy, a.yz, a.yw};
        auto az = Vec4{a.zx, a.zy, a.zz, a.zw};
        auto bx = Vec4{b.xx, b.yx, b.zx, 0};
        auto by = Vec4{b.xy, b.yy, b.zy, 0};
        auto bz = Vec4{b.xz, b.yz, b.zz, 0};
        auto bw = Vec4{b.xw, b.yw, b.zw, 1};
        return {
            Vec4{dot(ax, bx), dot(ax, by), dot(ax, bz), dot(ax, bw)},
            Vec4{dot(ay, bx), dot(ay, by), dot(ay, bz), dot(ay, bw)},
            Vec4{dot(az, bx), dot(az, by), dot(az, bz), dot(az, bw)}
        };
    }
    inline Mat23 mul(Mat23 a, Mat23 b) {
        auto ax = Vec3{a.xx, a.xy, a.xz};
        auto ay = Vec3{a.yx, a.yy, a.yz};
        auto bx = Vec3{b.xx, b.yx, 0};
        auto by = Vec3{b.xy, b.yy, 0};
        auto bz = Vec3{b.xz, b.yz, 1};
        return {
            Vec3{dot(ax, bx), dot(ax, by), dot(ax, bz)},
            Vec3{dot(ay, bx), dot(ay, by), dot(ay, bz)}
        };
    }
    inline Mat12 mul(Mat12 a, Mat12 b) {
        auto ax = Vec2{a.xx, a.xy};
        auto bx = Vec2{b.xx, 0};
        auto by = Vec2{b.xy, 1};
        return {
            Vec2{dot(ax, bx), dot(ax, by)}
        };
    }
    inline Vec3 mul(Mat34 a, Vec3 b) {
        auto ax = Vec4{a.xx, a.xy, a.xz, a.xw};
        auto ay = Vec4{a.yx, a.yy, a.yz, a.yw};
        auto az = Vec4{a.zx, a.zy, a.zz, a.zw};
        auto bb = Vec4{b, 1};
        return {dot(ax, bb), dot(ay, bb), dot(az, bb)};
    }
    inline Vec2 mul(Mat23 a, Vec2 b) {
        auto ax = Vec3{a.xx, a.xy, a.xz};
        auto ay = Vec3{a.yx, a.yy, a.yz};
        auto bb = Vec3{b, 1};
        return {dot(ax, bb), dot(ay, bb)};
    }
    inline Scalar mul(Mat12 a, Scalar b) {
        auto ax = Vec2{a.xx, a.xy};
        auto bb = Vec2{b, 1};
        return dot(ax, bb);
    }
    //==========================================================================
    inline Mat34 translate(Vec3 t) {
        return {
            Vec4{1, 0, 0, t.x},
            Vec4{0, 1, 0, t.y},
            Vec4{0, 0, 1, t.z}
        };
    }
    inline Mat23 translate(Vec2 t) {
        return {
            Vec3{1, 0, t.x},
            Vec3{0, 1, t.y}
        };
    }
    inline Mat12 translate(Scalar t) {
        return {
            Vec2{1, t}
        };
    }
    //==========================================================================
    inline Mat34 scale(Vec3 s) {
        return {
            Vec4{s.x,   0,   0, 0},
            Vec4{  0, s.y,   0, 0},
            Vec4{  0,   0, s.z, 0}
        };
    }
    inline Mat23 scale(Vec2 s) {
        return {
            Vec3{s.x,   0, 0},
            Vec3{  0, s.y, 0},
        };
    }
    inline Mat12 scale(Scalar s) {
        return {
            Vec2{s, 0}
        };
    }
    //==========================================================================
    inline constexpr Scalar EARTH_A = 6'378'137.0;
    inline constexpr Scalar EARTH_F_INV = 298.257'223'563;
    // TODO: do this in C++20
    // template <Scalar F_INV = 6'378'137.0, Scalar A = 298.257'223'563>
    struct WGS84toECEF {
        static constexpr Scalar f_inv = EARTH_F_INV;
        static constexpr Scalar a = EARTH_A;
        static constexpr Scalar f = 1.0 / f_inv;
        static constexpr Scalar b = a * (1.0 - f);
        static constexpr Scalar e_squared = f * (2.0 - f);
        static constexpr Scalar one_minus_e_squared = 1.0 - e_squared;
        static constexpr Scalar squared_one_minus_e_squared = one_minus_e_squared * one_minus_e_squared;

        static Vec3 position(Vec2 latLon) {
            Scalar sinLat = std::sin(latLon.x);
            Scalar cosLat = std::cos(latLon.x);
            Scalar sinLon = std::sin(latLon.y);
            Scalar cosLon = std::cos(latLon.y);
            Scalar N = a / std::sqrt(1.0 - e_squared * (sinLat * sinLat));
            return {
                N * (cosLat * cosLon),
                N * (cosLat * sinLon),
                N * (one_minus_e_squared * sinLat)
            };
        }

        static Vec3 normal(Vec3 pos) {
            Scalar posLen = length(pos);
            return {
                pos.x / posLen * squared_one_minus_e_squared,
                pos.y / posLen * squared_one_minus_e_squared,
                pos.z / posLen * one_minus_e_squared
            };
        }

        static Vec3 tangent(Scalar lon) {
            return { -std::sin(lon), std::cos(lon), Scalar{0} };
        }

        static Vec3 bitangent(Vec3 nor, Vec3 tan) {
            // optimized cross product
            return {
                -nor.z * tan.y,
                nor.z * tan.x,
                nor.x * tan.y - nor.y * tan.x
            };
        }

        static Mat34 view(Vec2 latLon) {
            Vec3 p = position(latLon);
            Vec3 n = normal(p);
            Vec3 t = tangent(latLon.y);
            Vec3 b = bitangent(n, t);
            return algebra::view(t, b, n, p);
        }
    };
}
