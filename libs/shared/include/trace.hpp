#pragma once

#include <cstdint>
#include <filesystem>
#include <fstream>
#include <thread>

#include <fmt/format.h>
#include <fmt/ostream.h>

// TODO: make thread-safe
// TODO: get process id
// TODO: thread-local or at least non global aka. regular local state
class trace {
public:
    static inline constexpr auto DO_TRACE = false;
    static inline constexpr int64_t MESSAGE_COUNT_LIMIT = 300'000;

    static void init(const std::filesystem::path & traceOutFilePath) {
        if constexpr (DO_TRACE) {
            sOut.open(traceOutFilePath, std::ofstream::trunc | std::ofstream::out);
            const auto timeNow = ns();
            const auto timeUs = timeNow / 1000u;
            const auto timeFrac = timeNow % 1000u;
            fmt::print(
                sOut,
                "[\n{{"
                "\"pid\":0,"
                "\"tid\":{},"
                "\"ts\":{}.{:0>3},"
                "\"ph\":\"i\","
                "\"cat\":\"trace\","
                "\"name\":\"voxel\","
                "\"args\":{{}},"
                "\"s\":\"g\""
                "}}",
                std::this_thread::get_id(),
                timeUs,
                timeFrac
            );
        }
    }

    static void event_instant(const char * name) {
        if constexpr (DO_TRACE) {
            if (sLogEntryCounter >= MESSAGE_COUNT_LIMIT)
                return;
            ++sLogEntryCounter;
            const auto timeNow = ns();
            const auto timeUs = timeNow / 1000u;
            const auto timeFrac = timeNow % 1000u;
            fmt::print(
                sOut,
                ",\n{{"
                "\"pid\":0,"
                "\"tid\":{},"
                "\"ts\":{}.{:0>3},"
                "\"ph\":\"i\","
                "\"cat\":\"trace\","
                "\"name\":\"{}\","
                "\"args\":{{}},"
                "\"s\":\"g\""
                "}}",
                std::this_thread::get_id(),
                timeUs,
                timeFrac,
                name
            );
        }
    }

    template <std::size_t M, std::size_t N>
    static void event_counter(const char (&name)[M], const char (&varName)[N], std::int64_t varValue) {
        if constexpr (DO_TRACE) {
            if (sLogEntryCounter >= MESSAGE_COUNT_LIMIT)
                return;
            ++sLogEntryCounter;
            const auto timeNow = ns();
            const auto timeUs = timeNow / 1000u;
            const auto timeFrac = timeNow % 1000u;
            fmt::print(
                sOut,
                ",\n{{"
                "\"pid\":0,"
                "\"tid\":{},"
                "\"ts\":{}.{:0>3},"
                "\"ph\":\"C\","
                "\"cat\":\"trace\","
                "\"name\":\"{}\","
                "\"args\":{{\"{}\":{}}}"
                "}}",
                std::this_thread::get_id(),
                timeUs,
                timeFrac,
                name,
                varName,
                varValue
            );
        }
    }

    static void finish() {
        if constexpr (DO_TRACE) {
            fmt::print(sOut, "\n]");
            sOut.close();
        }
    }

    static std::uint64_t ns() {
        return std::chrono::duration_cast<std::chrono::duration<std::uint64_t, std::nano>>(
                std::chrono::steady_clock::now().time_since_epoch()
            ).count();
    }

    struct event_complete {
    public:
        template <std::size_t N>
        event_complete(const char (&name)[N]) :
            event_complete{name, "default"}
        {}
        template <std::size_t M, std::size_t N>
        event_complete(const char (&name)[M], const char (&category)[N]) {
            if constexpr (DO_TRACE) {
                if (sLogEntryCounter >= MESSAGE_COUNT_LIMIT)
                    return;
                mName = name;
                mCategory = category;
                mStart = ns();
            }
        }
        ~event_complete() {
            if constexpr (DO_TRACE) {
                if (sLogEntryCounter >= MESSAGE_COUNT_LIMIT)
                    return;
                ++sLogEntryCounter;
                const auto duration = ns() - mStart;
                const auto timeUs = duration / 1000u;
                const auto timeFrac = duration % 1000u;
                const auto timeUsBegin = mStart / 1000u;
                const auto timeFracBegin = mStart % 1000u;
                fmt::print(
                    sOut,
                    ",\n{{"
                    "\"pid\":0,"
                    "\"tid\":{},"
                    "\"ts\":{}.{:0>3},"
                    "\"ph\":\"X\","
                    "\"cat\":\"{}\","
                    "\"name\":\"{}\","
                    "\"args\":{{}},"
                    "\"dur\":{}.{:0>3}"
                    "}}",
                    std::this_thread::get_id(),
                    timeUsBegin,
                    timeFracBegin,
                    mCategory,
                    mName,
                    timeUs,
                    timeFrac
                );
            }
        }

    private:
        std::string_view mName;
        std::string_view mCategory;
        std::uint64_t mStart;

    };

private:
    static std::ofstream sOut;
    static std::int64_t sLogEntryCounter;

};
