#pragma once

#include <glm/vec3.hpp>
#include <array>
#include <chrono>
#include <cstddef>
#include <climits>

#include <gsl/span>

namespace cfg {
    // TODO: replace static constexpr by inline contexpr

    using index_type = std::ptrdiff_t;
    using tick_type = std::int32_t;
    using entity_id_type = std::int32_t;
    using entity_type_type = std::int32_t;

    // std::hardware_destructive_interference_size not supported by compiler (yet)
    // this is a guess
    inline constexpr std::size_t hardware_destructive_interference_size = 64;

    using ClientID = std::int16_t;
    using Block = std::uint8_t;
    static constexpr std::chrono::milliseconds TICK_DT{ 50 };
    static_assert(TICK_DT.count() > 0);

    // TODO: replace ClientID by client_id_type
    using client_id_type = ClientID;

    inline constexpr index_type CLIENT_TX_BUFFER_SIZE = 8 * 1024 * 1024;

    inline constexpr float MAX_VISIBLE_ENTITY_DISTANCE = 50.0f;

    static constexpr std::size_t LOAD_AND_RENDER_RADIUS_X = 4;
    static constexpr std::size_t LOAD_AND_RENDER_RADIUS_Y = 4;
    static constexpr std::size_t LOAD_AND_RENDER_RADIUS_Z = 4;
    static constexpr std::size_t UNLOAD_AND_RENDER_RADIUS_X = 16;
    static constexpr std::size_t UNLOAD_AND_RENDER_RADIUS_Y = 10;
    static constexpr std::size_t UNLOAD_AND_RENDER_RADIUS_Z = 16;
    static_assert(LOAD_AND_RENDER_RADIUS_X > 0);
    static_assert(LOAD_AND_RENDER_RADIUS_Y > 0);
    static_assert(LOAD_AND_RENDER_RADIUS_Z > 0);
    static_assert(LOAD_AND_RENDER_RADIUS_X < UNLOAD_AND_RENDER_RADIUS_X);
    static_assert(LOAD_AND_RENDER_RADIUS_Y < UNLOAD_AND_RENDER_RADIUS_Y);
    static_assert(LOAD_AND_RENDER_RADIUS_Z < UNLOAD_AND_RENDER_RADIUS_Z);

    static constexpr std::size_t MAX_MESSAGE_SIZE{ 1 * 1024 * 1024 };
    inline constexpr index_type MIN_MESSAGE_SIZE = 1; // because of id TODO: get rid of magic number 1
    static_assert(MIN_MESSAGE_SIZE > 0);
    static_assert(MIN_MESSAGE_SIZE <= MAX_MESSAGE_SIZE);

    static constexpr std::array<std::int32_t, 3> FULL_QUAD_SIZE{ { 16, 16, 16 } };

    using message_size_type = std::uint32_t;
    inline constexpr index_type PER_MESSAGE_BUFFER_OVERHEAD = sizeof(message_size_type);

    static constexpr std::size_t MAX_BLOCK_PLACEMENTS_PER_TICK{ 16 };
    static constexpr std::size_t MAX_PENDING_CHUNK_REQUESTS{256};
    static constexpr std::size_t MIN_PENDING_CHUNK_REQUESTS{8};
    static_assert(MAX_BLOCK_PLACEMENTS_PER_TICK > 0);
    static_assert(MAX_PENDING_CHUNK_REQUESTS > 0);
    /**
     *  This includes dead clients pending to be disconnected.
     */
    inline constexpr index_type MAX_CONNECTED_CLIENT_COUNT{ 4 };
    static_assert(MAX_CONNECTED_CLIENT_COUNT > 0);

    inline constexpr int ACCEPTOR_BACKLOG_SIZE_HINT = MAX_CONNECTED_CLIENT_COUNT * 2;

    /**
     * This value can be set to something like std::numeric_limits<ClientID>::max(),
     * but is kept low to test the robustness of the system that handles running ut of IDs.
     */
//    static constexpr ClientID MAX_CLIENT_ID_VALUE{ 6 };
//    static_assert(MAX_CLIENT_ID_VALUE >= MAX_CONNECTED_CLIENT_COUNT);

    static constexpr std::size_t DEFRAGMENT_GARBAGE_THRESHOLD = 1024 * 256;
    static_assert(DEFRAGMENT_GARBAGE_THRESHOLD > 0);
    // TODO: do testing if region cache size works correctly
    static constexpr std::size_t MAX_REGIONS_LOADED = 64;
    static_assert(MAX_REGIONS_LOADED > 0);
    // TODO: do testing if chunk cache size works correctly
    static constexpr std::size_t MAX_CHUNKS_LOADED = 16384;
    static_assert(MAX_CHUNKS_LOADED > 0);

    inline constexpr int CHUNK_SIZE_X = 32;
    inline constexpr int CHUNK_SIZE_Y = 32;
    inline constexpr int CHUNK_SIZE_Z = 32;
    inline constexpr static const glm::ivec3 CHUNK_SIZE{ CHUNK_SIZE_X, CHUNK_SIZE_Y, CHUNK_SIZE_Z };
    inline constexpr int REGION_SIZE_X = 32;
    inline constexpr int REGION_SIZE_Y = 32;
    inline constexpr int REGION_SIZE_Z = 32;
    inline constexpr static const glm::ivec3 REGION_SIZE{ REGION_SIZE_X, REGION_SIZE_Y, REGION_SIZE_Z };

    enum class CompressionStrategy : std::uint8_t {
        IDENTITY = 0,
        DEFLATE = 1
    };

    inline constexpr index_type MAX_USER_NAME_LENGTH = 16;
    inline constexpr index_type MIN_USER_NAME_LENGTH = 1;
    inline constexpr index_type MAX_REGISTERED_USER_COUNT = 6;

    using user_name_type = std::array<char, MAX_USER_NAME_LENGTH>;

    enum class EntityType : entity_type_type {
        BASE = 0,
        PLAYER = 1,
        BLOB = 2,
        // do not forget to update
        TYPE_COUNT = 3
    };

    namespace Message {
        // TODO: change to std::uint8_t to better reflect the header format and make sure it does not cause any issues
        enum struct Id : std::uint32_t {
            CHUNK_DATA = 0,
            ENTITY_DATA = 1,
            CHUNK_REQUEST_DATA = 2,
            BLOCK_DATA = 3,
            REASSIGN_ENTITY_ID = 4, // deprecated
            DEAD_ENTITIES = 5, // deprecated
            STOP_SERVER = 6,
            CLIENT_DISCONNECTED = 7,
            NEW_TICK = 8,
            AUTHENTICATE = 9,
            PLAYER_ORIENTATION = 10,
            MOVE_AWARE_AREA = 11,
            AWARE_AREA_OUT_OF_SYNC = 12,
            ADD_ENTITIES = 13,
            REMOVE_ENTITIES = 14,
            // do not forget to update
            // do not forget to add the name in MESSAGE_NAMES
            ID_COUNT = 15,
        };

        constexpr const char * const MESSAGE_NAMES[] = {
            "CHUNK_DATA",
            "ENTITY_DATA",
            "CHUNK_REQUEST_DATA",
            "BLOCK_DATA",
            "REASSIGN_ENTITY_ID",
            "DEAD_ENTITIES",
            "STOP_SERVER",
            "CLIENT_DISCONNECTED",
            "NEW_TICK",
            "AUTHENTICATE",
            "PLAYER_ORIENTATION",
            "MOVE_AWARE_AREA",
            "AWARE_AREA_OUT_OF_SYNC",
            "ADD_ENTITIES",
            "REMOVE_ENTITIES",
            "ID_COUNT",
        };

        struct Header {
            Id id;
            std::uint32_t size;
        };

        namespace Body {
            // TODO: decouple struct from format
            using ChunkDataPosition = glm::ivec3;
            struct EntityData {
                std::uint32_t id;
                glm::vec3 position;
            };
            using ChunkRequestDataPosition = glm::ivec3;
            struct BlockData {
                glm::ivec3 position;
                std::uint32_t value;
            };
            struct ReassignEntityId {
                std::uint32_t oldID;
                std::uint32_t newID;
            };
            using DeadEntities = std::uint32_t;
        }

        template <typename T> void deserialize(gsl::span<const std::byte> in, T & out);
        template <typename T> void serialize(const T & in, gsl::span<std::byte> out);

        // no error checking with assert for deserialize* functions because it is the sender fault
        static_assert(CHAR_BIT == 8);
        static constexpr std::size_t HEADER_SIZE_BYTES = 4;
        static_assert(static_cast<std::uintmax_t>(Id::ID_COUNT) <= 255u);
        static_assert(MAX_MESSAGE_SIZE <= 16777215u);
        /**
         * i ... ID bits
         * s ... size bits
         * iiiiiiii ssssssss ssssssss ssssssss
         */
        template<>
        inline void deserialize(gsl::span<const std::byte> in, Header & out) {
            static_assert(HEADER_SIZE_BYTES == 4);
            assert(size(in) >= HEADER_SIZE_BYTES);
            out.id = Id{ static_cast<unsigned char>(in[0]) };
            out.size =
                static_cast<decltype(out.size)>(in[1]) << 16u |
                static_cast<decltype(out.size)>(in[2]) <<  8u |
                static_cast<decltype(out.size)>(in[3]);
        }
        template<>
        inline void serialize(const Header & in, gsl::span<std::byte> out) {
            static_assert(HEADER_SIZE_BYTES == 4);
            assert(in.size <= MAX_MESSAGE_SIZE && in.size >= MIN_MESSAGE_SIZE);
            assert(in.id < Id::ID_COUNT);
            assert(size(out) >= HEADER_SIZE_BYTES);
            out[0] = std::byte{ static_cast<unsigned char>(in.id) };
            out[1] = std::byte{ static_cast<unsigned char>(in.size >> 16u        ) };
            out[2] = std::byte{ static_cast<unsigned char>(in.size >>  8u & 0xffu) };
            out[3] = std::byte{ static_cast<unsigned char>(in.size        & 0xffu) };
        }
    }

    inline const std::array<glm::ivec3, 8> CHUNK_OFFSETS_FOR_MESH { {
        { 0, 0, 0 },
        { 0, 0, 1 },
        { 0, 1, 0 },
        { 0, 1, 1 },
        { 1, 0, 0 },
        { 1, 0, 1 },
        { 1, 1, 0 },
        { 1, 1, 1 }
    } };

    inline const std::array<glm::ivec3, 8> MESH_OFFSETS_FOR_CHUNK { {
        { -1, -1, -1 },
        { -1, -1,  0 },
        { -1,  0, -1 },
        { -1,  0,  0 },
        {  0, -1, -1 },
        {  0, -1,  0 },
        {  0,  0, -1 },
        {  0,  0,  0 }
    } };

    /**
     * 
     * 
     * 
     * 
     * The following functions and constants do not need to be edited
     * they are deduced from the parameters above.
     * 
     * 
     * 
     * 
     */

    static_assert(CHUNK_SIZE_X > 0 && CHUNK_SIZE_Y > 0 && CHUNK_SIZE_Z > 0);
    static constexpr std::size_t CHUNK_VOLUME{
        static_cast<decltype(CHUNK_VOLUME)>(CHUNK_SIZE_X) *
        static_cast<decltype(CHUNK_VOLUME)>(CHUNK_SIZE_Y) *
        static_cast<decltype(CHUNK_VOLUME)>(CHUNK_SIZE_Z)
    };
    static_assert(REGION_SIZE_X > 0 && REGION_SIZE_Y > 0 && REGION_SIZE_Z > 0);
    static constexpr std::size_t REGION_VOLUME{
        static_cast<decltype(REGION_VOLUME)>(REGION_SIZE_X) *
        static_cast<decltype(REGION_VOLUME)>(REGION_SIZE_Y) *
        static_cast<decltype(REGION_VOLUME)>(REGION_SIZE_Z)
    };

    namespace {
        constexpr auto MAX_VAL_CHUNK_VOLUME = std::numeric_limits<decltype(CHUNK_VOLUME)>::max();
        static_assert(static_cast<decltype(MAX_VAL_CHUNK_VOLUME)>(CHUNK_SIZE_X) <= MAX_VAL_CHUNK_VOLUME / static_cast<decltype(MAX_VAL_CHUNK_VOLUME)>(CHUNK_SIZE_Y), "Overflow.");
        static_assert(static_cast<decltype(MAX_VAL_CHUNK_VOLUME)>(CHUNK_SIZE_X) * static_cast<decltype(MAX_VAL_CHUNK_VOLUME)>(CHUNK_SIZE_Y) <= MAX_VAL_CHUNK_VOLUME / static_cast<decltype(MAX_VAL_CHUNK_VOLUME)>(CHUNK_SIZE_Z), "Overflow.");
    }

    namespace {
        constexpr auto MAX_VAL_REGION_VOLUME = std::numeric_limits<decltype(REGION_VOLUME)>::max();
        static_assert(static_cast<decltype(MAX_VAL_REGION_VOLUME)>(REGION_SIZE_X) <= MAX_VAL_REGION_VOLUME / static_cast<decltype(MAX_VAL_REGION_VOLUME)>(REGION_SIZE_Y), "Overflow.");
        static_assert(static_cast<decltype(MAX_VAL_REGION_VOLUME)>(REGION_SIZE_X) * static_cast<decltype(MAX_VAL_REGION_VOLUME)>(REGION_SIZE_Y) <= MAX_VAL_REGION_VOLUME / static_cast<decltype(MAX_VAL_REGION_VOLUME)>(REGION_SIZE_Z), "Overflow.");
    }

    static constexpr std::size_t CHUNK_SIZE_BYTES = CHUNK_VOLUME * sizeof(Block);
    // yes, some compilers can have sizeof(x) return 0 (struct X { char x[0]; };)
    static_assert(sizeof(Block) > 0 && CHUNK_VOLUME <= std::numeric_limits<decltype(CHUNK_SIZE_BYTES)>::max() / sizeof(Block), "Overflow.");

    static_assert(std::is_same<decltype(CHUNK_SIZE), decltype(REGION_SIZE)>::value, "Required for following functions.");

    static constexpr std::size_t MAX_COMPRESSED_CHUNK_SIZE_BYTES = CHUNK_SIZE_BYTES + sizeof(CompressionStrategy);

    // TODO: can these functions all be written more efficiently by cast to unsigned and unsigned division and cast back to signed?
    inline decltype(CHUNK_SIZE) blockPositionToChunkPosition(const decltype(CHUNK_SIZE) & blockPosition) {
        static_assert(std::is_integral<decltype(blockPosition.x)>::value);
        static_assert(std::is_integral<decltype(CHUNK_SIZE_X)>::value);
        static_assert(CHUNK_SIZE_X > 0 && CHUNK_SIZE_Y > 0 && CHUNK_SIZE_Z > 0, "Division by zero.");
        return {
            (blockPosition.x + (blockPosition.x < 0)) / CHUNK_SIZE_X - (blockPosition.x < 0),
            (blockPosition.y + (blockPosition.y < 0)) / CHUNK_SIZE_Y - (blockPosition.y < 0),
            (blockPosition.z + (blockPosition.z < 0)) / CHUNK_SIZE_Z - (blockPosition.z < 0)
        };
    }

    inline decltype(CHUNK_SIZE) chunkPositionToRegionPosition(const decltype(CHUNK_SIZE) & chunkPosition) {
        static_assert(std::is_integral<decltype(chunkPosition.x)>::value);
        static_assert(std::is_integral<decltype(REGION_SIZE_X)>::value);
        static_assert(REGION_SIZE_X > 0 && REGION_SIZE_Y > 0 && REGION_SIZE_Z > 0, "Division by zero.");
        return {
            (chunkPosition.x + (chunkPosition.x < 0)) / REGION_SIZE_X - (chunkPosition.x < 0),
            (chunkPosition.y + (chunkPosition.y < 0)) / REGION_SIZE_Y - (chunkPosition.y < 0),
            (chunkPosition.z + (chunkPosition.z < 0)) / REGION_SIZE_Z - (chunkPosition.z < 0)
        };
    }

    inline decltype(CHUNK_SIZE) blockPositionToRegionPosition(const decltype(CHUNK_SIZE) & chunkPosition) {
        constexpr auto MAX_VAL = std::numeric_limits<decltype(CHUNK_SIZE_X * REGION_SIZE_X)>::max();
        static_assert(std::is_integral<decltype(chunkPosition.x)>::value);
        static_assert(std::is_integral<decltype(CHUNK_SIZE_X)>::value);
        static_assert(std::is_integral<decltype(REGION_SIZE_X)>::value);
        static_assert(CHUNK_SIZE_X > 0 && REGION_SIZE_X > 0 && CHUNK_SIZE_X <= MAX_VAL / REGION_SIZE_X, "Overflow or division by zero or negative.");
        static_assert(CHUNK_SIZE_Y > 0 && REGION_SIZE_Y > 0 && CHUNK_SIZE_Y <= MAX_VAL / REGION_SIZE_Y, "Overflow or division by zero or negative.");
        static_assert(CHUNK_SIZE_Z > 0 && REGION_SIZE_Z > 0 && CHUNK_SIZE_Z <= MAX_VAL / REGION_SIZE_Z, "Overflow or division by zero or negative.");
        return {
            (chunkPosition.x + (chunkPosition.x < 0)) / (CHUNK_SIZE_X * REGION_SIZE_X) - (chunkPosition.x < 0),
            (chunkPosition.y + (chunkPosition.y < 0)) / (CHUNK_SIZE_Y * REGION_SIZE_Y) - (chunkPosition.y < 0),
            (chunkPosition.z + (chunkPosition.z < 0)) / (CHUNK_SIZE_Z * REGION_SIZE_Z) - (chunkPosition.z < 0)
        };
    }

    inline decltype(CHUNK_SIZE) blockPositionToBlockPositionInChunk(const decltype(CHUNK_SIZE) & blockPosition) {
        constexpr auto MAX_VAL = std::numeric_limits<decltype(CHUNK_SIZE_X)>::max();
        static_assert(CHUNK_SIZE_X > 0 && CHUNK_SIZE_X <= MAX_VAL / 2, "Overflow or remainder of zero or negative.");
        static_assert(CHUNK_SIZE_Y > 0 && CHUNK_SIZE_Y <= MAX_VAL / 2, "Overflow or remainder of zero or negative.");
        static_assert(CHUNK_SIZE_Z > 0 && CHUNK_SIZE_Z <= MAX_VAL / 2, "Overflow or remainder of zero or negative.");
        static_assert(std::is_integral<decltype(blockPosition.x)>::value);
        static_assert(std::is_integral<decltype(CHUNK_SIZE_X)>::value);
        return {
            (blockPosition.x % CHUNK_SIZE_X + CHUNK_SIZE_X) % CHUNK_SIZE_X,
            (blockPosition.y % CHUNK_SIZE_Y + CHUNK_SIZE_Y) % CHUNK_SIZE_Y,
            (blockPosition.z % CHUNK_SIZE_Z + CHUNK_SIZE_Z) % CHUNK_SIZE_Z
        };
    }

    inline std::size_t blockPositionInChunkToBlockIndex(const decltype(CHUNK_SIZE) & blockPositionInChunk) {
        assert(blockPositionInChunk.x < CHUNK_SIZE_X && blockPositionInChunk.x >= 0);
        assert(blockPositionInChunk.y < CHUNK_SIZE_Y && blockPositionInChunk.x >= 0);
        assert(blockPositionInChunk.z < CHUNK_SIZE_Z && blockPositionInChunk.x >= 0);
        return 
            (static_cast<std::size_t>(blockPositionInChunk.z) * static_cast<std::size_t>(CHUNK_SIZE_Y) + static_cast<std::size_t>(blockPositionInChunk.y))
            * static_cast<std::size_t>(CHUNK_SIZE_X) + static_cast<std::size_t>(blockPositionInChunk.x);
    }

    inline std::size_t blockPositionToBlockIndex(const decltype(CHUNK_SIZE) & blockPosition) {
        return blockPositionInChunkToBlockIndex(blockPositionToBlockPositionInChunk(blockPosition));
    }

    inline decltype(CHUNK_SIZE) chunkPositionToChunkPositionInRegion(const decltype(CHUNK_SIZE) & chunkPosition) {
        constexpr auto MAX_VAL = std::numeric_limits<decltype(REGION_SIZE_X)>::max();
        static_assert(REGION_SIZE_X > 0 && REGION_SIZE_X <= MAX_VAL / 2, "Overflow or remainder of zero or negative.");
        static_assert(REGION_SIZE_Y > 0 && REGION_SIZE_Y <= MAX_VAL / 2, "Overflow or remainder of zero or negative.");
        static_assert(REGION_SIZE_Z > 0 && REGION_SIZE_Z <= MAX_VAL / 2, "Overflow or remainder of zero or negative.");
        static_assert(std::is_integral<decltype(chunkPosition.x)>::value);
        static_assert(std::is_integral<decltype(REGION_SIZE_X)>::value);
        return {
            (chunkPosition.x % REGION_SIZE_X + REGION_SIZE_X) % REGION_SIZE_X,
            (chunkPosition.y % REGION_SIZE_Y + REGION_SIZE_Y) % REGION_SIZE_Y,
            (chunkPosition.z % REGION_SIZE_Z + REGION_SIZE_Z) % REGION_SIZE_Z
        };
    }

    inline std::size_t chunkPositionInRegionToChunkIndex(const decltype(CHUNK_SIZE) & chunkPositionInRegion) {
        assert(chunkPositionInRegion.x < REGION_SIZE_X && chunkPositionInRegion.x >= 0);
        assert(chunkPositionInRegion.y < REGION_SIZE_Y && chunkPositionInRegion.x >= 0);
        assert(chunkPositionInRegion.z < REGION_SIZE_Z && chunkPositionInRegion.x >= 0);
        return
            (static_cast<std::size_t>(chunkPositionInRegion.z) * static_cast<std::size_t>(REGION_SIZE_Y) + static_cast<std::size_t>(chunkPositionInRegion.y))
            * static_cast<std::size_t>(REGION_SIZE_X) + static_cast<std::size_t>(chunkPositionInRegion.x);
    }

    inline std::size_t chunkPositionToChunkIndex(const decltype(CHUNK_SIZE) & chunkPosition) {
        return chunkPositionInRegionToChunkIndex(chunkPositionToChunkPositionInRegion(chunkPosition));
    }

};

namespace std {
    template<> struct hash<cfg::user_name_type> {
        std::size_t operator() (const cfg::user_name_type & n) const {
            return std::hash<std::string_view>{}(std::string_view{ n.data(), n.size() });
        }  
    };
}
