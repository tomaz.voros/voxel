cmake_minimum_required(VERSION 3.10)
project(voxel)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

include(${CMAKE_BINARY_DIR}/conanbuildinfo_multi.cmake)
conan_basic_setup(TARGETS)

add_subdirectory(apps/server)
add_subdirectory(apps/monitor)
add_subdirectory(apps/client)
add_subdirectory(apps/rng)
add_subdirectory(libs/shared)
add_subdirectory(external/glad)
add_subdirectory(external/stb)
add_subdirectory(external/gsl)
add_subdirectory(external/imgui)
add_subdirectory(external/earcut)
add_subdirectory(data)